#pragma once

#ifdef _WIN32
	#ifdef COMMONDATA_EXPORTS
		#define COMMONDATA_API __declspec(dllexport)
	#else
		#define COMMONDATA_API __declspec(dllimport)
	#endif
#else
	#define COMMONDATA_API
#endif

#include <string>
#include <stdlib.h>
#include <exception>
#include <stdexcept>

namespace CommonData
{
	///this enum is used for communication between 
	enum COMMONDATA_API MapValues {
		UNWALKABLE = 0, WALKABLE = 1, CHOKEPOINT = 2, CHOKEPOINTBLOCK = 3,
		MAINBASE = 4, EXPANSION = 5, DEFENDER = 6, ATTACKER = 7, DEFENDERBACK = 8,
		SIGHTRANGEBLOCK = 9
	};

	class Functions
	{
	private:
		///returns pointer to buffer with path to the environment variable returned (works with linux as well as windows)
		///throws exception if the buffer is null (such environment variable does not exist)
		static std::string getEnvVariablePath(std::string name);
	public:
		///function providing input folder for FormationsEvolver
		static COMMONDATA_API std::string GetFormationsEvolverMainInputDirectory();
		///function providing output folder for FormationsEvolver
		static COMMONDATA_API std::string GetFormationsEvolverMainOutputDirectory();

		///function providing input folder for ChokePointParser
		static COMMONDATA_API std::string GetParsedMapsDirectory();
		///function providing output folder for ChokepointParser
		static COMMONDATA_API std::string GetParsedChokepointsDirectory();

		///function for FormationsEvolver, it needs this information when it
		///creates new sparcraft experiment
		static COMMONDATA_API std::string GetSparcraftAssetsDirectory();
	};
}
