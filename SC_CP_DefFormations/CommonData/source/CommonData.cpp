#include "CommonData.h"

namespace CommonData
{
	std::string Functions::getEnvVariablePath(std::string name)
	{
		char * buff = nullptr;
		size_t sz = 0;
		std::string envVariablePath("");

#ifdef _WIN32
		if (_dupenv_s(&buff, &sz, name.c_str()) == 0 && buff != nullptr)
		{
			envVariablePath = std::string(buff);
			free(buff);
		}
#else
		buff = getenv(name.c_str());
		if (buff != nullptr)
			envVariablePath = std::string(buff);
#endif
		if (envVariablePath == "")
			throw(std::runtime_error("CommonData module: incorrect environment variable request for environment variable: " + name));
		return envVariablePath;
	}

	std::string Functions::GetFormationsEvolverMainInputDirectory()
	{
		std::string resultingPath;
		resultingPath = getEnvVariablePath("SC_CP_DEFFORMATIONS_DIR") + "/FormationsEvolver/Input/";
		return resultingPath;
	}

	std::string Functions::GetFormationsEvolverMainOutputDirectory()
	{
		std::string resultingPath;
		resultingPath = getEnvVariablePath("SC_CP_DEFFORMATIONS_DIR") + "/FormationsEvolver/Output/";
		return resultingPath;
	}

	std::string Functions::GetParsedMapsDirectory()
	{
		std::string resultingPath;
		resultingPath = getEnvVariablePath("SC_CP_DEFFORMATIONS_DIR") + "/ChokePointParser/ParsedMaps/";
		return resultingPath;
	}

	std::string Functions::GetParsedChokepointsDirectory()
	{
		std::string resultingPath;
		resultingPath = getEnvVariablePath("SC_CP_DEFFORMATIONS_DIR") + "/ChokePointParser/ParsedChokepoints/";
		return resultingPath;
	}

	std::string Functions::GetSparcraftAssetsDirectory()
	{
		std::string resultingPath;
		resultingPath = getEnvVariablePath("SPARCRAFT_DIR") + "/assets/images/";
		return resultingPath;
	}

}
