#!/bin/sh

cd $SC_CP_FORMATIONS_DIR/CommonData

rm -f dll/*.so
rm -f source/*.o

cd source

echo "compiling common data module"

g++ -O3 -Wall -std=c++14 -c -fPIC *.cpp

echo "building common data shared library"

g++ -std=c++14 -shared *.o -o libCommonData.so

mv libCommonData.so ../dll

rm *.o

echo "done"
