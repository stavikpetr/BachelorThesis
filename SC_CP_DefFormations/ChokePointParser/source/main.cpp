/*This project will take starcraft tournament maps outputed by mapAnalyzer. These are text files with the following format
width: xxx - map width in walkable tiles
height: yyy - map height in walkable tiles
NumberOfChokePoints: xxx - number of chokepoints
walkTileX: ___ walkTileY: ___ positionX: ___ positionY: ___ directionX: ___ directionY: ___
0/1/2

0 = unwalkable tile
1 = walkable tile
2 = center of chokepoint

maps are currently located in ../Starcraft_tournamentMaps_info
foreach map and foreach chokepoint i will take some fixed size width and height and i will take rectangle around each chokepoint
this will give me maps for combat scenarios that will be used later
output will be
MapName: ___ - name of map
Chokepoint: x:___ y:___ - exact chokepoint center coordinates for this chokepoint returned by BWTA in mapAnalyzer (in order to later identify chokepoints in bot*/
#include "ChokePointParser.h"
#include <iostream>

int main()
{
	ChokePointParser cpp;
	cpp.parseChokePoints();
	std::cout << "---- job done -----" << std::endl;
}