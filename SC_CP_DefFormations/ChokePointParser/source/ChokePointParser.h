#ifndef CLASS_CHOKEPOINTPARSER
#define CLASS_CHOKEPOINTPARSER
#endif

#include <string>
#include <iostream>
#include <vector>
#include <cmath>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <boost/filesystem.hpp>
#include "CommonData.h"
#include "MapInfo.h"

struct Point
{
	int i_, j_;
public:
	friend bool operator==(const Point& lhs, const Point& rhs)
	{
		if (lhs.i_ == rhs.i_ && lhs.j_ == rhs.j_)
			return true;
		return false;
	}
	friend bool operator!=(const Point& lhs, const Point& rhs)
	{
		return !(lhs == rhs);
	}
	size_t hash(const Point& f) const {
		return f.i_ * 1000 + f.j_;
	}
	Point(int i, int j){
		i_ = i;
		j_ = j;
	}
};

namespace std {
	template <>
	struct hash<Point> {
		size_t operator () (const Point &f) const { return f.hash(f); }
	};
}

struct Vector
{
	double i_, j_;
public:
	Vector(double i, double j)
	{
		i_ = i;
		j_ = j;
	}
};

class ChokePointParser
{
	std::string inputDirectory_;
	std::string outputDirectory_;
	MapInfo mapInfo_;

	std::vector<std::string> split(const std::string& s, char delimiter);

	void parseMap();
	void parseChokePoint(ChokePointInfo& chpInfo);

	//helper methods
	bool contains(std::unordered_set<Point>& set, Point p);

	void getSuccessors8Dirs(Point walkTile, std::vector<Point>& successors, int smallestXLimit, int smallestYLimit,
		int biggestXLimit, int biggestYLimit);
	void getSuccessors4Dirs(Point from, std::vector<Point>& successors, int smallestXLimit, int smallestYLimit,
		int biggestXLimit, int biggestYLimit);

	//first step, marking the parts of the map by the values CHOKEPOINTBLOCK
	void markChokePointBlock(ChokePointInfo& chpInfo);
	void extendChokePointCenter(Point chpCenter, Point target, std::vector<Point>& walkTilesToExpand);
	
	//second step, populating each side of chokepoint with either DEFENDER or ATTACKER tiles
	//in addition, as, by populating each side, the maximum size of chokepoint
	//area is determined, this method also returns carved out portion of map
	//which represents the area around chokepoint we are interested in
	std::vector<std::vector<int>> populateChokepointSides(ChokePointInfo& chpInfo);
	std::pair<Point, Point> getExpansionPoints(ChokePointInfo& chpInfo);
	void populateSide(Point expansionPoint, CommonData::MapValues withWhat,
		ChokePointInfo& chpInfo, std::vector<std::vector<int>>& mapCopy);	
	int distanceToNearrestStartLocation(Point from);
	std::vector<std::vector<int>> cutMap(
		std::vector<std::vector<int>> & mapCopy,
		ChokePointInfo& chpInfo);

	//right now, the defender and attacker sides are populated, we now need to
	//populate the defender back for infrastructure hunt
	void populateDefenderBack(std::vector<std::vector<int>>& mapCut);
	int distanceToNearrestAttackerTile(Point from, std::vector<std::vector<int>>& mapCut);
	
	//the last phase is to populate the attacker side with block
	//that forces the placement of units more "to the back"
	void populateSightRangeBlock(std::vector<std::vector<int>>& mapCut);

	//the final method for output
	void outputChokePoint(std::vector<std::vector<int>>& chokePointSurround,
		ChokePointInfo& cpi);
public:
	void parseChokePoints();
	ChokePointParser();
};


