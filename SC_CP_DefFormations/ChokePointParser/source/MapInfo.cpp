#include "MapInfo.h"

MapInfo::MapInfo() :numOfWalkTilesX_(-1), numOfWalkTilesY_(-1), numberOfChokepoints_(-1), mapName_(""){};

MapInfo::MapInfo(int numOfWalkTilesX, int numOfWalkTilesY, int numberOfChokepoints, std::string& mapName) :
numOfWalkTilesX_(numOfWalkTilesX), numOfWalkTilesY_(numOfWalkTilesY), numberOfChokepoints_(numberOfChokepoints),
mapName_(mapName)
{
	for (size_t k = 0; k < numOfWalkTilesX; ++k)
	{
		std::vector<int> a;
		mapInfo_.push_back(a);
	}
}

void MapInfo::addMapInfo(int index, int c)
{
	mapInfo_[index].push_back(c);
}

void MapInfo::setMapInfo(int walkTileX, int walkTileY, int c)
{
	mapInfo_[walkTileX][walkTileY] = c;
}

void MapInfo::addChokePoint(const ChokePointInfo& cpi)
{
	chokePointInfos_.push_back(cpi);
}

std::string MapInfo::getMapName()
{
	return mapName_;
}

int MapInfo::getNumOfWalkTilesX()
{
	return numOfWalkTilesX_;
}

int MapInfo::getNumOfWalkTilesY()
{
	return numOfWalkTilesY_;
}

int MapInfo::getMapInfo(int walkTileX, int walkTileY)
{
	return mapInfo_[walkTileX][walkTileY];
}

std::vector<ChokePointInfo>& MapInfo::getChokePointInfos()
{
	return chokePointInfos_;
}

std::vector<std::vector<int>>& MapInfo::getWholeMap()
{
	return mapInfo_;
}

void MapInfo::setNewDirectory(const std::string& newDirectory)
{
	newDirectory_ = newDirectory;
}

std::string MapInfo::getNewDirectory()
{
	return newDirectory_;
}