#pragma once

#include <vector>
#include <string>
#include <stddef.h>
#include <limits>

struct ChokePointInfo
{
	int walkTileX_, walkTileY_, positionX_, positionY_, firstPointX_, firstPointY_, secondPointX_, secondPointY_, chokePointNumber_;
	int numOfWalkTilesX_, numOfWalkTilesY_, topLeftX_, topLeftY_;
	int minX_;
	int maxX_;
	int minY_;
	int maxY_;
public:

	ChokePointInfo(int walkTileX, int walkTileY,
		int positionX, int positionY,
		int firstPointX, int firstPointY,
		int secondPointX, int secondPointY) :
		walkTileX_(walkTileX),
		walkTileY_(walkTileY),
		positionX_(positionX),
		positionY_(positionY),
		firstPointX_(firstPointX),
		firstPointY_(firstPointY),
		secondPointX_(secondPointX),
		secondPointY_(secondPointY),
		minX_(std::numeric_limits<int>::max()),
		maxX_(std::numeric_limits<int>::min()),
		minY_(std::numeric_limits<int>::max()),
		maxY_(std::numeric_limits<int>::min())
	{
	};

};

class MapInfo
{
	int numOfWalkTilesX_, numOfWalkTilesY_, numberOfChokepoints_;
	std::vector<std::vector<int>> mapInfo_;
	std::vector<ChokePointInfo> chokePointInfos_;
	std::string mapName_;
	std::string newDirectory_;
public:
	MapInfo();
	MapInfo(int numOfWalkTilesX, int numOfWalkTilesY, int numberOfChokepoints, std::string& mapName);

	void setMapInfo(int walkTileX, int walkTileY, int c);
	void addChokePoint(const ChokePointInfo& cpi);
	void addMapInfo(int index, int c);
	std::string getMapName();
	int getNumOfWalkTilesX();
	int getNumOfWalkTilesY();
	int getMapInfo(int walkTileX, int walkTileY);
	std::vector<ChokePointInfo>& getChokePointInfos();
	std::vector<std::vector<int>>& getWholeMap();
	void setNewDirectory(const std::string& newDirectory);
	std::string getNewDirectory();

};