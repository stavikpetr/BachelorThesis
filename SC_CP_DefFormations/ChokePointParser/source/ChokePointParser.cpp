#include "ChokePointParser.h"

ChokePointParser::ChokePointParser()
{
	inputDirectory_ = CommonData::Functions::GetParsedMapsDirectory();
	outputDirectory_ = CommonData::Functions::GetParsedChokepointsDirectory();
}

void ChokePointParser::parseChokePoints() 
{
	boost::filesystem::directory_iterator directory_end;	
	boost::filesystem::path directoryPath(inputDirectory_);

	//first, lets delete every folder corresponding to already parsed map, we are doing new parsing, so every old map will be deleted
	boost::filesystem::path output(outputDirectory_);
	for (boost::filesystem::directory_iterator directory_start(output); directory_start != directory_end; directory_start++)
	{
		boost::filesystem::path p(*directory_start);
		if(boost::filesystem::is_directory(*directory_start))
			boost::filesystem::remove_all(p);
	}

	//foreach map in input
	for (boost::filesystem::directory_iterator directory_start(directoryPath);
		directory_start != directory_end; directory_start++)
	{
		boost::filesystem::path p(*directory_start);
		std::string mapName = p.filename().string();

		int numberOfWalkTilesX; //represented by width in file
		int numberOfWalkTilesY; //represented by height in file
		int numberOfChokepoints;

		std::ifstream input(p.string());
		std::string line;

		mapName = mapName.substr(0, mapName.size() - 4); // - .txt
		//next line contains width
		//keep in mind that width means numberOfWalkTilesX
		std::getline(input, line);
		auto  lineSplit = split(line, ' ');
		numberOfWalkTilesX = std::stoi(lineSplit[1]);

		//next line contains height
		//keep in mind that height means numberOfWalkTilesY
		std::getline(input, line);
		lineSplit = split(line, ' ');
		numberOfWalkTilesY = std::stoi(lineSplit[1]);

		//next line contains number of chokepoints
		std::getline(input, line);
		lineSplit = split(line, ' ');
		numberOfChokepoints = std::stoi(lineSplit[1]);

		//next "numberOfChokepoints" lines contain information about chokepoints
		mapInfo_ = MapInfo(numberOfWalkTilesX, numberOfWalkTilesY,
			numberOfChokepoints, mapName);

		for (int i = 0; i < numberOfChokepoints; i++)
		{
			std::getline(input, line);
			lineSplit = split(line, ' ');
			//[1] = walkTileX, [3] = walkTileY, [5] = positionX, [7] = positionY, [9] = firstPointX, [11] = firstPointY, [13] = secondPointX, [15] = secondPointY
			mapInfo_.addChokePoint(ChokePointInfo(std::stoi(lineSplit[1]),
				std::stoi(lineSplit[3]), std::stoi(lineSplit[5]), std::stoi(lineSplit[7]),
				std::stoi(lineSplit[9]), std::stoi(lineSplit[11]), std::stoi(lineSplit[13]), std::stoi(lineSplit[15])));
		}

		//now let's create 2d array and parse the map
		while (std::getline(input, line))
		{
			size_t i = 0;
			for (auto c : line)
			{
				mapInfo_.addMapInfo(i, c - '0');
				i++;
			}
			
		}
		parseMap();

		std::cout << p.string() << std::endl;
	}
}

void ChokePointParser::parseMap()
{
	int chokepointNumber = 0;
	auto& chokePointInfos = mapInfo_.getChokePointInfos();
	for (auto& chpInfo : chokePointInfos)
	{
		std::cout << "parsing chokepoint number: " << std::to_string(chokepointNumber) << std::endl;
		chpInfo.chokePointNumber_ = chokepointNumber;
		parseChokePoint(chpInfo);
		chokepointNumber++;
	}
	//boost::filesystem::path newDirectory(outputDirectory_ + mapInfo_.getMapName());
	//boost::system::error_code returnedError;

	//boost::filesystem::create_directory(newDirectory, returnedError);
	//if (!returnedError)
	//{		
	//	mapInfo_.setNewDirectory(newDirectory.string());
	//	int chokepointNumber = 0;
	//	auto& chokePointInfos = mapInfo_.getChokePointInfos();
	//	for (auto& chpInfo : chokePointInfos)
	//	{
	//		std::cout << "parsing chokepoint number: " << std::to_string(chokepointNumber) << std::endl;
	//		chpInfo.chokePointNumber_ = chokepointNumber;
	//		parseChokePoint(chpInfo);
	//		chokepointNumber++;
	//	}
	//}
	//else
	//	std::cout << "error while creating directory for map" << std::endl;
}

void ChokePointParser::parseChokePoint(ChokePointInfo& chpInfo)
{
	markChokePointBlock(chpInfo);
	auto resultingChokepointSurround(populateChokepointSides(chpInfo));
	populateDefenderBack(resultingChokepointSurround);
	populateSightRangeBlock(resultingChokepointSurround);
	outputChokePoint(resultingChokepointSurround, chpInfo);
}

std::vector<std::vector<int>> ChokePointParser::populateChokepointSides(ChokePointInfo& chpInfo)
{
	//first, lets get points around the CHOKEPOINTBLOCK which will represent the
	//initial sources of populating sides
	auto expPoints = getExpansionPoints(chpInfo);

	//now let's determine which chokepoint side belongs to which player
	int expDist0 = distanceToNearrestStartLocation(expPoints.first);
	int expDist1 = distanceToNearrestStartLocation(expPoints.second);
	std::vector<CommonData::MapValues> playerSides;
	if (expDist0 < expDist1)
	{
		playerSides.push_back(CommonData::DEFENDER);
		playerSides.push_back(CommonData::ATTACKER);
	}
	else
	{
		playerSides.push_back(CommonData::ATTACKER);
		playerSides.push_back(CommonData::DEFENDER);
	}
	//now, let's populate each side
	std::vector<std::vector<int>> mapCopy = mapInfo_.getWholeMap();
	populateSide(expPoints.first, playerSides[0], chpInfo, mapCopy);
	populateSide(expPoints.second, playerSides[1], chpInfo, mapCopy);

	chpInfo.numOfWalkTilesX_ = chpInfo.maxX_ - chpInfo.minX_ + 1;
	chpInfo.numOfWalkTilesY_ = chpInfo.maxY_ - chpInfo.minY_ + 1;
	chpInfo.topLeftX_ = chpInfo.minX_ * 8;
	chpInfo.topLeftY_ = chpInfo.minY_ * 8;

	auto resultingChokepointSurround = cutMap(mapCopy, chpInfo);
	return resultingChokepointSurround;
}

std::pair<Point, Point> ChokePointParser::getExpansionPoints(ChokePointInfo& chpInfo)
{
	std::vector<Point> start({ Point(chpInfo.walkTileX_, chpInfo.walkTileY_) });

	Vector fpVec(chpInfo.firstPointX_ - chpInfo.positionX_, chpInfo.firstPointY_ - chpInfo.positionY_);
	Vector spVec(chpInfo.secondPointX_ - chpInfo.positionX_, chpInfo.secondPointY_ - chpInfo.positionY_);

	Vector perpfpVec(fpVec.j_, -1 * fpVec.i_);
	Vector perpspVec(spVec.j_, -1 * spVec.i_);

	double perpfpVecMag = sqrt(perpfpVec.i_ * perpfpVec.i_ + perpfpVec.j_ * perpfpVec.j_);
	double perpspVecMag = sqrt(perpspVec.i_ * perpspVec.i_ + perpspVec.j_ * perpspVec.j_);

	Vector UVperpfpVec(perpfpVec.i_ / perpfpVecMag, perpfpVec.j_ / perpfpVecMag);
	Vector UVperpspVec(perpspVec.i_ / perpspVecMag, perpspVec.j_ / perpspVecMag);

	std::vector<Vector> uvDirs({ UVperpfpVec, UVperpspVec });
	std::vector<Point>expansionPoints;


	for (auto& uvDir : uvDirs)
	{
		Point expansionPoint(-1, -1);
		Vector v(chpInfo.positionX_, chpInfo.positionY_);
		for (int i = 0; i < 5; ++i) //let's try 5 iterations, up to this point, we really should find some expansion point
		{
			v = Vector(v.i_ + 8 * uvDir.i_, v.j_ + 8 * uvDir.j_);
			if (mapInfo_.getMapInfo((int)(v.i_ / 8), (int)(v.j_ / 8)) == CommonData::WALKABLE)
			{
				expansionPoint = Point((int)(v.i_ / 8), (int)(v.j_ / 8));
			}
		}
		if (expansionPoint.i_ == -1 && expansionPoint.j_ == -1)
		{
			throw(std::runtime_error("error while parsing the chokepoint"));
		}
		else
		{
			expansionPoints.push_back(expansionPoint);
		}
	}

	return std::pair<Point, Point>(expansionPoints[0], expansionPoints[1]);
}


void ChokePointParser::populateSide(Point expansionPoint, 
	CommonData::MapValues withWhat, ChokePointInfo& chpInfo,
	std::vector<std::vector<int>>& mapCopy)
{
	int maxDist = 58;
	std::queue<std::pair<int, Point>> q({ std::pair<int, Point>(0, expansionPoint) });
	std::unordered_set<Point> visitedNodes;

	while (q.size() != 0)
	{
		auto p = q.front();
		q.pop();
		if (p.first >= maxDist)
			continue;

		if (mapCopy[p.second.i_][p.second.j_] != CommonData::EXPANSION &&
			mapCopy[p.second.i_][p.second.j_] != CommonData::WALKABLE &&
			mapCopy[p.second.i_][p.second.j_] != CommonData::MAINBASE)
			continue;
		if (!contains(visitedNodes, p.second))
			visitedNodes.insert(p.second);
		else
			continue;

		mapCopy[p.second.i_][p.second.j_] = withWhat;
		if (p.second.i_ < chpInfo.minX_)
			chpInfo.minX_ = p.second.i_;
		if (p.second.i_ > chpInfo.maxX_)
			chpInfo.maxX_ = p.second.i_;
		if (p.second.j_ < chpInfo.minY_)
			chpInfo.minY_ = p.second.j_;
		if (p.second.j_ > chpInfo.maxY_)
		    chpInfo.maxY_ = p.second.j_;

		std::vector<Point> successors;
		getSuccessors4Dirs(p.second, successors, 0, 0, mapInfo_.getNumOfWalkTilesX(), mapInfo_.getNumOfWalkTilesY());

		for (auto& s : successors)
		{
			q.push(std::pair<int, Point>(p.first + 1, s));
		}
	}
}


std::vector<std::vector<int>> ChokePointParser::cutMap(std::vector<std::vector<int>> & mapCopy,
	ChokePointInfo& chpInfo)
{
	std::vector<std::vector<int>> result;
	for (int i = chpInfo.minX_; i <= chpInfo.maxX_; ++i)
	{
		std::vector<int>a;
		result.push_back(a);
	}
	for (int j = chpInfo.minY_; j <= chpInfo.maxY_; ++j)
	{
		size_t index = 0;
		for (int i = chpInfo.minX_; i <= chpInfo.maxX_; ++i)
		{
			int t = mapCopy[i][j];
			if (t == CommonData::WALKABLE ||
				t == CommonData::EXPANSION ||
				t == CommonData::MAINBASE)
			{
				result[index].push_back(CommonData::UNWALKABLE);
			}
			else
			{
				result[index].push_back(t);
			}
			index++;
		}
	}
	return result;
}

int ChokePointParser::distanceToNearrestStartLocation(Point from)
{
	std::queue<std::pair<int, Point>> q({ std::pair<int, Point>(0, from) });
	std::unordered_set<Point> closedNodes;
	bool expFound = false;
	int distToExp = -1;
	while (q.size() != 0)
	{
		auto p = q.front();
		q.pop();
		if (mapInfo_.getMapInfo(p.second.i_, p.second.j_) == CommonData::MAINBASE)
			return p.first;
		if (mapInfo_.getMapInfo(p.second.i_, p.second.j_) == CommonData::EXPANSION)
		{
			expFound = true;
			distToExp = p.first;
		}
		if (!contains(closedNodes, p.second))
			closedNodes.insert(p.second);
		else
			continue;
		std::vector<Point> successors;
		getSuccessors8Dirs(p.second, successors, 0, 0, mapInfo_.getNumOfWalkTilesX(), mapInfo_.getNumOfWalkTilesY());
		for (auto& s : successors)
		{
			if (mapInfo_.getMapInfo(s.i_, s.j_) == CommonData::WALKABLE ||
				mapInfo_.getMapInfo(s.i_, s.j_) == CommonData::MAINBASE ||
				mapInfo_.getMapInfo(s.i_, s.j_) == CommonData::EXPANSION)
				q.push(std::pair<int, Point>(p.first + 1, s));
		}
	}
	return expFound ? distToExp : -1;
}

void ChokePointParser::populateSightRangeBlock(std::vector<std::vector<int>>& mapCut)
{
	int blockRange = 200;
	//ok, so we want to block attacker to place units in regions around cliffs, how to do that ? 
	//solution is kinda easy alothough again a bit costly; i will take every defender tile that is 
	//adjacent to unwalkable tile; positions of these tiles is stored inside some vector; 
	//and now, for each attacker tile, if dist to at least one of these tiles is < blockRange, then
	//this tile is marked as blocking

	//first, determine the defenderTilesAdjacent to unwalkables

	std::vector<Point> edgeDefenderTilesPositions; //these are top left positions of tiles, so tile 
	std::vector<Point> successors;
	for (size_t j = 0; j < mapCut[0].size(); ++j) 
	{
		for (size_t i = 0; i < mapCut.size(); ++i)
		{
			if (mapCut[i][j] == CommonData::DEFENDER)
			{
				Point point(i, j);
				getSuccessors8Dirs(point, successors, 0, 0, mapCut.size(), mapCut[0].size());
				for (auto& p : successors)
				{
					if (mapCut[p.i_][p.j_] == CommonData::UNWALKABLE)
						edgeDefenderTilesPositions.push_back(Point(i * 8, j * 8));
				}
			}
		}
	}

	//in edgeDefenderTilePositions are now actual positions of tiles, so, if i have tile (1,1) that is edge, it will have position (8,8)
	for (size_t j = 0; j < mapCut[0].size(); ++j)
	{
		for (size_t i = 0; i < mapCut.size(); ++i)
		{
			if (mapCut[i][j] == CommonData::ATTACKER)
			{
				Point tilePos(i * 8, j * 8);
				for (auto& tile : edgeDefenderTilesPositions)
				{
					int dist =(int)(sqrt(pow(tilePos.i_ - tile.i_, 2) + pow(tilePos.j_ - tile.j_, 2)));
					if (dist < blockRange)
					{
						mapCut[i][j] = CommonData::SIGHTRANGEBLOCK;
						break;
					}
				}
			}
		}
	}
}


void ChokePointParser::populateDefenderBack(std::vector<std::vector<int>>& mapCut)
{
	//the below provides a really simple solution

	//this walks the map in this way:
	// ---------->
	// ---------->
	int furthestDistance = std::numeric_limits<int>::min();
	Point furthestPoint(-1, -1);
	std::unordered_map<Point, int> distances;
	for (size_t j = 0; j < mapCut[0].size(); ++j)
	{
		for (size_t i = 0; i < mapCut.size(); ++i)
		{
			if (mapCut[i][j] == CommonData::DEFENDER)
			{
				//let's determine the distance to nearrest attackerTile
				Point p(i, j);
				int dist = distanceToNearrestAttackerTile(p, mapCut);
				if (dist > furthestDistance)
				{
					furthestDistance = dist;
					furthestPoint = p;
				}
				distances.insert(std::pair<Point, int>(p, dist));
			}
		}		
	}
	int distanceLimit = furthestDistance - 20;
	int maxNumberOfPopulatedTiles =(int)(distances.size()*0.09);
	//now, we know the furthest point, let's populate for several iterations this area with defenderBackTiles
	std::queue<Point> points({ furthestPoint });
	while (points.size() != 0)
	{
		
		auto p = points.front();
		points.pop();

		if (mapCut[p.i_][p.j_] == CommonData::DEFENDER)
		{
			if (distances[p] >= distanceLimit)
			{
				mapCut[p.i_][p.j_] = CommonData::DEFENDERBACK;
				std::vector<Point> successors;
				getSuccessors4Dirs(p, successors, 0, 0, mapCut.size(), mapCut[0].size());
				for (auto&s : successors)
				{
					points.push(s);
				}
				maxNumberOfPopulatedTiles--;
			}
		}

	}	
}



int ChokePointParser::distanceToNearrestAttackerTile(Point from, std::vector<std::vector<int>>& mapCut)
{
	std::queue<std::pair<int, Point>> q({ std::pair<int, Point>(0, from) });
	std::unordered_set<Point> closedNodes;

	int distToAttacker = -1;
	while (q.size() != 0)
	{
		auto pair = q.front();
		q.pop();
		if (mapCut[pair.second.i_][pair.second.j_] == CommonData::ATTACKER)
			return pair.first;
		if (!contains(closedNodes, pair.second))
			closedNodes.insert(pair.second);
		else
			continue;
		std::vector<Point> successors;
		getSuccessors4Dirs(pair.second, successors, 0, 0, mapCut.size(), mapCut[0].size());
		for (auto& s : successors)
		{
			q.push(std::pair<int, Point>(pair.first + 1, s));
		}
	}
	return -1;
}


bool ChokePointParser::contains(std::unordered_set<Point>& set, Point p)
{
	std::unordered_set<Point>::const_iterator i = set.find(p);
	if (i == set.end())
		return false;
	else
		return true;
}

//simple method that returns successors in 4 directions
void ChokePointParser::getSuccessors4Dirs(Point from, std::vector<Point>& successors, int smallestXLimit, int smallestYLimit,
	int biggestXLimit, int biggestYLimit)
{
	std::vector<Point> candidates = {
		Point(from.i_ - 1, from.j_),
		Point(from.i_, from.j_ - 1),
		Point(from.i_, from.j_+1),
		Point(from.i_+1,from.j_)
	};

	for (auto& c : candidates)
	{
		if (c.i_ >= smallestXLimit && c.i_ <= biggestXLimit - 1 &&
			c.j_ >= smallestYLimit && c.j_ <= biggestYLimit - 1)
			successors.push_back(c);
	}
}

//simple method that returns successors in 8 directions
void ChokePointParser::getSuccessors8Dirs(Point walkTile, std::vector<Point>& successors, int smallestXLimit, int smallestYLimit,
	int biggestXLimit, int biggestYLimit)
{
	std::vector<Point> candidates = {
		Point(walkTile.i_-1, walkTile.j_-1),
		Point(walkTile.i_-1, walkTile.j_),
		Point(walkTile.i_-1, walkTile.j_+1),
		Point(walkTile.i_, walkTile.j_-1),
		Point(walkTile.i_, walkTile.j_+1),
		Point(walkTile.i_+1, walkTile.j_-1),
		Point(walkTile.i_+1, walkTile.j_),
		Point(walkTile.i_+1, walkTile.j_+1)
	};
	successors.clear();
	for (auto& c : candidates)
	{
		if (c.i_ >= smallestXLimit && c.i_ <=biggestXLimit - 1 && 
			c.j_ >= smallestYLimit && c.j_ <=biggestYLimit - 1)
			successors.push_back(c);
	}
}


void ChokePointParser::markChokePointBlock(ChokePointInfo& chpInfo)
{
	/*
	this method works like this: 
	1. it takes the chokepoint edge points returned by the BWTA, these are
	chpInfo.firstPoint and chpInfo.secondPoint
	2. each tiles, that are crossed by the line from chpInfo.firstPoint to chpInfo.secondPoint
	are marked as CHOKEPOINTBLOCK, in addition, each of these marked tiles in this step
	is further expanded in 8 directions
	this single expansion of each point in 8 directions forms the rest of the chokepoint block
	*/

	std::vector<Point> walkTilesToExpand;
	walkTilesToExpand.push_back(Point(chpInfo.walkTileX_, chpInfo.walkTileY_));
	extendChokePointCenter(Point(chpInfo.positionX_, chpInfo.positionY_),
		Point(chpInfo.firstPointX_, chpInfo.firstPointY_),
		walkTilesToExpand);
	extendChokePointCenter(Point(chpInfo.positionX_, chpInfo.positionY_),
		Point(chpInfo.secondPointX_, chpInfo.secondPointY_),
		walkTilesToExpand);

	for (auto walkTileToExpand : walkTilesToExpand)
	{
		std::vector<Point> successors;
		getSuccessors8Dirs(walkTileToExpand, successors, 0, 0, mapInfo_.getNumOfWalkTilesX(), mapInfo_.getNumOfWalkTilesY());
		for (auto successor : successors)
		{
			if (mapInfo_.getMapInfo(successor.i_, successor.j_) == CommonData::WALKABLE)
				mapInfo_.setMapInfo(successor.i_, successor.j_, CommonData::CHOKEPOINTBLOCK);
		}
	}
}

void ChokePointParser::extendChokePointCenter(Point chpCenter, Point target, std::vector<Point>& walkTilesToExpand)
{
	/*
	a helper method to the method markChokePointBlock
	*/
	Vector v(target.i_ - chpCenter.i_, target.j_ - chpCenter.j_);
	double vectorMagnitude = sqrt(v.i_*v.i_ + v.j_*v.j_);

	Vector unitVector(v.i_ / vectorMagnitude, v.j_ / vectorMagnitude);
	Vector curPoint(chpCenter.i_, chpCenter.j_);
	Point curWalkTile(chpCenter.i_ / 8, chpCenter.j_ / 8);

	while (true)
	{
		curPoint = Vector(curPoint.i_ + unitVector.i_, curPoint.j_ + unitVector.j_);

		//determine current tile
		Point p((int)(curPoint.i_ / 8), (int)(curPoint.j_ / 8));
		//if this is the last visited tile, then continue
		if (p == curWalkTile)
			continue;
		curWalkTile = p;
		walkTilesToExpand.push_back(curWalkTile);
		mapInfo_.setMapInfo(curWalkTile.i_, curWalkTile.j_, CommonData::CHOKEPOINTBLOCK);

		std::vector<Point> successors;
		getSuccessors8Dirs(curWalkTile, successors, 0, 0, mapInfo_.getNumOfWalkTilesX(), mapInfo_.getNumOfWalkTilesY());
		for (auto& s : successors)
		{
			if (mapInfo_.getMapInfo(s.i_, s.j_) == CommonData::UNWALKABLE)
				return;
		}
	}
}

void ChokePointParser::outputChokePoint(std::vector<std::vector<int>>& chokePointSurround, ChokePointInfo& cpi)
{
	std::ofstream ofstream(outputDirectory_ + "/" + mapInfo_.getMapName() + "_CHP_" + std::to_string(cpi.chokePointNumber_) + ".txt");

	ofstream << "numOfWalkTilesX: " << std::to_string(cpi.numOfWalkTilesX_)
		<< " numOfWalkTilesY: " << std::to_string(cpi.numOfWalkTilesY_)
		<< " topLeftX: " << std::to_string(cpi.topLeftX_)
		<< " topLeftY: " << std::to_string(cpi.topLeftY_)
		<< std::endl;
	ofstream << "walkTileX: " << std::to_string(cpi.walkTileX_)
		<< " walkTileY: " << std::to_string(cpi.walkTileY_)
		<< " positionX: " << std::to_string(cpi.positionX_)
		<< " positionY: " << std::to_string(cpi.positionY_)
		<< " firstPointX: " << std::to_string(cpi.firstPointX_)
		<< " firstPointY: " << std::to_string(cpi.firstPointY_)
		<< " secondPointX: " << std::to_string(cpi.secondPointX_)
		<< " secondPointY: " << std::to_string(cpi.secondPointY_)
		<< std::endl;

	for (size_t j = 0; j < chokePointSurround[0].size(); ++j)
	{
		size_t it = 0;
		for (size_t i = 0; i < chokePointSurround.size(); ++i)
		{
			ofstream << std::to_string(chokePointSurround[i][j]);
		}
		ofstream << std::endl;
	}
	ofstream.flush();
	ofstream.close();

}

std::vector<std::string> ChokePointParser::split(const std::string& s, char delimiter)
{
	std::vector<std::string> elems;
	std::string chars;
	for (auto& c : s)
	{
		if (c == delimiter)
		{
			elems.push_back(chars);
			chars.clear();
		}
		else
		{
			chars.push_back(c);
		}
	}
	if (chars.size() > 0)
		elems.push_back(chars);
	return elems;
}
