#!/bin/sh

cd $SC_CP_DEFFORMATIONS_DIR/ChokePointParser

echo "cleaning up"

rm -f source/*.o
rm -f bin/ChokePointParser
rm -rf ParsedChokepoints/*/

echo "compiling source"

cd source

g++ -std=c++14 -c *.cpp -I$SC_CP_DEFFORMATIONS_DIR/CommonData/source -I$BOOST_DIR

echo "linking object files"

g++ -std=c++14 *.o -Bstatic -lboost_system -lboost_filesystem -L$BOOST_DIR/stage/lib -Bdynamic -lCommonData -L$SC_CP_DEFFORMATIONS_DIR/CommonData/dll -o ChokePointParser

mv ChokePointParser ../bin
rm -f *.o

echo "done"
