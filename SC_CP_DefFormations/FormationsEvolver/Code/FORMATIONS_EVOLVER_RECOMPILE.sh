#!/bin/sh
cd $SC_CP_DEFFORMATIONS_DIR/FormationsEvolver/Code

echo "doing clean up"
rm -f source/*.o
rm -f bin/FormationsEvolver

echo "compiling"

cd source

g++ -std=c++14 -O3 -c *.cpp -I$SPARCRAFT_DIR/source/SparCraft -I$SPARCRAFT_DIR/source/SparCraft_experiment -I$BWAPI_DIR/include -I$BOOST_DIR -I$SC_CP_DEFFORMATIONS_DIR/CommonData/source

echo "linking"

g++ -std=c++14 *.o -Bstatic -lBWAPI -L$BWAPI_DIR/lib -lboost_system -lboost_filesystem -L$BOOST_DIR/stage/lib -Bdynamic -lCommonData -L$SC_CP_DEFFORMATIONS_DIR/CommonData/dll -lSparCraft_experiment -L$SPARCRAFT_DIR/dll -o FormationsEvolver

mv FormationsEvolver ../bin
rm -f *.o

echo "done"
