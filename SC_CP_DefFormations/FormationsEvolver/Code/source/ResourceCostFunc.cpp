#include "ResourceCostFunc.h"

using namespace FormationsEvolution;

int ResourceCostFunc::getScore(const SparCraft::SparCraftExperimentResult& experimentResult) const
{
	//simple scoring function that is based on the calculation of
	//lost units; value of each unit is calculated from its purchasing cost
	//with gas, as being the rarer resource, being multiplied by 1.5
	auto& defenderUnits = experimentResult.playerUnits_[General::DEFENDER_ID];
	auto& attackerUnits = experimentResult.playerUnits_[General::ATTACKER_ID];

	int totalValue(0);

	for (auto& dPair : defenderUnits)
	{
		if (dPair.second > 0)
		{
			double ratio = dPair.second / ((double)BWAPI::UnitType(dPair.first).maxHitPoints() +
				(double)BWAPI::UnitType(dPair.first).maxShields());
			int val = (int)(BWAPI::UnitType(dPair.first).gasPrice() * 1.5) + (BWAPI::UnitType(dPair.first).mineralPrice());
			totalValue += (int)(val*ratio);
		}
	}

	for (auto& aPair : attackerUnits)
	{
		if (aPair.second > 0)
		{
			double ratio = aPair.second / ((double)BWAPI::UnitType(aPair.first).maxHitPoints() +
				(double)BWAPI::UnitType(aPair.first).maxShields());
			int val = (int)(BWAPI::UnitType(aPair.first).gasPrice() * 1.5) + (BWAPI::UnitType(aPair.first).mineralPrice());
			totalValue -= (int)(val*ratio);
		}
	}
	return totalValue;

}