#include "LayoutEvaluatorBasic.h"

using namespace FormationsEvolution;

LayoutEvaluatorBasic::LayoutEvaluatorBasic(
	SparCraft::SearchExperiment* experiment,
	std::unique_ptr<ScoreFunc> scoreFunc) :
	LayoutEvaluator(
		experiment,
		std::move(scoreFunc))
{};

int LayoutEvaluatorBasic::evaluateTwoLayouts(
	const UnitLayout& first,
	int firstLayoutPlayerID,
	const UnitLayout& second,
	int secondLayoutPlayerID) const
{
	std::vector < std::pair < BWAPI::UnitType,
		SparCraft::Position >> forcePlayer1ForSparCraft;
	std::vector < std::pair < BWAPI::UnitType,
		SparCraft::Position >> forcePlayer2ForSparCraft;

	//create unit structures to match sparcraft
	for (auto& u : first.getUnits())
	{
		forcePlayer1ForSparCraft.push_back(std::pair<BWAPI::UnitType, SparCraft::Position>(
			u.getType(), SparCraft::Position(u.getPosition().x(), u.getPosition().y())));
	}

	for (auto& u : second.getUnits())
	{
		forcePlayer2ForSparCraft.push_back(std::pair<BWAPI::UnitType, SparCraft::Position>(
			u.getType(), SparCraft::Position(u.getPosition().x(), u.getPosition().y())));
	}

	//initialize new sparcraft experiment
	experiment_->setNewUnits(forcePlayer1ForSparCraft, firstLayoutPlayerID,
		forcePlayer2ForSparCraft, secondLayoutPlayerID);

	SparCraft::SparCraftExperimentResult experimentResult;
	
	//simply run the experiments as it is
	experiment_->runExperiment();

	//extract its results
	experimentResult = experiment_->extractExperimentResult();

	//and score it
	int achievedFitness = scoreFunc_->getScore(experimentResult);
	OutputHandler::getInstance().logScenarioEvalDone(achievedFitness);

	//return the score
	return achievedFitness;
}