#pragma once

#include "Common.h"
#include "Force.hpp"
#include "Chromosome.hpp"

namespace FormationsEvolution
{
	/**

	@ingroup GeneticAlgorithm
	@brief This class represents generation of the genetic algorithm made up of xyt chromosomes.
	*/
	class Generation
	{
		/// starting force of each chromosome
		const Force initialForce_;
		std::vector<Chromosome> chromosomes_;
		
		int playerID_; ///< to which player this generation belongs to
		/// this counter is used to determine how many ga iterations this
		/// generation undergo 
		int iterationsCounter_;
	public:
		/// <summary> Constructor for new Generation </summary>
		/// <param name="chromosomes"> chromosomes of this generation, see Chromosome </param>
		/// <param name="playerID"> to which player this generation belongs (this is important 
		///		for some advanced types of genetic algorithms like coevolution</param>
		///	<param name="initialForce"> the initial force that was used to made the individual chromosomes; again
		///		this is used in some advanced techniques of genetic algorithms
		Generation(
			std::vector<Chromosome>& chromosomes,
			int playerID,
			const Force& initialForce);


		/// <summary> returns players id to which this
		///		generation belongs </summary>
		int getPlayerID() const
		{
			return playerID_;
		};		

		/// <summary> replaces the chromosomes </summary>
		void replaceChromosomes(const std::vector<Chromosome>& newChromosomes)
		{
			chromosomes_ = newChromosomes;
		};
		
		/// <summary> increments counter of iterations </summary>
		void incrementCounter()
		{
			iterationsCounter_++;
		};

		/// <summary> returns the number of iterations this generation undergo </summary>
		int getIterationsCounter() const
		{
			return iterationsCounter_;
		};

		/// <summary> returns average fitness of the current chromosomes inside
		///		the generation </summary>
		int getAverageFitness() const
		{
			int val(0);
			for (auto& c : chromosomes_)
				val += c.getFitness();
			return val / (int)chromosomes_.size();
		};

		/// <summary>get index of the best chromosome from this generation </summary>
		size_t getBestIndex()const; 

		/// <summary>sorts the chromosomes inside this 
		///		generation according to theri fitness </summary>
		void sortByFitness(); 

		/// <summary> in a special cases, we need to distinguish 
		const Chromosome& getBestAfterNewGeneration() const
		{
			return chromosomes_[0];
		}

		/// <summary> special case for logging purposes </summary>
		const Chromosome& getBestChromosomeBeforeNewGeneration() const
		{
			return chromosomes_[getBestIndex()];
		};

		/// <summary> get the fitness of currently best chromosome </summary>
		int getBestFitness() const
		{
			return chromosomes_[getBestIndex()].getFitness();
		};

		/// <summary> returns force that was used to
		///		create the chromosomes of this generation </summary>
		const Force& getInitialForce() const
		{
			return initialForce_;
		};

		///<summary> returns number of chromosomes </summary>
		size_t size() const
		{
			return chromosomes_.size();
		};

		/// <summary> intuitive definition of the [] operator - 
		///		which returns chromosome at the provided index </summary>
		Chromosome& operator[](size_t index)
		{
			return chromosomes_[index];
		};

		/// <summary> intuitive definition of the [] operator - 
		///		which returns chromosome at the provided index </summary>
		const Chromosome& operator[](size_t index) const
		{
			return chromosomes_[index];
		};

	};
}