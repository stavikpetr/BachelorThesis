#pragma once

#include "Common.h"

namespace FormationsEvolution
{
	/**
	@ingroup Util
	@brief Class representing set of pairs <unitType, unitCount>
	*/
	class Force
	{
		std::vector<std::pair<BWAPI::UnitType, int>> force_;
	public:
		//default constructor that creates an empty force
		Force() {};
		// move ctor
		Force(std::vector<std::pair<BWAPI::UnitType, int>> && force) :force_(std::move(force)) {};

		/// <summary> adds new pair of {unitType, unitCount} to the force </summary>
		void addNewUnits(BWAPI::UnitType unitType, int numberOfUnits)
		{
			force_.push_back(std::pair<BWAPI::UnitType, int>(unitType, numberOfUnits));
		}

		/// <summary> returns force this class holds </summary>
		const std::vector<std::pair<BWAPI::UnitType, int>>& getForce() const
		{
			return force_;
		}

		/// <summary> clears the units in this force </summary>
		void clear()
		{
			force_.clear();
		};
	};
}