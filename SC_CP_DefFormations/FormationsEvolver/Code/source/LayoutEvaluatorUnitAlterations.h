#pragma once

#include "LayoutEvaluator.h"
#include "UnitAlteration.hpp"

namespace FormationsEvolution
{
	/**
	For the role of layout evaluators, see LayoutEvaluator.

	@ingroup GeneticAlgorithm
	@brief One possible implemenation of layout evaluator that may alter the units of both players
	*/
	class LayoutEvaluatorUnitAlterations : public LayoutEvaluator
	{
		UnitAlteration unitAlteration_;
	public:
		/// <summary> constructor of this layout evaluator </summary>
		/// <param name="experiment"> layout evaluator needs reference to
		///		the sparcraft simulation </param>
		///	<param name="scoreFunc"> scoring function for scoring the simulation
		///		results </param>
		///	<param name="unitAlteraion"> this layout evaluator works in a different
		///		manner and it may alter the unit compositions before executing
		///		the sparcraft simulation, this parameter contains the information
		///		about the alteration </param>
		LayoutEvaluatorUnitAlterations(
			SparCraft::SearchExperiment* experiment,
			std::unique_ptr<ScoreFunc> scoreFunc,
			UnitAlteration& unitAlteration);

		/// <summary> method for evaluating two unit layouts with
		///		that belong to respective players </summary>
		///	<param name="first"> first unit layout </param>
		///	<param name="firstLayoutPlayerID"> id of the player to which the first layout belongs </param>
		///	<param name="second"> second unit layout </param>
		///	<param name="secondLayoutPlayerID"> id of the player to which the second layout belongs </param>
		///	<returns> the score assigned to the result of the simulation by the score func that
		///		belongs to this layout evaluator </returns>
		int evaluateTwoLayouts(
			const UnitLayout& first,
			int firstLayoutPlayerID,
			const UnitLayout& second,
			int secondLayoutPlayerID) const;
	};
}