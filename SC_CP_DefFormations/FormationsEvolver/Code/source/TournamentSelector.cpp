#include "TournamentSelector.h"

using namespace FormationsEvolution;

int TournamentSelector::selectOne(const Generation& generation) const
{
	//select two random chromosomes
	int i = Random::getRandInt(0, generation.size());
	int j = Random::getRandInt(0, generation.size());

	//and return "the better" one based on the player (one maximizes, the other minimizes)
	if (generation.getPlayerID() == General::ATTACKER_ID)
	{
		if (generation[i].getFitness() < generation[j].getFitness())
			return i;
		return j;
	}
	else if (generation.getPlayerID() == General::DEFENDER_ID)
	{
		if (generation[i].getFitness() > generation[j].getFitness())
			return i;
		return j;
	}
	else
	{
		System::FatalError("invalid player");
		return -1;
	}
};

//possible implementation of selection - tournament selection
std::pair<int, int> TournamentSelector::selectTwo(const Generation& generation) const
{
	int first(-1);
	int second(-1);

	//select two distinct chromosomes
	first = selectOne(generation);
	second = selectOne(generation);
	while (second == first)
		second = selectOne(generation);

	return std::pair<int, int>(first, second);
};