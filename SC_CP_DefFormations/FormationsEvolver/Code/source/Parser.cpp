#include "Parser.h"

using namespace FormationsEvolution;

void Parser::splitString(
	const std::string& s, std::vector<char> delimiters, std::vector<std::string>& toFill)
{
	toFill.clear();
	std::string chars;

	//go through the chars and check if the char is equal to delimiter
	for (auto& c : s)
	{
		bool del = false;
		for (auto& d : delimiters)
		{
			if (c == d)
			{
				toFill.push_back(chars);
				chars.clear();
				del = true;
				continue;
			}
		}
		if (!del)
			chars.push_back(c);
	}
	if (chars.size() > 0)
		toFill.push_back(chars);
}

void Parser::readFileByLines(std::ifstream& fileStream,
	std::vector<std::string>& toFill)
{
	//read the whole file by lines and place them into the passed vector
	std::string line;

	while (std::getline(fileStream, line))
	{
		bool isEmpty = true;
		//check if the line is not empty
		for (size_t i(0); i < line.size(); ++i)
		{
			if (line[i] != ' ' && line[i] != '\n' && line[i] != '\r' && line[i] != '\t')
			{
				isEmpty = false;
				break;
			}
		}
		//if the line is not empty and it is not a comment, place it into the vector
		if (!isComment(line) && !isEmpty)
			toFill.push_back(std::move(line));
	}
}

bool Parser::isComment(const std::string& line)
{
	//comments in files start with tuple "##"
	if (line.size() > 1)
	{
		if (line[0] == '#'  && line[1] == '#')
			return true;
	}
	return false;
}

std::string Parser::getMapName(const std::string& path)
{
	std::vector<std::string> tokens;
	splitString(path, std::vector<char>({ '/', '\\' }), tokens);
	std::string fileName = tokens[tokens.size() - 1];
	tokens.clear();
	splitString(fileName, std::vector<char>({ '_' }), tokens);
	return tokens[0];
}


//string to boolean
bool Parser::stob(const std::string& s)
{
	if (s == "false")
		return false;
	else if (s == "true")
		return true;
	else
		System::FatalError("stob, error");
}

//string to selection type
GA::SelectionType Parser::stoSelectionType(const std::string& s)
{
	if (s == "Tournament")
		return GA::SelectionType::TOURNAMENT;
	else if (s == "RoulletteWheel")
		return GA::SelectionType::ROULETTEWHEEL;
	else
		System::FatalError("invalid string selection type");
}

//string to evolution type
GA::EvolutionType Parser::stoEvolutionType(const std::string& s)
{
	if (s == "CoEvolution")
		return GA::EvolutionType::COEVOLUTION;
	else if (s == "RoundRobin")
		return GA::EvolutionType::ROUNDROBIN;
	else
		System::FatalError("parser, invalid evolution type");
}

//string to crossover type
GA::CrossoverType Parser::stoCrossoverType(const std::string& s)
{
	if (s == "OnePoint")
		return GA::CrossoverType::ONEPOINT;
	else if (s == "TwoPoint")
		return GA::CrossoverType::TWOPOINT;
	else
		System::FatalError("parser, invalid crossover type");
}

//method that will parse a section of the input file related to CoEvolution genetic algorithm
void Parser::parseCoEvolution(const std::vector<std::string>& lines, size_t& index)
{
	std::vector<std::string> tokens;

	for (index; index < lines.size(); ++index)
	{
		splitString(lines[index], std::vector<char>({ ' ' }), tokens);
		if (tokens[0] == "PopulationSize")
		{
			GA::defPopulationSize = std::stoi(tokens[1]);
			GA::alt_attPopulationSize = std::stoi(tokens[2]);
		}
		else if (tokens[0] == "OptimizationSwap")
		{
			GA::alt_optSwap_turns_d = std::stoi(tokens[1]);
			GA::alt_optSwap_turns_a = std::stoi(tokens[2]);
		}
		else
			System::FatalError("parsing, coevolution, invalid option");
	}
}

//method that will parse a section of the input file related to RoundRobin genetic algorithm
void Parser::parseRoundRobinEvolution(const std::vector<std::string>& lines, size_t& index)
{
	std::vector<std::string> tokens;

	for (index; index < lines.size(); ++index)
	{
		splitString(lines[index], std::vector<char>({ ' ' }), tokens);
		if (tokens[0] == "PopulationSize")
		{
			GA::defPopulationSize = std::stoi(tokens[1]);
		}
		else if (tokens[0] == "NumberOfEnemyForces")
		{
			GA::rr_numOfEnemyForces = std::stoi(tokens[1]);
		}
		else
			System::FatalError("parsing, round robin, invalid option");
	}
}


void Parser::parseAndSetEvolutionSettings(const std::string& path)
{
	// the structure of file with evolution settings is specified in the user documentation
	//or you can check the input sample file

	std::ifstream evolutionSettings_input(path);
	std::vector<std::string> lines;
	readFileByLines(evolutionSettings_input, lines);
	evolutionSettings_input.close();

	for (size_t i(0); i < lines.size(); ++i)
	{
		std::vector<std::string> tokens;
		splitString(lines[i], std::vector<char>({ ' ' }), tokens);
		if (tokens[0] == "Seed")
		{
			//for testing only
			/*Random::setSeedIfNotSetAlready(1494245316);
			return;*/
			if (tokens[1] != "RANDOM")
				Random::setSeedIfNotSetAlready(std::stoi(tokens[1]));
			else if (tokens[1] == "RANDOM")
				Random::setSeedIfNotSetAlready((int)std::time(NULL));
		}
		else if (tokens[0] == "MaximumNumberOfIterations")
		{
			GA::maximumNumberOfIterations = std::stoi(tokens[1]);
		}
		else if (tokens[0] == "Mutation")
		{
			GA::mutationRate = std::stod(tokens[1]);
			if (tokens[2] == "uniform_int")
			{
				GA::mutationDistribution = GA::MutationDistribution::UNIFORM;
			}
			else if (tokens[2] == "normal")
			{
				GA::mutationDistribution = GA::MutationDistribution::NORMAL;
			}
			else
			{
				System::FatalError("invalid mutation distribution");
			}
			GA::mutationInitValue = std::stoi(tokens[3]);

			if (tokens[4] == "constant")
			{
				GA::mutationType = GA::MutationType::CONSTANT;
			}
			else if (tokens[4] == "decay")
			{
				GA::mutationType = GA::MutationType::DECAY;
				GA::decayToValue = std::stoi(tokens[5]);
				GA::decayToTurns = std::stoi(tokens[6]);
			}
			else if (tokens[4] == "plaguePeek")
			{
				GA::mutationType = GA::MutationType::PLAGUEPEEK;
				GA::plaguePeekAmount = std::stoi(tokens[5]);
				GA::plaguePeekDecayTurns = std::stoi(tokens[6]);
			}
			else
			{
				System::FatalError("invalidMutationType");
			}
		}
		else if (tokens[0] == "Crossover")
		{
			GA::crossoverRate = std::stod(tokens[1]);
			GA::crossoverType = stoCrossoverType(tokens[2]);
		}
		else if (tokens[0] == "Elitism")
		{
			GA::elitismPercentage = std::stod(tokens[1]);
			GA::elitism = stob(tokens[2]);
		}
		else if (tokens[0] == "Selection")
		{
			GA::selectionType = stoSelectionType(tokens[1]);
		}
		else if (tokens[0] == "EvolutionType")
		{
			GA::evolutionType = stoEvolutionType(tokens[1]);
			i++;
			if (GA::evolutionType == GA::EvolutionType::COEVOLUTION)
			{
				parseCoEvolution(lines, i);
			}
			else if (GA::evolutionType == GA::EvolutionType::ROUNDROBIN)
			{
				parseRoundRobinEvolution(lines, i);
			}
		}
		else if (tokens[0] == "Plague")
		{
			GA::plague = stob(tokens[1]);
			GA::plaguePercentage = std::stod(tokens[2]);
			GA::plagueStartIteration = std::stoi(tokens[3]);
			GA::plagueRepetitionCounter = std::stoi(tokens[4]);
		}
		else
			System::FatalError("parsing error");
	}
}


std::unique_ptr<ChokePoint> Parser::parseAndSetChokepoint(
	const std::string& path,
	SparCraft::SearchExperiment& searchExperiment)
{
	// the structure of file with chokepoint is specified in the user documentation
	//or you can check the input sample file

	std::string mapName = getMapName(path);

	std::ifstream chp_input(path);
	std::vector<std::string> lines;
	readFileByLines(chp_input, lines);
	chp_input.close();

	std::vector<std::string>tokens_line1;
	std::vector<std::string>tokens_line2;

	//first line contains
	//[1]=numOfWalkTilesX, [3] = numOfWalkTilesY, [5] = topLeftX, [7] = topLeftY
	splitString(lines[0], std::vector<char>({ ' ' }), tokens_line1);

	int numberOfWalkTilesX = std::stoi(tokens_line1[1]);
	int numberOfWalkTilesY = std::stoi(tokens_line1[3]);

	splitString(lines[1], std::vector<char>({ ' ' }), tokens_line2);

	std::unique_ptr<ChokePoint> chp = std::make_unique<ChokePoint>(
		mapName,
		numberOfWalkTilesX,
		numberOfWalkTilesY, 
		std::stoi(tokens_line2[1]), 
		std::stoi(tokens_line2[3]),
		Position(std::stoi(tokens_line1[5]), std::stoi(tokens_line1[7])), 
		Position(std::stoi(tokens_line2[5]), std::stoi(tokens_line2[7])));

	int vectorYIndex = 0;
	for (size_t i = 2; i < lines.size(); ++i)
	{
		for (size_t j = 0; j < lines[i].size(); ++j)
		{
			chp->setBaseTileValue(j, vectorYIndex, CommonData::MapValues((lines[i][j] - '0')));
		}
		vectorYIndex++;
	}

	lines[1] = std::to_string(numberOfWalkTilesX) + " " + std::to_string(numberOfWalkTilesY);
	searchExperiment.setNewMap(lines.begin() + 1, lines.end());

	return std::move(chp);
}

void Parser::parseAndSetPlayer(const std::string& path,
	SparCraft::SearchExperiment& sparcraftExperiment,
	int playerID)
{
	// the structure of file with players is specified in the user documentation
	//or you can check the input sample file

	//read the input
	std::ifstream p_input(path);
	std::vector<std::string> lines;
	readFileByLines(p_input, lines);
	p_input.close();

	std::string playerString = lines[0];
	OutputHandler::getInstance().setNewPlayer(playerID, playerString);
	if (playerID == General::DEFENDER_ID)
	{
		playerString += " HOLD";
	}
	else if (playerID == General::ATTACKER_ID)
	{
		playerString += " NONE";
	}

	//set the player to sparcraft
	sparcraftExperiment.setNewPlayer(playerString, playerID);
}

void Parser::parseAndFillForce(const std::string& path, Force& toFill)
{
	// the structure of file with units is specified in the user documentation
	//or you can check the input sample file

	toFill.clear();

	//read the input
	std::ifstream force_input(path);
	std::vector<std::string> lines;
	readFileByLines(force_input, lines);
	force_input.close();

	std::vector<std::string> tokens;
	for (auto& l : lines)
	{
		splitString(l, std::vector<char>({ ' ' }), tokens);
		BWAPI::UnitType type;

		for (const BWAPI::UnitType & t : BWAPI::UnitTypes::allUnitTypes())
		{
			if (t.getName().compare(tokens[0]) == 0)
			{
				type = t;
			}
		}

		toFill.addNewUnits(type, std::stoi(tokens[1]));
	}
}