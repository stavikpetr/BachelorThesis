#pragma once

#include "ScoreFunc.h"

namespace FormationsEvolution
{
	/**
	@ingroup GeneticAlgorithm
	@brief An implementation of ScoreFunc which takes into account the damage done to the infrastructure
	*/
	class InfrastructureHuntFunc : public ScoreFunc
	{
		double getInfrastructureHuntFuncMultiplier(
			int totalTime, int damageTime) const;

	public:
		/// <summary> method that takes the assigns the score to the passed sparcraft simulation </summary>
		///	<param name="experimentResult"> the result of the sparcraft simulation </param>
		///	<returns> the score of the experiment result </returns>
		int getScore(const SparCraft::SparCraftExperimentResult& experimentResult) const;
	};
}