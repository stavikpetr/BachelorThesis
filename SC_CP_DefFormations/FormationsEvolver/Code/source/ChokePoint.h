#pragma once

#include "Common.h"
#include "BaseTile.h"
#include "Position.hpp"

namespace FormationsEvolution
{
	/**
	This class represents a single chokepoint and provides several useful methods
	for getting information about it, for example which tiles are walkable and which
	are not. The chokepoint is made up of 2D vector of BaseTile.

	@ingroup ChokePoint
	@brief Class that represents a single chokepoint
	*/
	class ChokePoint
	{
		int numOfWalkTilesX_, numOfWalkTilesY_;
		Position topLeftCorner_;
		Position chokePointCenter_;
		int chokePointTileX_, chokePointTileY_;
		std::vector<std::vector<BaseTile>> chokepoint_;
		int pointSizeX_, pointSizeY_;
		std::string mapName_;

		CommonData::MapValues getBaseTileValue(int tileNumberX, int tileNumberY) const;
		bool isValidWalkTileXIndex(int walkTileXIndex) const;
		bool isValidWalkTileYIndex(int walkTileYIndex) const;
	public:
		/// <summary> constructor of chokepoint </summary>
		/// <param name="mapName"> name of the map </summary>
		/// <param name="numOfWalkTilesX"> number of walk tiles in the x direction </param>
		/// <param name="numOfWalkTilesY"> number of walk tiles in the y direction </param>
		/// <param name="chokePointTileX"> number of chokepoint tiles in x direction </param>
		/// <param name="chokePointTileY"> number of chokepoint tiles in y direction </param>
		/// <param name="topLeftCorner"> top left corner position of the chokepoint </param>
		/// <param name="chokePointCenter"> position of the chokepoint center </param>
		ChokePoint(std::string mapName, int numOfWalkTilesX, int numOfWalkTilesY, 
			int chokePointTileX, int chokePointTileY,
			const Position& topLeftCorner, const Position& chokePointCenter);

		/// <summary> method that will set map value of single base tile </summary>
		/// <param name="vectorXIndex"> x index of the base tile </param>
		/// <param name="vectorYIndex"> y index of the base tile </param>
		/// <param name="mapValue"> new map value </param>
		void setBaseTileValue(int vectorXIndex, int vectorYIndex, CommonData::MapValues mapValue);

		/// <summary> returns base tile at the given position </summary>
		CommonData::MapValues getBaseTileValue(const Position& p) const;

		/// <summary> this method checks, whether each base tile at the given
		///		rectangle specified by the four positions contains the map value
		///		provided in the argument </summary>
		bool checkRecntagleBaseTilesForValue(const Position& topLeft, const Position& topRight, 
			const Position& botLeft, const Position& botRight,
			CommonData::MapValues mapValue) const;

		/// <summary> this method checks if the tow units at the specified positions
		///		with their height and widht collide </summary>
		bool areUnitsColiding(const Position& topLeft1, int width1, int height1,
			const Position& topLeft2, int width2, int height2) const;
	
		/// <summary> checks, if the provided indices lay within the chokepoint boundaries </summary>
		bool isValidWalkTileIndex(int walkTileXIndex, int walkTileYIndex) const;
		
		/// <summary> this method checks if the unit location lays
		///		inside the map </summary>
		bool isUnitInsideMap(const Position& topLeft, const Position& botRight) const;


		/** few util functions */

		int getCenterX() const
		{
			return chokePointCenter_.x();
		}

		int getCenterY() const
		{
			return chokePointCenter_.y();
		}

		int getWalkTileSizeX() const
		{
			return numOfWalkTilesX_;
		}

		int getWalkTileSizeY() const
		{
			return numOfWalkTilesY_;
		}

		int getPointSizeX() const
		{
			return numOfWalkTilesX_*General::walkTileSize;
		}

		int getPointSizeY() const
		{
			return numOfWalkTilesY_*General::walkTileSize;
		}

		const std::string& getMapName() const
		{
			return mapName_;
		};

	};
	typedef const ChokePoint* const CHP_Observer;
}
