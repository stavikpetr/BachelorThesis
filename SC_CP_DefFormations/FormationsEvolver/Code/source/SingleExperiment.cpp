#include "SingleExperiment.h"

using namespace FormationsEvolution;

SingleExperiment::SingleExperiment(CHP_Observer chp,
	const Force& defenderForce,
	const Force& attackerForce) :
	chp_(chp),
	defenderForce_(defenderForce),
	attackerForce_(attackerForce)
{
};


void SingleExperiment::performSingleExperiment(SparCraft::SearchExperiment& experiment)
{
	//create new folder for output of this single experiment
	OutputHandler::getInstance().newExperiment(defenderForce_, attackerForce_, chp_);

	//create new object that will find the layout
	std::unique_ptr<LayoutFinder> layoutFinder;
	layoutFinder = std::make_unique<GALayoutFinder>(chp_, defenderForce_, attackerForce_);

	//these two layout finders were again used in the bachelor thesis for comparison of several methods
	//for finding a good initial layout
	//------
	//layoutFinder = std::make_unique<RandomLayoutFinder>(chp_, defenderForce_, attackerForce_);
	//layoutFinder = std::make_unique<HumanLayoutFinder>(chp_, defenderForce_, attackerForce_);

	//find the best layout
	UnitLayout bestLayout = layoutFinder->findBestLayout(experiment);

	//and output it
	OutputHandler::getInstance().outputFinalLayout(bestLayout);

	//This part was used in my bachelor thesis for comparison of several methods for finding
	//a good initial layout. If one cares only about the resulting layout of one particular method,
	//this does not have to be called
	//the result would be found in the folder Output/layoutCmp
	//-----
	//OutputHandler::getInstance().logPerformingComparingExperiment();
	//FinalLayoutTester finalTester;
	//finalTester.initialize(chp_, attackerForce_, experiment);
	//finalTester.performComparingExperiment(bestLayout);
	//OutputHandler::getInstance().logComparingExperimentEnd();

	OutputHandler::getInstance().logEndOfExperiment();
}
