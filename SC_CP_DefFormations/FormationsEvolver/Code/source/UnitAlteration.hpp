#pragma once

#include "Common.h"
#include "UnitLayout.hpp"

namespace FormationsEvolution
{
	/**
	This class represents a unit alteration with units for some player
	It is used in the more advanced scenarios, when we e.g. add some units to one
	player before performing the sparcraft simulation

	@ingroup Util
	@brief Class representing modification of unit composition for one player 
	*/
	class UnitAlteration
	{
		//whose player's units will we alter
		int playerID_;
		//units to add
		UnitLayout unitLayout_;
	public:
		/// <summary> constructor - it requires playersID - we need
		///		to specify whose unit composition will we modify and
		///		unitLayout - units that will be add to the player </summary>
		UnitAlteration(const UnitLayout& unitLayout,
			int playerID) :
			playerID_(playerID),
			unitLayout_(unitLayout)
		{};

		/// <summary> returns the unit layout </summary>
		const UnitLayout& getUnitLayout() const
		{
			return unitLayout_;
		}

		/// <summary> returns the player for this alteration </summary>
		int getPlayerID() const
		{
			return playerID_;
		};
	};
}