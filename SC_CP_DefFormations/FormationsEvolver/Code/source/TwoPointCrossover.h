#pragma once

#include "Common.h"
#include "CrossoverPerformer.h"

namespace FormationsEvolution
{
	/**
	@ingroup GeneticAlgorithm
	@brief This class implemenets one type of crossover - two point crossover
	*/
	class TwoPointCrossover : public CrossoverPerformer
	{
		CollisionTilesHolder collisionTiles1_;
		CollisionTilesHolder collisionTiles2_;
	public:
		/// <summary> constructor of two point crossover </summary>
		/// <param name="chp"> chokepoint reference for correct unit switching </param>
		TwoPointCrossover(CHP_Observer chp);

		/// <summary> method that facilitates the crossover operation </summary>
		///	<param name="first">first chromosome for crossover </param>
		///	<param name="second">second chromosome for crossover </param>
		void crossover(Chromosome& first, Chromosome& second);
	};
}