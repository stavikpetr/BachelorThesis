#include "GenerationGenerator.h"

using namespace FormationsEvolution;

GenerationGenerator::GenerationGenerator(
	std::unique_ptr<ChromosomeSelector>&& chromosomeSelector,
	std::unique_ptr<MutationPerformer>&& mutationPerformer,
	std::unique_ptr<CrossoverPerformer>&& crossoverPerformer,
	std::unique_ptr<PlaguePerformer>&& plaguePerformer,
	std::unique_ptr<ElitismPerformer>&& elitismPerformer):
	elitismPerformer_(std::move(elitismPerformer)),
	mutator_(std::move(mutationPerformer)),
	crossoverPerformer_(std::move(crossoverPerformer)),
	chromosomeSelector_(std::move(chromosomeSelector)),
	plaguePerformer_(std::move(plaguePerformer))
{
}

GenerationGenerator::GenerationGenerator(GenerationGenerator&& other) :
	elitismPerformer_(std::move(other.elitismPerformer_)),
	mutator_(std::move(other.mutator_)),
	crossoverPerformer_(std::move(other.crossoverPerformer_)),
	chromosomeSelector_(std::move(other.chromosomeSelector_)),
	plaguePerformer_(std::move(other.plaguePerformer_))
{
};

GenerationGenerator& GenerationGenerator::operator=(GenerationGenerator&& other)
{
	elitismPerformer_ = std::move(other.elitismPerformer_);
	mutator_ = std::move(other.mutator_);
	crossoverPerformer_ = std::move(other.crossoverPerformer_);
	chromosomeSelector_ = std::move(other.chromosomeSelector_);
	plaguePerformer_ = std::move(other.plaguePerformer_);
	return *this;
};

void GenerationGenerator::createNewGeneration(Generation& oldGeneration) const
{
	std::vector<Chromosome> newChromosomes;

	int slaughteredChromosomes(0);

	//perform elitism and plague first
	elitismPerformer_->performElitism(oldGeneration, newChromosomes);
	plaguePerformer_->performPlague(oldGeneration, newChromosomes);
	mutator_->initalizeParameters(oldGeneration.getIterationsCounter());

	//repeat while we dont have enough new chromosomes for new generation
	while (newChromosomes.size() != oldGeneration.size())
	{
		//first, selection
		auto indicesPair = chromosomeSelector_->selectTwo(oldGeneration);

		Chromosome first(oldGeneration[indicesPair.first]);
		Chromosome second(oldGeneration[indicesPair.second]);

		int crossoverChance = Random::getRandInt(0, 101);

		//now crossover
		if (crossoverChance / 100.0 < GA::crossoverRate)
			crossoverPerformer_->crossover(first, second);

		//now mutate
		mutator_->mutateChromosome(first);
		mutator_->mutateChromosome(second);

		//now, insert, but only if there is no really similar chromosomes
		// (so we have some diversity)
		slaughteredChromosomes += insertChromosome(first, newChromosomes, oldGeneration.size());
		slaughteredChromosomes += insertChromosome(second, newChromosomes, oldGeneration.size());
	}

	//log slaughtered chromosomes
	OutputHandler::getInstance().logSuccessfulChromosomeKills(slaughteredChromosomes);

	oldGeneration.replaceChromosomes(newChromosomes);
}

int GenerationGenerator::insertChromosome(const Chromosome& toInsert,
	std::vector<Chromosome>& newChromosomes, size_t oldGenerationSize) const
{
	//if there is elitism, let's prevent insertion of really similiar chromosomes 
	//(ofc, we could do this check for each chromosome in generation, but that would be too costly, few from the elitism that were not changed are enough)
	bool anySimiliar(false);
	if (GA::elitism)
	{
		size_t numberOfElements = (size_t)(oldGenerationSize * GA::elitismPercentage);
		if (numberOfElements > 0)
		{
			for (size_t i(0); i < numberOfElements; ++i)
			{
				if (areChromosomesSimiliar(toInsert, newChromosomes[i]))
				{
					anySimiliar = true;
					break;
				}
			}
		}
	}

	if (!anySimiliar && newChromosomes.size() != oldGenerationSize)
		newChromosomes.push_back(toInsert);
	return anySimiliar;
}

bool GenerationGenerator::areChromosomesSimiliar(const Chromosome& first,
	const Chromosome& second) const
{
	//some basic checks of similar chromosomes
	auto& firstChromosomeUnits = first.getUnitLayout().getUnits();
	auto& secondChromosomeUnits = second.getUnitLayout().getUnits();

	bool everyUnitSimiliar(true);
	for (auto& u : firstChromosomeUnits)
	{
		int index(-1);
		bool similiarWithSomeone(false);
		for (size_t i(0); i < secondChromosomeUnits.size(); ++i)
		{
			//this check is a bit more costly, but more correct (before i only compared units with same ids)
			//which was a bit error prone ... (but well, chances of this happening are very low, but, you know :))
			//and i don't believe this might any sort of bottleneck at all
			if (u.getType() == secondChromosomeUnits[i].getType())
			{
				if (abs(u.getXPosition() - secondChromosomeUnits[i].getXPosition()) < 8 &&
					abs(u.getYPosition() - secondChromosomeUnits[i].getYPosition()) < 8)
				{
					similiarWithSomeone = true;
				}
			}
		}
		if (!similiarWithSomeone)
			return false;
		else
			similiarWithSomeone = false;

	}
	return true;
	
}
