#pragma once

#include "Common.h"
#include "Generation.h"

namespace FormationsEvolution
{
	/**
	@ingroup Output
	@brief This class offers several specialized methods for output for plotting which are used by the OutputHandler
	*/
	class PlotLogger
	{
		int fitnessLog_counter_;
		std::ofstream* fitnessOutput_;
		std::string fitness_base_string_;
		int iterationCounter_;


		/**
		It is important to note, that the verbose output does not go to just one file, even if
		it could and the output is seperated into multiple files (this explains the following
		two methods)
		*/
		void closeIfOpen(std::ofstream* output);
		void openNewFiles();

	public:
		PlotLogger();

		/// <summary> as we are reusing the instance of Plot logger, we
		///		just set some new parameters when we have new experiment </summary>
		void setNewParams(const std::string& newOutputDir);

		/// <summary> logs the fitness achieved by each chromosome at
		///		the end of one iteration of genetic algorithm </summary>
		void logGenerationAfterIteration(const Generation& gen);
	};
}