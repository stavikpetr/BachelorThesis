#pragma once

#include "Common.h"
#include "ChokePoint.h"
#include "UnitLayout.hpp"
#include "Force.hpp"
#include "CollisionTilesHolder.h"
#include <random>

namespace FormationsEvolution
{
	/**
		@brief This singleton class allows a correct random placing of units without causing collisions
	*/
	class LayoutManager
	{
		LayoutManager();
	public:
		/// <summary> singleton class getter </summary>
		static LayoutManager& getInstance();

		/// <summary> method, that will create a new valid random layout for the
		///		force provided in the argument and returns it in the form
		///		of provided UnitLayout argument </summary>
		///	<param name="chp"> chokepoint for the unit placement </summary>
		///	<param name="force"> force to place </summary>
		/// <param name="collisionTilesHolder"> collision tiles into which the units are placed </summary>
		/// <param name="mapValue"> this paramater tells the layout manager on which tiles can
		///		it place the units </param>
		/// <param name="toFill"> return argument (prevents copying) </param>
		void getValidRandomLayout(CHP_Observer chp, const Force& force, CollisionTilesHolder& collisionTilesHolder,
			CommonData::MapValues mapValue, UnitLayout& toFill);

		/// <summary> this variant of method getValidRandomLayout is a special case
		///		of the method above as it also supplies which random_engine is used to place
		///		the units </summary>
		void getValidRandomLayout(CHP_Observer chp, const Force& force, CollisionTilesHolder& collisionTilesHolder,
			CommonData::MapValues mapValue, UnitLayout& toFill, std::default_random_engine& engine);

	};
}