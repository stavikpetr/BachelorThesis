#include "FinalLayoutTester.h"

using namespace FormationsEvolution;

UnitLayout FinalLayoutTester::getPlacedForce(CHP_Observer chp, const Force& toPlace,
	CommonData::MapValues where_to, std::default_random_engine& engine)
{
	UnitLayout layout;
	CollisionTilesHolder c(chp->getWalkTileSizeX(), chp->getWalkTileSizeY());
	LayoutManager::getInstance().getValidRandomLayout(chp,
		toPlace, c, where_to, layout, engine);
	return layout;
}

void FinalLayoutTester::initialize(CHP_Observer chp, const Force& attackerForce,
	SparCraft::SearchExperiment& searchExperiment)
{
	//the code below shows one possible type of comparison experiment

	//layout the attacker force and prepare unit alteration (probes) for defender

	//as we want to have the same coniditons for each experiment test, we layout
	//the attacker's units and probes with the same seed each time 
	//(=> we need to use new seed and new engine)
	int cmpSeed = 14052017;

	std::default_random_engine cmpEng(cmpSeed);
	int numberOfTestAttackerLayouts = 30;

	for (int i(0); i < numberOfTestAttackerLayouts; ++i)
	{
		attackerUnitLayouts_.push_back(getPlacedForce(chp,
			attackerForce, CommonData::ATTACKER, cmpEng));
	}

	Force probeInfrastructure(std::vector<std::pair<BWAPI::UnitType, int>>{(
		std::pair<BWAPI::UnitType, int>(BWAPI::UnitTypes::Protoss_Probe, 5)
		)});

	UnitAlteration unitAlteration(getPlacedForce(chp,
		probeInfrastructure, CommonData::DEFENDERBACK, cmpEng), General::DEFENDER_ID);

	unitAlterations_.push_back(unitAlteration);

	layoutEvaluators_.push_back(std::make_unique<LayoutEvaluatorBasic>(
		&searchExperiment,
		std::make_unique<ResourceCostFunc>()));
	layoutEvaluators_.push_back(std::make_unique<LayoutEvaluatorUnitAlterations>(
		&searchExperiment,
		std::make_unique<InfrastructureHuntFunc>(),
		unitAlteration));
}

void FinalLayoutTester::performComparingExperiment(const UnitLayout& defenderBestLayout)
{
	//perform the experiments
	std::vector<int> fitnesses;
	for (size_t i(0); i < attackerUnitLayouts_.size(); ++i)
	{
		int fitness(0);
		for (auto& le : layoutEvaluators_)
		{
			fitness += le->evaluateTwoLayouts(defenderBestLayout,
				General::DEFENDER_ID,
				attackerUnitLayouts_[i], General::ATTACKER_ID);
		}
		fitness /= (int)layoutEvaluators_.size();
		fitnesses.push_back(fitness);
	}

	//and write the results
	OutputHandler::getInstance().outputExperimentTestResult(
		defenderBestLayout,
		fitnesses,
		attackerUnitLayouts_,
		unitAlterations_);
};