#pragma once

#include "ScoreFunc.h"

namespace FormationsEvolution
{
	/**
	@ingroup GeneticAlgorithm
	@brief One possible implementation of ScoreFunc that is based on the value of survived defenders units
	*/
	class ResourceCostFunc : public ScoreFunc
	{
	public:
		/// <summary> method that takes the assigns the score to the passed sparcraft simulation </summary>
		///	<param name="experimentResult"> the result of the sparcraft simulation </param>
		///	<returns> the score of the experiment result </returns>
		int getScore(const SparCraft::SparCraftExperimentResult& experimentResult) const;
	};
}