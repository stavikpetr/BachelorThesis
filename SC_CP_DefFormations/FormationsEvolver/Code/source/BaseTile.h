#pragma once

#include "Common.h"

namespace FormationsEvolution
{
	/**
	Each base tile is an 8x8 tile which holds a single value, which
	denotes e.g. whether this tile is walkable.

	@ingroup ChokePoint
	@brief This class represents a single base tile for chokepoint
	*/
	class BaseTile
	{
		CommonData::MapValues mapValue_;
	public:
		/// <summary> default constructor that makes the tile unwalkable </summary>
		BaseTile():mapValue_(CommonData::UNWALKABLE)
		{};

		/// <summary> constructor that will also set the map value of this base tile
		BaseTile(CommonData::MapValues mapValue):mapValue_(mapValue)
		{};

		/// <summary> method that will set map value of this tile </summary>
		void setMapValue(CommonData::MapValues mapValue)
		{
			mapValue_ = mapValue;
		};

		/// <summary> method that will check if the current base tile is walkable </summary>
		bool isWalkable() const
		{
			return mapValue_ != CommonData::UNWALKABLE ? true : false;
		};

		/// <summary> method that will return the map value this base tile holds </summary>
		CommonData::MapValues mapValue() const
		{
			return mapValue_;
		};
	};
}
