#pragma once

#include "Common.h"
#include "Chromosome.hpp"
#include "CollisionTilesHolder.h"
#include "ChokePoint.h"

namespace FormationsEvolution
{
	/**
	Currently, there are two implementations - one point and two point crossover.
	In both of them, crossover swaps units between the chromosomes. However, various
	different implementations are also possible

	@ingroup GeneticAlgorithm
	@brief Base abstract class for different implementations of crossover operation
	*/
	class CrossoverPerformer
	{
	protected:
		// each crossover performer will need reference to the chokepoint
		CHP_Observer chokePoint_;
		void tryChangePosition(Unit& unit,const Position& targetPosition, CollisionTilesHolder& collisionTilesHolder,
			CommonData::MapValues playerTileType);
		void trySwapUnits(Chromosome& first, CollisionTilesHolder& firstChromosomeCollisionTiles,
			Chromosome& second, CollisionTilesHolder& secondChromosomeCollisionTiles,
			size_t index);
		void checkCrossoverWasCorrect(const Chromosome& first, const Chromosome& second) const;
		CrossoverPerformer(CHP_Observer chp);
	public:
		/// <summary> method that facilitates the crossover operation </summary>
		///	<param name="first">first chromosome for crossover </param>
		///	<param name="second">second chromosome for crossover </param>
		virtual void crossover(Chromosome& first, Chromosome& second) = 0;
		virtual ~CrossoverPerformer(){};
	};
}