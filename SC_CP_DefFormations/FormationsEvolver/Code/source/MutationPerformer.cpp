#include "MutationPerformer.h"

using namespace FormationsEvolution;

MutationPerformer::MutationPerformer(CHP_Observer chp) :chp_(chp),
collisionTilesHolder_(chp->getWalkTileSizeX(), chp->getWalkTileSizeY()),
nDistribution_(0, GA::mutationInitValue), uDistribution_(-1*(GA::mutationInitValue/2), GA::mutationInitValue/2)
{

}

int MutationPerformer::getRandomAmountForMutation()
{
	int val(0);
	if (GA::mutationDistribution == GA::MutationDistribution::NORMAL)
	{
		val = (int)nDistribution_(Random::getDefaultRandomEngine());
	}
	else if (GA::mutationDistribution == GA::MutationDistribution::UNIFORM)
	{
		val = (int)uDistribution_(Random::getDefaultRandomEngine());
	}

	//std::cout << "mutation, returning: " << std::to_string(val) << std::endl;
	return val;
}

void MutationPerformer::changeDistributionParams(double newSecondParam)
{
	OutputHandler::getInstance().logMutationDistributionSecondParamChange((int)newSecondParam);
	if (GA::mutationDistribution == GA::MutationDistribution::NORMAL)
	{
		nDistribution_ = std::normal_distribution<>(0, newSecondParam);
	}
	else if (GA::mutationDistribution == GA::MutationDistribution::UNIFORM)
	{
		uDistribution_ = std::uniform_int_distribution<>((int)(-1*(newSecondParam/2)), ((int)newSecondParam/2));
	}
}

int MutationPerformer::determineActualAmountFromTiles(int amount, const Position& direction,
	const Unit& unit, CommonData::MapValues playerTileType)
{
	int actualAmount(0);

	int deffinetlySafe(0);
	int numberOfRequiredIterations(0);
	
	//for debug
	Position topLeft = unit.topLeft();
	Position topRight = unit.topRight();
	Position botLeft = unit.botLeft();
	Position botRight = unit.botRight();

	Position from(-1, -1); //dummy
	Position to(-1, -1); //dummy

	if (direction.x() == 1)
	{
		deffinetlySafe = (General::walkTileSize - (unit.botRight().x() % General::walkTileSize)) % General::walkTileSize;
		from = Position(unit.botRight().x(), unit.botRight().y()); //y doesn't matter
	}
	else if (direction.x() == -1)
	{
		deffinetlySafe = unit.topLeft().x() % General::walkTileSize;
		from = Position(unit.topLeft().x(), unit.topLeft().y());
	}
	else if (direction.y() == -1)
	{
		deffinetlySafe = unit.topLeft().y() % General::walkTileSize;
		from = Position(unit.topLeft().x(), unit.topLeft().y()); //x doesn't matter
	}
	else if (direction.y() == 1)
	{
		deffinetlySafe = (General::walkTileSize - (unit.botLeft().y() % General::walkTileSize)) % General::walkTileSize;
		from = Position(unit.botLeft().x(), unit.botLeft().y()); //x doesn't matter
	}

	to = Position(from.x() + direction.x() * amount, from.y() + direction.y() * amount);

	//now, this might have caused "to" to be out of bounds

	if (to.x() < 0)
		to = Position(0, to.y());
	if (to.y() < 0)
		to = Position(to.x(), 0);
	if (to.x() >= chp_->getPointSizeX())
		to = Position(chp_->getPointSizeX() - 1, to.y());
	if (to.y() >= chp_->getPointSizeY())
		to = Position(to.x(), chp_->getPointSizeY() - 1);

	//now, let's compute the number of required iterations we need to do
	//this will work beacause one of tileDiffs will be for sure 0
	int tileDiffX = abs((from.x() / General::walkTileSize) - (to.x() / General::walkTileSize));
	int tileDiffY = abs((from.y() / General::walkTileSize) - (to.y() / General::walkTileSize));
	numberOfRequiredIterations = tileDiffX + tileDiffY;

	for (int i(1); i <= numberOfRequiredIterations; ++i)
	{
		Position newTopLeft, newTopRight, newBotLeft, newBotRight;
		newTopLeft = Position(unit.topLeft().x() + direction.x() * (i * General::walkTileSize), unit.topLeft().y() + direction.y() * (i* General::walkTileSize));
		newTopRight = Position(unit.topRight().x() + direction.x() *(i * General::walkTileSize), unit.topRight().y() + direction.y() * (i * General::walkTileSize));
		newBotLeft = Position(unit.botLeft().x() + direction.x() * (i*General::walkTileSize), unit.botLeft().y() + direction.y() * (i * General::walkTileSize));
		newBotRight = Position(unit.botRight().x() + direction.x() * (i * General::walkTileSize), unit.botRight().y() + direction.y() * (i * General::walkTileSize));

		if (chp_->checkRecntagleBaseTilesForValue(newTopLeft, newTopRight, newBotLeft, newBotRight, playerTileType))
		{
			deffinetlySafe += General::walkTileSize;
		}
		else
		{
			break;
		}
		if (deffinetlySafe >= amount)
			break;
	}

	if (deffinetlySafe < amount)
		actualAmount = deffinetlySafe;
	else
		actualAmount = amount;

	return actualAmount;
}

//this method will return the amount of pixels the provided
//unit can move until it bumps to another friendly unit
int MutationPerformer::determineActualAmountFromUnits(int amount,const Position& direction,
	const Unit& unit)
{
	std::vector<std::pair<int, int>> indices;
	collisionTilesHolder_.removeUnitFromTiles(unit);

	Position movingUnitTopLeft = unit.topLeft();
	Position movingUnitTopRight = unit.topRight();
	Position movingUnitBotLeft = unit.botLeft();
	Position movingUnitBotRight = unit.botRight();

	int actualAmount = amount;

	//loop should really only have one cycle...
	while (true)
	{
		bool collision(false);
		indices.clear();
		Position newTopLeft(unit.topLeft().x() + direction.x() * actualAmount, unit.topLeft().y() + direction.y()*actualAmount);
		Position newBotRight(unit.botRight().x() + direction.x() * actualAmount, unit.botRight().y() + direction.y() * actualAmount);
		collisionTilesHolder_.getCollisionTileIndicesForUnitsRectangle(newTopLeft, unit.width(), unit.height(), indices);

		for (auto& p : indices)
		{
			for (const auto& u : collisionTilesHolder_.getCollisionTile(p.first, p.second).getUnits())
			{

				if (chp_->areUnitsColiding(newTopLeft, unit.width(), unit.height(),
					u.topLeft(), u.width(), u.height()))
				{
					actualAmount = getNumberOfFreePixelsBetweenUnits(movingUnitTopLeft, movingUnitBotRight, u, direction);
					collision = true;
					break;
				}
			}
			if (collision)
				break;			
		}
		
		if (!collision)
		{
			break;
		}
	}
	return actualAmount;
}

void MutationPerformer::tryMoveUnit(int amount, const Position& direction, Unit& unit,
	CommonData::MapValues playerTileType)
{
	//we first handle case when unit could get blocked by stepping on non defender tile
	int actualAmount(determineActualAmountFromTiles(amount, direction, unit, playerTileType));
	//ok, we now know how much we are allowed to move to not step on non defender tile

	//now let's look at colliding with other units
	//i need to pass the amount allowed by first function
	int actualActualAmount(determineActualAmountFromUnits(actualAmount, direction, unit));

	//we now know exactly how much can we move, mutation of unit is done
	unit.setPosition(Position(unit.getPosition().x() + direction.x() * actualActualAmount, unit.getPosition().y() + direction.y() * actualActualAmount));
	collisionTilesHolder_.insertUnitToTiles(unit);

}

void MutationPerformer::initalizeParameters(int currentPlayerCounter)
{
	//initialization method based on the different types of mutation
	switch (GA::mutationType)
	{
	case GA::MutationType::CONSTANT:
	{
		//nothing,
		//log is for testing
		break;
	}
	case GA::MutationType::DECAY:
	{
		if (currentPlayerCounter >= GA::decayToTurns)
		{
			changeDistributionParams(GA::decayToValue);
		}
		else
		{
			int valuesDiff = GA::mutationInitValue - GA::decayToValue;
			int turnsLeft = GA::decayToTurns - currentPlayerCounter;
			double total = GA::decayToValue + (valuesDiff * (((double)turnsLeft / GA::decayToTurns)));
			changeDistributionParams(total);

		}
		break;
	}
	case GA::MutationType::PLAGUEPEEK:
	{
		if (GA::plague)
		{
			if (currentPlayerCounter >= GA::plagueStartIteration + 1)
			{
				int diff = currentPlayerCounter - (GA::plagueStartIteration + 1);

				if (diff % GA::plagueRepetitionCounter == 0)
					changeDistributionParams(GA::plaguePeekAmount);
				else
				{
					int val = diff % GA::plagueRepetitionCounter;
					if (val >= GA::plaguePeekDecayTurns)
						changeDistributionParams(GA::mutationInitValue);
					else
					{
						int curBonus(GA::plaguePeekAmount - GA::mutationInitValue);
						int total = GA::mutationInitValue;
						int a = curBonus / GA::plaguePeekDecayTurns;
						for (int i = 0; i < val; ++i)
						{
							curBonus -= a;
						}
						total += curBonus;
						changeDistributionParams(total);
					}
				}
			}
		}
		break;
	}

	}

}

//method that will mutate the provided chromosome
void MutationPerformer::mutateChromosome(Chromosome& chromosome)
{
	collisionTilesHolder_.resetEveryModifiedCollisionTile();
	collisionTilesHolder_.insertUnitsIntoCollisionTiles(chromosome.getUnitLayout().getUnits());

	for (auto& u : chromosome.getUnitLayout().getUnits())
	{
		//get chance of mutation
		int mutationChance = Random::getRandInt(0, 10001);

		//if we are withing bounds
		if (mutationChance / 10000.0 < GA::mutationRate)
		{
			//perform the mutation, that is, move the unit in
			//x and y directions
			int xAmount = getRandomAmountForMutation();
			if (xAmount < 0)
				tryMoveUnit(xAmount * -1, Position(-1, 0), u, chromosome.getPlayerTileType());
			else
				tryMoveUnit(xAmount, Position(1, 0), u, chromosome.getPlayerTileType());

			int yAmount = getRandomAmountForMutation();

			if (yAmount < 0)
				tryMoveUnit(yAmount * -1, Position(0, -1), u, chromosome.getPlayerTileType());
			else
				tryMoveUnit(yAmount, Position(0, 1), u, chromosome.getPlayerTileType());
		}
	}

	checkMutationWasCorrect(chromosome);
}


//Position will be either {1,0}, {0,1}, {-1,0} or {0,-1}
int MutationPerformer::getNumberOfFreePixelsBetweenUnits(const Position& bumpingUnitNewTopLeft,
	const Position& bumpingUnitNewBotRight, const Unit& standingUnit, const Position& direction)
{
	//right direction
	if (direction.x() == 1)
	{
		//i always need to subract one, because returned value indicates exactly how much can unit move in this direction without colliding

		int val = standingUnit.topLeft().x() - bumpingUnitNewBotRight.x();
		return val >=0 ? val : 0;
	}

	//left
	if (direction.x() == -1)
	{
		int val = bumpingUnitNewTopLeft.x() - standingUnit.topRight().x();
		return val >= 0 ? val : 0;
	}

	//top
	if (direction.y() == 1)
	{
		int val = standingUnit.topLeft().y() - bumpingUnitNewBotRight.y();
		return val >= 0 ? val : 0;
	}

	//bot
	if (direction.y() == -1)
	{
		int val = bumpingUnitNewTopLeft.y() - standingUnit.botRight().y();
		return val >= 0 ? val : 0;
	}

	System::FatalError("invalid direction type");
	return std::numeric_limits<int>::max();
}

void MutationPerformer::checkMutationWasCorrect(const Chromosome& chromosome) const
{
	//for debug, let's be sure and also check that no units are colliding

	for (const auto& u1 : chromosome.getUnitLayout().getUnits())
	{
		for (const auto& u2 : chromosome.getUnitLayout().getUnits())
		{
			if (u1.getID() == u2.getID())
				continue;
			if (chp_->areUnitsColiding(u1.topLeft(), u1.width(), u1.height(),
				u2.topLeft(), u2.width(), u2.height()))
				System::FatalError("mutation was not correct, two units are colliding");
		}
	}

	for (const auto& u : chromosome.getUnitLayout().getUnits())
	{		
		if (!(chp_->isUnitInsideMap(u.topLeft(), u.botRight())))
			System::FatalError("mutation was not correct and one of units is out of map");
		if (!(chp_->checkRecntagleBaseTilesForValue(u.topLeft(), u.topRight(),
			u.botLeft(), u.botRight(), chromosome.getPlayerTileType())))
			System::FatalError("mutation was not correct, one unit is not on defender tiles");
	}
}