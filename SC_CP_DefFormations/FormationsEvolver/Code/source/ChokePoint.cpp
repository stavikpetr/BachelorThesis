#include "ChokePoint.h"

using namespace FormationsEvolution;

ChokePoint::ChokePoint(std::string mapName, int numOfWalkTilesX, int numOfWalkTilesY, int chokePointTileX, int chokePointTileY,
	const Position& topLeftCorner, const Position& chokePointCenter):
	mapName_(mapName), numOfWalkTilesX_(numOfWalkTilesX), numOfWalkTilesY_(numOfWalkTilesY),
	chokePointTileX_(chokePointTileX), chokePointTileY_(chokePointTileY), topLeftCorner_(topLeftCorner), chokePointCenter_(chokePointCenter)
{
	//in constructor, fill all map tiles with unwalkable tiles
	//(in current use, each tile will be set in the parinsg"
	for (int i = 0; i < numOfWalkTilesX; ++i)
	{
		std::vector<BaseTile> baseTilesTmp;
		baseTilesTmp.reserve(numOfWalkTilesY);
		for (int j = 0; j < numOfWalkTilesY; ++j)
		{
			baseTilesTmp.push_back(BaseTile());
		}
		chokepoint_.push_back(std::move(baseTilesTmp));
	}
}

void ChokePoint::setBaseTileValue(int vectorXIndex, int vectorYIndex, CommonData::MapValues mapValue)
{
	if (vectorXIndex < 0 || vectorXIndex >= (int)chokepoint_.size())
		System::FatalError("setBaseTileValue, vectorXIndex is out of bounds");
	if (vectorYIndex < 0 || vectorYIndex >= (int)chokepoint_[0].size())
		System::FatalError("setBaseTileValue, vectorYIndex is out of bounds");
	chokepoint_[vectorXIndex][vectorYIndex].setMapValue(mapValue);
}

CommonData::MapValues ChokePoint::getBaseTileValue(int tileNumberX, int tileNumberY) const
{
	if (isValidWalkTileXIndex(tileNumberX) && isValidWalkTileYIndex(tileNumberY))
		return chokepoint_[tileNumberX][tileNumberY].mapValue();
	else
	{
		System::FatalError("trying to access invalid base tile");
		return CommonData::UNWALKABLE;
	}
}

CommonData::MapValues ChokePoint::getBaseTileValue(const Position& p) const
{
	return getBaseTileValue(p.x() / General::walkTileSize, p.y() / General::walkTileSize);
}

bool ChokePoint::isValidWalkTileXIndex(int walkTileXIndex) const
{
	if (walkTileXIndex >= 0 && walkTileXIndex < numOfWalkTilesX_)
		return true;
	return false;
}

bool ChokePoint::isValidWalkTileYIndex(int walkTileYIndex) const
{
	if (walkTileYIndex >= 0 && walkTileYIndex < numOfWalkTilesY_)
		return true;
	return false;
}

bool ChokePoint::isValidWalkTileIndex(int walkTileXIndex, int walkTileYIndex) const
{
	return isValidWalkTileXIndex(walkTileXIndex) && isValidWalkTileYIndex(walkTileYIndex);
}


bool ChokePoint::isUnitInsideMap(const Position& topLeft, const Position& botRight) const
{
	if (topLeft.x() < 0 || topLeft.y() < 0 ||
		botRight.x() > numOfWalkTilesX_*General::walkTileSize ||
		botRight.y() > numOfWalkTilesY_*General::walkTileSize)
		return false;
	return true;
}


bool ChokePoint::areUnitsColiding(const Position& topLeft1, int width1, int height1,
	const Position& topLeft2, int width2, int height2) const
{
	if (topLeft1.x() + width1 > topLeft2.x() &&
		topLeft1.x() < topLeft2.x() + width2 &&
		topLeft1.y() + height1 > topLeft2.y() &&
		topLeft1.y() < topLeft2.y() + height2)
		return true;
	return false;
}



bool ChokePoint::checkRecntagleBaseTilesForValue(const Position& topLeft, const Position& topRight,
	const Position& botLeft, const Position& botRight,
	CommonData::MapValues mapValue) const
{
	int width = topRight.x() - topLeft.x();
	int height = botLeft.y() - topLeft.y();

	int resultingX = topLeft.x() + width-1;
	int resultingY = topLeft.y() + height-1;

	if (topLeft.x() < 0 || topLeft.y() < 0)
		return false;

	int curX = topLeft.x();
	int curY = topLeft.y();
	int curXIndex, curYIndex;
	bool valid = true;

	//this big loop will iterate over each base tile in the rectangle
	//that is specified by the four provided positions
	//and checks whether it contains the map value provided as argument
	//the direciton of iteration is:
	// ---->
	// ---->

	while (true)
	{
		curX = topLeft.x();
		curXIndex = curX / General::walkTileSize;
		curYIndex = curY / General::walkTileSize;

		if (!isValidWalkTileIndex(curXIndex, curYIndex))
		{
			return false;
		}

		if (getBaseTileValue(curXIndex, curYIndex) != mapValue)
		{
			valid = false;
			break;
		}

		//go horizontally, until we reach the end of rectangle
		while (true)
		{
			curX += curX + General::walkTileSize <= resultingX ? General::walkTileSize : resultingX - curX;
			curXIndex = curX / General::walkTileSize;
			if (!isValidWalkTileXIndex(curXIndex))
			{
				valid = false;
				break;
			}
			if (getBaseTileValue(curXIndex, curYIndex) != mapValue)
			{
				valid = false;
				break;
			}
			if (curX == resultingX)
				break;
		}
		if (!valid)
			break;
		if (curY == resultingY)
			break;
		curY += curY + General::walkTileSize <= resultingY ? General::walkTileSize : resultingY - curY;
	}

	if (valid)
		return true;
	return false;
}
