#pragma once

#include "Common.h"
#include "CrossoverPerformer.h"

namespace FormationsEvolution
{
	/**
	@ingroup GeneticAlgorithm
	@brief This class implemenets one type of crossover - one point crossover
	*/
	class OnePointCrossover: public CrossoverPerformer
	{
		CollisionTilesHolder collisionTiles1_;
		CollisionTilesHolder collisionTiles2_;
	public:
		/// <summary> constructor for another variant of
		///		crossover - one point crossover </summary>
		/// <param name="chp"> each crossover performer needs
		///		reference to chokepoint for correct positioning of units </param>
		OnePointCrossover(CHP_Observer chp);

		/// <summary> method that facilitates the crossover operation </summary>
		///	<param name="first">first chromosome for crossover </param>
		///	<param name="second">second chromosome for crossover </param>
		void crossover(Chromosome& first, Chromosome& second);
	};
}
