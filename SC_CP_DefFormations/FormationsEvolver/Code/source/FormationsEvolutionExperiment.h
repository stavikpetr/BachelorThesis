#pragma once

#include "Common.h"
#include "SearchExperiment.h"
#include "SingleExperiment.h"
#include "Parser.h"
#include "OutputHandler.h"
#include "boost/filesystem.hpp"


namespace FormationsEvolution
{
	/**
	*	This class represents the run of the whole experiment. Its job 
		is to read a single configuration of the experiment, create new instance of class SingleExperiment 
		and forward the flow to this instance.
	*	@ingroup Experiment_core
	*   @brief This class governs the run of the whole program.
	*/
	class FormationsEvolutionExperiment
	{
		std::string inputDir_;
		Parser parser_;
		boost::filesystem::directory_iterator directory_end_;

		void readPaths(const std::string& dir, std::vector<std::string>* toFill);

	public:
		/// <summary> method that will initiate the execution of the whole experiment </summary>
		void runExperiments();
		/// <summary> Constructor for creating new experiment </summary>
		/// <param name="inputDirectory"> the input directory - it must have a special structure,
		///		which may be found in the user documentation</param>
		/// <param name="outputDirectory"> the output directory </param>
		//copy is on purpose
		FormationsEvolutionExperiment(std::string inputDirectory, std::string outputDirectory);
	};
}
