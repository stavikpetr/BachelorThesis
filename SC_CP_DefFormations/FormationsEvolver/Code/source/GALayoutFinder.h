#pragma once

#include "Common.h"
#include "LayoutFinder.h"
//---- GA SPECIFICS ----

#include "Chromosome.hpp"
#include "Generation.h"

//---- SCORE ----
#include "ScoreFunc.h"
#include "InfrastructureHuntFunc.h"
#include "ResourceCostFunc.h"

//---- LAYOUT EVALUATORS ----
#include "LayoutEvaluator.h"
#include "LayoutEvaluatorBasic.h"
#include "LayoutEvaluatorUnitAlterations.h"

//---- ELITISM PERFORMERS ----
#include "ElitismPerformer.h"

//---- MUTATORS ----
#include "MutationPerformer.h"

//---- CROSSOVERS ----
#include "CrossoverPerformer.h"
#include "OnePointCrossover.h"
#include "TwoPointCrossover.h"

//---- SELECTORS ----
#include "ChromosomeSelector.h"
#include "TournamentSelector.h"

//---- PLAGUE PERFORMERS
#include "PlaguePerformer.h"

#include "GenerationGenerator.h"

//---- GAs ----
#include "GA_Base.h"
#include "RoundRobin_GA.h"
#include "CoEvolution.h"

namespace FormationsEvolution
{
	/**
	This LayoutFinder represents the primary LayoutFinder for which this whole framework was built.
	It is able to use different types of genetic algorithms to find the best layout for defender's units.
	
	@ingroup Experiment_core
	@brief This class represents LayoutFinder which uses GA for finding of the layout
	*/
	class GALayoutFinder : public LayoutFinder
	{		
		/**
		 This whole set of methods is used to set up the instances of
		 classes which are used throughout the run of the genetic algorithm

		 The methods are straight forward - they just look at the current
		 setup of the experiment that is found in global state and based on the 
		 state, they simply construct a correct instance
		*/
		std::unique_ptr<ElitismPerformer> getElitismPerformer();
		std::unique_ptr<PlaguePerformer> getPlaguePerformer();
		std::unique_ptr<ChromosomeSelector> getChromosomeSelector();
		std::unique_ptr<MutationPerformer> getMutationPerformer();
		std::unique_ptr<CrossoverPerformer> getCrossoverPerformer();
		//the copies in the following methods are on purpose
		GenerationGenerator getGenerationGenerator();
		std::vector<std::unique_ptr<LayoutEvaluator>> getLayoutEvaluators(SparCraft::SearchExperiment& searchExperiment);
		std::unique_ptr<GA_Base> getGA(SparCraft::SearchExperiment& searchExperiment);
		UnitLayout getPlacedForce(const Force& toPlace, CommonData::MapValues where_to);
		Generation getInitialGeneration(int playerID, const Force& initialForce, size_t size);
	public:
		/// <summary> Constructor of GALayoutFinder</summary>
		/// <param name="chp"> ChokePoint instance</param>
		/// <param name="defenderForce"> Force of the defender </param>
		///	<param name="attackerForce"> Force of the attacker </param>
		GALayoutFinder(CHP_Observer chp,
			const Force& defenderForce,
			const Force& attackerForce);

		/// <summary> Method that will initiate process of finding the layout via a genetic algorithm.
		///		This method first constructs an instance of class derived from GA_Base and then performs
		///		each iteration of the GA by transfering the control to the aforementioned object </summary> 
		/// <param name="searchExperiment"> instance of spracraft simulation</param>
		/// <returns> returns the best found layout for defender's units
		///		(the copy is on purpose)</returns>
		UnitLayout findBestLayout(SparCraft::SearchExperiment& searchExperiment);
	};
}