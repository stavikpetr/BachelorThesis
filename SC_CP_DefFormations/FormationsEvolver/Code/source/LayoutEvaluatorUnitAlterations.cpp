#include "LayoutEvaluatorUnitAlterations.h"

using namespace FormationsEvolution;

LayoutEvaluatorUnitAlterations::LayoutEvaluatorUnitAlterations(
	SparCraft::SearchExperiment* experiment,
	std::unique_ptr<ScoreFunc> scoreFunc,
	UnitAlteration& unitAlteration) :
	LayoutEvaluator(
		experiment,
		std::move(scoreFunc)),
	unitAlteration_(unitAlteration)
{};

int LayoutEvaluatorUnitAlterations::evaluateTwoLayouts(
	const UnitLayout& first,
	int firstLayoutPlayerID,
	const UnitLayout& second,
	int secondLayoutPlayerID) const
{
	SparCraft::SparCraftExperimentResult experimentResult;

	std::vector<std::pair<BWAPI::UnitType,
		SparCraft::Position>> forcePlayer1ForSparCraft;
	std::vector<std::pair<BWAPI::UnitType,
		SparCraft::Position>> forcePlayer2ForSparCraft;

	//construct the forces for sparcraft (with modifications based on unit alterations)
	for (auto& u : first.getUnits())
	{
		forcePlayer1ForSparCraft.push_back(std::pair<BWAPI::UnitType, SparCraft::Position>(
			u.getType(), SparCraft::Position(u.getPosition().x(), u.getPosition().y())));
	}

	for (auto& u : second.getUnits())
	{
		forcePlayer2ForSparCraft.push_back(std::pair<BWAPI::UnitType, SparCraft::Position>(
			u.getType(), SparCraft::Position(u.getPosition().x(), u.getPosition().y())));
	}

	if (unitAlteration_.getPlayerID() == firstLayoutPlayerID)
	{
		for (const auto& unit : unitAlteration_.getUnitLayout().getUnits())
		{
			forcePlayer1ForSparCraft.push_back(std::pair<BWAPI::UnitType, SparCraft::Position>(
				unit.getType(), SparCraft::Position(unit.getXPosition(), unit.getYPosition())));
		}
	}
	else if (unitAlteration_.getPlayerID() == secondLayoutPlayerID)
	{
		for (const auto& unit : unitAlteration_.getUnitLayout().getUnits())
		{
			forcePlayer2ForSparCraft.push_back(std::pair<BWAPI::UnitType, SparCraft::Position>(
				unit.getType(), SparCraft::Position(unit.getXPosition(), unit.getYPosition())));
		}
	}

	//set up new sparcraft experiment
	experiment_->setNewUnits(forcePlayer1ForSparCraft, firstLayoutPlayerID,
		forcePlayer2ForSparCraft, secondLayoutPlayerID);

	//run it
	experiment_->runExperiment();

	//gather results
	experimentResult = experiment_->extractExperimentResult();

	//score them
	int achievedFitness = scoreFunc_->getScore(experimentResult);
	OutputHandler::getInstance().logScenarioEvalDone(achievedFitness);

	//and return the score
	return achievedFitness;

};