#pragma once

#include "GA_Base.h"
#include <functional>

namespace FormationsEvolution
{
	/**
	This class represents family of genetic algorithms that are based on the idea CoEvolution.
	Their idea is the following - instead of having fixed layouts for the attacker, let's instead
	optimize the defender against an evolving attacker layouts. This way, we would not achieve
	layout that was optimized against a set of fixed layouts, but a layout that was optimized
	against a whole range of attacker's layouts.
	The algorithm is further parametrizable by one functor, which determines the condition for swapping
	the optimization between defender and attacker. A typical example would be to have fixed
	number of turns for attacker and for defender.

	@ingroup GeneticAlgorithm
	@brief Class that represents the family of CoEvolution genetic algorithms
	*/
	class CoEvolution: public GA_Base
	{
		Generation attackerGeneration_;
		//these are just observers
		Generation* curGen_;
		Generation* oppositeGen_;
		UnitLayout oppBestLayout_;

		std::function<bool(Generation&g)> swapConditionFunc_;

	public:
		/// <summary> Constructor of the genetic algorithm based on CoEvolution </summary>
		/// <param name="defenderGeneration"> the defender's generation that will be evolved </summary>
		///	<param name="generationGenerator"> class that generates new generation of chromosomes from the current ones </summary>
		///	<param name="layoutEvaluatiors"> vector with layout evaluators </summary>
		///	<param name="attackerLayouts"> vector with different attacker layouts </summary>
		/// <param name="fitnessAssignmentFunc"> parameter that alters the behavior of roundRobin type of GA </param>
		CoEvolution(Generation& defenderGeneration,
			GenerationGenerator&& generationGenerator,
			std::vector<std::unique_ptr<LayoutEvaluator>>&& layoutEvaluators,
			Generation& attackerGeneration,
			std::function<bool(Generation&g)> swapConditionFunc);

		/// <summary> Method that will perform a single iteration of the genetic algorithm. 
		///		The number of iterations is handled by the GALayoutFinder</summary>
		void evolveNextPopulation();

		/// <summary> Method that will return the best layout found by the algorithm.
		///		Layout with the best score resides after the iteration of GA at 
		///		the zeroth index</summary>
		///	<returns> best layout found for the defender </summary>
		UnitLayout& returnFinalBestLayout()
		{
			return defenderGeneration_[0].getUnitLayout();
		};
	};
}