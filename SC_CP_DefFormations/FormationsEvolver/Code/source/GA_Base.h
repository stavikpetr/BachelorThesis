#pragma once

#include "Common.h"
#include "Generation.h"
#include "LayoutEvaluator.h"
#include "GenerationGenerator.h"


namespace FormationsEvolution
{
	/**
	This class represents a basic skeleton for each type of the genetic algorithm. 
	Each GA will have the defender Generation which it should evolve. It will also have a class
	that facilitates generation of a new generation - GenerationGenerator. And also several instances
	of LayoutEvaluator that can subdue the layout to different evaluation functions.

	@ingroup GeneticAlgorithm
	@brief A base class that represents skeleton for the different types of genetic algorithms
	*/
	class GA_Base
	{
	protected:
		Generation defenderGeneration_;
		std::vector<std::unique_ptr<LayoutEvaluator>> layoutEvaluators_;
		GenerationGenerator generationGenerator_;

		GA_Base(Generation& defenderGeneration,
			GenerationGenerator&& generationGenerator,
			std::vector<std::unique_ptr<LayoutEvaluator>>&& layoutEvaluators) :
			defenderGeneration_(defenderGeneration),
			generationGenerator_(std::move(generationGenerator)),
			layoutEvaluators_(std::move(layoutEvaluators))
		{};

	public:
		/// <summary> Method that will perform a single iteration of the genetic algorithm. 
		///		The number of iterations is handled by the GALayoutFinder</summary>
		virtual void evolveNextPopulation() = 0;
		/// <summary> Method that will return the best layout found by the algorithm </summary>
		///	<returns> best layout found for the defender </summary>
		virtual UnitLayout& returnFinalBestLayout() = 0;
		virtual ~GA_Base(){};
	};
}