#include "GALayoutFinder.h"

using namespace FormationsEvolution;

GALayoutFinder::GALayoutFinder(CHP_Observer chp,
	const Force& defenderForce,
	const Force& attackerForce) :
	LayoutFinder(chp, defenderForce, attackerForce)
{};

std::unique_ptr<ElitismPerformer> GALayoutFinder::getElitismPerformer()
{
	//currently, there is only a single elitism performer
	return std::make_unique<ElitismPerformer>();
}

std::unique_ptr<PlaguePerformer> GALayoutFinder::getPlaguePerformer()
{
	//currently, there is only a single plague performer
	return std::make_unique<PlaguePerformer>(chp_);
}

std::unique_ptr<ChromosomeSelector> GALayoutFinder::getChromosomeSelector()
{
	//currently, there is only a single tournament selector
	return std::make_unique<TournamentSelector>();
}

std::unique_ptr<MutationPerformer> GALayoutFinder::getMutationPerformer()
{
	//currently, there is only a single mutation performer implemented
	return std::make_unique<MutationPerformer>(chp_);
}

std::unique_ptr<CrossoverPerformer> GALayoutFinder::getCrossoverPerformer()
{
	//create correct crossover performer based on the global settings
	if (GA::crossoverType == GA::CrossoverType::ONEPOINT)
	{
		return std::make_unique<OnePointCrossover>(chp_);
	}
	else if (GA::crossoverType == GA::CrossoverType::TWOPOINT)
	{
		return std::make_unique<TwoPointCrossover>(chp_);
	}
	else
	{
		System::FatalError("invalid crossover type");
		return nullptr;
	}
}

GenerationGenerator GALayoutFinder::getGenerationGenerator()
{
	return std::move(GenerationGenerator(
		getChromosomeSelector(),
		getMutationPerformer(),
		getCrossoverPerformer(),
		getPlaguePerformer(),
		getElitismPerformer()
		));
}

UnitLayout GALayoutFinder::getPlacedForce(const Force& toPlace, CommonData::MapValues where_to)
{
	UnitLayout layout;
	CollisionTilesHolder c(chp_->getWalkTileSizeX(), chp_->getWalkTileSizeY());
	LayoutManager::getInstance().getValidRandomLayout(chp_,
		toPlace, c, where_to, layout);
	return layout;
}


std::vector<std::unique_ptr<LayoutEvaluator>> GALayoutFinder::getLayoutEvaluators(SparCraft::SearchExperiment& searchExperiment)
{
	Force probeInfrastructure(std::vector<std::pair<BWAPI::UnitType, int>>{(
			std::pair<BWAPI::UnitType, int>(BWAPI::UnitTypes::Protoss_Probe, 5)
		)});
	
	UnitAlteration unitAlteration(getPlacedForce(
		probeInfrastructure,CommonData::DEFENDERBACK), General::DEFENDER_ID);

	OutputHandler::getInstance().outputProbeInfrastructure(unitAlteration.getUnitLayout());


	std::vector<std::unique_ptr<LayoutEvaluator>> layoutEvaluators;

	layoutEvaluators.push_back(std::make_unique<LayoutEvaluatorBasic>(
		&searchExperiment,
		std::make_unique<ResourceCostFunc>()));

	layoutEvaluators.push_back(std::make_unique<LayoutEvaluatorUnitAlterations>(
		&searchExperiment,
		std::make_unique<InfrastructureHuntFunc>(),
		unitAlteration));

	return std::move(layoutEvaluators);
}

Generation GALayoutFinder::getInitialGeneration(int playerID, const Force& initialForce, size_t size)
{
	//create initial generation for the genetic algorithm
	std::vector<Chromosome> chromosomes;
	for (size_t i(0); i < size; ++i)
	{
		UnitLayout l;
		if (playerID == General::DEFENDER_ID)
			l = getPlacedForce(initialForce, CommonData::MapValues::DEFENDER);
		else if (playerID == General::ATTACKER_ID)
			l = getPlacedForce(initialForce, CommonData::MapValues::ATTACKER);
		l.shuffleUnits();
		chromosomes.push_back(Chromosome(l, playerID));
	}
	return Generation(chromosomes, playerID, initialForce);
}

std::unique_ptr<GA_Base> GALayoutFinder::getGA(SparCraft::SearchExperiment& searchExperiment)
{
	Generation defenderGeneration = getInitialGeneration(General::DEFENDER_ID, defenderForce_,
		GA::defPopulationSize);

	//create GA from the family of coevolution genetic algorithms
	if (GA::evolutionType == GA::EvolutionType::COEVOLUTION)
	{
		Generation attackerGeneration = getInitialGeneration(General::ATTACKER_ID, attackerForce_,
			GA::alt_attPopulationSize);

		auto swapConditionFunc = [](Generation& g)->bool{
			if (g.getPlayerID() == General::DEFENDER_ID)
			{
				if (g.getIterationsCounter() % GA::alt_optSwap_turns_d == 0
					&& g.getIterationsCounter() != 0)
					return true;
			}
			else if (g.getPlayerID() == General::ATTACKER_ID)
			{
				if (g.getIterationsCounter() % GA::alt_optSwap_turns_a == 0
					&& g.getIterationsCounter() != 0)
					return true;
			}
			return false;
		};
		
		return std::make_unique<CoEvolution>(
			defenderGeneration,
			std::move(getGenerationGenerator()),
			std::move(getLayoutEvaluators(searchExperiment)),
			attackerGeneration,
			swapConditionFunc);
		
	}
	//create GA from the family of roundrobin genetic algorithms
	else if (GA::evolutionType == GA::EvolutionType::ROUNDROBIN)
	{
		std::vector<UnitLayout> attackerUnitLayout;
		for (int i(0); i < GA::rr_numOfEnemyForces; ++i)
		{
			attackerUnitLayout.push_back(getPlacedForce(attackerForce_, CommonData::MapValues::ATTACKER));
		}
		auto fitnessAssignmentFunc = [](Chromosome& c, std::vector<int>& fitnesses)->void{
			//auto it = std::min_element(fitnesses.begin(), fitnesses.end());
			//c.setFitness(*it);
			int total(0);
			for(size_t i(0); i < fitnesses.size(); ++i)
			{
				total+=fitnesses[i];
			}
			total /= (int)fitnesses.size();
			c.setFitness(total);
		};

		for (size_t i(0); i < attackerUnitLayout.size(); ++i)
		{
			OutputHandler::getInstance().outputRRAttackerUnitLayout(attackerUnitLayout[i], (int)i);
		}

		return std::make_unique<RoundRobin_GA>(
			defenderGeneration,
			std::move(getGenerationGenerator()),
			std::move(getLayoutEvaluators(searchExperiment)),
			attackerUnitLayout,
			fitnessAssignmentFunc);
	}
	else
	{
		System::FatalError("invalid ga type");
		return nullptr;
	}
}

UnitLayout GALayoutFinder::findBestLayout(SparCraft::SearchExperiment& searchExperiment)
{
	//log the new experiment
	OutputHandler::getInstance().logNewExperiment();

	//based on the settings in the input file, create a new instance of class derived from GA_base 
	std::unique_ptr<GA_Base> ga_base = getGA(searchExperiment);

	//perform each iteration of GA
	for (int i(1); i <= GA::maximumNumberOfIterations; ++i)
	{
		OutputHandler::getInstance().logNewGAIteration(i);
		ga_base->evolveNextPopulation();
	}

	//and return the best layout found
	return ga_base->returnFinalBestLayout();
}
