#include "PlaguePerformer.h"

using namespace FormationsEvolution;

PlaguePerformer::PlaguePerformer(CHP_Observer chp) :
	chp_(chp)
{};

void PlaguePerformer::addNewChromosomes(const Generation& oldGeneration,
	std::vector<Chromosome>& toFill) const
{
	//this method will add new chromosomes to the provided vector
	//based on the paramaterers of plague
	size_t amountOfNewChromosomes = (size_t)(oldGeneration.size() * GA::plaguePercentage);
	CollisionTilesHolder c(chp_->getWalkTileSizeX(), chp_->getWalkTileSizeY());

	for (size_t i(0); i < amountOfNewChromosomes; ++i)
	{
		c.resetEveryModifiedCollisionTile();
		UnitLayout unitLayout;
		LayoutManager::getInstance().getValidRandomLayout(chp_,
			oldGeneration.getInitialForce(), c, oldGeneration[0].getPlayerTileType(), unitLayout);
		unitLayout.shuffleUnits();
		toFill.push_back(Chromosome(unitLayout, oldGeneration.getPlayerID()));
	}
};

void PlaguePerformer::performPlague(
	const Generation& oldGeneration,
	std::vector<Chromosome>& toFill) const
{
	//one possible implementation of plague: simply add several completely
	//new random chromosomes each several iterations
	if (GA::plague)
	{
		if (oldGeneration.getIterationsCounter() >= GA::plagueStartIteration)
		{
			int diff = oldGeneration.getIterationsCounter() - GA::plagueStartIteration;

			if (diff % GA::plagueRepetitionCounter == 0)
			{
				OutputHandler::getInstance().logPlague();
				addNewChromosomes(oldGeneration, toFill);
			}
		}
	}
};