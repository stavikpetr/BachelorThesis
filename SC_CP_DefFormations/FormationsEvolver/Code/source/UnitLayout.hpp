#pragma once

#include "Common.h"
#include "Unit.hpp"

#include <algorithm>
#include <random>

namespace FormationsEvolution
{
	/**
	This class represents a single unit layout - it is just a thin layer
	over a vector of units (which hold the positions);

	@ingroup Util
	@brief Class representing collection of units 
	*/
	class UnitLayout
	{
		std::vector<Unit> unitLayout_;
	public:
		//move ctor
		UnitLayout(std::vector<Unit>&& unitLayout) : unitLayout_(std::move(unitLayout)) {};
		
		//default ctor makes the unit vector empty
		UnitLayout(){};

		/// <summary> adds new unit to the unit layout </summary>
		void addUnit(const Unit & u)
		{
			unitLayout_.push_back(u);
		}

		/// <summary> shuffles the indices of units in the vector
		///		(it is sometimes useful in the run of Genetic algorithm) </summary>
		void shuffleUnits()
		{
			std::shuffle(std::begin(unitLayout_), std::end(unitLayout_), Random::getDefaultRandomEngine());
		};

		/// <summary> gets the units of this unit layout, const version </summary>
		const std::vector<Unit>& getUnits() const
		{
			return unitLayout_;
		};

		/// <summary> gets the units of this unit layout </summary>
		/// this is correctly not const, the units may be changed, mainly, their positions
		std::vector<Unit>& getUnits()
		{
			return unitLayout_;
		}

		/// <summary> intuitive redefinition of the [] operator </summary>
		Unit& operator[](size_t index)
		{
			return unitLayout_[index];
		};

		/// <summary> intuitive redefinition of the [] operator - const version </summary>
		const Unit& operator[](size_t index) const
		{
			return unitLayout_[index];
		};


	};
}