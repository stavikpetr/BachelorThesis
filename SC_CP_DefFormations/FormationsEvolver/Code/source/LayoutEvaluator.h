#pragma once

#include "Common.h"
#include "ScoreFunc.h"
#include "Chromosome.hpp"
#include "SearchExperiment.h"
#include "OutputHandler.h"

namespace FormationsEvolution
{
	/**
	The role of the layout evaluators in my genetic algorithms is to perform 
	the actual battle between the attacker and defender. This is done by constructing
	new sparcraft simulation, simulating it and gathering results.

	One may wonder, why is there an abstract class for this. The reason is that one
	may use different scenarios. Currently, there are two implementations. The LayoutEvaluatorBasic
	simply performs the simulation. However, one may also want to see the performance of the defenders
	layout in scenario when it guards some building infrastructure - in that case, the layout evaluator
	must add some additional units to the sparcraft simulation. This is implemented in 
	LayoutEvaluatorUnitAlterations.

	Each layout evaluator must have some ScoreFunc assigned to it. This function will score
	the results of the performed simulation. However, this score is not necessarily the fitness of the 
	chromosome - this is left to the genetic algorithm itself.

	@ingroup GeneticAlgorithm
	@brief Base abstract class for different implementation of layout evaluators
	*/
	class LayoutEvaluator
	{
	protected:
		std::unique_ptr<ScoreFunc> scoreFunc_;
		SparCraft::SearchExperiment* experiment_;

		LayoutEvaluator(
			SparCraft::SearchExperiment* experiment,
			std::unique_ptr<ScoreFunc> scoreFunc) :
			experiment_(experiment),
			scoreFunc_(std::move(scoreFunc))
		{
		};

	public:
		/// <summary> method for evaluating two unit layouts with
		///		that belong to respective players </summary>
		///	<param name="first"> first unit layout </param>
		///	<param name="firstLayoutPlayerID"> id of the player to which the first layout belongs </param>
		///	<param name="second"> second unit layout </param>
		///	<param name="secondLayoutPlayerID"> id of the player to which the second layout belongs </param>
		///	<returns> the score assigned to the result of the simulation by the score func that
		///		belongs to this layout evaluator </returns>
		virtual int evaluateTwoLayouts(
			const UnitLayout& first,
			int firstLayoutPlayerID,
			const UnitLayout& second,
			int secondLayoutPlayerID) const = 0;
		virtual ~LayoutEvaluator(){};

	};
}