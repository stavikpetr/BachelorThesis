#include "RoundRobin_GA.h"

using namespace FormationsEvolution;

RoundRobin_GA::RoundRobin_GA(Generation& defenderGeneration,
	GenerationGenerator&& generationGenerator,
	std::vector<std::unique_ptr<LayoutEvaluator>>&& layoutEvaluators,
	const std::vector<UnitLayout>& attackerLayouts,
	std::function<void(Chromosome& c, std::vector<int>& fitnesses)> fitnessAssignmentFunc) :
	GA_Base(defenderGeneration,
		std::move(generationGenerator),
		std::move(layoutEvaluators)),
	attackerLayouts_(attackerLayouts),
	fitnessAssignmentFunc_(fitnessAssignmentFunc)
{};

void RoundRobin_GA::evolveNextPopulation()
{
	OutputHandler::getInstance().logNewGenerationEval(General::DEFENDER_ID);
	defenderGeneration_.incrementCounter();

	//simply go through each defender chromosome and with that
	//evaluate it against each attacker chromosome and then
	//assign fitness to the chromosome based on the lambda function

	for (size_t def(0); def < defenderGeneration_.size(); ++def)
	{
		std::vector<int> fitnesses;

		for (size_t att(0); att < attackerLayouts_.size(); ++att)
		{
			int fitness(0);
			for (auto& le : layoutEvaluators_)
			{
				fitness += le->evaluateTwoLayouts(defenderGeneration_[def].getUnitLayout(),
					General::DEFENDER_ID, attackerLayouts_[att], General::ATTACKER_ID);
			}
			fitness /= (int)layoutEvaluators_.size();
			fitnesses.push_back(fitness);
		}

		//as there might be many possible variants for assigning fitness
		//to the chromosome (for example, i can take minimum, i can sum the lowest 4,
		//and so on...), the assignment is given as lambda function which is stored
		//in std::function object 

		fitnessAssignmentFunc_(defenderGeneration_[def], fitnesses);

		OutputHandler::getInstance().logLayoutEvalDone(
			defenderGeneration_[def].getFitness());
	}

	OutputHandler::getInstance().logGenerationAfterIteration(defenderGeneration_);

	generationGenerator_.createNewGeneration(defenderGeneration_);

}