#pragma once

#include "Common.h"
#include "Chromosome.hpp"
#include "Generation.h"

namespace FormationsEvolution
{
	/**
	@ingroup GeneticAlgorithm
	@brief Base class for any class that offers the operation selection in genetic algorithm
	*/
	class ChromosomeSelector
	{
	public:
		/// <summary> method that performs the selection operation - it selects two
		///		chromosomes based on the e.g. tournament selection and returns the
		///		indices of the selected chromosomes in the generation </summary>
		///	<param name="generation"> generation from which the selection is performed </param>
		///	<returns> a pair of indices of the two selected chromosomes </returns>
		virtual std::pair<int, int> selectTwo(const Generation& generation) const = 0;
		virtual ~ChromosomeSelector(){};
	};
}