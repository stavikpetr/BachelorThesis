#pragma once

#include "Common.h"
#include "UnitLayout.hpp"
#include "CollisionTilesHolder.h"

namespace FormationsEvolution
{
	/**
		This class represents one chromosome of the population of the gentic algorithm.
		It's state is made of current fitness value, unit layout, and to which player this chromosome belongs.

		@ingroup GeneticAlgorithm
		@brief A single individual of the generation of the genetic algorithm
	*/
	class Chromosome
	{
		UnitLayout unitLayout_;
		CommonData::MapValues playerTileType_;
		int fitnessValue_;
		int playerID_;
	public:

		/// <summary> Constructor for new Chromosome </summary>
		/// <param name="unitLayout"> the layout (=units + their positions) which this chromosome holds</param>
		/// <param name="playerID"> to which player this chromosome belongs (this is important 
		///		for some advanced types of genetic algorithms like coevolution</param>
		Chromosome(
			const UnitLayout& unitLayout, 
			int playerID):
			unitLayout_(unitLayout),
			fitnessValue_(0),
			playerID_(playerID)

		{
			if (playerID == General::ATTACKER_ID)
				playerTileType_ = CommonData::MapValues::ATTACKER;
			else if (playerID == General::DEFENDER_ID)
				playerTileType_ = CommonData::MapValues::DEFENDER;
		};

		Chromosome(){};
		
		/// <summary> sets fitness of this chromosome </summary>
		void setFitness(int fitnessValue)
		{
			fitnessValue_ = fitnessValue;
		}

		/// <summary> each chromosome's layout can operate
		///		on a different set of map tiles, which determines
		///		where the units can be placed on map (e.g. during
		///		mutation operation); this method returns tileType
		///		of this chromsome </summary>
		CommonData::MapValues getPlayerTileType() const
		{
			return playerTileType_;
		}

		/// <summary> returns current fitness of this
		///		chromosome </summary>
		int getFitness() const
		{
			return fitnessValue_;
		}

		/// <summary> returns id of player to which this chromosome
		///		belongs </summary>
		int getPlayerID() const 
		{
			return playerID_;
		};

		/// <summary> returns layout of this chromosome </summary>
		/// this is correctly not const, the unit layout of single
		/// chromosomes will change throughout its life
		UnitLayout& getUnitLayout()
		{
			return unitLayout_;
		};

		/// <summary> returns layout of this chromosome - const version </summary>
		const UnitLayout& getUnitLayout() const
		{
			return unitLayout_;
		};

	};
}