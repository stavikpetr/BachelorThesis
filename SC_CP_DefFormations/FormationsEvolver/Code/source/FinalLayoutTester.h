#pragma once

#include "Common.h"
#include "SearchExperiment.h"
#include "UnitLayout.hpp"
#include "ResourceCostFunc.h"
#include "InfrastructureHuntFunc.h"
#include "Force.hpp"
#include "LayoutManager.h"
#include "LayoutEvaluatorBasic.h"
#include "LayoutEvaluatorUnitAlterations.h"
#include <memory>

namespace FormationsEvolution
{
	/**
		The purpose of this class is to evaluate a different types of methods
		for searching the best defender's layout, such as the one i did in my
		bachelor's thesis

	    @brief This class tests the found layout in a series of unseen tests.
	*/
	class FinalLayoutTester
	{
		std::vector<std::unique_ptr<LayoutEvaluator>> layoutEvaluators_;
		std::vector<UnitLayout> attackerUnitLayouts_;
		std::vector<UnitAlteration> unitAlterations_;

		UnitLayout getPlacedForce(CHP_Observer chp, const Force& toPlace,
			CommonData::MapValues where_to, std::default_random_engine& engine);
	public:
		/// <summary> Initialization of the final test experiment </summary>
		/// <param name="chp"> pointer to chokepoint instance </param>
		/// <param name="attackerForce"> the units against which will be the best defender's
		///		layout tested </param>
		///	<param name="searchExperiment"> the sparcraft simulation that is used to perform the
		///		comparing experiment </param>
		void initialize(CHP_Observer chp, const Force& attackerForce,
			SparCraft::SearchExperiment& searchExperiment);

		/// <summary> method that will perform the comparing experiment and that will write the 
		///		results into the layoutCmp folder </summary>
		///	<param name="defenderBestLayout"> the found defender layout whose quality will be tested </param>
		void performComparingExperiment(const UnitLayout& defenderBestLayout);

	};
}