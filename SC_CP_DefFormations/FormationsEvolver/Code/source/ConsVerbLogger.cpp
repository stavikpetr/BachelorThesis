#include "ConsVerbLogger.h"

using namespace FormationsEvolution;

ConsVerbLogger::ConsVerbLogger() :cout_(&std::cout)
{};

void ConsVerbLogger::openNewFiles()
{
	closeIfOpen(verb_);
	verb_ = new std::ofstream(log_verbose_baseString_ + "_" +
		std::to_string(verb_log_count_) + ".txt");
	verb_log_count_++;
};

void ConsVerbLogger::closeIfOpen(std::ofstream* output)
{
	if (output)
	{
		if (output->is_open())
			output->close();
	}
};

void ConsVerbLogger::printStrings(std::vector<std::string> linesToPrint, bool consLog)
{
	//log to console and file
	//it was convenient for me to limit the amount of logging
	//on unix machines, hence the following #ifdef
#ifdef _WIN32
	consLog = true;
#endif
	for (auto& l : linesToPrint)
	{
		if (cout_ && consLog)
			(*cout_) << l << std::endl;
		if (verb_)
			(*verb_) << l << std::endl;
	}
};

void ConsVerbLogger::setNewParams(const std::string& newOutputDir)
{
	verb_log_count_ = 0;
	log_verbose_baseString_ = newOutputDir + "/" + "log__verbose";
	closeIfOpen(verb_);
};

void ConsVerbLogger::logSuccessfulChromosomeKills(int howMany)
{
	printStrings(std::vector<std::string>({
		"successfully slaughtered " + std::to_string(howMany) + "chromosomes"
	}), true);
};

void ConsVerbLogger::logPlague()
{
	printStrings(std::vector<std::string>({ "PLAGUE ! ! ! ! !" }), true);
};

void ConsVerbLogger::logMutationDistributionSecondParamChange(int secondParam)
{
	printStrings(std::vector<std::string>({ "new second param for mutation distribution: " +
		std::to_string(secondParam) }), true);
};

void ConsVerbLogger::logGenerationAfterIteration(const Generation& gen)
{
	printStrings(std::vector<std::string>({
		"___GENERATION EVAL SUMMARY___",
		"current fitness: " +
		std::to_string(gen.getBestChromosomeBeforeNewGeneration().getFitness()),
		"current average fitness: " + std::to_string(gen.getAverageFitness())
	}), true);
};

void ConsVerbLogger::logNewExperiment()
{
	printStrings(std::vector<std::string>({
		"seed: " + std::to_string(Random::seed),
		"***************** NEW EXPERIMENT *****************"
	}), true);
};

void ConsVerbLogger::logEndOfExperiment()
{
	printStrings(std::vector<std::string>({
		"* * * * * * * * * * * * * * * * * * * * * * * * * *"
	}), true);
};

void ConsVerbLogger::logNewChromosomeEvaluation()
{
	printStrings(std::vector<std::string>({
		"-- evaluating new chromosome -- "
	}));
};

void ConsVerbLogger::logPerformingComparingExperiment()
{
	printStrings(std::vector<std::string>({
		" [[[ Performing Comparing experiment ]]] "
	}), true);
};

void ConsVerbLogger::logComparingExperimentEnded()
{
	printStrings(std::vector<std::string>({
		" [[[ Comparing experiment ended ]]] "
	}), true);
};

void ConsVerbLogger::logNewGAIteration(int currentIteration)
{
	if (currentIteration % 20 == 1)
		openNewFiles();
	printStrings(std::vector<std::string>({
		"****** GAITERATION NUMBER: " + std::to_string(currentIteration) +
		" (out of " + std::to_string(GA::maximumNumberOfIterations) + ") ******"
	}), true);
};

void ConsVerbLogger::logScenarioEvalDone(int achievedFitness)
{
	printStrings(std::vector<std::string>({
		"scenario evalauted; fitness contribution: " + std::to_string(achievedFitness)
	}));
};

void ConsVerbLogger::logLayoutEvalDone(int totalFitness)
{
	printStrings(std::vector<std::string>({
		"layout eval DONE, total achieved fitness: " + std::to_string(totalFitness)
	}));
};

void ConsVerbLogger::logNewGenerationEval(int playerId)
{
	std::string player = "";
	if (playerId == General::ATTACKER_ID)
		player = "attacker";
	else if (playerId == General::DEFENDER_ID)
		player = "defender";

	printStrings(std::vector<std::string>({
		"*** evaluating " + player + "'s generation ***"
	}), true);
};
