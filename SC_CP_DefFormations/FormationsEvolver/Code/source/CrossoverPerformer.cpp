#include "CrossoverPerformer.h"

using namespace FormationsEvolution;

CrossoverPerformer::CrossoverPerformer(CHP_Observer chp) : chokePoint_(chp)
{
}

//this method will try to change the position of provided unit
//main concerns are collisions with other friendly units or
//placing the unit out of its allowed tiles
void CrossoverPerformer::tryChangePosition(Unit& unit, const Position& targetPosition, 
	CollisionTilesHolder& collisionTilesHolder,
	CommonData::MapValues playerTileType)
{
	Position prevPos = unit.getPosition();
	collisionTilesHolder.removeUnitFromTiles(unit);
	unit.setPosition(targetPosition);

	std::vector<std::pair<int, int>> colTileIndices;
	collisionTilesHolder.getCollisionTileIndicesForUnitsRectangle(unit.topLeft(),
		unit.width(), unit.height(), colTileIndices);

	bool collision(false);

	//check if we collide with some friendly unit
	for (auto & p : colTileIndices)
	{
		for (const auto& u : collisionTilesHolder.getCollisionTile(p.first, p.second).getUnits())
		{
			if (chokePoint_->areUnitsColiding(unit.topLeft(), unit.width(), unit.height(),
				u.topLeft(), u.width(), u.height()))
			{
				collision = true;
				break;
			}
		}
		if (collision)
			break;
	}

	//if not, check that the unit is not placed on incorrect tile
	if (!collision)
	{
		if (!(chokePoint_->checkRecntagleBaseTilesForValue(unit.topLeft(), unit.topRight(), unit.botLeft(),
			unit.botRight(), playerTileType)))
		{
			collision = true;
		}
	}

	//if we did collide, restore the unit's original position
	if (collision)
	{
		unit.setPosition(prevPos);
		collisionTilesHolder.insertUnitToTiles(unit);
	}
	else
	{
		collisionTilesHolder.insertUnitToTiles(unit);
	}

}

//this method tries to swap units in the chromosomes at the provided index
void CrossoverPerformer::trySwapUnits(Chromosome& first, CollisionTilesHolder& firstChromosomeCollisionTiles,
	Chromosome& second, CollisionTilesHolder& secondChromosomeCollisionTiles,
	size_t index)
{
	//get the units from chromosomes at the provided index
	auto& firstUnit = first.getUnitLayout()[index];
	auto& secondUnit = second.getUnitLayout()[index];
	//take their position
	const Position& firstUnitTargetPos = secondUnit.getPosition();
	const Position& secondUnitTargetPos = firstUnit.getPosition();
	//and try to swap them
	tryChangePosition(firstUnit, firstUnitTargetPos, firstChromosomeCollisionTiles, first.getPlayerTileType());
	tryChangePosition(secondUnit, secondUnitTargetPos, secondChromosomeCollisionTiles, second.getPlayerTileType());
}

//several checks that were used mainly in the debugging phase of development
void CrossoverPerformer::checkCrossoverWasCorrect(const Chromosome& first,
	const Chromosome& second) const
{
	//to be sure, let's also check that no two units are colliding
	for (const auto& u1 : first.getUnitLayout().getUnits())
	{
		for (const auto& u2 : first.getUnitLayout().getUnits())
		{
			if (u1.getID() == u2.getID())
				continue;
			if (chokePoint_->areUnitsColiding(u1.topLeft(), u1.width(), u1.height(),
				u2.topLeft(), u2.width(), u2.height()))
				System::FatalError("crossover was not correct, two units from first chromosome are colliding");
		}
	}

	for (const auto& u1 : second.getUnitLayout().getUnits())
	{
		for (const auto& u2 : second.getUnitLayout().getUnits())
		{
			if (u1.getID() == u2.getID())
				continue;
			if (chokePoint_->areUnitsColiding(u1.topLeft(), u1.width(), u1.height(),
				u2.topLeft(), u2.width(), u2.height()))
				System::FatalError("mutation was not correct, two from second chromosome are colliding");
		}
	}

	for (const auto& u : first.getUnitLayout().getUnits())
	{
		if (!(chokePoint_->isUnitInsideMap(u.topLeft(), u.botRight())))
			System::FatalError("crossover is not correct, one of units from first chromsome is out of map");
		if (!(chokePoint_->checkRecntagleBaseTilesForValue(u.topLeft(), u.topRight(),
			u.botLeft(), u.botRight(), first.getPlayerTileType())))
			System::FatalError("corssover was not correct, one unit from first chromosome is not on defender tiles");
	}

	for (const auto& u : second.getUnitLayout().getUnits())
	{
		if (!(chokePoint_->isUnitInsideMap(u.topLeft(), u.botRight())))
			System::FatalError("crossover is not correct, one of units from second chromsome is out of map");
		if (!(chokePoint_->checkRecntagleBaseTilesForValue(u.topLeft(), u.topRight(),
			u.botLeft(), u.botRight(), second.getPlayerTileType())))
			System::FatalError("corssover was not correct, one unit from second chromosome is not on defender tiles");
	}
}
