#pragma once

#include "Common.h"
#include "ChokePoint.h"
#include "Force.hpp"
#include "UnitLayout.hpp"
#include "SearchExperiment.h"
#include "OutputHandler.h"

namespace FormationsEvolution
{
	/**
	@ingroup Experiment_core
	@brief This abstract class is the base class for different algorithms for finding the layout for defender's units
	*/
	class LayoutFinder
	{
	protected:
		/**
		Each layout finder will operate on some chokepoint and it will
		work with defender's and attacker's force
		*/
		CHP_Observer chp_;
		const Force defenderForce_;
		const Force attackerForce_;

		LayoutFinder(
			CHP_Observer chp,
			const Force& defenderForce,
			const Force& attackerForce
			) :
			chp_(chp),
			defenderForce_(defenderForce),
			attackerForce_(attackerForce)
		{};
	public:
		/// <summary> Method that will instruct the instantiated LayoutFinder to initiate finding of 
		///		the best layout for the defender's force</summary>
		/// <param name="searchExperiment"> instance of spracraft simulation</param>
		/// <returns> returns the best found layout for defender's units 
		///		(the copy is on purpose)</returns>
		virtual UnitLayout findBestLayout(SparCraft::SearchExperiment& searchExperiment) = 0;

		virtual ~LayoutFinder(){};
	};
}