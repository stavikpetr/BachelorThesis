#pragma once

#include "LayoutEvaluator.h"

namespace FormationsEvolution
{
	/**
	For the role of layout evaluators, see LayoutEvaluator.

	@ingroup GeneticAlgorithm
	@brief One possible implemenation of layout evaluator that simply performs the simulation
	*/
	class LayoutEvaluatorBasic : public LayoutEvaluator
	{
	public:
		/// <summary> constructor of this layout evaluator </summary>
		/// <param name="experiment"> layout evaluator needs reference to
		///		the sparcraft simulation </param>
		///	<param name="scoreFunc"> scoring function for scoring the simulation
		///		results </param>
		LayoutEvaluatorBasic(
			SparCraft::SearchExperiment* experiment,
			std::unique_ptr<ScoreFunc> scoreFunc);

		/// <summary> method for evaluating two unit layouts with
		///		that belong to respective players </summary>
		///	<param name="first"> first unit layout </param>
		///	<param name="firstLayoutPlayerID"> id of the player to which the first layout belongs </param>
		///	<param name="second"> second unit layout </param>
		///	<param name="secondLayoutPlayerID"> id of the player to which the second layout belongs </param>
		///	<returns> the score assigned to the result of the simulation by the score func that
		///		belongs to this layout evaluator </returns>
		int evaluateTwoLayouts(
			const UnitLayout& first,
			int firstLayoutPlayerID,
			const UnitLayout& second,
			int secondLayoutPlayerID) const;
	};
}