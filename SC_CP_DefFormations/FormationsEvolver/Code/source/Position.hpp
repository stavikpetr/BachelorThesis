#pragma once

namespace FormationsEvolution
{
	/**
	This class represents a single position - it is basically a thin layer
	over x and y coordinates with several useful methods and operators; it is
	also usable in standard hash tables

	@ingroup Util
	@brief Position - pair of x and y coordinates
	*/
	class Position
	{
		int x_, y_;
	public:
		Position(int x, int y) :x_(x), y_(y){};
		Position() :x_(0), y_(0) {};

		int x() const {
			return x_;
		}
		
		int y() const {
			return y_;
		}

		//equality comparison
		friend bool operator==(const Position& lhs, const Position& rhs)
		{
			if (lhs.x_ == rhs.x_ && lhs.y_ == rhs.y_)
				return true;
			return false;
		}
		friend bool operator!=(const Position& lhs, const Position& rhs)
		{
			return !(lhs == rhs);
		}
		//definition of hash function
		size_t hash(const Position& f) const {
			return f.x_ * 10000 + f.y_;
		}

	};
}

/**
\cond
*/

/**
allows usability of position class in standard hashtable collections
like unordered_map
*/
namespace std {
	template <>
	struct hash<FormationsEvolution::Position> {
		size_t operator () (const FormationsEvolution::Position &f) const { return f.hash(f); }
	};
}
/**
\endcond
*/