#pragma once

#include "Common.h"
#include "ChromosomeSelector.h"

namespace FormationsEvolution
{
	/**
	@ingroup GeneticAlgorithm
	@brief Class that implements one type of selection method - tournament selection
	*/
	class TournamentSelector : public ChromosomeSelector
	{
		int selectOne(const Generation& generation) const;
	public:
		/// <summary> method that performs the tournament selection and returns the
		///		indices of the selected chromosomes in the generation </summary>
		///	<param name="generation"> generation from which the selection is performed </param>
		///	<returns> a pair of indices of the two selected chromosomes </returns>
		std::pair<int, int> selectTwo(const Generation& generation) const;
	};
}