#pragma once

#include "Common.h"
#include "ChokePoint.h"
#include "GASettings.h"
#include "Generation.h"
#include "Chromosome.hpp"
#include "CollisionTilesHolder.h"
#include "LayoutManager.h"
#include "OutputHandler.h"

namespace FormationsEvolution
{
	/**
	The plague operation tries to allow the genetic algorithm to escape some plateau
	by adding several completely new chromosomes into the generation

	@ingroup GeneticAlgorithm
	@brief Class that implements one type of plague operation and provides interface for additional possible implementations
	*/
	class PlaguePerformer
	{
		void addNewChromosomes(const Generation& oldGeneration, std::vector<Chromosome>& toFill) const;
	protected:
		CHP_Observer chp_;
	public:
		/// <summary> Constructor of PlaguePerformer that requires chokepoint for placing new units</summary>
		///	<param name="chp"> passed instance of chokepoint </param>
		PlaguePerformer(CHP_Observer chp);
		/// <summary> method that performs the plague operation </summary>
		///	<param name="oldGeneration"> in order to allow new chromosomes in either
		///		the generation of defender or attacker (during coevolution), the current
		///		generation has to be passed into the method</param>
		///	<param name="toFill"> this vector is passed empty and the plague operation
		///		adds new chromosomes into this vector </param>
		virtual void performPlague(
			const Generation& oldGeneration,
			std::vector<Chromosome>& toFill) const;

		virtual ~PlaguePerformer(){};
	};
}