#pragma once

#include "GA_Base.h"
#include <functional>

namespace FormationsEvolution
{
	/**
	This class represents one family of genetic algorithms - RoundRobin genetic algorithms. 
	Their idea is the following - each defender's layout is subdued to several layouts
	of attacker's units. From this vector of fitness values, several methods may be used to 
	assign a fitness score to the chromomosome and the algorithm is parametrizable by this option,
	which is represented by the field fitnessAssignmentFunc_

	@ingroup GeneticAlgorithm
	@brief Class that represents RoundRobin family of genetic algorithms
	*/
	class RoundRobin_GA : public GA_Base
	{
		const std::vector<UnitLayout> attackerLayouts_;
		std::function<void(Chromosome& c, std::vector<int>& fitnesses)> fitnessAssignmentFunc_;
		
	public:
		/// <summary> Constructor of the genetic algorithm of the RoundRobin family </summary>
		/// <param name="defenderGeneration"> the defender's generation that will be evolved </summary>
		///	<param name="generationGenerator"> class that generates new generation of chromosomes from the current ones </summary>
		///	<param name="layoutEvaluatiors"> vector with layout evaluators </summary>
		///	<param name="attackerLayouts"> vector with different attacker layouts </summary>
		/// <param name="fitnessAssignmentFunc"> parameter that alters the behavior of roundRobin type of GA </param>
		RoundRobin_GA(Generation& defenderGeneration,
			GenerationGenerator&& generationGenerator,
			std::vector<std::unique_ptr<LayoutEvaluator>>&& layoutEvaluators,
			const std::vector<UnitLayout>& attackerLayouts,
			std::function<void(Chromosome& c, std::vector<int>& fitnesses)> fitnessAssignmentFunc);

		/// <summary> Method that will perform a single iteration of the genetic algorithm. 
		///		The number of iterations is handled by the GALayoutFinder</summary>
		void evolveNextPopulation();

		/// <summary> Method that will return the best layout found by the algorithm.
		///		Layout with the best score resides after the iteration of GA at 
		///		the zeroth index</summary>
		///	<returns> best layout found for the defender </summary>
		UnitLayout& returnFinalBestLayout()
		{
			return defenderGeneration_[0].getUnitLayout();
		};
	};
}