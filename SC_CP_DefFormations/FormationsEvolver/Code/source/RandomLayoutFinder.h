#pragma once

#include "Common.h"
#include "LayoutFinder.h"
#include "LayoutManager.h"

namespace FormationsEvolution
{
	/**
	The sole purpose for this class was the comparison of the GALayoutFinders against some base. See also
	LayoutFinder or HumanLayoutFinder

	@brief This class represents LayoutFinder that places the defender's units at random
	*/
	class RandomLayoutFinder : public LayoutFinder
	{
	public:
		RandomLayoutFinder(CHP_Observer chp,
			const Force& defenderForce,
			const Force& attackerForce) :
			LayoutFinder(chp, defenderForce, attackerForce)
		{};


		UnitLayout findBestLayout(SparCraft::SearchExperiment& searchExperiment)
		{
			UnitLayout l;
			CollisionTilesHolder c(chp_->getWalkTileSizeX(), chp_->getWalkTileSizeY());
			LayoutManager::getInstance().getValidRandomLayout(
				chp_, defenderForce_, c, CommonData::DEFENDER, l);
			return l;
		};
	};
}