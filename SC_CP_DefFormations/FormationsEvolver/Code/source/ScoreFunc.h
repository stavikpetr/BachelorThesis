#pragma once

#include "Common.h"
#include "SearchExperiment.h"

namespace FormationsEvolution
{
	/**
	@ingroup GeneticAlgorithm
	@brief Base abstract class for different implementations of score functions
	*/
	class ScoreFunc
	{
	public:
		/// <summary> method that takes the assigns the score to the passed sparcraft simulation </summary>
		///	<param name="experimentResult"> the result of the sparcraft simulation </param>
		///	<returns> the score of the experiment result </returns>
		virtual int getScore(const SparCraft::SparCraftExperimentResult& experimentResult) const = 0;
		virtual ~ScoreFunc(){};
	};
}