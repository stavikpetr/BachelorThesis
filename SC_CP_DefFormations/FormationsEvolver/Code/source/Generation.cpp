#include "Generation.h"

using namespace FormationsEvolution;

Generation::Generation(std::vector<Chromosome>& chromosomes,
	int playerID,
	const Force& initialForce) :
	playerID_(playerID),
	chromosomes_(chromosomes),
	initialForce_(initialForce)
{};

size_t Generation::getBestIndex() const
{
	if (playerID_ == General::DEFENDER_ID)
	{
		auto it = std::max_element(chromosomes_.begin(), chromosomes_.end(),
			[](Chromosome a, Chromosome b) {
			return a.getFitness() < b.getFitness();
		});
		size_t index = it - chromosomes_.begin();
		return index;
	}
	else if (playerID_ == General::ATTACKER_ID)
	{
		auto it = std::min_element(chromosomes_.begin(), chromosomes_.end(),
			[](Chromosome a, Chromosome b) {
			return a.getFitness() < b.getFitness();
		});
		size_t index = it - chromosomes_.begin();
		return index;
	}

	System::FatalError("invalid player");
}

void Generation::sortByFitness()
{
	if (playerID_ == General::DEFENDER_ID)
	{
		std::sort(chromosomes_.begin(), chromosomes_.end(), [](Chromosome a, Chromosome b)
		{
			return a.getFitness() < b.getFitness();
		});
	}
	else if (playerID_ == General::ATTACKER_ID)
	{
		std::sort(chromosomes_.begin(), chromosomes_.end(), [](Chromosome a, Chromosome b)
		{
			return a.getFitness() > b.getFitness();
		});
	}
}
