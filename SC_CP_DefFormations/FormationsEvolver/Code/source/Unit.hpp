#pragma once

#include "Common.h"
#include "Position.hpp"

namespace FormationsEvolution
{
	typedef		unsigned char		IDType;

	/**
	@ingroup Util
	@brief This class represents a single Unit which has a unit type, position and an id
	*/
	class Unit
	{
		BWAPI::UnitType bwapiType_;
		Position p_;
		/*id is required to be unique among units in a single chromosome, no other requirement*/
		IDType id_;
	public:
		/**
		several constructors, each useful in different scenarios
		*/
		Unit(BWAPI::UnitType type, Position p, IDType id) :p_(p), bwapiType_(type), id_(id) {};
		Unit(BWAPI::UnitType type, IDType id) : bwapiType_(type), id_(id) {};
		Unit(BWAPI::UnitType type) :bwapiType_(type) {};

		Unit(BWAPI::UnitType type, Position p) :p_(p), bwapiType_(type) {};

		Unit() {};

		//one would want to have two sets of methods, namely
		//void setPosition(const Position& p)
		//void setPosition(Position p)
		// to allow calls with temporary and calls that take the argument as const ref
		//however, compiler cannot differentiate between them and
		// as the case with temporary is heavily used throughout the program
		//i have stuck with it
		//sets the units position
		void setPosition(Position p)
		{
			p_ = p;
		};

		/**
		below is a variety of simple methods for retrieving information about the unit
		*/
		int dimUp() const { return bwapiType_.dimensionUp(); };
		int dimLeft() const { return bwapiType_.dimensionLeft(); };
		int dimBot() const { return bwapiType_.dimensionDown(); };
		int dimRight() const { return bwapiType_.dimensionRight(); };
		int width() const { return bwapiType_.width(); };
		int height() const { return bwapiType_.height(); };

		BWAPI::UnitType getType() const
		{
			return bwapiType_;
		}

		const Position& getPosition() const
		{
			return p_;
		}

		int getXPosition() const
		{
			return p_.x();
		}
		int getYPosition() const
		{
			return p_.y();
		}
		IDType getID() const
		{
			return id_;
		}

		Position topLeft() const
		{
			return Position(p_.x() - dimLeft(), p_.y() - dimUp());
		};
		Position topRight() const
		{
			return Position(p_.x() + dimRight()+1, p_.y() - dimUp());
		};
		Position botLeft() const
		{
			return Position(p_.x() - dimLeft(), p_.y() + dimBot()+1);
		};
		Position botRight() const
		{
			return Position(p_.x() + dimRight()+1, p_.y() + dimBot()+1);
		};
	};
}