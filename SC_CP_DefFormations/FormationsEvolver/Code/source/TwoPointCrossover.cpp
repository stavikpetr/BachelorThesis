#include "TwoPointCrossover.h"

using namespace FormationsEvolution;

TwoPointCrossover::TwoPointCrossover(CHP_Observer chp) :
	CrossoverPerformer(chp),
	collisionTiles1_(chp->getWalkTileSizeX(), chp->getWalkTileSizeY()),
	collisionTiles2_(chp->getWalkTileSizeX(), chp->getWalkTileSizeY())
{};

void TwoPointCrossover::crossover(Chromosome& first, Chromosome& second)
{
	//insert units in the collision tiles
	collisionTiles1_.resetEveryModifiedCollisionTile();
	collisionTiles2_.resetEveryModifiedCollisionTile();
	collisionTiles1_.insertUnitsIntoCollisionTiles(first.getUnitLayout().getUnits());
	collisionTiles2_.insertUnitsIntoCollisionTiles(second.getUnitLayout().getUnits());

	//get index in the vectors of units
	size_t size = first.getUnitLayout().getUnits().size();
	size_t index = Random::getRandInt(0, size);
	size_t secondIndex = Random::getRandInt(index, size);

	//try swap units between the chromosomes from this index to the
	//ending index
	for (size_t i(index); i <= secondIndex; ++i)
		trySwapUnits(first, collisionTiles1_, second, collisionTiles2_, index);

	//finally, check that e.g. two units do not collide with each other
	checkCrossoverWasCorrect(first, second);
};