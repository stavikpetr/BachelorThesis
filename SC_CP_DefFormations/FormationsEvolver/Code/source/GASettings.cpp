#include "GASettings.h"

namespace FormationsEvolution
{
	namespace GA
	{
		std::string selectionTypeEnumToString(SelectionType t)
		{
			switch (t)
			{
			case SelectionType::ROULETTEWHEEL: return "roulleteWheel"; break;
			case SelectionType::TOURNAMENT: return "tournament"; break;
			default: return "[NOT_FOUND]"; break;
			}
		}
		std::string crossoverEnumToString(CrossoverType t)
		{
			switch (t)
			{
			case CrossoverType::ONEPOINT: return "onepoint"; break;
			case CrossoverType::TWOPOINT: return "twopoint"; break;
			default: return "[NOT_FOUND]"; break;
			}
		}
		std::string evolutionTypeToString(EvolutionType t)
		{
			switch (t)
			{
			case EvolutionType::COEVOLUTION: return "coevolution"; break;
			case EvolutionType::ROUNDROBIN: return "roundrobin"; break;
			default: return "[NOT_FOUND]"; break;
			}
		}
		std::string mutationDistributionToString(MutationDistribution t)
		{
			switch (t)
			{
			case MutationDistribution::NORMAL: return "normal"; break;
			case MutationDistribution::UNIFORM: return "uniform"; break;
			default: return "[NOT_FOUND]"; break;
			}
		}
		std::string mutationTypeToString(MutationType t)
		{
			switch (t)
			{
			case MutationType::CONSTANT: return "constant"; break;
			case MutationType::DECAY: return "decay"; break;
			case MutationType::PLAGUEPEEK: return "plaguepeek"; break;
			default: return "[NOT_FOUND]"; break;
			}
		}

		int maximumNumberOfIterations;

		double mutationRate;
		int mutationInitValue;
		MutationDistribution mutationDistribution;
		MutationType mutationType;

		int decayToValue;
		int decayToTurns;

		int plaguePeekAmount;
		int plaguePeekDecayTurns;

		CrossoverType crossoverType;
		double crossoverRate;

		double elitismPercentage;
		bool elitism;

		SelectionType selectionType;

		bool plague;
		double plaguePercentage;
		int plagueStartIteration;
		int plagueRepetitionCounter;

		EvolutionType evolutionType;

		int defPopulationSize;
		int alt_attPopulationSize;
		int alt_optSwap_turns_d;
		int alt_optSwap_turns_a;

		int rr_numOfEnemyForces;

		std::string btos(bool b)
		{
			return b ? "true" : "false";
		}


		void outputGASettings(std::ofstream* out)
		{
			*out << "Seed: " << std::to_string(Random::seed) << std::endl;
			*out << "MaximumNumberOfIterations: " << std::to_string(maximumNumberOfIterations) << std::endl;


			*out << "Mut: " << std::to_string(mutationRate) << " " <<
				mutationDistributionToString(mutationDistribution) << " " <<
				std::to_string(mutationInitValue) << " " <<
				mutationTypeToString(mutationType) << " ";
			switch (mutationType)
			{
			case MutationType::CONSTANT: break;
			case MutationType::DECAY: *out << std::to_string(decayToValue)
				<< " " << std::to_string(decayToTurns); break;
			case MutationType::PLAGUEPEEK: *out << std::to_string(plaguePeekAmount) << " "
				<< std::to_string(plaguePeekDecayTurns); break;
			}
			*out << std::endl;
			*out << "Crossover: " << std::to_string(crossoverRate) << " " <<
				crossoverEnumToString(crossoverType) << std::endl;

			*out << "Elitism: " << std::to_string(elitismPercentage) << " " <<
				btos(elitism) << std::endl;

			*out << "Selection: " << selectionTypeEnumToString(selectionType) << std::endl;
			*out << "Plague: " << btos(plague) << " " 
				<< std::to_string(plaguePercentage) << " " 
				<< std::to_string(plagueStartIteration) << " "
				<< std::to_string(plagueRepetitionCounter) << std::endl;

			*out << "EvolutionType: " << evolutionTypeToString(evolutionType) << std::endl;

			switch (evolutionType)
			{
			case EvolutionType::COEVOLUTION:
				*out << "PopulationSize: " << std::to_string(defPopulationSize) << " " <<
					std::to_string(alt_attPopulationSize) << " " <<
					std::endl <<
					"OptimizationSwap: " << std::to_string(alt_optSwap_turns_d) << " " <<
					std::to_string(alt_optSwap_turns_a) << std::endl; break;
			case EvolutionType::ROUNDROBIN:
				*out << "PopulationSize: " << std::to_string(defPopulationSize) << std::endl <<
					"NumberOfEnemyForces: " << std::to_string(rr_numOfEnemyForces) << std::endl;
				break;
			}

		}
	}
}