#include "ElitismPerformer.h"

using namespace FormationsEvolution;

void ElitismPerformer::performElitism(Generation& oldGeneration,
	std::vector<Chromosome>& newChromosomes) const
{
	if (GA::elitism)
	{
		//first, sort the generation so that we have the best chromosomes in back
		oldGeneration.sortByFitness();
		size_t numberOfElements = (size_t)(oldGeneration.size() * GA::elitismPercentage);

		//take the number of elements from the old generation and place
		//them in the new one
		if (numberOfElements > 0)
		{
			for (size_t i(oldGeneration.size());
				i > oldGeneration.size() - numberOfElements;
				--i)
			{
				newChromosomes.push_back(oldGeneration[i - 1]);
			}
		}
	}
};