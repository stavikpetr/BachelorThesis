#pragma once

#include "Common.h"
#include "Force.hpp"
#include "ChokePoint.h"
#include "SearchExperiment.h"

//---- Layout Finders ----
#include "LayoutFinder.h"
#include "GALayoutFinder.h"
#include "RandomLayoutFinder.h"
#include "HumanLayoutFinder.h"

//---- Layout testers ----
#include "FinalLayoutTester.h"


#include "OutputHandler.h"
namespace FormationsEvolution
{
	/**
		This class represents a single experiment that was constructed by the FormationsEvolutionExperiment.
		That means a combination of EvolutionSettings, Map, defender player and his units, attacker player and his units, even though
		not all of these settings are necesarrily relevant to this class's state and are not contained in it - for example, Evolution settings
		are set globally and defender + attacker player are relevant only for the sparcraft simulation.

		@ingroup Experiment_core
		@brief This class governs the execution of a single experiment
	*/
	class SingleExperiment
	{
		CHP_Observer chp_;
		const Force defenderForce_;
		const Force attackerForce_;
	public:
		/// <summary> Constructor for creating new experiment </summary>
		/// <param name="chp"> pointer to chokepoint instance</param>
		/// <param name="defenderForce"> force of the defending player </param>
		/// <param name="attackerForce"> force of the attacking player </param>
		SingleExperiment(CHP_Observer chp,
			const Force& defenderForce,
			const Force& attackerForce);

		/// <summary> method that will initiate the search for a new best layout for this experiment </summary>
		/// <param name="experiment"> object that represents already configured sparcraft simulation </param>
		void performSingleExperiment(SparCraft::SearchExperiment& experiment);
	};
}