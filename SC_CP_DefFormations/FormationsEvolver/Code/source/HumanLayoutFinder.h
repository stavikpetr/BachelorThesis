#pragma once

#include "Common.h"
#include "LayoutFinder.h"

namespace FormationsEvolution
{
	/**
	The sole purpose for this class was the comparison of the GALayoutFinders against some base. See also
	LayoutFinder or RandomLayoutFinder.

	@brief This class represents LayoutFinder that uses our manual placement of units
	*/
	class HumanLayoutFinder : public LayoutFinder
	{
	public:
		HumanLayoutFinder(CHP_Observer chp,
			const Force& defenderForce,
			const Force& attackerForce) :
			LayoutFinder(chp, defenderForce, attackerForce)
		{};

		UnitLayout findBestLayout(SparCraft::SearchExperiment& searchExperiment)
		{
			//note that these manual layouts match a specific types of experiments and they should not 
			//be used in different scenarios with varrying maps

			UnitLayout l;
			std::vector<Unit> units;

			//3 zealots vs 10 zerglings
			if (defenderForce_.getForce().size() == 1)
			{
				if (BWAPI::UnitType(defenderForce_.getForce()[0].first) == BWAPI::UnitTypes::Protoss_Zealot)
				{
					units.push_back(Unit(BWAPI::UnitTypes::Protoss_Zealot, Position(480, 420)));
					units.push_back(Unit(BWAPI::UnitTypes::Protoss_Zealot, Position(516, 438)));
					units.push_back(Unit(BWAPI::UnitTypes::Protoss_Zealot, Position(548, 474)));
				}
			}
			else if (defenderForce_.getForce().size() == 2)
			{
				int state = -1;
				if (defenderForce_.getForce()[0].second == 2 && defenderForce_.getForce()[1].second == 2)
				{
					units.push_back(Unit(BWAPI::UnitTypes::Protoss_Zealot, Position(490, 182)));
					units.push_back(Unit(BWAPI::UnitTypes::Protoss_Zealot, Position(530, 192)));
					units.push_back(Unit(BWAPI::UnitTypes::Protoss_Dragoon, Position(578, 167)));
					units.push_back(Unit(BWAPI::UnitTypes::Protoss_Dragoon, Position(513, 139)));
					
				}
				else
				{
					//case with 3 marines and 4 medics
					if (BWAPI::UnitType(defenderForce_.getForce()[0].first) == BWAPI::UnitTypes::Terran_Marine ||
						BWAPI::UnitType(defenderForce_.getForce()[1].first == BWAPI::UnitTypes::Terran_Marine))
					{
						if (attackerForce_.getForce().size() == 1)
						{
							//against 6 zealots
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Marine, Position(513, 139)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Marine, Position(550, 126)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Marine, Position(578, 167)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(523, 183)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(550, 188)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(478, 171)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(502, 168)));

						}
						else
						{
							//against the zealots and dragoons
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Marine, Position(284, 515)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Marine, Position(370, 545)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Marine, Position(400, 550)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(410, 518)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(381, 520)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(353, 510)));
							units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(330, 484)));
						}
					}
					else
					{
						if (attackerForce_.getForce().size() == 1)
						{
							//against 6 zealots
							units.push_back(Unit(BWAPI::UnitTypes::Protoss_Zealot, Position(521, 188)));
							units.push_back(Unit(BWAPI::UnitTypes::Protoss_Zealot, Position(483, 178)));
							units.push_back(Unit(BWAPI::UnitTypes::Protoss_Dragoon, Position(560, 130)));
							units.push_back(Unit(BWAPI::UnitTypes::Protoss_Dragoon, Position(616, 186)));
							units.push_back(Unit(BWAPI::UnitTypes::Protoss_Dragoon, Position(510, 120)));
						}
						else
						{
							//against the dragoons and zealots
							units.push_back(Unit(BWAPI::UnitTypes::Protoss_Zealot, Position(330, 535)));
							units.push_back(Unit(BWAPI::UnitTypes::Protoss_Zealot, Position(392, 540)));
							units.push_back(Unit(BWAPI::UnitTypes::Protoss_Dragoon, Position(291, 505)));
							units.push_back(Unit(BWAPI::UnitTypes::Protoss_Dragoon, Position(350, 582)));
							units.push_back(Unit(BWAPI::UnitTypes::Protoss_Dragoon, Position(435, 580)));
						}
					}
					
				}
			}
			//only 3 medics, 2 firebats and 1 marine
			else if (defenderForce_.getForce().size() == 3)
			{
				units.push_back(Unit(BWAPI::UnitTypes::Terran_Marine, Position(355, 545)));
				units.push_back(Unit(BWAPI::UnitTypes::Terran_Firebat, Position(410, 518)));
				units.push_back(Unit(BWAPI::UnitTypes::Terran_Firebat, Position(353, 480)));
				units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(320, 530)));
				units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(381, 520)));
				units.push_back(Unit(BWAPI::UnitTypes::Terran_Medic, Position(350, 510)));
			}

			if (units.size() == 0)
				System::FatalError("human layout finder ... the size is 0");
			l = UnitLayout(std::move(units));
			return l;
		};
	};
}