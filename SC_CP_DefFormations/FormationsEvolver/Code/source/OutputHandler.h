#pragma once

#include "Common.h"
#include "GASettings.h"
#include "UnitLayout.hpp"
#include "UnitAlteration.hpp"
#include "Generation.h"
#include "boost/filesystem.hpp"

#include "ConsVerbLogger.h"
#include "PlotLogger.h"
#include "ChokePoint.h"

#include "Force.hpp"

namespace FormationsEvolution
{
	/**
	This singleton class offers a variety of logging and output methods
	that are used throughout the whole program; output handler does some outputting
	on its own but for some it uses some specialized loggers like ConsVerbLogger and
	Plotlogger

	Even though other code could directly use these, i think it is better to
	remove this need of knowledge from the other code and it is better to all of
	the logic behind deciding what to log where here in one place

	@ingroup Output
	@brief Class with varienty of logging and output methods
	*/
	class OutputHandler
	{
		/** these fields represent mainly paths in the
		output directory 
		*/
		std::string baseOutputDir_;
		std::string curIDString_;
		std::string	layoutCmpResultDir_;
		std::string finalLayoutDir_;
		std::string logDir_;
		std::string graphDir_;
		std::string miscDir_;

		std::string defenderString_;
		std::string attackerString_;

		/**
		specialized loggers
		*/
		ConsVerbLogger consVerbLogger_;
		PlotLogger plotLogger_;

		/**
		several helper private methods
		*/
		std::string getCurrentDateAndTimeString();
		std::string getUnitShortenedName(BWAPI::UnitType unitType);
		void outputUnits(const std::vector<Unit>& units, int playerId,
			std::ofstream* out, bool db=false);
		void outputForce(const Force& f, std::ofstream* out);

		/**
		private constructor due to the singleton
		*/
		OutputHandler();
	public:
		/// <summary> singleton instance getter </summary>
		static OutputHandler& getInstance();

		/**
		Methods for initalization of new parameters that
		will e.g. change the current root output directory
		*/
		/// <summary> initalization when a new experiment is initiated </summary>
		void initialize(const std::string& baseOutputDir, const std::string& inputFolderName);
		/// <summary> sets a few fields after parsing of the player file </summary>
		void setNewPlayer(int playerId, const std::string& playerString);
		/// <summary> sets a few fields after we've parse the new experiment
		///		settings like map and units for each player </summary>
		void newExperiment(const Force& force1, const Force& force2, CHP_Observer chp);

		
		/**
		several small logging methods that are used mainly throughout
		the run of the genetic algorithm

		these methods are only a thin wrappers which usually directly
		call appropriate methods on the other loggers, see their
		implementation for details
		*/

		/// <summary> logs number of killed similar chromosomes
		///		during the new generation phase </summary>
		void logSuccessfulChromosomeKills(int howMany);
		/// <summary> logs the occurence of plague </summary>
		void logPlague();
		/// <summary> logs a change in mutation parameters </summary>
		void logMutationDistributionSecondParamChange(int secondParam);
		/// <summary> logs that new experiment has started </summary>
		void logNewExperiment();
		/// <summary> logs end of current experiment </summary>
		void logEndOfExperiment();
		/// <summary> logs that we start evaluation of new chromosome
		///		in one GA iteration </summary>
		void logNewChromosomeEvaluation();
		/// <summary> logs start of new iteration of genetic algorithm </summary>
		void logNewGAIteration(int currentIteration);
		/// <summary> logs that one scenario within the evaluation of chromosome
		///		is done </summary>
		void logScenarioEvalDone(int achievedFitness);
		/// <summary> logs new fitness of chromosome after evaluation of all
		///		scenarios </summary>
		void logLayoutEvalDone(int totalFitness);
		/// <summary> logs evaluation of new genetic algorithm generation </summary>
		void logNewGenerationEval(int playerId);
		/// <summary> logs the state of generation after a single iteration of
		///		genetic algorithm </summary>
		void logGenerationAfterIteration(const Generation& gen);

		/// <summary> log start of final comparing experiment </summary>
		void logPerformingComparingExperiment();
		/// <summary> log end of final comparing experiment </summary>
		void logComparingExperimentEnd();

		/// <summary> outputs the building infrastructure
		///		that is used in the infrastructure hunt scenarios </summary>
		void outputProbeInfrastructure(const UnitLayout& probeLayout);
		/// <summary> outputs attacker's layout that are used in the round
		///		robin genetic algorithm </summary>
		void outputRRAttackerUnitLayout(const UnitLayout& l, int num);
		/// <summary> output the final achieved layout </summary>
		void outputFinalLayout(const UnitLayout& unitLayout);
		/// <summary> log that an optimization swap occurred during
		///		the coevolution algorithm </summary>
		void logOptSwap(const UnitLayout& optimizedAgainst, int optimizedAgainstPlayer, 
			const UnitLayout& producedLayout, int producedLayoutPlayer);
	
		/// <summary> output the results of the final comparing experiment </summary>
		void outputExperimentTestResult(const UnitLayout& finalDefenderLayout,
			const std::vector<int>& testFitnesses,
			const std::vector<UnitLayout>& againstLayouts,
			const std::vector<UnitAlteration>& unitAlterations);
	};
}
