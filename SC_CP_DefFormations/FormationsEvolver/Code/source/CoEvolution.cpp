#include "CoEvolution.h"

using namespace FormationsEvolution;

CoEvolution::CoEvolution(Generation& defenderGeneration,
	GenerationGenerator&& generationGenerator,
	std::vector<std::unique_ptr<LayoutEvaluator>>&& layoutEvaluators,
	Generation& attackerGeneration,
	std::function<bool(Generation&g)> swapConditionFunc) :
	GA_Base(defenderGeneration,
		std::move(generationGenerator),
		std::move(layoutEvaluators)),
	attackerGeneration_(attackerGeneration),
	swapConditionFunc_(swapConditionFunc)
{
	curGen_ = &defenderGeneration_;
	oppositeGen_ = &attackerGeneration_;
	oppBestLayout_ = attackerGeneration_[Random::getRandInt(0, attackerGeneration_.size())].getUnitLayout();

}

void CoEvolution::evolveNextPopulation()
{
	//first, check if we swap who's generation will we evolve this
	//iteration of the genetic algorithm
	//this condition is checked with the provided lambda function as there
	//may be many possible ways to do this swap (e.g. fixed number of turns, 
	// variable number of turns, ...)
	if (swapConditionFunc_((*curGen_)))
	{
		OutputHandler::getInstance().logOptSwap(oppBestLayout_,
			oppositeGen_->getPlayerID(),
			(*curGen_).getBestAfterNewGeneration().getUnitLayout(),
			curGen_->getPlayerID());
		oppBestLayout_ = (*curGen_).getBestAfterNewGeneration().getUnitLayout();
		Generation* tmp = curGen_;
		curGen_ = oppositeGen_;
		oppositeGen_ = tmp;
	}

	OutputHandler::getInstance().logNewGenerationEval((*curGen_).getPlayerID());

	//if we have the correct generation
	//increment the counter
	(*curGen_).incrementCounter();

	//and simulate for each chromosome
	for (size_t genIndex(0); genIndex < (*curGen_).size(); ++genIndex)
	{
		int fitness(0);
		for (auto& le : layoutEvaluators_)
		{
			fitness += le->evaluateTwoLayouts(
				(*curGen_)[genIndex].getUnitLayout(), (*curGen_).getPlayerID(),
				oppBestLayout_, (*oppositeGen_).getPlayerID());
		}
		fitness /= (int)layoutEvaluators_.size();
		(*curGen_)[genIndex].setFitness(fitness);
		OutputHandler::getInstance().logLayoutEvalDone(
			(*curGen_)[genIndex].getFitness());

	}
	OutputHandler::getInstance().logGenerationAfterIteration((*curGen_));

	generationGenerator_.createNewGeneration(*curGen_);
}