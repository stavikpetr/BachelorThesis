#include "PlotLogger.h"

using namespace FormationsEvolution;

void PlotLogger::closeIfOpen(std::ofstream* output)
{
	if (output)
	{
		if (output->is_open())
			output->close();
	}
};

void PlotLogger::openNewFiles()
{
	closeIfOpen(fitnessOutput_);
	fitnessOutput_ = new std::ofstream(fitness_base_string_ + "_" +
		std::to_string(fitnessLog_counter_) + ".txt");
	fitnessLog_counter_++;
};

PlotLogger::PlotLogger() :
	iterationCounter_(0)
{};

void PlotLogger::setNewParams(const std::string& newOutputDir)
{
	fitnessLog_counter_ = 0;
	fitness_base_string_ = newOutputDir + "/" + "fitness_out";
	closeIfOpen(fitnessOutput_);
};

void PlotLogger::logGenerationAfterIteration(const Generation& gen)
{
	iterationCounter_++;
	if (iterationCounter_ % 20 == 1)
	{
		openNewFiles();
	}

	for (size_t i(0); i < gen.size(); ++i)
	{
		(*fitnessOutput_) << std::to_string(gen[i].getFitness());

		if (i == gen.size() - 1)
			(*fitnessOutput_) << std::endl;
		else
			(*fitnessOutput_) << ",";
	}

};