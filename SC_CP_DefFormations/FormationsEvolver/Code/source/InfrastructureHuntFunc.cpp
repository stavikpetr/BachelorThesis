#include "InfrastructureHuntFunc.h"

using namespace FormationsEvolution;

double InfrastructureHuntFunc::getInfrastructureHuntFuncMultiplier(
	int totalTime, int damageTime) const
{
	double firstParam = -1.0 / (double)(totalTime / 4);
	double logParam = log(3.5);
	double addParam = 0.5;
	double expParam = firstParam * damageTime + logParam;
	double multiplier = exp(expParam);
	multiplier += addParam;
	return multiplier;
};

int InfrastructureHuntFunc::getScore(const SparCraft::SparCraftExperimentResult& experimentResult) const
{
	//second type of scoring function which is used in the scenarios
	//when the attacker wants to attack the defender's infrastructure
	double totalVal(0.0);

	for (auto& p : experimentResult.infrastructureDamage_)
	{
		double multiplier = getInfrastructureHuntFuncMultiplier(
			experimentResult.totalTime_, p.first);
		double actualDamage = p.second * multiplier;
		totalVal -= actualDamage;
	}
	return (int)totalVal;
};