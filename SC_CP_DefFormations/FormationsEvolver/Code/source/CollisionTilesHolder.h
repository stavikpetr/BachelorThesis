#pragma once

#include "Common.h"
#include "CollisionTile.h"
#include "Unit.hpp"

namespace FormationsEvolution
{
	/**
	In order to resolve collision between units, for example when performing mutation,
	the map is divided into 50x50 CollisionTiles. The collisionTiles hold the current
	units in them and thus provide quite an efficient method of resolving collisions

	@ingroup ChokePoint
	@brief This class holds the collision tiles which are used to resolve collisions
	*/
	class CollisionTilesHolder
	{
		size_t collisionTilesNumberHorizontal_;
		size_t collisionTilesNumberVertical_;
		std::vector<std::vector<CollisionTile>> collisionTiles_;
		std::unordered_set<Position> modifiedColTiles_;
		
		bool isValidCollisionTileXIndex(int vectorXIndex) const;
		bool isValidCollisionTIleYIndex(int vectorYIndex) const;
		bool isValidCollisionTilePosition(int vectorXIndex, int vectorYIndex) const;
	public:
		/// <summary> constructor that will initialize the collision tiles 
		///		based on the number of walk tiles we have </summary>
		/// <param name="numOfWalkTilesX"> number of walking tiles in x direction </param>
		///	<param name="numOfWalkTilesY"> number of walking tiles in y direction </param>
		CollisionTilesHolder(int numOfWalkTilesX, int numOfWalkTilesY);

		/// <summary> this method will remove the provided unit from the collision 
		///		tiles that have reference to this unit </summary>
		void removeUnitFromTiles(const Unit& u);

		/// <summary> this method will insert new units into tiles
		///		with which this unit overlaps </summary>
		void insertUnitToTiles(const Unit& u);

		/// <summary> in order to reuse the collision tiles holder (as it is not
		///		a light resource), it itself keeps track of every modified collision tile
		///		and this method will reset each such modified tile </summary>
		void resetEveryModifiedCollisionTile();

		/// <summary> returns collision tile at the specified index </summary>
		const CollisionTile& getCollisionTile(int vectorXIndex, int vectorYIndex) const;

		/// <summary> this method will return collision tile indices that overlap
		///		with rectangle specified by left corned with width and height; 
		///		the indices of these collision tiles are placed in the provided vector </summary>
		void getCollisionTileIndicesForRectangle(const Position& topLeftCorner, int width,
			int height, std::vector<std::pair<int, int>>& resultingIndices) const;
		/// <summary> this method serves as more efficient implementation of
		///		the method getCollisionTileIndicesForRectangle for cases when we know
		///		that the object does not overlap more than 2x2 collision tiles 
		///		(such as the case for units) </summary>
		void getCollisionTileIndicesForUnitsRectangle(const Position& topLeftCorner, 
			int width, int height, std::vector<std::pair<int, int>>& resultingIndices) const;
		
		/// <summary> this method will insert multiple units into the collision
		///		tiles each unit overlaps </summary>
		void insertUnitsIntoCollisionTiles(const std::vector<Unit>& units);
	};
}