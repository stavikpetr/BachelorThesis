#include "CollisionTilesHolder.h"

using namespace FormationsEvolution;

CollisionTilesHolder::CollisionTilesHolder(int numOfWalkTilesX, int numOfWalkTilesY)
{
	//this constructor will initialize each collision tile this
	//holder holds
	collisionTilesNumberHorizontal_ = ((numOfWalkTilesX * General::walkTileSize) / General::collisionTileSize) + 1;
	collisionTilesNumberVertical_ = ((numOfWalkTilesY * General::walkTileSize) / General::collisionTileSize) + 1;

	for (size_t i = 0; i < collisionTilesNumberHorizontal_; ++i)
	{
		std::vector<CollisionTile> baseTilesTmp;
		baseTilesTmp.reserve(collisionTilesNumberVertical_);
		for (size_t j = 0; j < collisionTilesNumberVertical_; ++j)
		{
			baseTilesTmp.push_back(CollisionTile());
		}
		collisionTiles_.push_back(std::move(baseTilesTmp));
	}

}


bool CollisionTilesHolder::isValidCollisionTileXIndex(int vectorXIndex) const
{
	if (vectorXIndex >= 0 && vectorXIndex < (int)collisionTilesNumberHorizontal_)
		return true;
	return false;
}

bool CollisionTilesHolder::isValidCollisionTIleYIndex(int vectorYIndex) const
{
	if (vectorYIndex >= 0 && vectorYIndex < (int)collisionTilesNumberVertical_)
		return true;
	return false;
}

bool CollisionTilesHolder::isValidCollisionTilePosition(int vectorXIndex, int vectorYIndex) const
{
	return isValidCollisionTileXIndex(vectorXIndex) && isValidCollisionTIleYIndex(vectorYIndex);
}

void CollisionTilesHolder::resetEveryModifiedCollisionTile()
{
	for (auto& pos : modifiedColTiles_)
	{
		collisionTiles_[pos.x()][pos.y()].reset();
	}
	modifiedColTiles_.clear();
}



const CollisionTile& CollisionTilesHolder::getCollisionTile(int vectorXIndex,
	int vectorYIndex) const
{
	if (isValidCollisionTileXIndex(vectorXIndex) && isValidCollisionTIleYIndex(vectorYIndex))
	{
		return collisionTiles_[vectorXIndex][vectorYIndex];
	}
	else
	{
		throw(std::runtime_error("get collision tile, invalid call"));
		return collisionTiles_[-1][-1];
	}
}

void CollisionTilesHolder::getCollisionTileIndicesForUnitsRectangle(const Position& topLeftCorner,
	int width, int height, std::vector<std::pair<int, int>>& resultingIndices) const
{
	int x1 = topLeftCorner.x();
	int x2 = topLeftCorner.x() + width;
	int y1 = topLeftCorner.y();
	int y2 = topLeftCorner.y() + height;

	bool xOverlapping = x1 / General::collisionTileSize != x2 / General::collisionTileSize ? true : false;
	bool yOverlapping = y1 / General::collisionTileSize != y2 / General::collisionTileSize ? true : false;

	if (!xOverlapping && !yOverlapping)
	{
		resultingIndices.push_back(std::pair<int, int>(x1 / General::collisionTileSize, y1 / General::collisionTileSize));
	}
	if ((xOverlapping && !yOverlapping) || (!xOverlapping && yOverlapping))
	{
		resultingIndices.push_back(std::pair<int, int>(x1 / General::collisionTileSize, y1 / General::collisionTileSize));
		resultingIndices.push_back(std::pair<int, int>(x2 / General::collisionTileSize, y2 / General::collisionTileSize));
	}
	if (xOverlapping && yOverlapping)
	{
		resultingIndices.push_back(std::pair<int, int>(x1 / General::collisionTileSize, y1 / General::collisionTileSize));
		resultingIndices.push_back(std::pair<int, int>(x2 / General::collisionTileSize, y1 / General::collisionTileSize));
		resultingIndices.push_back(std::pair<int, int>(x2 / General::collisionTileSize, y2 / General::collisionTileSize));
		resultingIndices.push_back(std::pair<int, int>(x1 / General::collisionTileSize, y2 / General::collisionTileSize));
	}
}



void CollisionTilesHolder::getCollisionTileIndicesForRectangle(const Position& topLeftCorner,
	int width, int height, std::vector<std::pair<int, int>>& resultingIndices) const
{
	int resultingX = topLeftCorner.x() + width;
	int resultingY = topLeftCorner.y() + height;

	int curX, curY, curXIndex, curYIndex, prevXIndex, prevYIndex;
	prevXIndex = std::numeric_limits<int>::min(); //dummy;
	prevYIndex = std::numeric_limits<int>::min();
	curY = topLeftCorner.y();
	curXIndex;
	curYIndex = curY / General::collisionTileSize;

	//this big loop will iterate over each collision tile in the rectangle
	//that is specified in the arguments with top left corner and width + height
	//and marks each new matching index for return
	//the direciton of iteration is:
	// ---->
	// ---->

	while (true)
	{
		curX = topLeftCorner.x();
		curXIndex = curX / General::collisionTileSize;
		curYIndex = curY / General::collisionTileSize;
		if ((curXIndex != prevXIndex) || (curYIndex != prevYIndex))
		{
			if (isValidCollisionTilePosition(curXIndex, curYIndex))
			{
				resultingIndices.push_back(std::pair<int, int>(curXIndex, curYIndex));
			}
			prevXIndex = curXIndex;
			prevYIndex = curYIndex;
		}

		while (true)
		{
			curX += curX + General::collisionTileSize <= resultingX ? General::collisionTileSize : resultingX - curX;
			curXIndex = curX / General::collisionTileSize;
			if (curXIndex != prevXIndex)
			{
				if (isValidCollisionTilePosition(curXIndex, curYIndex))
				{
					resultingIndices.push_back(std::pair<int, int>(curXIndex, curYIndex));
				}
			}
			if (curX == resultingX)
				break;
		}
		if (curY == resultingY)
			break;
		curY += curY + General::collisionTileSize <= resultingY ? General::collisionTileSize : resultingY - curY;
	}
}

void CollisionTilesHolder::removeUnitFromTiles(const Unit& u)
{
	std::vector<std::pair<int, int>> indices;
	getCollisionTileIndicesForUnitsRectangle(u.topLeft(), u.width(), u.height(), indices);
	for (auto& p : indices)
	{
		collisionTiles_[p.first][p.second].removeUnit(u.getID());
	}
}

void CollisionTilesHolder::insertUnitToTiles(const Unit& u)
{
	std::vector<std::pair<int, int>> indices;
	getCollisionTileIndicesForUnitsRectangle(u.topLeft(), u.width(), u.height(), indices);
	for (auto& p : indices)
	{
		collisionTiles_[p.first][p.second].addUnit(u);
		modifiedColTiles_.insert(Position(p.first, p.second));
	}

}

void CollisionTilesHolder::insertUnitsIntoCollisionTiles(const std::vector<Unit>& units)
{
	std::vector<std::pair<int, int>> indices;
	for (auto& unit : units)
	{
		indices.clear();
		getCollisionTileIndicesForUnitsRectangle(unit.topLeft(), unit.width(), unit.height(), indices);
		for (auto& p : indices)
		{
			collisionTiles_[p.first][p.second].addUnit(unit);
			modifiedColTiles_.insert(Position(p.first, p.second));
		}
	}
}
