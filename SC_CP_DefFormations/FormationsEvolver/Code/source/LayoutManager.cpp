#include "LayoutManager.h"

using namespace FormationsEvolution;

LayoutManager::LayoutManager()
{
}

LayoutManager& LayoutManager::getInstance()
{
	static LayoutManager layoutManager;
	return layoutManager;
};

void LayoutManager::getValidRandomLayout(CHP_Observer chp, const Force& force, CollisionTilesHolder& collisionTilesHolder,
	CommonData::MapValues mapValue, UnitLayout& toFill)
{
	getValidRandomLayout(chp, force, collisionTilesHolder, mapValue, toFill, Random::getDefaultRandomEngine());
}

void LayoutManager::getValidRandomLayout(CHP_Observer chp, const Force& force,
	CollisionTilesHolder& collisionTilesHolder,
	CommonData::MapValues mapValue, UnitLayout& toFill,
	std::default_random_engine& engine)
{
	int iterations = 0;
	std::vector<std::pair<int, int>> colTileIndices;
	for (const auto& pair : force.getForce())
	{
		for (int i(0); i < pair.second; ++i)
		{
			//take the first unit
			Unit u(pair.first, iterations);
			while (true)
			{
				//choose a random base tile
				int randX = Random::getRandInt(0, chp->getPointSizeX(), engine);
				int randY = Random::getRandInt(0, chp->getPointSizeY(), engine);

				if (!(chp->isValidWalkTileIndex(randX / General::walkTileSize, randY / General::walkTileSize)))
					continue;

				//get its map value
				if (chp->getBaseTileValue(Position(randX, randY)) == mapValue)
				{
					u.setPosition(Position(randX, randY)); //will be cleared if it is not valid

					//lets check if every 8x8 tile unit wants to occupy is on the corresponding value tile 
					if (chp->checkRecntagleBaseTilesForValue(u.topLeft(), u.topRight(), u.botLeft(), u.botRight(), mapValue))
					{
						//now check that we do not collide with any friendly unit
						colTileIndices.clear();
						collisionTilesHolder.getCollisionTileIndicesForRectangle(Position(randX - u.dimLeft(), randY - u.dimUp()), u.width(), u.height(), colTileIndices);

						bool collision(false);
						for (auto& pair : colTileIndices)
						{
							const auto & unitsInTile = collisionTilesHolder.getCollisionTile(pair.first, pair.second).getUnits();
							
							for (size_t i(0); i < unitsInTile.size(); ++i)
							{
								if (chp->areUnitsColiding(u.topLeft(), u.width(), u.height(),
									unitsInTile[i].topLeft(), unitsInTile[i].width(), unitsInTile[i].height()))
								{
									collision = true;
									break;
								}
							}
							if (collision)
								break;
						}

						//if not we can place the unit
						if (!collision)
						{
							u.setPosition(Position(randX, randY));
							collisionTilesHolder.insertUnitToTiles(u);
							toFill.addUnit(u);
							break;
						}

					}
				}
			}
			iterations++;
		}
	}
}