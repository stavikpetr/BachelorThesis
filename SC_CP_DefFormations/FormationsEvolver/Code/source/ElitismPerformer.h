#pragma once

#include "Common.h"
#include "Generation.h"
#include "GASettings.h"

namespace FormationsEvolution
{	
	/**
	This class offers basic implementation of elitism operation for the genetic algorithm,
	but the provided methods may be easily overriden by a new implementation.
	The idea of elitism is to preserve the best chromosomes - maybe 10% between each
	generation.

	@ingroup GeneticAlgorithm
	@brief Base class for different implementations of elitism operation
	*/
	class ElitismPerformer
	{
	public:
		/// <summary> this method facilitates the elitism operation </summary>
		/// <param name="oldGeneration"> current generation of the genetic algorithm </param>
		///	<param name="newChromosomes"> the elitismPerformer will place the chromosomes
		///		that should be preserved into this vector </param>
		virtual void performElitism(Generation& oldGeneration,
			std::vector<Chromosome>& newChromosomes) const;

		virtual ~ElitismPerformer()
		{};
	};
}