#include "Common.h"
#include <ctime>

namespace FormationsEvolution
{
	namespace System
	{
		void FatalError(const std::string& errorMessage)
		{
			std::cout << "\n\n\nEvaluation Fatal Error: \n\n\n      " + errorMessage + "\n\n";
			throw(std::runtime_error("Fatal error occured"));
		}
	}

	namespace Random
	{
		int seed;
		std::default_random_engine randomEngine;
		std::uniform_int_distribution<int> unifInt(0, std::numeric_limits<int>::max());

		std::default_random_engine& getDefaultRandomEngine()
		{
			return randomEngine;
		};

		void setSeedIfNotSetAlready(int s)
		{
			//flag
			static bool set = false;
			if (!set)
			{
				seed = s;
				randomEngine = std::default_random_engine(seed);
				set = true;
			}
		};

		int getRandInt(int from, int to, std::default_random_engine& engine)
		{
			int diff = to - from;
			return ((unifInt(engine) % diff) + from);
		}

		int getRandInt(int from, int to)
		{
			int diff = to - from;
			return ((unifInt(randomEngine) % diff) + from);
		};
	}
}