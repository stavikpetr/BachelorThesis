#pragma once

#include "Common.h"
#include "GASettings.h"
#include "Force.hpp"
#include "ChokePoint.h"
#include "OutputHandler.h"
#include "SearchExperiment.h"

namespace FormationsEvolution
{
	/**
	@ingroup Input
	@brief Class representing parser for input parsing
	*/
	class Parser
	{
		//splits string s at the delimiter char and places the individual
		//bits into the toFill argument 
		//copy on purpose
		void splitString(const std::string& s,
			std::vector<char> delimiters, std::vector<std::string>& toFill);

		//reads the whole file by lines and places them to the "toFill" argument
		void readFileByLines(std::ifstream& fileStream,
			std::vector<std::string>& toFill);
		
		//checks if the provided line is comment (they start with tuple "##")
		bool isComment(const std::string& line);

		//helper for returing name of the map from file
		std::string getMapName(const std::string& path);

		/** 
		a few helper stringToSomething methods used during parsing
		*/
		bool stob(const std::string& s);
		GA::SelectionType stoSelectionType(const std::string& s);
		GA::EvolutionType stoEvolutionType(const std::string& s);
		GA::CrossoverType stoCrossoverType(const std::string& s);

		/**
		large portion of input files depend on the used type of genetic algorithm;
		the following two methods handle the differences
		*/
		void parseRoundRobinEvolution(const std::vector<std::string>& lines, size_t& index);
		void parseCoEvolution(const std::vector<std::string>& lines, size_t& index);
	public:
		/// <summary> this method will parse the file that contains the settings
		///		of the genetic algorithm  </summary>
		void parseAndSetEvolutionSettings(const std::string& path);

		/// <summary> this method parses the file that contains the map </summary>
		std::unique_ptr<ChokePoint> parseAndSetChokepoint(
			const std::string& path,
			SparCraft::SearchExperiment& searchExperiment);

		
		/// <summary> this method will parse the player settings file </summary>
		void parseAndSetPlayer(
			const std::string& path,
			SparCraft::SearchExperiment& sparcraftExperiment,
			int playerID);

		/// <summary> this method will parse an input file that contains force </summary>
		void parseAndFillForce(const std::string& path, Force& toFill);


	};
}