#include "FormationsEvolutionExperiment.h"

/*! \mainpage My Personal Index Page

	\section architecture_sec Basic Architecture

	The architecture of this module is highly modular and follows the basic
	principles of object oriented programming. The basic architectural view
	stripped from disruptive details is presented below. The control flow
	is presented with black filled arrows and derived classes are presented with
	black empty arrows.

	\section extension_sec Extending the FormationsEvolver

	Extending this module should be, due to its modularity, fairly easy. 
	It typically involves deriving one of the predefined base classes,
	overriding one or two methods from its public interface and adding
	a few configuration steps that would for example 
	allow construction of the newly added type.

	In order to illustrate the extension capabilities, 
	let us consider adding an arbitrary new type of crossover - CrossoverX.
	As we would want to easily experiment with a different types of crossover,
	the first step is to allow the use of CrossoverX in the input configuration file.
	For that, we need to add an enumeration constant to the CrossoverType enum which 
	is located in a separate namespace along with other settings for the GA. 
	Now, we can modify the input parsing module to recognize CrossoverX in the configuration file.
	In the case of crossover operator, this means adding a new else if branch,
	that would, given a string identifier for CrossoverX, return our newly added constant of CrossoverType.

	With the new type of crossover recognized, we may now add a new class that will inheri
	t from the base crossover class - CrossoverPerformer. This new class has to override 
	the function void CrossoverPerformer::crossover(Chromosome& c1, Chromosome & c2) which is 
	called for performing crossover between two chromosomes. 
	The base class provides some basic functions that may be used to manipulate the positions
	of units - functionality that was used in the already implemented two point and one point crossover.

	The last step would be to pass an instance of this newly implemented class to GenerationGenerator
	in the phase of creating new instance of class GA_Base. Methods that implement this may be found
	in the GALayoutFinder. In our case, we are interested in method GALayoutFinder::getCrossoverPerformer().
	To this method, we simply need to add two lines of code for a new if else branch 
	that will return an instance of our newly added CrossoverX.

	Adding a new types of other operators, genetic algorithms or layout evaluators
	follows this basic idea as well and adding them should not be any harder.

*/

/**
*	@defgroup Experiment_core
	Classes that provide the skeleton for performing individual experiments.

	The flow of control is: FormationsEvolutionExperiment -> SingleExperiment -> GALayoutFinder
*/

/**
*	@defgroup GeneticAlgorithm
	This group represents classes that make the individual bits of the genetic algorithms.
*/


/**
*	@defgroup ChokePoint
	Classes related to map layout.
*/

/**
*	@defgroup Output
	Classes that provide varying output functionality
*/

/**
*	@defgroup Util
	Small light-weight utility classes used in different bits of the program
*/

/**
*	@defgroup Input
	Classes used for input parsing
*/



int main(int argc, char**argv)
{
	std::string inputFolder("InputSample/");
	std::string outputFolder("OutputSample/");
	std::string databaseNameFolder("");

	if (argc == 1)
	{
		//OK
	}
	else if (argc == 3)
	{
		inputFolder = std::string(argv[1]) + std::string("/");
		outputFolder = std::string(argv[2]) + std::string("/");
	}
	else
	{
		std::cout << "invalid number of arguments, please refer to documentation" << std::endl;
		std::cout << "Shutting down" << std::endl;
		return 1;
	}

	FormationsEvolution::FormationsEvolutionExperiment exp(
		CommonData::Functions::GetFormationsEvolverMainInputDirectory() + inputFolder,
		CommonData::Functions::GetFormationsEvolverMainOutputDirectory() + outputFolder);
	exp.runExperiments();
	return 0;
}