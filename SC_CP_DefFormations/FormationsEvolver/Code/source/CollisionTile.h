#pragma once

#include "Common.h"
#include "Unit.hpp"

namespace FormationsEvolution
{
	/**
	The whole state of this class is represented by the units currently
	contained within the collision tile. Collision tile is 50x50 tile.

	@ingroup ChokePoint
	@brief This class represents a single collision tile
	*/
	class CollisionTile
	{
		std::vector<Unit> units_;
	public:
		/// <summary> method that will add new unit to this collision tile </summary>
		void addUnit(const Unit& unit)
		{
			units_.push_back(unit);
		};

		/// <summary>method for retrieving the units held by this collision tile </summary>
		const std::vector<Unit>& getUnits() const
		{
			return units_;
		};

		/// <summary> this method will reset this collision tile </summary>
		void reset()
		{
			units_.clear();
		};

		/// <summary> this method will remove the unit with provided ID
		///		from this collision tile </summary>
		void removeUnit(IDType id)
		{
			for (auto it = units_.begin(); it != units_.end(); it++)
			{
				if (it->getID() == id)
				{
					units_.erase(it);
					break;
				}
			}
		};
	};
}