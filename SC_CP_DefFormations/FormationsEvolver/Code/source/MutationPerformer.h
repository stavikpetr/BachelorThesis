#pragma once

#include "Common.h"
#include "GASettings.h"
#include "Chromosome.hpp"
#include "CollisionTilesHolder.h"
#include "ChokePoint.h"
#include "OutputHandler.h"
#include "Generation.h"
#include <random>

namespace FormationsEvolution
{
	/**
	This class offers basic implementation of mutation operation for the genetic algorithm,
	but the provided methods may be easily overriden by a new implementation.
	The current implementation simply shifts each unit in some direction

	@ingroup GeneticAlgorithm
	@brief Base class for different implementations of mutation operation
	*/
	class MutationPerformer
	{		
		CollisionTilesHolder collisionTilesHolder_;
		std::normal_distribution<> nDistribution_;
		std::uniform_int_distribution<> uDistribution_;

		int getRandomAmountForMutation();
		void changeDistributionParams(double newSecondParam);

		//new top left and new bot right are sufficient to determine the number of free pixels and this solution doesn't require setting position of bumping unit
		int getNumberOfFreePixelsBetweenUnits(const Position& bumpingUnitNewTopLeft, 
			const Position& bumpingUnitNewBotRight, const Unit& standingUnit, const Position& direction);

		//how much can i move before stepping on nonDefender tile?
		int determineActualAmountFromTiles(int amount, const Position& direction, const Unit& unit,
			CommonData::MapValues playerTileType);
		int determineActualAmountFromUnits(int amount,  const Position& direction, const Unit& unit);
		
		void tryMoveUnit(int amount, const Position& direction, Unit& unit,
			CommonData::MapValues playerTileType);

		void checkMutationWasCorrect(const Chromosome& chromosome) const;
	protected:
		CHP_Observer chp_;
	public:
		/// <summary> Constructor of MutationPerformer that requires chokepoint
		///		in order to check the correct shifting of units</summary>
		///	<param name="chp"> passed instance of chokepoint </param>
		MutationPerformer(CHP_Observer chp);
		/// <summary> method that facilitates the mutation operation </summary>
		///	<param name="chromosome"> chromosome upon which the mutation will be performed </param>
		virtual void mutateChromosome(Chromosome& chromosome);
		/// <summary>method for initializing some internal parameters of the MutationPerformer </summary>
		virtual void initalizeParameters(int currentPlayerCounter);
		virtual ~MutationPerformer(){};
	};
}