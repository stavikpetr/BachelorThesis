#pragma once

#include "Common.h"
#include "GASettings.h"
#include "Chromosome.hpp"
#include "MutationPerformer.h"
#include "CrossoverPerformer.h"
#include "ChromosomeSelector.h"
#include "PlaguePerformer.h"
#include "ElitismPerformer.h"
#include "OutputHandler.h"

namespace FormationsEvolution
{
	/**
	This class performs the last step in the basic genetic algorithm - it creates a new generation based
	on the fitness of the current individual. Apart from the basic three operations - selection, mutation
	and crossover, it also supports two additional one - Elitism and Plague. Each operation is
	facilitated by its own class and GenerationGenerator is only the governor of these operations.

	@ingroup GeneticAlgorithm
	@brief Class that allows the generating of new generation after each iteration of genetic algorithm
	*/
	class GenerationGenerator
	{
		std::unique_ptr<ElitismPerformer> elitismPerformer_;
		std::unique_ptr<MutationPerformer> mutator_;
		std::unique_ptr<CrossoverPerformer> crossoverPerformer_;
		std::unique_ptr<ChromosomeSelector> chromosomeSelector_;
		std::unique_ptr<PlaguePerformer> plaguePerformer_;
		
		bool areChromosomesSimiliar(const Chromosome& first, const Chromosome& second) const;
		int insertChromosome(const Chromosome& toInsert, 
			std::vector<Chromosome>& newChromosomes, size_t oldGenerationSize) const;

	public:
		/// <summary> Constructor of the generation generator </summary>
		/// <param name="chromosomeSelector"> class that facilitates the selection operation </summary>
		///	<param name="mutationPerformer"> class that facilitates the mutation operation  </summary>
		///	<param name="crossoverPerformer"> class that facilitates the crossover operation  </summary>
		///	<param name="plaguePerformer">class that facilitates the plague operation  </summary>
		/// <param name="elitismPerformer"> class that facilitates the elitism operation  </param>
		GenerationGenerator(
			std::unique_ptr<ChromosomeSelector>&& chromosomeSelector,
			std::unique_ptr<MutationPerformer>&& mutationPerformer,
			std::unique_ptr<CrossoverPerformer>&& crossoverPerformer,
			std::unique_ptr<PlaguePerformer>&& plaguePerformer,
			std::unique_ptr<ElitismPerformer>&& elitismPerformer);

		/// <summary> method that will perform the stanard loop
		///		of genetic algorithm, that will create new generation
		///		based on the previous one; the old generation will be replaced
		///		with the new one </summary>
		void createNewGeneration(Generation& oldGeneration) const;

		/// <summary>As the class contains unique_ptrs, we need move constructor</summary>
		GenerationGenerator(GenerationGenerator&& other);
		/// <summary>As the class contains unique_ptrs, we need move assignments</summary>
		GenerationGenerator& operator=(GenerationGenerator&& other);
		
	};
}