#pragma once

#include "Common.h"
#include "Generation.h"
#include "GASettings.h"

namespace FormationsEvolution
{
	/**

	@ingroup Output
	@brief This class offers several specialized methods for verbose logging to file
	and console which are used by the OutputHandler
	*/
	class ConsVerbLogger
	{
		std::ostream* cout_;
		std::ofstream* verb_;
		int verb_log_count_;
		std::string log_verbose_baseString_;


		/**
		It is important to note, that the verbose output does not go to just one file, even if
		it could and the output is seperated into multiple files (this explains the following
		two methods)
		*/
		void closeIfOpen(std::ofstream* output);
		void openNewFiles();

		//print string to both console and file
		//why by value? look at the implementation, the calls are with temporaries
		void printStrings(std::vector<std::string> linesToPrint, bool consLog = false);

	public:
		ConsVerbLogger();

		/// <summary> as we are resuing the instance of Console logger, we
		///		just set some new parameters when we have new experiment </summary>
		void setNewParams(const std::string& newOutputDir);

		/**
		these methods basically mirror methods that outputHandler exposes
		to other code; see comments in outputHandler for further description
		of their functionality, but they basically just output some information
		during the run of the genetic algorithm
		*/
		void logSuccessfulChromosomeKills(int howMany);
		void logPlague();
		void logMutationDistributionSecondParamChange(int secondParam);
		void logGenerationAfterIteration(const Generation& gen);
		void logNewExperiment();
		void logEndOfExperiment();
		void logNewChromosomeEvaluation();
		void logPerformingComparingExperiment();		
		void logComparingExperimentEnded();
		void logNewGAIteration(int currentIteration);
		void logScenarioEvalDone(int achievedFitness);
		void logLayoutEvalDone(int totalFitness);
		void logNewGenerationEval(int playerId);

	};
}