#include "FormationsEvolutionExperiment.h"
#include <ctime>

using namespace FormationsEvolution;

FormationsEvolutionExperiment::FormationsEvolutionExperiment(
	std::string inputDirectory, 
	std::string outputDirectory) :
	inputDir_(inputDirectory)
{	
	std::string s(inputDirectory.substr(0, inputDirectory.size() - 1));
	  
	size_t i(0);
	for (size_t j(s.size()); j > 0; --j)
	{
		if (s[j] != '/')
			i++;
		else
			break;
	}
	s = s.substr(s.size() - i + 1, s.size());

	//initialize few basic fields of the logger
	OutputHandler::getInstance().initialize(outputDirectory, s);
};

void FormationsEvolutionExperiment::readPaths(const std::string& dir, std::vector<std::string>* toFill)
{
	for (boost::filesystem::directory_iterator it(inputDir_ + dir);
		it != boost::filesystem::directory_iterator(); it++)
	{
		toFill->push_back(boost::filesystem::path((*it)).string());
	}
}

/// <summary> One may wonder about the implementation of this loop, specifically
/// about the nested for loops. The reason for this is to allow execution of
/// multiple experiments in one run of the program.
///
///	This method will basically iterate through every combination of EvolutionSettings, Maps and
/// Configuration of players with their units 
///	and performs a single experiment for each such combination </summary>
void FormationsEvolutionExperiment::runExperiments()
{	
	//string constants that respect the current input directory structure
	std::vector<std::string> dirs({ "/EvolutionSettings", "/Maps", "/Players/Defender",
		"/Players/Attacker", "/Forces/Defender", "/Forces/Attacker" });

	//for the whole set of experiments, we need only one sparcraft experiment (we will be reusing the objects in order)
	//to speed up the simulation
	SparCraft::SearchExperiment s_exp(
		CommonData::Functions::GetSparcraftAssetsDirectory(), 1);

	std::vector<std::string> evol_paths, chps_paths, pdef_paths, patt_paths, fdef_paths, fatt_paths;

	std::vector<std::vector<std::string>*> tmpPathsHolder({ &evol_paths, &chps_paths, &pdef_paths,
		&patt_paths, &fdef_paths, &fatt_paths });

	//now, lets prepare each path for evolution settings, chokepoint, defender player and so on
	for (size_t i(0); i < dirs.size(); ++i)
	{
		readPaths(dirs[i], tmpPathsHolder[i]);
	}

	Force def_force, att_force;

	//lets now go through each combination of evolution settings, chokepoint,
	//attacker and defender player, and attacker and defender force
	//other ways around this ugly loop would be even more ugly 
	//because i need to go through each state and in each state change i need to
	//execute different function

	//for each evolution settings
	for (auto& ev : evol_paths)
	{
		parser_.parseAndSetEvolutionSettings(ev);
		//for each chokepoint
		for (auto& chp : chps_paths)
		{
			std::unique_ptr<ChokePoint> chp_p = parser_.parseAndSetChokepoint(chp, s_exp);
			//for each defender player
			for (auto& pdef : pdef_paths)
			{
				parser_.parseAndSetPlayer(pdef, s_exp, General::DEFENDER_ID);
				//for each attacker player
				for (auto& patt : patt_paths)
				{
					parser_.parseAndSetPlayer(patt, s_exp, General::ATTACKER_ID);
					//for each defender force
					for (auto& fdef : fdef_paths)
					{
						parser_.parseAndFillForce(fdef, def_force);
						//for each attacker force
						for (auto& fatt : fatt_paths)
						{
							parser_.parseAndFillForce(fatt, att_force);

							//EVALUATE
							{
								SingleExperiment exp(chp_p.get(), def_force, att_force);
								exp.performSingleExperiment(s_exp);
							}
						}
					}
				}
			}
		}
	}
}
