#pragma once

#include "Common.h"

namespace FormationsEvolution
{
	/**
	This namespace contains important global settings for the genetic algorithm
	*/
	namespace GA
	{
		//enum for different types of selection
		enum SelectionType {
			TOURNAMENT, ROULETTEWHEEL
		};
		//enum for different crossover types
		enum CrossoverType {
			ONEPOINT, TWOPOINT
		};
		//enum for family of genetic algorithms
		enum EvolutionType {
			COEVOLUTION, ROUNDROBIN
		};
		//enum for mutation distribution used during mutation
		enum MutationDistribution {
			NORMAL, UNIFORM
		};
		//enum for different types of mutations
		enum MutationType {
			CONSTANT, DECAY, PLAGUEPEEK
		};

		/**
		several helpers methods that are able to transform the genetic algorithm
		specific enums to strings 
		*/
		std::string selectionTypeEnumToString(SelectionType t);
		std::string crossoverEnumToString(CrossoverType t);
		std::string evolutionTypeToString(EvolutionType t);
		std::string mutationDistributionToString(MutationDistribution t);
		std::string mutationTypeToString(MutationType t);
		//bool to string method
		std::string btos(bool b);

		/**
		below are variety of constants that together
		form current settings of the genetic algorithm
		(they are set in parser when parsing new experiment)
		*/
		extern int maximumNumberOfIterations;

		//mutation related constants
		extern double mutationRate;
		extern int mutationInitValue;
		extern MutationDistribution mutationDistribution;
		extern MutationType mutationType;

		extern int decayToValue;
		extern int decayToTurns;

		extern int plaguePeekAmount;
		extern int plaguePeekDecayTurns;

		//crossover related constants
		extern CrossoverType crossoverType;
		extern double crossoverRate;

		//elitism related constants
		extern double elitismPercentage;
		extern bool elitism;

		//selection related constants
		extern SelectionType selectionType;

		//plague related constants
		extern bool plague;
		extern double plaguePercentage;
		extern int plagueStartIteration;
		extern int plagueRepetitionCounter;

		//specific constants for each family of genetic algorithms
		extern EvolutionType evolutionType;

		extern int defPopulationSize;
		extern int alt_attPopulationSize;
		extern int alt_optSwap_turns_d;
		extern int alt_optSwap_turns_a;

		extern int rr_numOfEnemyForces;

		void outputGASettings(std::ofstream* out);
	}
}