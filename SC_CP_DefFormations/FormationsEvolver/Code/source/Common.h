#pragma once

#include "BWAPI.h"
#include "CommonData.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <memory>
#include <random>

namespace FormationsEvolution
{
	/**
	This namespace contains a few important global settings
	*/
	namespace General
	{
		//these are ids for sparcraft
		const int DEFENDER_ID = 0;
		const int ATTACKER_ID = 1;
		const int collisionTileSize = 50;
		const int walkTileSize = 8;
	}

	/**
	This namespace contains random related functions and variables
	*/
	namespace Random
	{
		extern std::default_random_engine randomEngine;
		extern int seed;
		extern std::uniform_int_distribution<int> unifInt;
		void setSeedIfNotSetAlready(int s);
		std::default_random_engine& getDefaultRandomEngine();
		//to is exclusive
		int getRandInt(int from, int to);
		int getRandInt(int from, int to, std::default_random_engine& engine);

	}

	/**
	System namespace for functionality like reporting fatal errors
	*/
	namespace System
	{
		void FatalError(const std::string& errorMessage);
	}

}