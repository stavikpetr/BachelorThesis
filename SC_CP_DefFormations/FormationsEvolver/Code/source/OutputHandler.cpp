#include "OutputHandler.h"

using namespace FormationsEvolution;

OutputHandler::OutputHandler()
{}

std::string OutputHandler::getCurrentDateAndTimeString()
{
	/*localtime_s is microsoft implementation, localtime_r unix*/
	time_t currentTime;
	struct tm localTime;
	time(&currentTime);
#ifdef _WIN32
	localtime_s(&localTime, &currentTime);
#else
	localtime_r(&currentTime, &localTime);
#endif

	int Day = localTime.tm_mday;
	int Month = localTime.tm_mon + 1;
	int Year = localTime.tm_year + 1900;
	int Hour = localTime.tm_hour;
	int Min = localTime.tm_min;
	int Sec = localTime.tm_sec;

	return std::string(std::to_string(Day) + "_" + std::to_string(Month) + "_" + std::to_string(Year) + "__" +
		std::to_string(Hour) + "_" + std::to_string(Min) + "_" + std::to_string(Sec));
}

std::string OutputHandler::getUnitShortenedName(BWAPI::UnitType unitType)
{
	std::string acutalName(unitType.getName());
	std::string shortened;

	shortened += acutalName[0];
	for (size_t i(0); i < acutalName.size(); ++i)
	{
		if (acutalName[i] == '_')
			shortened += acutalName[i + 1];
	}
	return shortened;
}


void OutputHandler::initialize(const std::string& baseOutputDir,
	const std::string& inputFolderName)
{
	baseOutputDir_ = baseOutputDir;
	curIDString_ = inputFolderName;
}

void OutputHandler::setNewPlayer(int playerId, const std::string& playerString)
{
	//change the defender or attacker string based on the provided id
	if (playerId == General::DEFENDER_ID)
		defenderString_ = playerString;
	else if (playerId == General::ATTACKER_ID)
		attackerString_ = playerString;
}

void OutputHandler::outputUnits(const std::vector<Unit>& units,
	int playerId, std::ofstream* out, bool db)
{
	//outputs unit with its position
	for (auto& u : units)
	{
		BWAPI::UnitType type(u.getType());
		(*out) << type.toString();
		if (!db)
			(*out) << " " << std::to_string(playerId);
		(*out) << " " << std::to_string(u.getXPosition()) << " " << std::to_string(u.getYPosition());
		(*out) << std::endl;
	}
}

void OutputHandler::outputForce(const Force& f, std::ofstream* out)
{
	//outputs pairs #ofUnits unitType
	for (auto& fp : f.getForce())
	{
		(*out) << std::to_string(fp.second);
		(*out) << " ";
		(*out) << BWAPI::UnitType(fp.first).toString();
		(*out) << " ";
	}
	(*out) << std::endl;
}

OutputHandler& OutputHandler::getInstance()
{
	static OutputHandler outputHandler;
	return outputHandler;
}

void OutputHandler::newExperiment(const Force& defenderForce,
	const Force& attackerForce, CHP_Observer chp)
{
	
	//this method will initialize several important fields when new experiment
	// is being launched and it also creates important folder structure
	//of the output folder

	//first, create identifier of the experiment, which is in the form of
	//InputFolderName_CurrentTimeAndDate_DefenderUnitsShortened_AttackerUnitsShortened
	//it is complicated, but vital for differentianting many experiments launched
	//at similar times
	curIDString_ += "_"+getCurrentDateAndTimeString() + "__";

	std::string unitsName;
	for (auto& unit : defenderForce.getForce())
	{
		unitsName += std::to_string(unit.second);
		unitsName += getUnitShortenedName(unit.first);
		unitsName += "_";
	}

	for (auto& unit : attackerForce.getForce())
	{
		unitsName += "_";
		unitsName += std::to_string(unit.second);
		unitsName += getUnitShortenedName(unit.first);
	}

	curIDString_ += unitsName;
	
	std::string newOutputDir = baseOutputDir_ + "/" + curIDString_;

	//now, in the output directory, create a new folder with the newly
	//formed experiment id
	boost::filesystem::path newDirectory(newOutputDir);
	boost::system::error_code returnedError;
	boost::filesystem::create_directory(newDirectory, returnedError);

	if (returnedError)
		System::FatalError("Outputhandler::newExperiment(), unable to create directory");

	//let's output all of the most important settings into some file
	std::ofstream out(newOutputDir + "/" + "exp_settings.txt");

	out << "MapName: " << chp->getMapName() << std::endl;
	out << "ChokePointCoords x: " << std::to_string(chp->getCenterX())
		+ " y: " << std::to_string(chp->getCenterY()) << std::endl;
	out << "Defender: " << defenderString_ << std::endl;
	out << "Attacker: " << attackerString_ << std::endl;
	out << "DefForce: ";
	outputForce(defenderForce, &out);
	out << "AttForce: ";
	outputForce(attackerForce, &out);

	out << std::endl;
	out << " ---- Evol Settings ---- " << std::endl;
	GA::outputGASettings(&out);
	out.close();

	//now, create new directory that will contain the results of the final
	//layout comparing experiment which will test the found layout (see FinalLayoutTester)
	layoutCmpResultDir_ = newOutputDir + "/" + "layoutCmp";
	newDirectory = layoutCmpResultDir_;
	boost::filesystem::create_directory(newDirectory, returnedError);
	if (returnedError)
		System::FatalError("Outputhandler::newExperiment(), unable to create directory");

	//now, create directory for logging of the genetic algorithm run
	logDir_ = newOutputDir + "/" + "logs";
	newDirectory = logDir_;
	boost::filesystem::create_directory(newDirectory, returnedError);
	if (returnedError)
		System::FatalError("Outputhandler::newExperiment(), unable to create directory");

	//now, create directory that will contain the input for graphs
	graphDir_ = newOutputDir + "/" + "graphs";
	newDirectory = graphDir_;
	boost::filesystem::create_directory(newDirectory, returnedError);
	if (returnedError)
		System::FatalError("Outputhandler::newExperiment(), unable to create directory");


	//now create directory that will contain the resulting layout
	finalLayoutDir_ = newOutputDir + "/" + "res";
	newDirectory = finalLayoutDir_;
	boost::filesystem::create_directory(newDirectory, returnedError);
	if (returnedError)
		System::FatalError("OutputHandler::newExperiment(), unable to create directory");


	//and finally, set new parameters for the specialized loggers
	plotLogger_.setNewParams(graphDir_);
	consVerbLogger_.setNewParams(logDir_);

}

void OutputHandler::logPerformingComparingExperiment()
{
	consVerbLogger_.logPerformingComparingExperiment();
}
void OutputHandler::logComparingExperimentEnd()
{
	consVerbLogger_.logComparingExperimentEnded();
}
void OutputHandler::logSuccessfulChromosomeKills(int howMany)
{
	consVerbLogger_.logSuccessfulChromosomeKills(howMany);
}
void OutputHandler::logPlague()
{
	consVerbLogger_.logPlague();
}
void OutputHandler::logMutationDistributionSecondParamChange(int secondParam)
{
	consVerbLogger_.logMutationDistributionSecondParamChange(secondParam);
}
void OutputHandler::logNewExperiment()
{
	consVerbLogger_.logNewExperiment();
}
void OutputHandler::logEndOfExperiment()
{
	consVerbLogger_.logEndOfExperiment();
}
void OutputHandler::logNewChromosomeEvaluation()
{
	consVerbLogger_.logNewChromosomeEvaluation();
}
void OutputHandler::logNewGAIteration(int currentIteration)
{
	consVerbLogger_.logNewGAIteration(currentIteration);
}
void OutputHandler::logScenarioEvalDone(int achievedFitness)
{
	consVerbLogger_.logScenarioEvalDone(achievedFitness);
}
void OutputHandler::logLayoutEvalDone(int totalFitness)
{
	consVerbLogger_.logLayoutEvalDone(totalFitness);
}
void OutputHandler::logNewGenerationEval(int playerId)
{
	consVerbLogger_.logNewGenerationEval(playerId);
}

void OutputHandler::logGenerationAfterIteration(const Generation& gen)
{
	consVerbLogger_.logGenerationAfterIteration(gen);
	plotLogger_.logGenerationAfterIteration(gen);
}

void OutputHandler::outputRRAttackerUnitLayout(const UnitLayout& l, int num)
{
	std::ofstream out(logDir_ + "/" + "attInitialLayout" + std::to_string(num) + ".txt");
	outputUnits(l.getUnits(), General::ATTACKER_ID, &out);
	out.close();
}

void OutputHandler::outputProbeInfrastructure(const UnitLayout& probeLayout)
{
	std::ofstream out(logDir_ + "/" + "probeInfr.txt");
	outputUnits(probeLayout.getUnits(), General::DEFENDER_ID, &out);
	out.close();	
}

void OutputHandler::outputFinalLayout(const UnitLayout& unitLayout)
{
	std::ofstream out1(finalLayoutDir_ + "/" + "defFinalLayout_SC.txt");
	outputUnits(unitLayout.getUnits(), General::DEFENDER_ID, &out1);
	out1.close();
}

void OutputHandler::logOptSwap(const UnitLayout& optimizedAgainst, int optimizedAgainstPlayer,
	const UnitLayout& producedLayout, int producedLayoutPlayer)
{
	static int logNumber = 0;
	logNumber++;
	std::ofstream out(logDir_ + "/" + "optSwapLog" + std::to_string(logNumber)+ ".txt");

	out << "optimized against layout: " << std::endl;
	outputUnits(optimizedAgainst.getUnits(), optimizedAgainstPlayer,
		&out);
	
	out << " --- " << std::endl;
	out << "achieved layout: " << std::endl;
	outputUnits(producedLayout.getUnits(), producedLayoutPlayer, &out);
	out.close();
}


void OutputHandler::outputExperimentTestResult(const UnitLayout& finalDefenderLayout,
	const std::vector<int>& testFitnesses,
	const std::vector<UnitLayout>& againstLayouts,
	const std::vector<UnitAlteration>& unitAlterations)
{
	//first, output the fitnesses
	std::ofstream out(layoutCmpResultDir_ + "/" + "defFinalLayout.txt");

	outputUnits(finalDefenderLayout.getUnits(), General::DEFENDER_ID, &out);
	out.close();

	std::ofstream out2(layoutCmpResultDir_ + "/" + "fitnesses.txt");

	for (size_t i(0); i < testFitnesses.size(); ++i)
	{
		out2 << std::to_string(testFitnesses[i]);
		if (i == testFitnesses.size() - 1)
			out2 << std::endl;
		else
			out2 << ",";
	}
	out2.close();

	for (size_t i(0); i < againstLayouts.size(); ++i)
	{
		std::ofstream out3(layoutCmpResultDir_ + "/" + "attackerTestLayout_" + std::to_string(i) + ".txt");
		outputUnits(againstLayouts[i].getUnits(), General::ATTACKER_ID, &out3);
		out3.close();
	}

	for (size_t i(0); i < unitAlterations.size(); ++i)
	{
		std::ofstream out3(layoutCmpResultDir_ + "/" + "unitAlteration_" + std::to_string(i) + ".txt");
		outputUnits(unitAlterations[i].getUnitLayout().getUnits(), unitAlterations[i].getPlayerID(), &out3);
		out3.close();
	}
	
}