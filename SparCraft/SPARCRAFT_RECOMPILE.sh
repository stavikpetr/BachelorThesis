#!/bin/sh

cd $SPARCRAFT_DIR

echo "doing clean up"
rm -f source/SparCraft/*.o
rm -f source/SparCraft_experiment/*.o
rm -f source/SparCraft_main/*.o
rm -f dll/*.so
rm -f bin/SparCraft_main

echo "compiling module SparCraft"
#first i will compile module source/SparCraft
cd source/SparCraft

g++ -std=c++14 -O3 -fPIC -c *.cpp -I../SparCraft_experiment -I$BWAPI_DIR/include -I.

echo "compiling SparCraft_experiment"

cd ../SparCraft_experiment
g++ -std=c++14 -O3 -c -fPIC *.cpp -I$BWAPI_DIR/include -I../SparCraft -I.

echo "buidling shared library SparCraft_experiment.so"

g++ -std=c++14 -shared *.o $BWAPI_DIR/BWAPILIB/*.o ../SparCraft/*.o -o libSparCraft_experiment.so
mv libSparCraft_experiment.so ../../dll

echo "compiling module SparCraft_main"

cd ../SparCraft_main
g++ -std=c++14 *.cpp -O3 -Wall -I$BWAPI_DIR/include -I../SparCraft_experiment/ -I../SparCraft/ -lSparCraft_experiment -L$SPARCRAFT_DIR/dll -o SparCraft_main
mv SparCraft_main ../../bin

echo "doing after clean up"
cd ../
rm -f SparCraft/*.o
rm -f SparCraft_experiment/*.o
rm -f SparCraft_main/*.o


