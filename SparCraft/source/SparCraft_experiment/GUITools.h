#pragma once

#include "Common.h"

#include "Position.hpp"
#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#endif

namespace SparCraft
{
	namespace GUITools
	{
		const int FLIP_VERTICAL = 1;
		const int FLIP_HORIZONTAL = 2;

		void DrawLine(const Position & p1, const Position & p2, const float thickness, const float * rgba);
		void DrawString(const Position & p, const std::string & text, const float * rgba);
		void DrawChar(const Position & tl, const Position & br, char ch, const float * rgba);
		void DrawCircle(const Position & p, float r, int num_segments);
		void DrawTexturedRect(const Position & tl, const Position & br, const int & textureID, const float * rgba);
		void DrawRect(const Position & tl, const Position & br, const float * rgba);
		void DrawRectGradient(const Position & tl, const Position & br, const float * rgbaLeft, const float * rgbaRight);
		void SetColor(const float * src, float * dest);
		void SetColor(const float * src, float * dest);
	}
}
