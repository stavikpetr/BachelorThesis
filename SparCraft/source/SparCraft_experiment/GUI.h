#pragma once

#include "BWAPI.h"
#include "GUITools.h"
#include "Timer.h"
#include "Position.hpp"
#include "GUIGame.h"
#include <vector>

#ifdef _WIN32
#include <SDL.h>
#endif
#undef main

#ifdef _WIN32
#include <SDL_opengl.h>
#include <SDL_image.h>
#endif
#include "GUITools.h"
#include <sys/stat.h>

namespace SparCraft
{

	class GUI
	{
		int                 _initialWidth;
		int                 _initialHeight;
		int                 _windowSizeY;
		int                 _cameraX;
		int                 _cameraY;
		bool                _isStarted;
		int                 _previousMouseX;
		int                 _previousMouseY;
		bool                _mousePressed;
		bool                _shiftPressed;
		double              _previousRenderTime;

		size_t              _currentFrame;
#ifdef _WIN32
		SDL_Window *        _window;
		SDL_Surface *       _surface;
		SDL_GLContext       _glcontext;
#endif

		GUIGame             _guiGame;
		std::string			imageDir_;
#ifdef _WIN32
		std::vector<GLuint>     _textures;
#endif
		std::vector<Position>   _textureSizes;

		std::map<BWAPI::UnitType, int> _unitTypeTextureID;
		std::map<BWAPI::TechType, int> _techTypeTextureID;
		std::map<BWAPI::UpgradeType, int> _upgradeTypeTextureID;

		void handleEvents();
		void render();
		void renderTextGlut(int x, int y, std::string & s);
		void loadTextures();
		bool loadTexture(int textureNumber, const std::string & fileName);
		void drawAllBWAPIUnits();

		void onStart();
		void testRender();

		bool isStarted() const;

		static std::string GetTextureFileName(const BWAPI::UnitType & type);
		static std::string GetTextureFileName(const BWAPI::UpgradeType & type);
		static std::string GetTextureFileName(const BWAPI::TechType & type);

	public:

		static const int TextureASCIIOffset;
		static const int TextureFont;

		GUI(int width, int height, std::string& imageDir);

		int width();
		int height();

		void onFrame();
		void setCenter(int x, int y);
		void setGame(const Game & game);
		void drawUnitType(const BWAPI::UnitType & type, const Position & p, bool isInvisible);
		const int cameraX() const;
		const int cameraY() const;

		const Game & getGame() const;

		bool saveScreenshotBMP(const std::string & filename);
	};
}