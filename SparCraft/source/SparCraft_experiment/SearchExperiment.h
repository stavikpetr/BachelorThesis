#pragma once

#include <iomanip>
#include "SparCraft.h"

#ifdef _WIN32
	#ifdef SPARCRAFT_EXPERIMENT_EXPORTS
		#define SPARCRAFT_EXPERIMENT_API __declspec(dllexport)
	#else
		#define SPARCRAFT_EXPERIMENT_API __declspec(dllimport)
	#endif
#else
	#define SPARCRAFT_EXPERIMENT_API
#endif
 
namespace SparCraft
{
	class SearchExperiment;
}

typedef std::vector<std::string> sv;
typedef std::vector<sv> svv;
typedef std::vector<int> iv;
typedef std::vector<iv> ivv;
typedef std::vector<ivv> ivvv;
typedef std::vector<double> dv;
typedef std::vector<dv> dvv;
typedef std::vector<dvv> dvvv;

class TranspositionTable;

namespace SparCraft
{
	class GameState;
	class Game;

	struct SparCraftExperimentResult
	{
		int totalTime_;
		std::vector<std::vector<std::pair<BWAPI::UnitType, int>>> playerUnits_;
		std::vector<std::pair<int, int>> infrastructureDamage_;
		SparCraftExperimentResult()
		{
			playerUnits_.push_back(std::vector<std::pair<BWAPI::UnitType, int>>());
			playerUnits_.push_back(std::vector<std::pair<BWAPI::UnitType, int>>());
		};
	};

	class SearchExperiment
	{
		int                         _numOfTriesInLayout;
		std::vector<PlayerPtr>      players[2];
		std::vector<std::string>    playerStrings[2];
		std::vector<GameState>      states;
		bool                        showDisplay;

		std::unique_ptr<Map>		map_;
		std::unique_ptr<PathFinder> pathFinder_;
		std::unique_ptr<CollisionResolver> collisionResolver_;


		//std::vector<std::vector<std::pair<BWAPI::UnitType, int>>> toExtract;
		SparCraftExperimentResult experimentResultToExtract_;

		std::string                 resultsFile;
		bool                        appendTimeStamp;
		std::string                 timeString;
		std::string                 configFileFull;
		std::string                 configFileSmall;
		std::string                 imageDir_;
		int							displaySleepAmount_;

		iv                          resultsPlayers[2];
		ivvv                        resultsStateNumber;
		ivvv                        resultsNumUnits;
		ivvv                        resultsEval;
		ivvv                        resultsRounds;
		dvvv                        resultsTime;
		ivv                         numGames;
		ivv                         numWins;
		ivv                         numLosses;
		ivv                         numDraws;

		RandomInt					rand;

		void parseConfigFile(const std::string & filename);
		void addPlayer(const std::string & line);

		void addState(const std::string & line);
		void parseStateDescriptionFile(const std::string & fileName);
		void   addSeparatedState(std::vector<std::string> & unitTypes, std::vector<int> & numUnits,
			const int cx1, const int cy1, const int cx2, const int cy2,
			const PositionType & xLimit, const PositionType & yLimit);
		GameState   getSymmetricState(std::vector<std::string> & unitTypes, std::vector<int> & numUnits,
			const PositionType & xLimit, const PositionType & yLimit);
		GameState getStateAligned(int p1BX, int p1BY, int p2BX, int p2BY, bool horizontal, bool randomness,
			std::vector<std::string>& p1UnitVec, std::vector<int>& p1NumUnits,
			std::vector<std::string>& p2UnitVec, std::vector<int>& p2NumUnits);
		bool isUnitAddable(GameState& state, BWAPI::UnitType unitType, PositionType possibleX, PositionType possibleY);
		bool tryAddUnit(GameState& state, IDType playerID, BWAPI::UnitType unitType, PositionType posX, PositionType posY);
		BWAPI::UnitType getUnitType(const std::string & unitTypeString);
		void addGameState(const GameState & state);

		void parseSimulationSettings(const std::string& line);
		void setupResults();
		void writeResultsSummary();
		void writeConfig(const std::string & configfile);
		void padString(std::string & str, const size_t & length);
		void splitString(const std::string& s, char delimiter, std::vector<std::string>& toFill);
		std::string getResultsSummaryFileName();
		std::string getResultsOutFileName();
		std::string getConfigOutFileName();
		std::string getDateTimeString();
		void setCurrentDateTime();
		svv getExpDescription(const size_t & p1, const size_t & p2, const size_t & state);
		std::vector<std::string> getLines(const std::string & filename);
		std::string getBaseFilename(const std::string & filename);
		void printStateUnits(std::ofstream & results, GameState & state);
		void readFileByLines(std::ifstream& fileStream, std::vector<std::string>& toFill);
		void runExperimentInitialization();
		void prepareStateResult(const GameState& finishedState);
	public:

		SPARCRAFT_EXPERIMENT_API SearchExperiment(const std::string & configFile);
		SPARCRAFT_EXPERIMENT_API SearchExperiment(std::string&& imageDir, int numberOfStates);

		SPARCRAFT_EXPERIMENT_API void constructNewStates(int numberOfStates);
		SPARCRAFT_EXPERIMENT_API void setNewPlayer(std::string& playerString, int playerId);
		SPARCRAFT_EXPERIMENT_API void setNewMap(std::vector<std::string>::iterator begin, std::vector<std::string>::iterator end);
		SPARCRAFT_EXPERIMENT_API void setNewDisplaySettings(bool display, int displaySleepAmount = 60);
		SPARCRAFT_EXPERIMENT_API void setNewSimulationSettings(bool invisibility, bool collisions);
		SPARCRAFT_EXPERIMENT_API void setNewUnits(std::vector<std::pair<BWAPI::UnitType, Position>> & firstPlayerUnits, int firstPlayerId,
			std::vector<std::pair<BWAPI::UnitType, Position>>& secondPlayerUnits, int secondPlayerId);
		SPARCRAFT_EXPERIMENT_API SparCraftExperimentResult extractExperimentResult();

		SPARCRAFT_EXPERIMENT_API void runExperiment();
	};
}
