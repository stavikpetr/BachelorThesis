#include "SearchExperiment.h"
#include "GUI.h"

#include <thread>
#include <chrono>

using namespace SparCraft;

//-----------------------------
//-----
//-----    PUBLIC API
//-----
//-----------------------------

SearchExperiment::SearchExperiment(const std::string & configFile) : showDisplay(false), appendTimeStamp(true),
rand(0, std::numeric_limits<int>::max()),
_numOfTriesInLayout(10000), displaySleepAmount_(45)
{
	SparCraft::init();
	configFileSmall = getBaseFilename(configFile);
	parseConfigFile(configFile);
}

SearchExperiment::SearchExperiment(std::string&& imageDir, int numberOfStates) : imageDir_(std::move(imageDir)),
rand(0, std::numeric_limits<int>::max()), _numOfTriesInLayout(10000), displaySleepAmount_(45)
{
	SparCraft::init();
	for (int t(0); t < numberOfStates; ++t)
	{
		states.push_back(GameState());
	}
}

void SearchExperiment::constructNewStates(int numberOfStates)
{
	states.clear();
	for (int t(0); t < numberOfStates; ++t)
	{
		states.push_back(GameState());
	}
}

void SearchExperiment::setNewPlayer(std::string& playerString, int playerId)
{
	players[playerId].clear();
	addPlayer(std::string("Player " + std::to_string(playerId) + " " + playerString));
}

void SearchExperiment::setNewDisplaySettings(bool display, int displaySleepAmount)
{
	showDisplay = display;
	displaySleepAmount_ = displaySleepAmount;
}

void SearchExperiment::setNewMap(std::vector<std::string>::iterator begin, std::vector<std::string>::iterator end)
{
	//first line contains width in WALKTILES and height in walkTiles, so for example 50 22
	std::vector<std::string> toFill;
	splitString(*(begin), ' ', toFill);
	int widthWalkTilesX(std::stoi(toFill[0]));
	int heightWalkTilesY(std::stoi(toFill[1]));
	//actual map width and map size is in 1x1 tiles
	Constants::General::setSimulationParametersMap(widthWalkTilesX * Constants::Map::walkTileSize, heightWalkTilesY * Constants::Map::walkTileSize);
	Constants::General::setSimulationParametersGui(widthWalkTilesX * Constants::Map::walkTileSize, heightWalkTilesY * Constants::Map::walkTileSize);

	begin++;
	map_ = std::make_unique<Map>();
	size_t c(0);
	while (begin != end)
	{
		for (size_t t(0); t < (*begin).size(); ++t)
		{
			bool walkable = (*begin)[t] == '0' ? false : true;
			map_->setWalkableBaseTile(t, c, walkable);
		}
		begin++;
		c++;
	}
	pathFinder_ = std::make_unique<PathFinder>(map_.get());
	collisionResolver_ = std::make_unique<CollisionResolver>();
}

void SearchExperiment::setNewSimulationSettings(bool invisibility, bool collisions)
{
	Constants::General::setSimulationParametersCollisionsInvisibility(collisions, invisibility);
}

SparCraftExperimentResult SearchExperiment::extractExperimentResult()
{
	return experimentResultToExtract_;
}


void SearchExperiment::setNewUnits(std::vector<std::pair<BWAPI::UnitType, Position>> & firstPlayerUnits, int firstPlayerId,
	std::vector<std::pair<BWAPI::UnitType, Position>>& secondPlayerUnits, int secondPlayerId)
{
	for (size_t t(0); t < states.size(); ++t)
	{
		states[t].clearUnits(firstPlayerId);
		states[t].clearUnits(secondPlayerId);
		for (size_t i(0); i < firstPlayerUnits.size(); ++i)
		{
			states[t].addUnit(firstPlayerUnits[i].first, firstPlayerId, firstPlayerUnits[i].second);
		}
		for (size_t i(0); i < secondPlayerUnits.size(); ++i)
		{
			states[t].addUnit(secondPlayerUnits[i].first, secondPlayerId, secondPlayerUnits[i].second);
		}
	}

}

void SearchExperiment::prepareStateResult(const GameState& finishedState)
{
	experimentResultToExtract_.totalTime_ = finishedState.getTime();
	experimentResultToExtract_.playerUnits_[0].clear();
	experimentResultToExtract_.playerUnits_[1].clear();
	
	for (size_t t(0); t < finishedState.numUnits(0); ++t)
	{
		auto& u = finishedState.getUnit(0, t);
		experimentResultToExtract_.playerUnits_[0].push_back(std::pair<BWAPI::UnitType, int>(u.type(), u.currentHP()));
	}

	for (size_t t(0); t < finishedState.numUnits(1); ++t)
	{
		auto& u = finishedState.getUnit(1, t);
		experimentResultToExtract_.playerUnits_[1].push_back(std::pair<BWAPI::UnitType, int>(u.type(), u.currentHP()));
	}
	experimentResultToExtract_.infrastructureDamage_ = finishedState.getInfrastructureHits();
}


//-----------------------------
//-----
//-----    RUNING EXPERIMENT
//-----
//-----------------------------

void SearchExperiment::runExperimentInitialization()
{
	// set the map file for all states
	if (!map_)
		System::FatalError("map was not set before runningExperiment");

	for (size_t state(0); state < states.size(); ++state)
	{
		states[state].setMap(map_.get());
		states[state].setPathFinder(pathFinder_.get());
		states[state].setCollisionResolver(collisionResolver_.get());
		//after I've set the map, the units i need to now create new collisionTilesHolder inside gameState and also insert units
		//into it
		states[state].insertUnitsIntoTiles();
	}
	//now, for one state lets check that the units are on walkable tiles (we don't have to check for every single unit...)
	if (states.size()>0)
		states[0].checkUnitsAreOnWalkableTiles();
}



void SearchExperiment::runExperiment()
{
	runExperimentInitialization();

	// for each player one player
	for (size_t p1Player(0); p1Player < players[0].size(); p1Player++)
	{
		// for each player two player
		for (size_t p2Player(0); p2Player < players[1].size(); p2Player++)
		{
			// for each state we care about
			for (size_t state(0); state < states.size(); ++state)
			{

				// get player one
				PlayerPtr playerOne(players[0][p1Player]);

				//i need to refresh bool for formatioon policy after each state
				if (playerOne->getFormation() != nullptr)
					playerOne->setFormationPolicy(true);

				// give it a new transposition table if it's an alpha beta player
				Player_AlphaBeta * p1AB = dynamic_cast<Player_AlphaBeta *>(playerOne.get());
				if (p1AB)
				{
					p1AB->setTranspositionTable(TTPtr(new TranspositionTable()));
				}

				// get player two
				PlayerPtr playerTwo(players[1][p2Player]);

				if (playerTwo->getFormation() != nullptr)
					playerTwo->setFormationPolicy(true);

				Player_AlphaBeta * p2AB = dynamic_cast<Player_AlphaBeta *>(playerTwo.get());
				if (p2AB)
				{
					p2AB->setTranspositionTable(TTPtr(new TranspositionTable()));
				}

				// construct the game
				Game g(states[state], playerOne, playerTwo, 20000);
				//this is weird and should be fixed later...
				ScoreType gameEval = 0;
#ifndef _WIN32
				showDisplay = false;
#endif
				//showDisplay = false;
				if (showDisplay)
				{			
					static GUI gui(Constants::General::guiWidth, Constants::General::guiHeight, imageDir_);
					gui.setGame(g);

					while (!gui.getGame().gameOver())
					{
						std::this_thread::sleep_for(std::chrono::milliseconds(displaySleepAmount_));
						gui.onFrame();
					}

					gameEval = gui.getGame().getState().eval(Players::Player_One, SparCraft::EvaluationMethods::LTD2).val();
					prepareStateResult(gui.getGame().getState());
					
				}
				else
				{
					while (!g.gameOver())
					{
						g.playNextTurn();						
					}
					prepareStateResult(g.getState());

				}
			}
		}
	}

	//results.close();
}

//-----------------------------
//-----
//-----    PARSING DESC FILE
//-----
//-----------------------------

void SearchExperiment::parseConfigFile(const std::string & filename)
{
	std::vector<std::string> lines(getLines(filename));

	for (size_t l(0); l<lines.size(); ++l)
	{
		std::istringstream iss(lines[l]);
		std::string option;
		iss >> option;

		if (strcmp(option.c_str(), "SimulationSettings") == 0)
		{
			parseSimulationSettings(lines[l]);
		}
		else if (strcmp(option.c_str(), "Player") == 0)
		{
			addPlayer(lines[l]);
		}
		else if (strcmp(option.c_str(), "State") == 0)
		{
			addState(lines[l]);
		}
		else if (strcmp(option.c_str(), "MapFile") == 0)
		{
			std::string fileString;
			iss >> fileString;
			std::ifstream input(fileString);
			std::vector<std::string> toFill;
			readFileByLines(input, toFill);
			setNewMap(toFill.begin(), toFill.end());
		}
		else if (strcmp(option.c_str(), "Display") == 0)
		{
			std::string option;
			iss >> option;
			
			iss >> imageDir_;
			if (strcmp(option.c_str(), "true") == 0)
			{
				showDisplay = true;
			}
		}
		else if (strcmp(option.c_str(), "ResultsFile") == 0)
		{
			std::string fileString;
			std::string append;
			iss >> fileString;
			iss >> append;
			resultsFile = fileString;

			appendTimeStamp = strcmp(append.c_str(), "true") == 0 ? true : false;
		}
		else if (strcmp(option.c_str(), "PlayerUpgrade") == 0)
		{
			int playerID(0);
			std::string upgradeName;
			int upgradeLevel(0);

			iss >> playerID;
			iss >> upgradeName;
			iss >> upgradeLevel;

			for (const BWAPI::UpgradeType & type : BWAPI::UpgradeTypes::allUpgradeTypes())
			{
				if (type.getName().compare(upgradeName) == 0)
				{
					PlayerProperties::Get(playerID).SetUpgradeLevel(type, upgradeLevel);
					break;
				}
			}
		}
		else if (strcmp(option.c_str(), "PlayerTech") == 0)
		{
			int playerID(0);
			std::string techName;

			iss >> playerID;
			iss >> techName;

			for (const BWAPI::TechType & type : BWAPI::TechTypes::allTechTypes())
			{
				if (type.getName().compare(techName) == 0)
				{
					PlayerProperties::Get(playerID).SetResearched(type, true);
					break;
				}
			}
		}
		else
		{
			System::FatalError("Invalid Option in Configuration File: " + option);
		}
	}
}


//-----------------------------
//-----
//-----    ADDING STATES	
//-----
//-----------------------------

//State separatedstate 50 ... Zerg_Hydralisk 2
void SearchExperiment::addState(const std::string & line)
{
	std::istringstream iss(line);

	// the first number is the playerID
	//BP

	std::string state;
	std::string stateType;
	int numStates;

	iss >> state;
	iss >> stateType;
	iss >> numStates;

	if (strcmp(stateType.c_str(), "StateSymmetric") == 0)
	{
		int xLimit, yLimit;
		iss >> xLimit;
		iss >> yLimit;

		std::vector<std::string> unitVec;
		std::vector<int> numUnitVec;
		std::string unitType;
		int numUnits;

		while (iss >> unitType)
		{
			iss >> numUnits;
			unitVec.push_back(unitType);
			numUnitVec.push_back(numUnits);
		}

		//std::cout << "\nAdding " << numStates <<  " Symmetric State(s)\n\n";

		for (int s(0); s<numStates; ++s)
		{
			states.push_back(getSymmetricState(unitVec, numUnitVec, xLimit, yLimit));
		}
	}
	else if (strcmp(stateType.c_str(), "StateRawDataFile") == 0)
	{
		std::string filename;
		iss >> filename;

		for (int i(0); i<numStates; ++i)
		{
			states.push_back(GameState(filename));
		}
	}
	else if (strcmp(stateType.c_str(), "StateDescriptionFile") == 0)
	{
		std::string filename;
		iss >> filename;

		for (int i(0); i<numStates; ++i)
		{
			parseStateDescriptionFile(filename);
		}
	}
	else if (strcmp(stateType.c_str(), "SeparatedState") == 0)
	{
		int xLimit, yLimit;
		int cx1, cy1, cx2, cy2;
		iss >> xLimit;
		iss >> yLimit;
		iss >> cx1;
		iss >> cy1;
		iss >> cx2;
		iss >> cy2;

		std::vector<std::string> unitVec;
		std::vector<int> numUnitVec;
		std::string unitType;
		int numUnits;

		while (iss >> unitType)
		{
			iss >> numUnits;
			unitVec.push_back(unitType);
			numUnitVec.push_back(numUnits);
		}

		//std::cout << "\nAdding " << numStates <<  " Symmetric State(s)\n\n";

		for (int s(0); s<numStates / 2; ++s)
		{
			addSeparatedState(unitVec, numUnitVec, cx1, cy1, cx2, cy2, xLimit, yLimit);
		}
	}
	//lets check if its on of StateAligned states
	else if (stateType.compare(0, 12, "StateAligned") == 0) //"StateAligned".length() == 12
	{
		bool randomness = stateType == "StateAlignedR";
		int cDevianceX, cDevianceY, p1BX, p1BY, p2BX, p2BY, unitNum;
		std::vector<std::string> p1UnitVec, p2UnitVec;
		std::vector<int> p1NumUnits, p2NumUnits;
		std::string nextWord;
		bool horizontal;
		iss >> nextWord;

		if (nextWord == "Horizontal")
			horizontal = true;
		else if (nextWord == "Vertical")
			horizontal = false;
		else
			System::FatalError("ivalid orientation");
		iss >> nextWord;
		if (nextWord == "P1")
		{
			iss >> p1BX;
			iss >> p1BY;
			for (;;)
			{
				iss >> nextWord;
				if (nextWord == "P2")
					break;
				if (nextWord == "BLANK")
				{
					p1UnitVec.push_back(nextWord);
					continue;
				}
				else
				{
					p1UnitVec.push_back(nextWord);
					iss >> unitNum;
					p1NumUnits.push_back(unitNum);
				}
			}
			iss >> p2BX;
			iss >> p2BY;
			//now, until the end of stream..
			while (iss >> nextWord)
			{
				if (nextWord == "BLANK")
				{
					p2UnitVec.push_back(nextWord);
					continue;
				}
				else
				{
					p2UnitVec.push_back(nextWord);
					iss >> unitNum;
					p2NumUnits.push_back(unitNum);
				}
			}
		}
		else
		{
			cDevianceX = std::stoi(nextWord);
			iss >> cDevianceY;
			if (!horizontal) //if vertical...
			{
				p1BX = Constants::General::mapWidth / 2 - cDevianceX;
				p1BY = Constants::General::mapHeight / 2 + cDevianceY;
				p2BX = Constants::General::mapWidth / 2 + cDevianceX;
				p2BY = Constants::General::mapHeight / 2 + cDevianceY;
			}
			else if (horizontal)
			{
				p1BX = Constants::General::mapWidth / 2 + cDevianceX;
				p1BY = Constants::General::mapHeight / 2 + cDevianceY;
				p2BX = Constants::General::mapWidth / 2 + cDevianceX;
				p2BY = Constants::General::mapHeight / 2 - cDevianceY;
			}

			iss >> nextWord; //P1
			for (;;)
			{
				iss >> nextWord;
				if (nextWord == "P2")
					break;
				if (nextWord == "BLANK")
				{
					p1UnitVec.push_back(nextWord);
					continue;
				}
				else
				{
					p1UnitVec.push_back(nextWord);
					iss >> unitNum;
					p1NumUnits.push_back(unitNum);
				}
			}
			while (iss >> nextWord)
			{
				if (nextWord == "BLANK")
				{
					p2UnitVec.push_back(nextWord);
					continue;
				}
				else
				{
					p2UnitVec.push_back(nextWord);
					iss >> unitNum;
					p2NumUnits.push_back(unitNum);
				}
			}
		}

		for (int s(0); s < numStates; s++)
		{
			states.push_back(getStateAligned(p1BX, p1BY, p2BX, p2BY, horizontal, randomness,
				p1UnitVec, p1NumUnits, p2UnitVec, p2NumUnits));
		}
	}
	else
	{
		System::FatalError("Invalid State Type in Configuration File: " + stateType);
	}
}

void SearchExperiment::parseStateDescriptionFile(const std::string & fileName)
{
	std::vector<std::string> lines = getLines(fileName);

	//BP
	GameState currentState;

	for (size_t u(0); u<lines.size(); ++u)
	{
		std::stringstream iss(lines[u]);
		std::string unitType;

		int playerID(0);
		int x(0);
		int y(0);

		iss >> unitType;
		if (unitType == "--")
			continue;
		iss >> playerID;
		iss >> x;
		iss >> y;
		auto unitType2 = getUnitType(unitType);
		if (!tryAddUnit(currentState, playerID, getUnitType(unitType), x, y))
			System::FatalError("unit is not addable - collisions");
	}

	states.push_back(currentState);
}

void SearchExperiment::addSeparatedState(std::vector<std::string> & unitTypes, std::vector<int> & numUnits,
	const PositionType cx1, const PositionType cy1,
	const PositionType cx2, const PositionType cy2,
	const PositionType & xLimit, const PositionType & yLimit)
{
	GameState state;
	GameState state2;

	// for each unit type to add
	for (size_t i(0); i<unitTypes.size(); ++i)
	{
		BWAPI::UnitType type;
		for (const BWAPI::UnitType & t : BWAPI::UnitTypes::allUnitTypes())
		{
			if (t.getName().compare(unitTypes[i]) == 0)
			{
				type = t;
				break;
			}
		}

		// add the symmetric unit for each count in the numUnits Vector
		for (int u(0); u<numUnits[i]; ++u)
		{
			Position r((rand.nextInt() % (2 * xLimit)) - xLimit, (rand.nextInt() % (2 * yLimit)) - yLimit);
			Position u1(cx1 + r.x(), cy1 + r.y());
			Position u2(cx2 - r.x(), cy2 - r.y());

			bool addedSuccessfuly = false;
			for (int i = 0; i < _numOfTriesInLayout; i++)
			{
				if (!type.isFlyer())
				{
					if ((isUnitAddable(state, type, u1.x(), u1.y()) && isUnitAddable(state, type, u2.x(), u2.y())
						&& isUnitAddable(state2, type, u2.x(), u2.y()) && isUnitAddable(state2, type, u1.x(), u1.y())))
					{
						state.addUnit(type, Players::Player_One, u1);
						state.addUnit(type, Players::Player_Two, u2);
						state2.addUnit(type, Players::Player_One, u2);
						state2.addUnit(type, Players::Player_Two, u1);
						addedSuccessfuly = true;
						break;
					}
				}
				else
				{
					state.addUnit(type, Players::Player_One, u1);
					state.addUnit(type, Players::Player_Two, u2);
					state2.addUnit(type, Players::Player_One, u2);
					state2.addUnit(type, Players::Player_Two, u1);
					addedSuccessfuly = true;
					break;
				}
			}
			if (!addedSuccessfuly)
				System::FatalError("failed to add randomly distributed units - collision");
		}
	}

	state.finishedMoving();

	states.push_back(state);
	states.push_back(state2);
	//BP
}

GameState SearchExperiment::getSymmetricState(std::vector<std::string> & unitTypes, std::vector<int> & numUnits,
	const PositionType & xLimit, const PositionType & yLimit)
{
	//argument unitTypes is actually unitVec, so if i have line State .. Zerg_Hydralisk Zerg_Mutalisk
	//it will contain these two unit types
	GameState state;
	//
	//
	Position mid(640, 360);

	// for each unit type to add
	for (size_t i(0); i<unitTypes.size(); ++i)
	{
		BWAPI::UnitType type;
		for (const BWAPI::UnitType & t : BWAPI::UnitTypes::allUnitTypes())
		{
			if (t.getName().compare(unitTypes[i]) == 0)
			{
				type = t;
				break;
			}
		}

		// add the symmetric unit for each count in the numUnits Vector
		// i maybe want to have numUnits[0] = 5 Zerg_Hydralisk and numUnits[1] = 2 Zerg_Mutalisk
		for (int u(0); u<numUnits[i]; ++u)
		{
			bool addedSuccessfuly = false;
			for (int i = 0; i < _numOfTriesInLayout; i++)
			{
				Position r((rand.nextInt() % (2 * xLimit)) - xLimit, (rand.nextInt() % (2 * yLimit)) - yLimit);
				Position u1(mid.x() + r.x(), mid.y() + r.y());
				Position u2(mid.x() - r.x(), mid.y() - r.y());
				if (!type.isFlyer())
				{
					if (isUnitAddable(state, type, u1.x(), u1.y()) && isUnitAddable(state, type, u2.x(), u2.y()))
					{
						state.addUnit(type, Players::Player_One, u1);
						state.addUnit(type, Players::Player_Two, u2);
						addedSuccessfuly = true;
						break;
					}
				}
				else
				{
					state.addUnit(type, Players::Player_One, u1);
					state.addUnit(type, Players::Player_Two, u2);
					addedSuccessfuly = true;
					break;
				}
			}
			if (!addedSuccessfuly)
				System::FatalError("random units weren't added - collisions");
		}
	}

	//std::cout << std::endl;
	state.finishedMoving();
	return state;
}


GameState SearchExperiment::getStateAligned(int p1BX, int p1BY, int p2BX, int p2BY, bool horizontal, bool randomness,
	std::vector<std::string>& p1UnitVec, std::vector<int>& p1NumUnits,
	std::vector<std::string>& p2UnitVec, std::vector<int>& p2NumUnits)

{
	//when horizontal, p1 is the bottom player, when vertical, p1 is the left player
	GameState state;
	int unitPlusX = horizontal == true ? 70 : 0;
	int unitPlusY = horizontal == false ? 70 : 0;
	int linePlusX = horizontal == true ? 0 : 70;
	int linePlusY = horizontal == false ? 0 : 70;
	int origp1BX = p1BX;
	int origp1BY = p1BY;
	int origp2BX = p2BX;
	int origp2BY = p2BY;
	int randX = 0;
	int randY = 0;

	//base values for p1: if horizontal, base values are for the left most unit
	//if vertical, base values are for the bottom most unit
	int p1NumUnitsIndex = 0;
	for (size_t i = 0; i < p1UnitVec.size(); i++)
	{
		if (p1UnitVec[i] == "BLANK")
			continue;
		BWAPI::UnitType unitType = getUnitType(p1UnitVec[i]);

		p1BX += i*linePlusX;
		p1BY -= i*linePlusY;

		p1BX -= ((p1NumUnits[p1NumUnitsIndex] - 1) / 2) * unitPlusX;
		p1BY -= ((p1NumUnits[p1NumUnitsIndex] - 1) / 2) * unitPlusY;
		if ((p1NumUnits[p1NumUnitsIndex] - 1) % 2 == 1)
		{
			p1BX -= unitPlusX / 2;
			p1BY -= unitPlusY / 2;
		}
		for (int j = 0; j < p1NumUnits[p1NumUnitsIndex]; j++)
		{
			p1BX += unitPlusX;
			p1BY += unitPlusY;
			if (randomness)
			{
				randX = rand.nextInt() % 21 - 10;
				randY = rand.nextInt() % 21 - 10;
			}
			Position r(p1BX + randX, p1BY + randY);
			//state.addUnit(unitType, Players::Player_One, r);
			if (!tryAddUnit(state, Players::Player_One, unitType, r.x(), r.y()))
				System::FatalError("unit is not addable - collisions");
		}
		p1BX = origp1BX;
		p1BY = origp1BY;
		p1NumUnitsIndex++;
	}

	//base values for p1: if horizontal, base values are for the right most unit
	//if vertical, base values are for the top most unit
	int p2NumUnitsIndex = 0;
	randX = 0;
	randY = 0;
	for (size_t i(0); i < p2UnitVec.size(); i++)
	{
		if (p2UnitVec[i] == "BLANK")
			continue;
		BWAPI::UnitType unitType = getUnitType(p2UnitVec[i]);
		p2BX -= i * linePlusX;
		p2BY += i*linePlusY;
		p2BX -= ((p2NumUnits[p2NumUnitsIndex] - 1) / 2) * unitPlusX;
		p2BY -= ((p2NumUnits[p2NumUnitsIndex] - 1) / 2) * unitPlusY;
		if ((p2NumUnits[p2NumUnitsIndex] - 1) % 2 == 1)
		{
			p2BX -= unitPlusX / 2;
			p2BY -= unitPlusY / 2;
		}
		for (int j = 0; j < p2NumUnits[p2NumUnitsIndex]; j++)
		{
			p2BX += unitPlusX;
			p2BY += unitPlusY;
			if (randomness)
			{
				randX = rand.nextInt() % 21 - 10;
				randY = rand.nextInt() % 21 - 10;
			}
			Position r(p2BX + randX, p2BY + randY);
			//state.addUnit(unitType, Players::Player_Two, r);
			if (!tryAddUnit(state, Players::Player_Two, unitType, r.x(), r.y()))
				System::FatalError("unit is not addable - collisions");
		}
		p2BX = origp2BX;
		p2BY = origp2BY;
		p2NumUnitsIndex++;
	}
	state.finishedMoving();
	return state;
}

bool SearchExperiment::tryAddUnit(GameState& state, IDType playerID, BWAPI::UnitType unitType, PositionType posX, PositionType posY)
{
	//for debugging

	if (unitType.isFlyer())
	{
		state.addUnit(unitType, playerID, Position(posX, posY));
		return true;
	}
	else
	{
		if (isUnitAddable(state, unitType, posX, posY))
		{
			state.addUnit(unitType, playerID, Position(posX, posY));
			return true;
		}
		else
		{
			return false;
		}
	}
}


bool SearchExperiment::isUnitAddable(GameState& state, BWAPI::UnitType unitType, PositionType possibleX, PositionType possibleY)
{
	if (!Constants::General::collisions)
		return true;
	Position aimedTopLeft(possibleX - unitType.dimensionLeft(), possibleY - unitType.dimensionUp());
	


	for (IDType p(0); p < Constants::Num_Players; ++p)
	{
		for (IDType u(0); u < state.numUnits(p); ++u)
		{
			const Unit& unit = state.getUnit(p, u);

			
			//for some reason, linker can'T find the Helpers::areUnitsColliding function here, will get to it later..
			if (CollisionHelperFunctions::areAlignedCollisionBoxesColliding(
				unit.topLeftX(), unit.topLeftY(),
				unit.width(), unit.height(),
				possibleX - unitType.dimensionLeft(), possibleY - unitType.dimensionUp(), unitType.width(), unitType.height()))
				return false;
		}
	}
	return true;
}

BWAPI::UnitType SearchExperiment::getUnitType(const std::string & unitTypeString)
{
	BWAPI::UnitType type;

	for (const BWAPI::UnitType & t : BWAPI::UnitTypes::allUnitTypes())
	{
		if (t.getName().compare(unitTypeString) == 0)
		{
			type = t;
		}
	}

	System::checkSupportedUnitType(type);

	return type;
}

void SearchExperiment::addGameState(const GameState & state)
{
	if (states.size() >= 10000)
	{
		System::FatalError("Search Experiment cannot contain more than 10,000 states.");
	}
}


//-----------------------------
//-----
//-----    ADDING PLAYERS
//-----
//-----------------------------


void SearchExperiment::addPlayer(const std::string & line)
{
	std::istringstream iss(line);

	// Regular expressions for line validation (if I ever want to use them)
	//std::regex ScriptRegex("[a-zA-Z]+[ ]+[0-1][ ]+[a-zA-Z]+[ ]*");
	//std::regex AlphaBetaRegex("[a-zA-Z]+[ ]+[0-1][ ]+[a-zA-Z]+[ ]+[0-9]+[ ]+[0-9]+[ ]+[a-zA-Z]+[ ]+[a-zA-Z]+[ ]+[a-zA-Z]+[ ]+[a-zA-Z]+[ ]+[a-zA-Z]+[ ]+[a-zA-Z]+[ ]*");
	//std::regex UCTRegex("[a-zA-Z]+[ ]+[0-1][ ]+[a-zA-Z]+[ ]+[0-9]+[ ]+[0-9.]+[ ]+[0-9]+[ ]+[0-9]+[ ]+[a-zA-Z]+[ ]+[a-zA-Z]+[ ]+[a-zA-Z]+[ ]+[a-zA-Z]+[ ]+[a-zA-Z]+[ ]+[a-zA-Z]+[ ]*");
	//std::regex PortfolioRegex("[a-zA-Z]+[ ]+[0-1][ ]+[a-zA-Z]+[ ]+[0-9]+[ ]+[a-zA-Z]+[ ]+[0-9]+[ ][0-9]+[ ]*");

	// the first number is the playerID

	std::string player;
	int playerID;
	int playerModelID;
	std::string playerModelString;
	std::string formationPolicy;

	Formation* formation = nullptr;
	bool followsFormationPolicy;

	iss >> player;
	iss >> playerID;
	iss >> playerModelString;
	iss >> formationPolicy;

	if (formationPolicy.compare("HOLD") == 0)
	{
		formation = new Formation(FormationTypes::HOLD);
		followsFormationPolicy = true;

	}
	else if (formationPolicy.compare("NONE") == 0)
	{
		formation = nullptr;
		followsFormationPolicy = false;
	}
	else
	{
		System::FatalError("invalid formation policy: " + formationPolicy);
	}

	playerStrings[playerID].push_back(playerModelString);

	playerModelID = PlayerModels::getID(playerModelString);

	if (playerModelID == PlayerModels::AttackClosest)
	{
		players[playerID].push_back(PlayerPtr(new Player_AttackClosest(playerID)));
	}
	else if (playerModelID == PlayerModels::AttackClosestUnmodified)
	{
		players[playerID].push_back(PlayerPtr(new Player_AttackClosestUnmodified(playerID)));
	}
	else if (playerModelID == PlayerModels::MyScriptedAI)
	{
		players[playerID].push_back(PlayerPtr(new Player_MyScriptedAI(playerID, followsFormationPolicy, formation)));
	}
	else if (playerModelID == PlayerModels::MyScriptedAI_D)
	{
		players[playerID].push_back(PlayerPtr(new Player_MyScriptedAI_D(playerID, followsFormationPolicy, formation)));
	}
	else if (playerModelID == PlayerModels::MyScriptedAI_A)
	{
		players[playerID].push_back(PlayerPtr(new Player_MyScriptedAI_A(playerID, followsFormationPolicy, formation)));
	}
	else if (playerModelID == PlayerModels::AttackDPS)
	{
		players[playerID].push_back(PlayerPtr(new Player_AttackDPS(playerID)));
	}
	else if (playerModelID == PlayerModels::AttackWeakest)
	{
		players[playerID].push_back(PlayerPtr(new Player_AttackWeakest(playerID)));
	}
	else if (playerModelID == PlayerModels::Kiter)
	{
		players[playerID].push_back(PlayerPtr(new Player_Kiter(playerID)));
	}
	else if (playerModelID == PlayerModels::KiterDPS)
	{
		players[playerID].push_back(PlayerPtr(new Player_KiterDPS(playerID)));
	}
	else if (playerModelID == PlayerModels::Kiter_NOKDPS)
	{
		players[playerID].push_back(PlayerPtr(new Player_Kiter_NOKDPS(playerID)));
	}
	else if (playerModelID == PlayerModels::Cluster)
	{
		players[playerID].push_back(PlayerPtr(new Player_Cluster(playerID)));
	}
	else if (playerModelID == PlayerModels::NOKDPS)
	{
		players[playerID].push_back(PlayerPtr(new Player_NOKDPS(playerID, followsFormationPolicy, formation)));
	}
	else if (playerModelID == PlayerModels::Random)
	{
		players[playerID].push_back(PlayerPtr(new Player_Random(playerID)));
	}
	else if (playerModelID == PlayerModels::PortfolioGreedySearch)
	{
		std::string enemyPlayerModel;
		size_t timeLimit(40);
		int iterations(1);
		int responses(0);

		iss >> timeLimit;
		iss >> enemyPlayerModel;
		iss >> iterations;
		iss >> responses;

		players[playerID].push_back(PlayerPtr(new Player_PortfolioGreedySearch(playerID, PlayerModels::getID(enemyPlayerModel), iterations, responses, timeLimit)));
	}
	else if (playerModelID == PlayerModels::AlphaBeta)
	{
		int             timeLimitMS;
		int             maxChildren;
		std::string     moveOrdering;
		std::string     evalMethod;
		std::string     playoutScript1;
		std::string     playoutScript2;
		std::string     playerToMoveMethod;
		std::string     opponentModelScript;

		// read in the values
		iss >> timeLimitMS;
		iss >> maxChildren;
		iss >> moveOrdering;
		iss >> evalMethod;
		iss >> playoutScript1;
		iss >> playoutScript2;
		iss >> playerToMoveMethod;
		iss >> opponentModelScript;

		// convert them to the proper enum types
		int moveOrderingID = MoveOrderMethod::getID(moveOrdering);
		int evalMethodID = EvaluationMethods::getID(evalMethod);
		int playoutScriptID1 = PlayerModels::getID(playoutScript1);
		int playoutScriptID2 = PlayerModels::getID(playoutScript2);
		int playerToMoveID = PlayerToMove::getID(playerToMoveMethod);
		int opponentModelID = PlayerModels::getID(opponentModelScript);

		// construct the parameter object
		AlphaBetaSearchParameters params;

		// give the default parameters we can't set via options
		params.setMaxDepth(50);
		params.setSearchMethod(SearchMethods::IDAlphaBeta);

		// set the parameters from the options in the file
		params.setMaxPlayer(playerID);
		params.setTimeLimit(timeLimitMS);
		params.setMaxChildren(maxChildren);
		params.setMoveOrderingMethod(moveOrderingID);
		params.setEvalMethod(evalMethodID);
		params.setSimScripts(playoutScriptID1, playoutScriptID2);
		params.setPlayerToMoveMethod(playerToMoveID);

		// add scripts for move ordering
		if (moveOrderingID == MoveOrderMethod::ScriptFirst)
		{
			params.addOrderedMoveScript(PlayerModels::NOKDPS);
			params.addOrderedMoveScript(PlayerModels::KiterDPS);
			//params.addOrderedMoveScript(PlayerModels::Cluster);
			//params.addOrderedMoveScript(PlayerModels::AttackWeakest);
		}

		// set opponent modeling if it's not none
		if (opponentModelID != PlayerModels::None)
		{
			if (playerID == 0)
			{
				params.setSimScripts(playoutScriptID1, opponentModelID);
				params.setPlayerModel(1, playoutScriptID2);
			}
			else
			{
				params.setSimScripts(opponentModelID, playoutScriptID2);
				params.setPlayerModel(0, playoutScriptID1);
			}
		}

		PlayerPtr abPlayer(new Player_AlphaBeta(playerID, params, TTPtr((TranspositionTable *)NULL)));
		players[playerID].push_back(abPlayer);
	}
	else if (playerModelID == PlayerModels::UCT)
	{
		int             timeLimitMS;
		double          cValue;
		int             maxTraversals;
		int             maxChildren;
		std::string     moveOrdering;
		std::string     evalMethod;
		std::string     playoutScript1;
		std::string     playoutScript2;
		std::string     playerToMoveMethod;
		std::string     opponentModelScript;

		// read in the values
		iss >> timeLimitMS;
		iss >> cValue;
		iss >> maxTraversals;
		iss >> maxChildren;
		iss >> moveOrdering;
		iss >> evalMethod;
		iss >> playoutScript1;
		iss >> playoutScript2;
		iss >> playerToMoveMethod;
		iss >> opponentModelScript;

		// convert them to the proper enum types
		int moveOrderingID = MoveOrderMethod::getID(moveOrdering);
		int evalMethodID = EvaluationMethods::getID(evalMethod);
		int playoutScriptID1 = PlayerModels::getID(playoutScript1);
		int playoutScriptID2 = PlayerModels::getID(playoutScript2);
		int playerToMoveID = PlayerToMove::getID(playerToMoveMethod);
		int opponentModelID = PlayerModels::getID(opponentModelScript);

		// construct the parameter object
		UCTSearchParameters params;

		// set the parameters from the options in the file
		params.setTimeLimit(timeLimitMS);
		params.setCValue(cValue);
		params.setMaxPlayer(playerID);
		params.setMaxTraversals(maxTraversals);
		params.setMaxChildren(maxChildren);
		params.setMoveOrderingMethod(moveOrderingID);
		params.setEvalMethod(evalMethodID);
		params.setSimScripts(playoutScriptID1, playoutScriptID2);
		params.setPlayerToMoveMethod(playerToMoveID);
		//params.setGraphVizFilename("__uct.txt");

		// add scripts for move ordering
		if (moveOrderingID == MoveOrderMethod::ScriptFirst)
		{
			params.addOrderedMoveScript(PlayerModels::NOKDPS);
			params.addOrderedMoveScript(PlayerModels::KiterDPS);
			//params.addOrderedMoveScript(PlayerModels::Cluster);
		}

		// set opponent modeling if it's not none
		if (opponentModelID != PlayerModels::None)
		{
			if (playerID == 0)
			{
				params.setSimScripts(playoutScriptID1, opponentModelID);
				params.setPlayerModel(1, playoutScriptID2);
			}
			else
			{
				params.setSimScripts(opponentModelID, playoutScriptID2);
				params.setPlayerModel(0, playoutScriptID1);
			}
		}

		PlayerPtr uctPlayer(new Player_UCT(playerID, params));
		players[playerID].push_back(uctPlayer);
	}
	else
	{
		System::FatalError("Invalid Player Type in Configuration File: " + playerModelString);
	}
}

//-----------------------------
//-----
//-----    HELPERS
//-----
//-----------------------------

void SearchExperiment::parseSimulationSettings(const std::string& line)
{
	int mapWidth, mapHeight, guiWidth, guiHeight;
	bool invisibility, collisions;
	std::istringstream iss(line);

	std::string nextWord;
	iss >> nextWord; //SimulationSettings

	while (iss >> nextWord)
	{
		if (nextWord == "MapWidth")
		{
			iss >> mapWidth;
		}
		else if (nextWord == "MapHeight")
		{
			iss >> mapHeight;
		}
		else if (nextWord == "GUIWidth")
		{
			iss >> guiWidth;
		}
		else if (nextWord == "GUIHeight")
		{
			iss >> guiHeight;
		}
		else if (nextWord == "invisibility")
		{
			iss >> nextWord;
			if (nextWord == "ON")
				invisibility = true;
			else if (nextWord == "OFF")
				invisibility = false;
			else
				System::FatalError("invalid cloak option");
		}
		else if (nextWord == "collisions")
		{
			iss >> nextWord;
			if (nextWord == "ON")
				collisions = true;
			else if (nextWord == "OFF")
				collisions = false;
			else
				System::FatalError("invalid collisions option");
		}
		else
		{
			System::FatalError("invalid simulation settings");
		}
	}
	Constants::General::setSimulationParameters(mapWidth, mapHeight, guiWidth, guiHeight, collisions, invisibility);
	map_ = std::make_unique<Map>();
	pathFinder_ = std::make_unique<PathFinder>(map_.get());
	collisionResolver_ = std::make_unique<CollisionResolver>();
}

void SearchExperiment::setupResults()
{
	size_t np1 = players[0].size();
	size_t np2 = players[1].size();

	resultsStateNumber = ivvv(np1, ivv(np2, iv()));
	resultsNumUnits = ivvv(np1, ivv(np2, iv()));
	resultsEval = ivvv(np1, ivv(np2, iv()));
	resultsRounds = ivvv(np1, ivv(np2, iv()));
	resultsTime = dvvv(np1, dvv(np2, dv()));
	numGames = ivv(np1, iv(np2, 0));
	numWins = ivv(np1, iv(np2, 0));
	numLosses = ivv(np1, iv(np2, 0));
	numDraws = ivv(np1, iv(np2, 0));
}

void SearchExperiment::writeConfig(const std::string & configfile)
{
	return;
	std::ofstream config(getConfigOutFileName().c_str());
	if (!config.is_open())
	{
		System::FatalError("Problem Opening Output File: Config");
	}

	std::vector<std::string> lines(getLines(configfile));

	for (size_t l(0); l<lines.size(); ++l)
	{
		config << lines[l] << std::endl;
	}

	config.close();
}

void SearchExperiment::writeResultsSummary()
{
	return;
	std::ofstream results(getResultsSummaryFileName().c_str());
	if (!results.is_open())
	{
		System::FatalError("Problem Opening Output File: Results Summary");
	}

	for (size_t p1(0); p1 < players[0].size(); ++p1)
	{
		for (size_t p2(0); p2 < players[1].size(); ++p2)
		{
			double score = 0;
			if (numGames[p1][p2] > 0)
			{
				score = ((double)numWins[p1][p2] / (double)(numGames[p1][p2])) + 0.5*((double)numDraws[p1][p2] / (double)numGames[p1][p2]);
			}

			results << std::setiosflags(std::ios::fixed) << std::setw(12) << std::setprecision(7) << score << " ";
		}

		results << std::endl;
	}

	results.close();
}

void SearchExperiment::padString(std::string & str, const size_t & length)
{
	while (str.length() < length)
	{
		str = str + " ";
	}
}

void SearchExperiment::splitString(const std::string& s, char delimiter, std::vector<std::string>& toFill)
{
	toFill.clear();
	std::string chars;
	for (auto&& c : s)
	{
		if (c == delimiter)
		{
			toFill.push_back(chars);
			chars.clear();
		}
		else
		{
			chars.push_back(c);
		}
	}
	if (chars.size() > 0)
		toFill.push_back(chars);
}

std::string SearchExperiment::getResultsSummaryFileName()
{
	std::string res = resultsFile;

	if (appendTimeStamp)
	{
		res += "_" + getDateTimeString();
	}

	res += "_results_summary.txt";
	return res;
}

std::string SearchExperiment::getResultsOutFileName()
{
	std::string res = resultsFile;

	if (appendTimeStamp)
	{
		res += "_" + getDateTimeString();
	}

	res += "_results_raw.txt";
	return res;
}

std::string SearchExperiment::getConfigOutFileName()
{
	std::string conf = resultsFile;

	if (appendTimeStamp)
	{
		conf += "_" + getDateTimeString();
	}
	conf += "_config.txt";
	return conf;
}

std::string SearchExperiment::getDateTimeString()
{
	return timeString;
}

void SearchExperiment::setCurrentDateTime()
{
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];

	std::fill(buf, buf + 80, '0');
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y-%m-%d_%X", &tstruct);
	//strftime(buf, sizeof(buf), "%X", &tstruct);

	// need to replace ':' for windows filenames
	for (int i(0); i<80; i++)
	{
		if (buf[i] == ':')
		{
			buf[i] = '-';
		}
	}

	timeString = std::string(buf);
}

std::vector<std::string> SearchExperiment::getLines(const std::string & filename)
{
	// set up the file
	std::ifstream fin(filename.c_str());
	if (!fin.is_open())
	{
		System::FatalError("Problem Opening File: " + filename);
	}

	std::string line;

	std::vector<std::string> lines;

	// each line of the file will be a new player to add
	while (fin.good())
	{
		// get the line and set up the string stream
		getline(fin, line);

		// skip blank lines and comments
		if (line.length() > 1 && line[0] != '#')
		{
			lines.push_back(line);
		}
	}

	fin.close();

	return lines;
}

svv SearchExperiment::getExpDescription(const size_t & p1Ind, const size_t & p2Ind, const size_t & state)
{
	// 2-column description vector
	svv desc(2, sv());

	std::stringstream ss;

	desc[0].push_back("Player 1:");
	desc[0].push_back("Player 2:");
	desc[0].push_back("State #:");
	desc[0].push_back("Units:");

	for (size_t p1(0); p1 < players[0].size(); ++p1)
	{
		std::stringstream ss;
		ss << "P1 " << p1 << ":";
		desc[0].push_back(ss.str());
	}

	for (size_t p2(0); p2 < players[1].size(); ++p2)
	{
		std::stringstream ss;
		ss << "P2 " << p2 << ":";
		desc[0].push_back(ss.str());
	}

	for (size_t p1(0); p1 < players[0].size(); ++p1)
	{
		for (size_t p2(0); p2 < players[1].size(); ++p2)
		{
			std::stringstream ps;
			ps << p1 << " vs " << p2;
			desc[0].push_back(ps.str());
		}
	}

	ss << PlayerModels::getName(players[0][p1Ind]->getType());        desc[1].push_back(ss.str()); ss.str(std::string());
	ss << PlayerModels::getName(players[1][p2Ind]->getType());        desc[1].push_back(ss.str()); ss.str(std::string());
	ss << state << " of " << states.size();                         desc[1].push_back(ss.str()); ss.str(std::string());
	ss << states[state].numUnits(0);                                desc[1].push_back(ss.str()); ss.str(std::string());

	for (size_t p1(0); p1 < players[0].size(); ++p1)
	{
		desc[1].push_back(PlayerModels::getName(players[0][p1]->getType()));
	}

	for (size_t p2(0); p2 < players[1].size(); ++p2)
	{
		desc[1].push_back(PlayerModels::getName(players[1][p2]->getType()));
	}

	char buf[30];
	for (size_t p1(0); p1 < players[0].size(); ++p1)
	{
		for (size_t p2(0); p2 < players[1].size(); ++p2)
		{
			double score = 0;
			if (numGames[p1][p2] > 0)
			{
				score = ((double)numWins[p1][p2] / (double)(numGames[p1][p2])) + 0.5*((double)numDraws[p1][p2] / (double)numGames[p1][p2]);
			}

			sprintf(buf, "%.7lf", score);
			desc[1].push_back(std::string(buf));
		}
	}

	return desc;
}

void SearchExperiment::readFileByLines(std::ifstream& fileStream, std::vector<std::string>& toFill)
{
	std::string line;
	while (std::getline(fileStream, line))
	{
		toFill.push_back(std::move(line));
	}
}

std::string SearchExperiment::getBaseFilename(const std::string & filename)
{
	for (int i(filename.length() - 1); i >= 0; --i)
	{
		if (filename[i] == '/' || filename[i] == '\\')
		{
			return filename.substr(i + 1, filename.length());
		}
	}

	return filename;
}


void SearchExperiment::printStateUnits(std::ofstream & results, GameState & state)
{
	return;
	std::stringstream ss;
	for (size_t p(0); p<Constants::Num_Players; ++p)
	{
		for (size_t u(0); u<state.numUnits(p); ++u)
		{
			Unit & unit(state.getUnit(p, u));
			Position pos(unit.position());

			ss << " | " << unit.name() << " " << (int)unit.player() << " " << unit.currentHP() << " " << pos.x() << " " << pos.y();
		}
	}
	results << ss.str();
}