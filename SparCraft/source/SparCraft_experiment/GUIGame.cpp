#include "GUIGame.h"
#include "GUITools.h"
#include "GUI.h"

#define drawHelpersTrue 1
#define drawHelpersFalse 0
#define DRAWHELPERS drawHelpersTrue

using namespace SparCraft;

float White[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float EnergyColors[4] = { 0.05f, 0.2f, 1.0f, 1.0f };
float CollisionColors[4] = { 0.2f, 0.7f, 0.9f, 0.5f };
float WalkableGridColors[4] = { 0.2f, 0.7f, 0.9f, 0.1f };
float CollisionTilesColors[4] = { 0.6f, 0.85f, 0.8f, 0.25f };
float PathColors[4] = { 0.08f, 1.0f, 0.6f, 1.0f };

float PlayerColors[2][4] = { { 1.0f, 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f } };
float PlayerColorsDark[2][4] = { { 0.7f, 0.0f, 0.0f, 1.0f }, { 0.0f, 0.7f, 0.0f, 1.0f } };

float OccupiedWalkableTile[4] = { 0.6f, 0.0f, 0.0f, 1.0f };
float UnwalkableBaseTiles[4] = {1.0f, 0.40f, 0.40f, 1.0f };
float WalkableBaseTilesC1[4] = { 0.4f, 0.4f, 0.4f, 0.05f };
float WalkableBaseTilesC2[4] = { 0.6f, 0.6f, 0.6f, 0.1f };
float StormColors[4] = { 0.2f, 0.75f, 0.90f, 0.8f };
float ProjectileColors[4] = { 0.9f, 0.95f, 0.2f, 1.0f };
float miniMapOutlineColors[4] = { 0.83f, 0.7f, 0.57f, 1.0f };
float miniMapLookColors[4] = { 0.75f, 0.57f, 0.85f, 1.0f };

GUIGame::GUIGame(GUI & gui)
	: _game(GameState(), 0)
	, _gui(gui)
{

}

void GUIGame::onFrame()
{
	Timer turnTimer;
	turnTimer.start();

	//drawGame();
	//drawHPBars();

	if (!_game.gameOver())
	{
		_game.playNextTurn();
		_previousTurnTimer = turnTimer.getElapsedTimeInMilliSec();

		for (size_t p(0); p < 2; ++p)
		{
			Player_UCT *        uct = dynamic_cast<Player_UCT *>        (_game.getPlayer(p).get());
			Player_AlphaBeta *  ab = dynamic_cast<Player_AlphaBeta *>  (_game.getPlayer(p).get());

			if (uct)
			{
				setParams(p, uct->getParams().getDescription());
				setResults(p, uct->getResults().getDescription());
			}

			if (ab)
			{
				setParams(p, ab->getParams().getDescription());
				setResults(p, ab->results().getDescription());
			}
		}
		drawBaseTiles();
		drawWalkableGrid();
		drawGame();
		drawHPBars();
		drawCollisionTiles();
		drawMinimap();

		drawParameters(5, 15);
		drawSearchResults(5, 150);
		drawInfo();
	}
	
}

void GUIGame::drawMinimap()
{
	return;
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	int baseMinimapX = _gui.cameraX() + 700;
	int baseMinmapY = _gui.cameraY() + 10;
	int minimapWidth = Constants::General::mapWidth / 15;
	int minimapHeight = Constants::General::mapHeight / 15;
	int ratioX = Constants::General::mapWidth / minimapWidth;
	int ratioY = Constants::General::mapHeight / minimapHeight;
	int mapWidth = Constants::General::mapWidth;
	int mapHeight = Constants::General::mapHeight;
	int guiWidth = Constants::General::guiWidth;
	int guiHeight = Constants::General::guiHeight;
	Position p1min = Position(baseMinimapX, baseMinmapY);
	Position p2min = Position(baseMinimapX, baseMinmapY + minimapHeight);
	Position p3min = Position(baseMinimapX + minimapWidth, baseMinmapY);
	Position p4min = Position(baseMinimapX + minimapWidth, baseMinmapY + minimapHeight);

	int cameraXR = _gui.cameraX() / ratioX;
	int cameraYR = _gui.cameraY() / ratioY;
	int gwR = guiWidth / ratioX;
	int ghR = guiHeight / ratioY;
	int mwR = mapWidth / ratioX;
	int mhR = mapHeight / ratioY;

	Position p1 = Position(cameraXR < mwR ? cameraXR : mwR, cameraYR < mhR ? cameraYR : mhR);
	Position p2 = Position(cameraXR < mwR ? cameraXR : mwR, cameraYR + ghR < mhR ? cameraYR + ghR : mhR);
	Position p3 = Position(cameraXR + gwR < mwR ? cameraXR + gwR : mwR, cameraYR < mhR ? cameraYR : mhR);
	Position p4 = Position(cameraXR + gwR < mwR ? cameraXR + gwR : mwR, cameraYR + ghR < mhR ? cameraYR + ghR : mhR);
	p1 = Position((p1.x() < 0 ? 0 : p1.x()) + p1min.x(), (p1.y() < 0 ? 0 : p1.y()) + p1min.y());
	p2 = Position((p2.x() < 0 ? 0 : p2.x()) + p2min.x(), p2.y() + p1min.y());
	p3 = Position(p3.x() + p1min.x(), (p3.y() < 0 ? 0 : p3.y()) + p3min.y());
	p4 = Position(p4.x() + p1min.x(), p4.y() + p1min.y());
	GUITools::DrawLine(p1min, p3min, 1, miniMapOutlineColors);
	GUITools::DrawLine(p1min, p2min, 1, miniMapOutlineColors);
	GUITools::DrawLine(p3min, p4min, 1, miniMapOutlineColors);
	GUITools::DrawLine(p2min, p4min, 1, miniMapOutlineColors);

	GUITools::DrawLine(p1, p3, 1, miniMapLookColors);
	GUITools::DrawLine(p1, p2, 1, miniMapLookColors);
	GUITools::DrawLine(p3, p4, 1, miniMapLookColors);
	GUITools::DrawLine(p2, p4, 1, miniMapLookColors);
}
void GUIGame::drawUnitOnMinimap(Position unitPos, int playerid)
{
	return;
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	unitPos = Position(unitPos.x() / 15 + _gui.cameraX() + 700, unitPos.y() / 15 + _gui.cameraY() + 10);
	GUITools::DrawRect(unitPos, Position(unitPos.x() + 2, unitPos.y() + 2), PlayerColors[playerid]);

}

void GUIGame::drawBaseTiles()
{
	for (int i = 0; i < _game.getState().getMap()->getWalkTilesNumberHorizontal(); i++)
	{
		for (int j = 0; j < _game.getState().getMap()->getWalkTilesNumberVertical(); j++)
		{
			if (!(_game.getState().getMap()->isWalkableBaseTile(i, j)))
			{
				bool anySurroundingWalkable(false);
				if ((_game.getState().getMap()->isWalkableBaseTile(i - 1, j - 1)) ||
					(_game.getState().getMap()->isWalkableBaseTile(i, j - 1)) ||
					(_game.getState().getMap()->isWalkableBaseTile(i + 1, j - 1)) ||
					(_game.getState().getMap()->isWalkableBaseTile(i - 1, j)) ||
					(_game.getState().getMap()->isWalkableBaseTile(i + 1, j)) ||
					(_game.getState().getMap()->isWalkableBaseTile(i - 1, j + 1)) ||
					(_game.getState().getMap()->isWalkableBaseTile(i, j + 1)) ||
					(_game.getState().getMap()->isWalkableBaseTile(i + 1, j + 1)))
					anySurroundingWalkable = true;
				if(anySurroundingWalkable)
					GUITools::DrawRectGradient(Position(i*Constants::Map::walkTileSize, j*Constants::Map::walkTileSize),
					Position(i*Constants::Map::walkTileSize + Constants::Map::walkTileSize, j*Constants::Map::walkTileSize + Constants::Map::walkTileSize),
					UnwalkableBaseTiles, UnwalkableBaseTiles);
			}
			else
			{
				GUITools::DrawRectGradient(Position(i*Constants::Map::walkTileSize, j*Constants::Map::walkTileSize),
				Position(i*Constants::Map::walkTileSize + Constants::Map::walkTileSize, j*Constants::Map::walkTileSize + Constants::Map::walkTileSize),
				WalkableBaseTilesC1, WalkableBaseTilesC2);
			}
		}
	}
	
	
}

//left bottom side info
void GUIGame::drawInfo()
{
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	std::stringstream ss;
	ss << "Frame Draw Time: " << _previousDrawGameTimer << "ms\n\n";
	ss << "Turn Time: " << _previousTurnTimer << "ms";

	GUITools::DrawString(Position(5 + _gui.cameraX(), _gui.height() - 20 + _gui.cameraY()), ss.str(), White);
}

void GUIGame::drawGame()
{
	Timer drawGameTimer;
	drawGameTimer.start();

	const GameState & state = _game.getState();


	for (size_t p(0); p < 2; ++p)
	{
		for (size_t u(0); u < state.numUnits(p); u++)
		{
			drawUnit(state.getUnit(p, u));
		}
	}

	_previousDrawGameTimer = drawGameTimer.getElapsedTimeInMilliSec();
}

void GUIGame::drawWalkableGrid()
{
	return;
	for (int i = 0; i <= Constants::General::mapWidth / Constants::Map::walkTileSize; i++)
	{
		GUITools::DrawLine(Position(i*Constants::Map::walkTileSize, 0), Position(i*Constants::Map::walkTileSize, Constants::General::mapHeight), 1, WalkableGridColors);
	}

	for (int i = 0; i <= Constants::General::mapHeight / Constants::Map::walkTileSize; i++)
	{
		GUITools::DrawLine(Position(0, i*Constants::Map::walkTileSize), Position(Constants::General::mapWidth, i*Constants::Map::walkTileSize), 1, WalkableGridColors);
	}
}

void GUIGame::drawCollisionTiles()
{
	return;
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	for (int i = 0; i <= Constants::General::mapWidth / Constants::Map::collisionTileSize; i++)
	{
		GUITools::DrawLine(Position(i*Constants::Map::collisionTileSize, 0), Position(i*Constants::Map::collisionTileSize, Constants::General::mapHeight), 1, CollisionTilesColors);
	}
	for (int i = 0; i <= Constants::General::mapHeight / Constants::Map::collisionTileSize; i++)
	{
		GUITools::DrawLine(Position(0, i*Constants::Map::collisionTileSize), Position(Constants::General::mapWidth, i* Constants::Map::collisionTileSize), 1, CollisionTilesColors);
	}

	for (int i = 0; i <= Constants::General::mapWidth / Constants::Map::collisionTileSize; i++)
	{
		for (int j = 0; j <= Constants::General::mapHeight / Constants::Map::collisionTileSize; j++)
		{
			auto& a = _game.getState().getCollisionTilesHolder().getCollisionTile(i, j);
			int count = a.getUnitsCount();
			GUITools::DrawString(Position(Constants::Map::collisionTileSize * i + 10, Constants::Map::collisionTileSize * j + 10), std::to_string(count), White);
		}
	}

}

void GUIGame::drawSiegeTankSplasCircle(Position impactUnitPosition, int radiusZone1, int radiusZone2)
{
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	GUITools::DrawCircle(impactUnitPosition, (float)radiusZone1, 30);
	GUITools::DrawCircle(impactUnitPosition, (float)radiusZone2, 30);
}

void GUIGame::drawMutaliskAttack(Position(&hitUnitsPositions)[3], IDType playerID)
{
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	Position& curPos = hitUnitsPositions[0];
	if (curPos.x() == 0 && curPos.y() == 0)
		return;
	for (int i = 1; i < 3; i++)
	{
		if (hitUnitsPositions[i].x() == 0 && hitUnitsPositions[i].y() == 0)
			return;
		GUITools::DrawLine(curPos, hitUnitsPositions[i], 1, PlayerColors[playerID]);
		curPos = hitUnitsPositions[i];
	}
}

void GUIGame::drawEffects(const std::vector<std::unique_ptr<Effect>>& effects)
{
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	for (auto& e : effects)
	{
		if (e->getEffectType() == EffectTypes::EffectType::PSIONICSTORM)
		{
			auto castedToStorm = dynamic_cast<PsionicStormEffect*>(e.get());
			Position stormPosition = castedToStorm->getPosition();
			Position p1 = Position(stormPosition.x() - castedToStorm->getRadius(), stormPosition.y() - castedToStorm->getRadius());
			Position p2 = Position(p1.x() + castedToStorm->getRadius() * 2 + 1, p1.y());
			Position p3 = Position(p1.x(), p1.y() + castedToStorm->getRadius() * 2 + 1);
			Position p4 = Position(p1.x() + castedToStorm->getRadius() * 2 + 1, p1.y() + castedToStorm->getRadius() * 2 + 1);
			GUITools::DrawLine(p1, p2, 1, CollisionColors);
			GUITools::DrawLine(p1, p3, 1, CollisionColors);
			GUITools::DrawLine(p2, p4, 1, CollisionColors);
			GUITools::DrawLine(p3, p4, 1, CollisionColors);
		}
		else if (e->getEffectType() == EffectTypes::EffectType::EMP)
		{
			auto castedToEmp = dynamic_cast<EMPShockwaveEffect*>(e.get());
			Position empPosition = castedToEmp->getPosition();
			Position p1 = Position(empPosition.x() - castedToEmp->getRadius(), empPosition.y() - castedToEmp->getRadius());
			Position p2 = Position(p1.x() + castedToEmp->getRadius() * 2 + 1, p1.y());
			Position p3 = Position(p1.x(), p1.y() + castedToEmp->getRadius() * 2 + 1);
			Position p4 = Position(p1.x() + castedToEmp->getRadius() * 2 + 1, p1.y() + castedToEmp->getRadius() * 2 + 1);
			GUITools::DrawLine(p1, p2, 1, CollisionColors);
			GUITools::DrawLine(p1, p3, 1, CollisionColors);
			GUITools::DrawLine(p2, p4, 1, CollisionColors);
			GUITools::DrawLine(p3, p4, 1, CollisionColors);
		}
	}
}
 
void GUIGame::drawProjectiles(const std::vector<std::unique_ptr<Projectile>>& projectiles)
{
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	for (auto& projectile : projectiles)
	{
		GUITools::DrawRectGradient(projectile->getCurrentPosition(), Position(projectile->getCurrentPosition().x() + 5,
			projectile->getCurrentPosition().y() + 5), ProjectileColors, ProjectileColors);
	}
}

void GUIGame::drawLurkerEffectRectangles(Position p1_, Position p2_, Position p3_, Position p4_)
{
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	GUITools::DrawLine(p1_, p3_, 1, CollisionColors);
	GUITools::DrawLine(p1_, p2_, 1, CollisionColors);
	GUITools::DrawLine(p2_, p4_, 1, CollisionColors);
	GUITools::DrawLine(p3_, p4_, 1, CollisionColors);
}

void GUIGame::drawPath(std::deque<Position>& path)
{
	return;
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	for (auto& pathElement : path)
	{
		GUITools::DrawRectGradient(Position(pathElement.x(), pathElement.y()),
			Position(pathElement.x() + 3, pathElement.y() + 3),
			PathColors, PathColors);
	}
}

void GUIGame::drawFirebatRectangles(Position p1, Position p2, Position p3, Position p4,
	Position p1_, Position p2_, Position p3_, Position p4_,
	Position p1__, Position p2__, Position p3__, Position p4__)
{
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	GUITools::DrawLine(p1, p2, 1, CollisionColors);
	GUITools::DrawLine(p1, p3, 1, CollisionColors);
	GUITools::DrawLine(p2, p4, 1, CollisionColors);
	GUITools::DrawLine(p3, p4, 1, CollisionColors);

	GUITools::DrawLine(p1_, p2_, 1, CollisionColors);
	GUITools::DrawLine(p1_, p3_, 1, CollisionColors);
	GUITools::DrawLine(p2_, p4_, 1, CollisionColors);
	GUITools::DrawLine(p3_, p4_, 1, CollisionColors);

	GUITools::DrawLine(p1__, p2__, 1, CollisionColors);
	GUITools::DrawLine(p1__, p3__, 1, CollisionColors);
	GUITools::DrawLine(p2__, p4__, 1, CollisionColors);
	GUITools::DrawLine(p3__, p4__, 1, CollisionColors);

}

//HP bars on the right side
void GUIGame::drawHPBars()
{
	const GameState & state = _game.getState();

	for (IDType p(0); p<Constants::Num_Players; ++p)
	{
		int energyBar(0);
		for (IDType u(0); u<_initialState.numUnits(p); ++u)
		{
			int barHeight = 12;

			const Unit &			unit(state.getUnitDirect(p, u));

			const BWAPI::UnitType	type(unit.type());
			const Position			pos(Constants::General::guiWidth + _gui.cameraX() - 280 + 170 * p, 40 + barHeight*u + energyBar + u * 5 + _gui.cameraY());


			const int				x0(pos.x());
			const int				x1(pos.x() + 150);
			const int				y0(pos.y());
			const int				y1(pos.y() + 15);

			// draw the unit HP box
			double	percHP = (double)unit.currentHP() / (double)unit.maxHP();
			int		w = 150;
			int		h = barHeight;
			int		cw = (int)(w * percHP);
			int		xx = pos.x() - w / 2;
			int		yy = pos.y() - h - (y1 - y0) / 2;

			if (unit.isAlive())
			{
				auto y = unit.type().getName();
				if (type.isSpellcaster())
				{
					double percEnergy = (double)unit.currentEnergy() / (double)unit.maxEnergy();
					int cw2 = (int)(w * percEnergy);
					energyBar += 6;
					GUITools::DrawRect(Position(xx, yy + h), Position(xx + cw2, yy + h + 6), EnergyColors);
				}

				GUITools::DrawRectGradient(Position(xx, yy), Position(xx + cw, yy + h), PlayerColors[p], PlayerColorsDark[p]);
			}

			//unit portraits in the right hp bar side
			//if (unit.ID() < 255)
			//{
			//	glEnable( GL_TEXTURE_2D );
			//		glBindTexture( GL_TEXTURE_2D, unit.type().getID() );

			//		// draw the unit to the screen
			//		glColor4f(1, 1, 1, 1);
			//		glBegin( GL_QUADS );
			//			glTexCoord3d(0.0,0.0,.5); glVertex2i(xx, yy);
			//			glTexCoord3d(0.0,1.0,.5); glVertex2i(xx, yy+h);
			//			glTexCoord3d(1.0,1.0,.5); glVertex2i(xx+h,yy+h);
			//			glTexCoord3d(1.0,0.0,.5); glVertex2i(xx+h, yy);
			//		glEnd();
			//	glDisable( GL_TEXTURE_2D );
			//}
		}
	}
}

void GUIGame::drawParameters(int x, int y)
{
	return;
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif

	int size = 11;
	int spacing = 3;
	int colwidth = 175;
	int playerspacing = 350;
	for (size_t pp(0); pp < 2; ++pp)
	{
		GUITools::DrawString(Position(x + pp*playerspacing + _gui.cameraX(), y + _gui.cameraY()), "Player 1 Settings", PlayerColors[pp]);

		for (size_t p(0); _params[pp].size() > 0 && p<_params[pp][0].size(); ++p)
		{
			GUITools::DrawString(Position(x + pp*playerspacing + _gui.cameraX(), y + ((p + 1)*(size + spacing)) + _gui.cameraY()), _params[pp][0][p], White);
			GUITools::DrawString(Position(x + pp*playerspacing + colwidth + _gui.cameraX(), y + ((p + 1)*(size + spacing)) + _gui.cameraY()), _params[pp][1][p], White);
		}
	}

	//// Player 1 Settings
	//if (_params[0].size() > 0)
	//{
	//    GUITools::DrawString(Position(x, y), "Player 1 Settings", PlayerColors[0]);

	//    for (size_t p(0); _params[0].size() > 0 && p<_params[0][0].size(); ++p)
	//    {
	//        GUITools::DrawString(Position(x, y+((p+1)*(size+spacing))), _params[0][0][p], White);
	//        GUITools::DrawString(Position(x+colwidth, y+((p+1)*(size+spacing))), _params[0][1][p], White);
	//    }
	//}

	//if (_params[1].size() > 0)
	//{
	//    // Player 2 Settings
	//    x += playerspacing;
	//    glColor3f(0.0, 1.0, 0.0);
	//    DrawText(x, y , size, "Player 2 Settings");
	//    glColor3f(1.0, 1.0, 1.0);

	//    for (size_t p(0); params[1].size() > 0 && p<params[1][0].size(); ++p)
	//    {
	//        DrawText(x, y+((p+1)*(size+spacing)), size, params[1][0][p]);
	//        DrawText(x+colwidth, y+((p+1)*(size+spacing)), size, params[1][1][p]);
	//    }
	//}
}

void GUIGame::drawSearchResults(int x, int y)
{
	return;
#if DRAWHELPERS == drawHelpersFalse
	return;
#endif
	int size = 11;
	int spacing = 3;
	int colwidth = 175;
	int playerspacing = 350;
	for (size_t pp(0); pp < 2; ++pp)
	{
		//GUITools::DrawString(Position(x + pp*playerspacing + _gui.cameraX(), y + _gui.cameraY()), "Player 1 Search Results", PlayerColors[pp]);

		for (size_t p(0); _results[pp].size() > 0 && p<_results[pp][0].size(); ++p)
		{
			GUITools::DrawString(Position(x + pp*playerspacing + _gui.cameraX(), y + ((p + 1)*(size + spacing)) + _gui.cameraY()), _results[pp][0][p], White);
			GUITools::DrawString(Position(x + pp*playerspacing + colwidth + _gui.cameraX(), y + ((p + 1)*(size + spacing)) + _gui.cameraY()), _results[pp][1][p], White);
		}
	}
}


//drawing units itslef
void GUIGame::drawUnit(const Unit & unit)
{
	if (!unit.isAlive())
	{
		return;
	}

	const int healthBoxHeight = 4;

	const GameState & state = _game.getState();
	const BWAPI::UnitType & type = unit.type();
	const Position pos(unit.position());

	const int x0(pos.x() - type.dimensionUp());
	const int x1(pos.x() + type.dimensionDown());
	const int y0(pos.y() - type.dimensionUp());
	const int y1(pos.y() + type.dimensionDown());


	//GUITools::DrawLine(unit.topLeft(), unit.topRight(), 1, CollisionColors);
	//GUITools::DrawLine(unit.topLeft(), unit.botLeft(), 1, CollisionColors);
	//GUITools::DrawLine(unit.botLeft(), unit.botRight(), 1, CollisionColors);
	//GUITools::DrawLine(unit.botRight(), unit.topRight(), 1, CollisionColors);


	_gui.drawUnitType(unit.type(), pos, unit.isInvisible());
	drawUnitOnMinimap(pos, unit.player());
	//GUITools::DrawCircle(pos, 1.0f, 10);
	//GUITools::DrawCircle(Position(pos.x(), y1), 1.0f, 10);


	double	percHP = (double)unit.currentHP() / (double)unit.maxHP();
	int		cw = (int)((x1 - x0) * percHP);
	int		xx = pos.x() - (x1 - x0) / 2;
	int		yy = pos.y() - healthBoxHeight - (y1 - y0) / 2 - 5;

	GUITools::DrawRect(Position(xx, yy), Position(xx + cw, yy + healthBoxHeight), PlayerColors[unit.player()]);

	const Action & action = unit.previousAction();


	if (action.type() == ActionTypes::MOVE)
	{
		//glColor4f(1, 1, 1, 0.75);
		//glBegin(GL_LINES);
		//glVertex2i(pos.x(), pos.y());
		//glVertex2i(unit.pos().x(), unit.pos().y());
		//glEnd();
	}
	else if (action.type() == ActionTypes::ATTACK || action.type() == ActionTypes::ATTACKSPLASH || action.type() == ActionTypes::CASTABILITY)
	{


		//tady si nejsem �pln� jistej jestli je to dob�e, jestli se mi nem��e st�t, �e
		//se prost� jednotka pohne..
		GUITools::DrawLine(pos, unit.getPrevActionTargetPosition(), 1, PlayerColors[unit.player()]);
		//GUITools::DrawLine(pos, Position(320, 300), 1, PlayerColors[unit.player()]);
	}

}

void GUIGame::setGame(const Game & g)
{
	_game = g;
	_initialState = g.getState();
}

const Game & GUIGame::getGame() const
{
	return _game;
}

void GUIGame::setResults(const IDType & player, const std::vector<std::vector<std::string> > & r)
{
	_results[player] = r;
}

void GUIGame::setParams(const IDType & player, const std::vector<std::vector<std::string> > & p)
{
	_params[player] = p;
}
