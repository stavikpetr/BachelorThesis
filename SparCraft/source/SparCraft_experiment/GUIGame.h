#pragma once

#include "Common.h" //dealt with
#include "Position.hpp"
#include "Action.h"
#include "Game.h"
 
namespace SparCraft
{

	class GUI;
	class GUIGame
	{
		GUI &           _gui;
		Game            _game;
		double          _previousDrawGameTimer;
		double          _previousTurnTimer;
		GameState       _initialState;

		std::vector<std::vector<std::string> > _params[2];
		std::vector<std::vector<std::string> > _results[2];
		std::vector<std::vector<std::string> > _exp;

		void drawGame();
		void drawInfo();
		void drawWalkableGrid();
		void drawCollisionTiles();
		void drawHPBars();
		void drawMinimap();
		void drawBaseTiles();
		void drawUnitOnMinimap(Position unitPos, int playerid);
		void drawUnit(const Unit & unit);
		void drawParameters(int x, int y);
		void drawSearchResults(int x, int y);

		void setResults(const IDType & player, const std::vector<std::vector<std::string> > & r);
		void setParams(const IDType & player, const std::vector<std::vector<std::string> > & p);

	public:

		GUIGame(GUI & gui);

		const Game & getGame() const;
		void setGame(const Game & g);
		void onFrame();
		static void drawPath(std::deque<Position>& path);
		static void drawEffects(const std::vector<std::unique_ptr<Effect>>& effects);
		static void drawProjectiles(const std::vector<std::unique_ptr<Projectile>>& projectiles);
		static void drawFirebatRectangles(Position p1, Position p2, Position p3, Position p4,
			Position p1_, Position p2_, Position p3_, Position p4_,
			Position p1__, Position p2__, Position p3__, Position p4__);
		static void drawLurkerEffectRectangles(
			Position p1_, Position p2_, Position p3_, Position p4_);
		static void drawMutaliskAttack(Position(&hitUnitsPositions)[3], IDType playerID);
		static void drawSiegeTankSplasCircle(Position impactUnitPosition, int radiusZone1, int radiusZone2);
	};

}