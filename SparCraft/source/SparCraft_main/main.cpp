#include "SearchExperiment.h"

int main(int argc, char *argv[])
{
	try
    {
        if (argc == 2)
        {
            SparCraft::SearchExperiment exp(argv[1]);
			std::cout << "running new experiment" << std::endl;
			exp.runExperiment();
			std::cout << "experiment over" << std::endl;
        }
        else
        {
			std::cout << "please provide an experiment file as only argument" << std::endl;
        }
    }
    catch(int e)
    {
        if (e == SparCraft::System::SPARCRAFT_FATAL_ERROR)
        {
            std::cerr << "\nSparCraft FatalError Exception, Shutting Down\n\n";
        }
        else
        {
            std::cerr << "\nUnknown Exception, Shutting Down\n\n";
        }
    }
    return 0;
}
