#pragma once

#include "Common.h"
#include "Projectile.h"

namespace SparCraft
{
	class LurkerAttackProjectile : public Projectile
	{
		Unit* _caster;
		//the shared pointer is actually necessity
		std::shared_ptr<std::unordered_set<IDType>> _affectedUnits;
	public:
		LurkerAttackProjectile(Position startingPosition, Position targetPosition, double speed, int destinationDistance,
			TimeType nextActionTime, Unit* caster);
		void nextAction(GameState& state);
		bool didAffectUnit(IDType unitID);
		void addAffectedUnit(IDType unitID);
		Unit* getCaster();
		//due to the copy ctor of gamestate
		void setAffectedUnits(std::shared_ptr<std::unordered_set<IDType>> affectedUnits);
		std::shared_ptr<std::unordered_set<IDType>> getAffectedUnits();
	};
}