#include "SingleTargetAbility.h"

using namespace SparCraft;

SingleTargetAbility::SingleTargetAbility(int castingTime, BWAPI::TechType techTypeID,Unit*unit, int cooldown,
	int range) :
	Ability(castingTime, techTypeID, unit, cooldown)
{
	_range = range;
	_abilityTargetType = AbilityTargetTypes::SINGLE;
}

const int SingleTargetAbility::getRange() const
{
	return _range;
}
