#pragma once

#include "Common.h"
#include "Position.hpp"
#include "Vector.hpp"

namespace SparCraft
{
	namespace Constants
	{
		namespace Effects
		{
			extern bool EMPIsStackable;
			extern int EMPShieldDrain;
			extern int EMPEnergyDrain;
			extern int EMPRadius;
			extern int EMPtickInterval;
			extern TimeType EMPLastingTime;

			extern bool LurkerIsStackable;
			extern int LurkerDamage;
			extern int LurkerEffectLength;
			extern int LurkerEffectWidth;
			extern int LurkerTickInterval;
			extern TimeType LurkerLastingTime;

			extern bool StormIsStackable;
			extern int StormRadius;
			extern int StormDamage;
			extern TimeType StormTickInterval;
			extern TimeType StormLastingTime;
			extern TimeType StormDamageInteval;

			extern bool StimpackIsStackable;
			extern TimeType StimpackTickInterval;
			extern TimeType StimpackLastingTime;
			extern double StimpackASMultiplier;
			extern double StimpackMSMultiplier;

			extern double HealPerTickAmount;
			extern TimeType HealLastingTime;
			extern TimeType HealTickInterval;
		}
	}

	class Unit;
	class Effect;
	namespace EffectTypes
	{
		enum class EffectType;
	}

	class EffectFactory
	{
		EffectFactory()
		{};
		EffectFactory& operator=(const EffectFactory&) = delete;
		EffectFactory(const EffectFactory&) = delete;

		std::unique_ptr<Effect> getHealEffect(TimeType startingTime);
		std::unique_ptr<Effect> getStimpackEffect(TimeType startingTime);
	public:
		static EffectFactory& getInstance()
		{
			static EffectFactory effFactory;
			return effFactory;
		}

		std::unique_ptr<Effect> getChangeInvisibilityEffect(TimeType startingTime,
			bool newInvisibilityValue);

		std::unique_ptr<Effect> getEMPShockwaveEffect(TimeType startingTime, 
			IDType castedByUnitId, Position position);

		std::unique_ptr<Effect> getPsionicStormEffect(TimeType startingTime, Position position);

		std::unique_ptr<Effect> getLurkerAttackEffect(TimeType startingTime,
			std::shared_ptr<std::unordered_set<IDType>> affectedUnitIds, Position position,
			Vector dirUnitVector, Unit* caster);

		std::unique_ptr<Effect> getEffect(EffectTypes::EffectType effectType, TimeType startingTime);
	};
}
