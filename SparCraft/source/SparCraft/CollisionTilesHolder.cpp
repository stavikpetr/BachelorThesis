#include "CollisionTilesHolder.h"
#include "Unit.h"
#include <chrono>
using namespace SparCraft;
using namespace std;
using namespace std::chrono;
CollisionTilesHolder::CollisionTilesHolder() 
{
	collisionTilesNumberHorizontal_ = Constants::General::mapWidth / Constants::Map::collisionTileSize + 1;
	collisionTilesNumberVertical_ = Constants::General::mapHeight / Constants::Map::collisionTileSize + 1;

	collisionTiles_ = std::vector<std::vector<CollisionTile>>(collisionTilesNumberHorizontal_, std::vector<CollisionTile>(collisionTilesNumberVertical_, CollisionTile()));

}

CollisionTile& CollisionTilesHolder::getCollisionTile(int vectorXIndex, int vectorYIndex)
{
	return collisionTiles_[vectorXIndex][vectorYIndex];
}


int CollisionTilesHolder::getColTileXNumber(int posX)
{
	return posX / Constants::Map::collisionTileSize;
}

int CollisionTilesHolder::getColTileYNumber(int posY)
{
	return posY / Constants::Map::collisionTileSize;
}

bool CollisionTilesHolder::isValidColTileXIndex(int colTileXIndex)
{
	return colTileXIndex >= 0 && colTileXIndex < (int)collisionTilesNumberHorizontal_;
}

bool CollisionTilesHolder::isValidColTileYIndex(int colTileYIndex)
{
	return colTileYIndex >= 0 && colTileYIndex < (int)collisionTilesNumberVertical_;
}

std::pair<int, int> CollisionTilesHolder::getColTileNumber(Position pos)
{
	return std::make_pair<int, int>(getColTileXNumber(pos.x()), getColTileYNumber(pos.y()));
}

bool CollisionTilesHolder::isValidCollisionTilePosition(const Position& pos)
{
	return isValidColTileXIndex(getColTileXNumber(pos.x()))
		&& isValidColTileYIndex(getColTileYNumber(pos.y()))
		&& pos.x() >= 0 && pos.y() >= 0;
}

void CollisionTilesHolder::getCollisionTileIndices(const Position& topLeft, const Position& botRight,
	std::vector<std::pair<int, int>>& toFill)
{

	if (!isValidCollisionTilePosition(topLeft) || !isValidCollisionTilePosition(botRight))
		System::FatalError("get Collision tile Indices, not valid positions");

	toFill.clear();
	int origStartX = topLeft.x();
	int curX = topLeft.x();
	int curY = topLeft.y();
	int prevIndex, curIndex;
	while (true)
	{
		while (true)
		{
			toFill.push_back(getColTileNumber(Position(curX, curY)));
			if (curX == botRight.x() - 1) //remember collision box convention
				break;
			//indices prevents adding same tile multiple times
			prevIndex = getColTileXNumber(curX);
			curX += (curX + Constants::Map::collisionTileSize) >= botRight.x() ? botRight.x() - curX - 1 : Constants::Map::collisionTileSize;
			curIndex = getColTileXNumber(curX);
			if (prevIndex == curIndex)
				break;
		}
		if (curY == botRight.y() - 1) //remember collision box convention
			break;
		curX = origStartX;
		prevIndex = getColTileYNumber(curY);
		curY += (curY + Constants::Map::collisionTileSize) >= botRight.y() ? botRight.y() - curY - 1 : Constants::Map::collisionTileSize;
		curIndex = getColTileYNumber(curY);
		if (prevIndex == curIndex)
			break;
	}
}

void CollisionTilesHolder::resetCollisionTiles()
{
	for (auto& col : collisionTiles_)
	{
		for (auto& tile : col)
		{
			tile.resetTile();
		}
	}
}


void CollisionTilesHolder::updateCollisionTilesAfterMovement(Unit* unit)
{
	if (unit->type().isFlyer())
		return;
	if (!(unit->isAlive()))
		return;
	const Position p = unit->getPreviousPosition();

	getCollisionTileIndices(
		Position(p.x() - unit->dimLeft(), p.y() - unit->dimUp()),
		Position(p.x() + unit->dimRight() + 1, p.y() + unit->dimBot() + 1),
		indices_);

	for (auto& colTileIndex : indices_)
	{
		collisionTiles_[colTileIndex.first][colTileIndex.second].removeUnit(unit->ID());
	}
	addUnitToTiles(unit);

}

void CollisionTilesHolder::removeUnitFromTiles(Unit& deadUnit)
{
	if (deadUnit.type().isFlyer())
		return;
	getCollisionTileIndices(deadUnit.topLeft(), deadUnit.botRight(), indices_);
	for (auto& colTileIndex : indices_)
	{
		collisionTiles_[colTileIndex.first][colTileIndex.second].removeUnit(deadUnit.ID());
	}
}

void CollisionTilesHolder::addUnitToTiles(Unit* unit)
{
	if (unit->type().isFlyer())
		return;
	getCollisionTileIndices(unit->topLeft(), unit->botRight(), indices_);
	for (auto& colTileIndex : indices_)
	{
		collisionTiles_[colTileIndex.first][colTileIndex.second].addUnit(unit);
	}
}

void CollisionTilesHolder::getCollisionTilesIndicesForOrientedRectangle(Position p1,
	Position p2, Position p3, Position p4, std::vector<std::pair<int, int>>& toFill)
{
	std::array<Position, 4> positions({ { p1, p2, p3, p4 } });
	int maxX = std::numeric_limits<int>::min();
	int maxY = std::numeric_limits<int>::min();
	int minX = std::numeric_limits<int>::max();
	int minY = std::numeric_limits<int>::max();
	for (auto& p : positions)
	{
		if (p.x() > maxX)
			maxX = p.x();
		if (p.x() < minX)
			minX = p.x();
		if (p.y() > maxY)
			maxY = p.y();
		if (p.y() < minY)
			minY = p.y();
	}
	Position topLeft(minX, minY);
	Position botRight(maxX, maxY);

	if (botRight.x() <= 0 || botRight.y() <= 0 || topLeft.x() >= Constants::General::mapWidth ||
		topLeft.y() >= Constants::General::mapHeight)
		return;
	if (topLeft.x() < 0)
		topLeft = Position(0, topLeft.y());
	if (topLeft.y() < 0)
		topLeft = Position(topLeft.x(), 0);
	if (botRight.x() > Constants::General::mapWidth)
		botRight = Position(Constants::General::mapWidth, botRight.y());
	if (botRight.y() > Constants::General::mapHeight)
		botRight = Position(botRight.x(), Constants::General::mapHeight);
	getCollisionTileIndices(topLeft, botRight,toFill);
}

std::vector<Unit*> CollisionTilesHolder::getUniqueUnitsFromCollisionTiles(Position topLeft, Position botRight)
{

	if (topLeft.x() < 0)
		topLeft = Position(0, topLeft.y());
	if (topLeft.y() < 0)
		topLeft = Position(topLeft.x(), 0);
	if (botRight.x() > Constants::General::mapWidth)
		botRight = Position(Constants::General::mapWidth, botRight.y());
	if (botRight.y() > Constants::General::mapHeight)
		botRight = Position(botRight.x(), Constants::General::mapHeight);

	std::vector<std::pair<int, int>> collisionTileIndices;
	getCollisionTileIndices(topLeft, botRight, collisionTileIndices);
	return getUniqueUnitsFromCollisionTiles(collisionTileIndices);
}

std::vector<Unit*> CollisionTilesHolder::getUniqueUnitsFromCollisionTiles(Position p1, Position p2, Position p3_oppositeP1, Position p4_oppositeP2)
{
	std::vector<std::pair<int, int>> collisionTileIndices;
	getCollisionTilesIndicesForOrientedRectangle(p1, p2, p3_oppositeP1, p4_oppositeP2,collisionTileIndices);
	return getUniqueUnitsFromCollisionTiles(collisionTileIndices);
}

std::vector<Unit*> CollisionTilesHolder::getUniqueUnitsFromCollisionTiles(Position center, int radius)
{
	return getUniqueUnitsFromCollisionTiles(Position(center.x() - radius, center.y() - radius), Position(center.x() + radius + 1, center.y() + radius + 1));
}

std::vector<Unit*> CollisionTilesHolder::getUniqueUnitsFromCollisionTiles(std::vector<std::pair<int, int>>& collisionTileIndices)
{
	std::vector<Unit*> result;
	std::unordered_set<IDType> encounteredUnitIds;
	for (auto& colTileIndex : collisionTileIndices)
	{
		auto& colTile = collisionTiles_[colTileIndex.first][colTileIndex.second];
		for (auto& unit : colTile.getUnits())
		{
			if (encounteredUnitIds.find(unit->ID()) != encounteredUnitIds.end())
			{
				//we already have this unit...
			}
			else
			{
				result.push_back(unit);
				encounteredUnitIds.insert(unit->ID());
			}
		}
	}
	return result;
}