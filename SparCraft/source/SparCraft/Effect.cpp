#include "Effect.h"

using namespace SparCraft;

Effect::Effect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval) :_lastingTime(lastingTime),
_tickInterval(tickInterval), _startingTime(startingTime)
{
	_currentTime = _startingTime;
	_nextActionTime = _startingTime;
}

const TimeType Effect::getLastingTime() const
{
	return _lastingTime;
}

const EffectTypes::EffectType Effect::getEffectType() const
{
	return _effectType;
}

const TimeType Effect::getStartingTime() const
{
	return _startingTime;
}
const TimeType Effect::getNextActionTime() const
{
	return _nextActionTime;
}
void Effect::updateNextActionTime()
{
	if (_lastingTime == 0)
	{
		_nextActionTime = std::numeric_limits<int>::max();
	}
	else
	{
		_nextActionTime = _nextActionTime + _tickInterval;
	}
}
