#pragma once

#include "Common.h"
#include "StateEffect.h"
#include "Position.hpp"
#include "Vector.hpp"

namespace SparCraft
{
	class LurkerAttackProjectile;
	class LurkerAttackEffect : public StateEffect
	{
		//there is unfortunately no way around this pointer and this really need to be shared pointer
		std::shared_ptr<std::unordered_set<IDType>> _affectedUnitIDs;
		int _rectHeight;
		int _rectWidth;	
		Unit* _caster;
		Position _p;
		Vector _dirUnitVector;
	public:
		LurkerAttackEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval,
			std::shared_ptr<std::unordered_set<IDType>> affectedUnitIds, Position position,
			int rectHeight, int rectWidth, Vector directionUnitVector, Unit* caster);

		void AffectUnit(Unit* unitToAffect, TimeType& currentGameTime);
		int getRectWidth();
		int getRectHeight();
		Vector getDirectionUnitVector();
		Position getPosition();
		std::unordered_set<IDType>* getAffectedUnitIDs();
		std::vector<Unit*> getUnitsToAffect(GameState& state);
		Unit* getCaster();
	};
}