#pragma once

#include "Common.h"
#include "AoEAbility.h"

namespace SparCraft
{
	class EMPShockwaveAbility : public AoEAbility
	{
	public:
		const bool isCastableNow(const TimeType& currentTime) const;
		void beCasted(GameState& state, Position p);

		EMPShockwaveAbility(int castingTime, BWAPI::TechType techTypeID, Unit*, int cooldown,
			int range);
	};
}