#pragma once

#include "Common.h"

namespace SparCraft
{
	namespace EffectTypes
	{
		enum class EffectType { PSIONICSTORM, HEAL, STIMPACK, EMP, LURKERATTACK, CHANGEINVISIBILITY };
	}

	class Unit;
	class Effect
	{
	protected:
		TimeType	_lastingTime;
		TimeType	_tickInterval;
		TimeType _currentTime;
		TimeType _startingTime;
		TimeType _nextActionTime;
		EffectTypes::EffectType _effectType;

		Effect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval);
	public:
		virtual void AffectUnit(Unit* unit, TimeType& currentGameTime) = 0;
		const TimeType getLastingTime() const;
		const EffectTypes::EffectType getEffectType() const;
		const TimeType getNextActionTime() const;
		const TimeType getStartingTime() const;
		void updateNextActionTime();

	};
}