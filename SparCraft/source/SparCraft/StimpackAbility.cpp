#include "StimpackAbility.h"
#include "Unit.h"

using namespace SparCraft;

StimpackAbility::StimpackAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown) :
	StateAbility(castingTime, techTypeID, unit, cooldown)
{
	_resourceCost = unit->type() == BWAPI::UnitTypes::Terran_Marine ? Constants::Abilities::StimpackMarineResourceCost : Constants::Abilities::StimpackFirebatResourceCost;
	_abilityType = AbilityTypes::STIMPACK;
}

const bool StimpackAbility::isCastableNow(const TimeType& currentTime) const
{
	if (currentTime != _timeCanBeCasted)
		return false;
	if (_unit->currentHP() <= _resourceCost)
		return false;
	return true;
}

void StimpackAbility::beCasted(TimeType gameTime)
{
	//we might get to the situation where the unit took damage in the same frame it wanted to cast stimpack
	//in this situation, if the hp dropped below _resourceCost (10 for marines, 20 for firebats)
	//we will set unit's HPS to 1
	if (_unit->currentHP() < _resourceCost)
	{
		_unit->updateCurrentHP(1);
	}
	else
	{
		_unit->updateCurrentHP(_unit->currentHP()-_resourceCost);
	}
	//if stimpack was casted while another one was still active, we will just remove the old one and replace it
	if (_unit->containsUnitEffect(EffectTypes::EffectType::STIMPACK))
		_unit->removeUnitEffectByEffectId(EffectTypes::EffectType::STIMPACK);

	_unit->addUnitEffect(EffectFactory::getInstance().getEffect(
		EffectTypes::EffectType::STIMPACK, gameTime));
}