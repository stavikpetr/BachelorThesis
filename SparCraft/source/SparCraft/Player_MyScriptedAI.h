#pragma once

#include "Common.h"
#include "Player.h"

namespace SparCraft
{
	class Player_MyScriptedAI;
}

/*----------------------------------------------------------------------
| My scripted AI
|----------------------------------------------------------------------
| My first ai that uses moving to position
| Behaves like player_attackClosest when it comes to attacking
`----------------------------------------------------------------------*/
class SparCraft::Player_MyScriptedAI : public SparCraft::Player
{
public:
	Player_MyScriptedAI(const IDType & playerID);
	Player_MyScriptedAI(const IDType & playerID, bool followsFormationPolicy, Formation * formation);
	void getMoves(GameState & state, const MoveArray & moves, std::vector<Action> & moveVec);
	IDType getType() { return PlayerModels::MyScriptedAI; }
};