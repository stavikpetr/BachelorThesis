#include "ChangeSiegeState.h"
#include "Unit.h"

using namespace SparCraft;

ChangeSiegeState::ChangeSiegeState(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
	SiegeState currentSiegeState) :
	StateAbility(castingTime, techTypeID, unit, cooldown)
{
	_abilityType = AbilityTypes::CHANGESIEGESTATE;
	_currentSiegeState = currentSiegeState;
}

const bool ChangeSiegeState::isCastableNow(const TimeType& currentTime) const
{
	if (currentTime != _timeCanBeCasted)
		return false;
	return true;
}

void ChangeSiegeState::beCasted(TimeType gameTime)
{
	if (_currentSiegeState == SiegeState::SIEGED)
	{
		_unit->switchUnitType(BWAPI::UnitTypes::Terran_Siege_Tank_Tank_Mode);
		_unit->changeRange(BWAPI::UnitTypes::Terran_Siege_Tank_Tank_Mode);
		_unit->changeDealsSplashDamage(false);
		_currentSiegeState = SiegeState::UNSIEGED;
	}
	else if (_currentSiegeState == SiegeState::UNSIEGED)
	{
		_unit->switchUnitType(BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode);
		_unit->changeRange(BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode);
		_unit->changeDealsSplashDamage(true);
		_currentSiegeState = SiegeState::SIEGED;
	}
}

const SiegeState ChangeSiegeState::getCurrentState() const
{
	return _currentSiegeState;
}
