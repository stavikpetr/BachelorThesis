#pragma once

#include "Common.h"

///implementation of a traditional binary heap data structure that is used
///in the A* algorithm for pathfiding
namespace SparCraft
{
	template <typename T>
	class Heap
	{
		std::vector<T*> _elements;

		void siftDown(size_t index);
		void siftUp(size_t index);
		void swap(const size_t i, const size_t j);

	public:

		const int count() const;

		void insert(T* item);
		void updateHeapIndex(const size_t index);
		T* min();

		Heap(T* firstElement);
	};

	template <typename T>
	void Heap<T>::swap(const size_t i, const size_t j)
	{
		T* temp = _elements[i];
		_elements[i] = _elements[j];
		_elements[j] = temp;
		(*_elements[i]).updateHeapIndex(i);
		(*_elements[j]).updateHeapIndex(j);
	}


	template <typename T>
	void Heap<T>::siftDown(size_t index)
	{
		size_t leftChildIndex = 2 * index + 1;
		size_t rightChildIndex = 2 * index + 2;
		while (true)
		{
			if (leftChildIndex >= _elements.size())
				break;
			else
			{
				int smaller;
				if (rightChildIndex < _elements.size())
				{
					if (*(_elements[leftChildIndex]) < *(_elements[rightChildIndex]))
						smaller = leftChildIndex;
					else
						smaller = rightChildIndex;
				}
				else
					smaller = leftChildIndex;
				if (*(_elements[index]) < *(_elements[smaller]))
					break;
				else
				{
					swap(index, smaller);
					index = smaller;
					leftChildIndex = 2 * smaller + 1;
					rightChildIndex = 2 * smaller + 2;
				}
			}
		}
		(*(_elements[index])).updateHeapIndex(index);
	}

	template <typename T>
	void Heap<T>::siftUp(size_t index)
	{
		size_t parentIndex = (index - 1) / 2;
		if (_elements.size() == 1)
		{
			index = 0;
		}
		while (true)
		{
			if (index == 0)
				break;
			if (*(_elements[parentIndex]) > *(_elements[index]))
			{
				swap(parentIndex, index);
				index = parentIndex;
				parentIndex = (index - 1) / 2;
			}
			else
				break;
		}
		(*(_elements[index])).updateHeapIndex(index);
	}

	template <typename T>
	Heap<T>::Heap(T* firstElement)
	{
		_elements.push_back(firstElement);

	}

	template <typename T>
	T* Heap<T>::min()
	{
		T* toReturn = _elements[0];

		if (_elements.size() > 1)
		{
			_elements[0] = _elements[_elements.size() - 1];
		}
		_elements.pop_back();
		if (_elements.size() > 1)
		{
			siftDown(0);
		}
		return toReturn;
	}

	template <typename T>
	const int Heap<T>::count() const
	{
		return _elements.size();
	}

	template<typename T>
	void Heap<T>::insert(T* item)
	{
		_elements.push_back(item);
		siftUp(_elements.size() - 1);
	}

	template <typename T>
	void Heap<T>::updateHeapIndex(size_t index)
	{
		siftUp(index);
	}
}