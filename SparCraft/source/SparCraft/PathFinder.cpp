#include "PathFinder.h"
#include "GameState.h"
//#include "GUIGame.h"

using namespace SparCraft;

PathFinder::PathFinder(Map* map)
{
	numberOfPathFindingTilesHorizontal_ = map->getWalkTilesNumberHorizontal();
	numberOfPathFindingTilesVertical_ = map->getWalkTilesNumberVertical();
	//position for pathFindingNodes are in the middle of walkTile
	int amount = Constants::Map::walkTileSize / 2;
	for (int i = 0; i < map->getWalkTilesNumberHorizontal(); i++)
	{
		std::vector<std::unique_ptr<PathFindingTile>> column;
		for (int j = 0; j < map->getWalkTilesNumberVertical(); j++)
		{
			column.push_back(std::make_unique<PathFindingTile>(
				Position(i * Constants::Map::walkTileSize + amount, j * Constants::Map::walkTileSize + amount),
				map->isWalkableBaseTile(i, j)));
		}
		pathFindingTiles_.push_back(std::move(column));
	}
}

void PathFinder::insertUnitsIntoTiles(const Unit& startingUnit, const GameState& state)
{
	std::vector<std::pair<int, int>> pathFindingTilesIndices;
	pathFindingTilesIndices.reserve(9); //usually...

	for (IDType playerIndex(0); playerIndex < Constants::Num_Players; ++playerIndex)
	{
		if (state.getTime() == 0)
		{
			if (startingUnit.player() == playerIndex)
				continue;
		}

		for (IDType unitIndex(0); unitIndex < state.numUnits(playerIndex); ++unitIndex)
		{
			const Unit* u = state.getUnitPtrRaw(playerIndex, unitIndex);
			if (u->ID() == startingUnit.ID())
				continue;
			if (u->type().isFlyer())
				continue;
			pathFindingTilesIndices.clear();
			getPathFindingTilesIndices(u->topLeft(), u->botRight(), pathFindingTilesIndices);
			for (auto& p : pathFindingTilesIndices)
			{
				pathFindingTiles_[p.first][p.second]->addUnit(u);
				modifiedPathFindingTiles_.push_back(pathFindingTiles_[p.first][p.second].get());
			}
		}
	}
}


/*Let's say that the AI decides that it wants to go with unit Z to position (x,y), but this position
is currently occupied by some other unit; in order to fix this, i need to find a suitable position for unit Z
that is as close as possible to location (x,y); this method finds this closest location; how does this work?
We will first ask, if we can go to tile (x,y) (to wanted location), if yes, then great, and we have our
location; if not (is is not walkable/there is some unit already, we will try tiles (x-1, y-1), (x, y-1)
(x+1, y-1), .... (x+1, y+1) ; if any works, great, if not, we will try (x-2, y-2) and so on...*/
const Position PathFinder::getActualTargetPosition(const Unit& startingUnit, Position& targetPosition)
{
	/* let's try the following situation:
	Terran_Marine 0 400 410
	Terran_Marine 0 370 400
	Terran_SCV 1 802 450
	and if we look at the resulting path of unit Terran_Marine 370 400, the result
	might be a little bit odd, but, with the current algorithm for determining
	the actual position, the result is actually correct; just think about the reason
	for baseDiffX and baseDiffY and their passing to canUnitFitInWalkableTile*/

	//now, let's determine the actual target position
	//to do this, i need to determin the closest walkable position so that the starting unit can fit in this
	//position without colliding with other things
	PathFindingTile* pathFindingTile = getPathFindingTile(startingUnit.position().x(), startingUnit.position().y());
	int baseDiffX = startingUnit.position().x() - pathFindingTile->getPosition().x();
	int baseDiffY = startingUnit.position().y() - pathFindingTile->getPosition().y();

	std::vector<Position> possiblePositions;

	int stopIndexModifier = 0;
	int curXIndex = getPFTileXNumber(targetPosition.x());
	int curYIndex = getPFTileYNumber(targetPosition.y());
	bool anyFiting = false;
	std::vector<std::pair<int, int>> pathFindingTileIndices;
	while (true)
	{
		int stopIndex = stopIndexModifier * 2 + 1;

		for (int i = 0; i < stopIndex; i++)
		{
			for (int j = 0; j < stopIndex; j++)
			{
				bool notFitting = false;
				if (!(isValidPFTileXIndex(curXIndex + i) && isValidPFTileYIndex(curYIndex + j)))
					continue;
				Position pathFindingTilePos(pathFindingTiles_[curXIndex+i][curYIndex+j]->getPosition());
				Position aimedTargetPosition(pathFindingTilePos.x() + baseDiffX, pathFindingTilePos.y() + baseDiffY);
				Position aimedTopLeft(aimedTargetPosition.x() - startingUnit.dimLeft(), aimedTargetPosition.y() - startingUnit.dimUp());
				Position aimedBotRight(aimedTargetPosition.x() + startingUnit.dimRight() + 1, aimedTargetPosition.y() + startingUnit.dimBot() + 1); // remember collision box convention
				if (!isValidPosition(aimedTopLeft) || !isValidPosition(aimedBotRight))
					continue;
				if (aimedTargetPosition.x() == startingUnit.position().x() &&
					aimedTargetPosition.y() == startingUnit.position().y())
					continue;
				pathFindingTileIndices.clear();
				getPathFindingTilesIndices(aimedTopLeft, aimedBotRight, pathFindingTileIndices);
				for (auto& p : pathFindingTileIndices)
				{
					if (!pathFindingTiles_[p.first][p.second]->isWalkable())
						break;
					if (pathFindingTiles_[p.first][p.second]->hasUnits())
					{
						auto& unitsInTile = pathFindingTiles_[p.first][p.second]->getUnits();
						for (auto u : unitsInTile)
						{
							if (CollisionHelperFunctions::areAlignedCollisionBoxesColliding(
								aimedTopLeft.x(), aimedTopLeft.y(), startingUnit.width(), startingUnit.height(),
								u->topLeftX(), u->topLeftY(), u->width(), u->height()))
							{
								notFitting = true;
								break;
							}
						}
					}
					if (notFitting)
						break;
				}
				if(!notFitting)
				{
					possiblePositions.push_back(aimedTargetPosition);
					anyFiting = true;
				}
			}
		}
		curXIndex--;
		curYIndex--;
		if (anyFiting)
			break;

		stopIndexModifier++;
	}

	//ok, we've found some fitting walkable tiles closest to target unit, now, lets pick out the closest
	//and set the targetPosition to this node's position
	int shortestFound = std::numeric_limits<int>::max();
	Position bestOne;
	for (auto& p : possiblePositions)
	{
		int dist = (int)Position::getDist(startingUnit.position(), p);
		if (dist < shortestFound)
		{
			shortestFound = dist;
			bestOne = p;
		}
	}
	return bestOne;

}


std::deque<Position> PathFinder::getPath(const GameState& currentState, const Unit& startingUnit, Position targetPosition)
{
	insertUnitsIntoTiles(startingUnit, currentState);
	//remember, maybe needs to be fixed (look at commented part in getActualTargetPosition, there
	//i am returning position of node!!
	Position actualTargetPosition(getActualTargetPosition(startingUnit, targetPosition));
	//Position actualTargetPosition(targetPosition);
	std::deque<Position> path;
	findPath(startingUnit, actualTargetPosition, path);
	return path;

}

void PathFinder::findPath(const Unit& startingUnit, Position& targetPosition, std::deque<Position>& pathToFill)
{
	PathFindingTile* startingNode = getPathFindingTile(startingUnit.position().x(), startingUnit.position().y());
	startingNode->sethCost(std::numeric_limits<int>::max());
	startingNode->updateState(0);
	modifiedPathFindingTiles_.push_back(startingNode);
	int baseDiffX = startingUnit.position().x() - startingNode->getPosition().x();
	int baseDiffY = startingUnit.position().y() - startingNode->getPosition().y();
	Heap<PathFindingTile> heap(startingNode);
	PathFindingTile* curNode;
	PathFindingTile* bestNode = startingNode;
	Position targetPFTileNumber(getPFTileXNumber(targetPosition.x()), getPFTileYNumber(targetPosition.y()));

	
	/*The algorithm is A*; we start with first node, we add it's neighbours and then we move on the node
	that is closest to goal; if the indices of tiles(x,y) are same as the target position then we are done*/
	
	//let's find the path
	int numberOfIterations(0);
	while (heap.count() > 0)
	{		
		curNode = heap.min();
		if (curNode->gethCost() == 1)
			int b = 5;
		if (curNode->gethCost() < bestNode->gethCost())
			bestNode = curNode;
		curNode->updateState(-1);

		if (curNode->xIndex() == targetPFTileNumber.x() &&
			curNode->yIndex() == targetPFTileNumber.y())
		{
			//path was found
			break;
		}

		insertNeighbours(heap, curNode, baseDiffX, baseDiffY,
			targetPosition, startingUnit);
		numberOfIterations++;
	}

	//let's extract the path
	while (bestNode->getgCost() != 0)
	{
		pathToFill.push_front(Position(bestNode->getPosition().x() + baseDiffX,
			bestNode->getPosition().y() + baseDiffY));
		bestNode = bestNode->getParent();
	}

	resetModifiedPathFindingTiles();
}


void PathFinder::insertNeighbours(Heap<PathFindingTile>& heap, PathFindingTile* curNode, int baseDiffX,
	int baseDiffY, Position& targetPosition, const Unit& startingUnit)
{
	Position targetPFTileNumber(getPFTileXNumber(targetPosition.x()), getPFTileYNumber(targetPosition.y()));
	std::vector<std::pair<int, int>> directionPathFindingTileIndices;
	int friendlyUnitCollisionPenalty = 250;
	int enemyUnitCollisionPenalty = 500;

	for (IDType i(0); i < Constants::Num_Directions; ++i)
	{
		int x = curNode->getPosition().x() + Constants::Move_Dir[i][0] * Constants::Map::walkTileSize;
		int y = curNode->getPosition().y() + Constants::Move_Dir[i][1] * Constants::Map::walkTileSize;
		auto pfTileNumber = getPFTileNumber(Position(x, y));
		bool isValidDirection = true;
		int unitCollisionPenalty = 0;
		if (!isValidPFTileXIndex(pfTileNumber.first) || !isValidPFTileYIndex(pfTileNumber.second))
			continue;

		PathFindingTile* neighbour = pathFindingTiles_[pfTileNumber.first][pfTileNumber.second].get();
		if (!neighbour->isWalkable() || neighbour->getState() == -1)
			continue;

		/*The following part may be a bit confusing, but it was written this way to improve performance
		as much as possible
		
		We start with some neighbour, for this neighbour to be valid, it must hold that if we place the unit
		inside this neighbour (+baseDiffs), then there can't be any collisions; so, how to test collisions
		efficiently? if the unit stands on this neighbour, it will have cooridnates aimedPosition and the thing
		is, that if i am moving in direction (1, 0) then i only need to test the strip of pathFindingTiles that
		the unit would step on newly by moving in this direction; these tiles in strip are determined by 
		function getDirectionTopLeftBotRight and getPathFindingTileIndices (getDirectionTopLeftBotRight takes
		an advantage of already presented method getPathFindingTileIndices); in a same manner, based on other
		directions, i determine the pathFindingTileIndices;

		when i have the indices, then; if any of the tiles is unwalkable then this direction is not an option
		if all are, and there are some units, i want to check if the units standing like this would actually
		collide, if yes, there is some penalty for this node
		*/

		Position aimedPosition(neighbour->getPosition().x() + baseDiffX,
			neighbour->getPosition().y() + baseDiffY);
		Position aimedTopLeft(aimedPosition.x() - startingUnit.dimLeft(),
			aimedPosition.y() - startingUnit.dimUp());
		Position aimedBotRight(aimedPosition.x() + startingUnit.dimRight() + 1,
			aimedPosition.y() + startingUnit.dimBot() + 1);

		//while neighbour may be valid, the strips (whichever one) may not, because strip may be out of map bounds
		//so i need to check that, if it is, then this neighbour is not an option
		if (!isValidPosition(aimedTopLeft) || !isValidPosition(aimedBotRight))
		{
			neighbour->updateState(-1);
			modifiedPathFindingTiles_.push_back(neighbour);
			continue;
		}

		std::pair<Position, Position> directionTopLeftBotRight(
			getDirectionTopLeftBotRight(aimedTopLeft, aimedBotRight, Constants::Move_Dir[i]));

		getPathFindingTilesIndices(directionTopLeftBotRight.first,
			directionTopLeftBotRight.second, directionPathFindingTileIndices);

		

		for (auto & p : directionPathFindingTileIndices)
		{
			if (!(pathFindingTiles_[p.first][p.second]->isWalkable()))
			{
				isValidDirection = false;
				break;
			}
			if (pathFindingTiles_[p.first][p.second]->hasUnits())
			{
				auto& units = pathFindingTiles_[p.first][p.second]->getUnits();
				for (auto& u : units)
				{
					auto tl = u->topLeft();

					if (CollisionHelperFunctions::areAlignedCollisionBoxesColliding(
						aimedTopLeft.x(), aimedTopLeft.y(),
						startingUnit.width(), startingUnit.height(),
						u->topLeftX(), u->topLeftY(), u->width(), u->height()))
					{
						if (u->player() == startingUnit.player())
						{
							if (unitCollisionPenalty < friendlyUnitCollisionPenalty)
								unitCollisionPenalty = friendlyUnitCollisionPenalty;
						}
						else
						{
							unitCollisionPenalty = enemyUnitCollisionPenalty;
						}
					}
				}
			}
		}

		if (!isValidDirection)
		{
			neighbour->updateState(-1);
			modifiedPathFindingTiles_.push_back(neighbour);
			continue;
		}

		//the value 1 is for diffX + diffY between index of this neighbour and curNode it's always one
		int newGCost = 1 + curNode->getgCost() + unitCollisionPenalty;

		int diffX = targetPFTileNumber.x() - neighbour->xIndex();
		int diffY = targetPFTileNumber.y() - neighbour->yIndex();
		if (diffX < 0)
			diffX *= -1;
		if (diffY < 0)
			diffY *= -1;
		int newHCost = diffX + diffY;

		if (neighbour->getState() == 1)
		{
			neighbour->setgCost(newGCost);
			neighbour->sethCost(newHCost);
			neighbour->updateState(0);
			modifiedPathFindingTiles_.push_back(neighbour);
			neighbour->setParentNode(curNode);
			heap.insert(neighbour);
		}
		else if (neighbour->getfCost() > newHCost + newGCost)
		{
			neighbour->setgCost(newGCost);
			neighbour->sethCost(newHCost);
			neighbour->setParentNode(curNode);
			heap.updateHeapIndex(neighbour->getHeapIndex());
		}
	}
}

std::pair<Position, Position> PathFinder::getDirectionTopLeftBotRight(Position& unitAimedTopLeft, Position& unitAimedBotRight,
	const int moveDir[2])
{
	Position topLeft;
	Position botRight;
	//moving right
	if (moveDir[0] == 1)
	{
		topLeft = Position(unitAimedBotRight.x() - 1, unitAimedTopLeft.y());
		botRight = unitAimedBotRight;
	}
	//moving left
	else if (moveDir[0] == -1)
	{
		topLeft = unitAimedTopLeft;
		botRight = Position(unitAimedTopLeft.x() + 1, unitAimedBotRight.y());
	}
	//moving bot
	else if (moveDir[1] == 1)
	{
		topLeft = Position(unitAimedTopLeft.x(), unitAimedBotRight.y() - 1);
		botRight = unitAimedBotRight;
	}
	//moving top
	else if (moveDir[1] == -1)
	{
		topLeft = unitAimedTopLeft;
		botRight = Position(unitAimedBotRight.x(), unitAimedTopLeft.y() + 1);
	}

	return std::pair<Position, Position>(topLeft, botRight);
	
}

bool PathFinder::isValidPosition(const Position& position)
{
	if (position.x() < 0 || position.y() < 0 ||
		position.x() >= Constants::General::mapWidth || position.y() >= Constants::General::mapHeight)
		return false;
	return true;
}

void PathFinder::resetModifiedPathFindingTiles()
{
	for (auto& pftile : modifiedPathFindingTiles_)
	{
		pftile->reset();
	}
	modifiedPathFindingTiles_.clear();
}


void PathFinder::getPathFindingTilesIndices(const Position& topLeft,const Position& botRight,
	std::vector<std::pair<int, int>>& toFill)
{
	toFill.clear();
	int origStartX = topLeft.x();
	int curX = topLeft.x();
	int curY = topLeft.y();
	int prevIndex, curIndex;
	while (true)
	{
		while (true)
		{
			toFill.push_back(getPFTileNumber(Position(curX, curY)));
			if (curX == botRight.x() -1) //remember collision box convention
				break;
			//indices prevents adding same tile multiple times
			prevIndex = getPFTileXNumber(curX);
			curX += (curX + Constants::Map::walkTileSize) >= botRight.x() ? botRight.x() - curX - 1 : Constants::Map::walkTileSize;
			curIndex = getPFTileXNumber(curX);
			if (prevIndex == curIndex)
				break;
		}
		if (curY == botRight.y() - 1) //remember collision box convention
			break;
		curX = origStartX;
		prevIndex = getPFTileYNumber(curY);
		curY += (curY + Constants::Map::walkTileSize) >= botRight.y() ? botRight.y() - curY - 1 : Constants::Map::walkTileSize;
		curIndex = getPFTileYNumber(curY);
		if (prevIndex == curIndex)
			break;
	}
}

const bool PathFinder::isValidPFTileXIndex(const int xIndex) const
{
	return xIndex >= 0 && xIndex < numberOfPathFindingTilesHorizontal_;
}

const bool PathFinder::isValidPFTileYIndex(const int yIndex) const
{
	return yIndex >= 0 && yIndex < numberOfPathFindingTilesVertical_;
}

const bool PathFinder::isValidPFTilePos(const Position& pos) const
{
	return isValidPFTileXIndex(getPFTileXNumber(pos.x())) && isValidPFTileYIndex(getPFTileYNumber(pos.y())) &&
		pos.x() >=0 && pos.y() >= 0;
}

int PathFinder::getPFTileXNumber(const int posX) const
{
	return posX / Constants::Map::walkTileSize;
}

PathFindingTile* PathFinder::getPathFindingTile(int posX, int posY)
{
	return pathFindingTiles_[getPFTileXNumber(posX)][getPFTileYNumber(posY)].get();
}


int PathFinder::getPFTileYNumber(const int posY) const
{
	return posY / Constants::Map::walkTileSize;
}

std::pair<int, int> PathFinder::getPFTileNumber(const Position & pos) const
{
	return std::make_pair<int, int>(getPFTileXNumber(pos.x()), getPFTileYNumber(pos.y()));
}