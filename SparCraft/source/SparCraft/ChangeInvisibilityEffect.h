#pragma once

#include "Common.h"
#include "Effect.h"

namespace SparCraft
{
	class ChangeInvisibilityEffect : public Effect
	{
		bool _newInvisibilityValue;
	public:
		void AffectUnit(Unit* unitToAffect, TimeType& currentGameTime);
		bool getNewInvisValue();
		ChangeInvisibilityEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval, bool newInvisibilityValue);
	};
}