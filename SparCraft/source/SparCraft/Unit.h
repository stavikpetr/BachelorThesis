#pragma once

#include "Common.h"

#include "Action.h"
#include "Position.hpp"
#include "Hash.h"
#include "PlayerProperties.h"
#include "UnitProperties.h"
#include "AnimationFrameData.h"
#include "SplashDamageDealer.h"

#include "Ability.h"
#include "AllAbilities.h"
#include "Effect.h"
#include "AllEffects.h"
#include <iostream>


namespace SparCraft
{
class Action;

class Unit 
{
    BWAPI::UnitType     _unitType;				// the BWAPI unit type that we are mimicing
    PositionType        _range;
	
	Position            _position;				// current location in a possibly infinite space
	
	IDType              _unitID;				// unique unit ID to the state it's contained in
    IDType              _playerID;				// the player who controls the unit
	
	HealthType          _currentHP;				// current HP of the unit
	double              _currentEnergy;

	TimeType            _timeCanMove;			// time the unit can next move
	TimeType            _timeCanAttack;			// time the unit can next attack

	Action              _previousAction;		// the previous action that the unit performed
	TimeType            _previousActionTime;	// the time the previous move was performed
	Position            _previousPosition;
		
    TimeType			_prevCurrentPosTime;
    Position			_prevCurrentPos;


	bool				_dealsSplashDamage;		//indicates whether this unit deals splash damage with its attack
	double              _energyRegenerationRate;
	double				_attackSpeedMultiplier;
	double				_moveSpeedMultiplier;
	bool				_isInvisible;


	Position			_previousActionTargetPosition;
	TimeType			_waitFrames;
	ActionTypes::ActionType _actionTypeBeforeWait;

	std::deque<Position> _path;

	std::vector<EffectTypes::EffectType> _activeStateEffects;

	std::vector<std::unique_ptr<Effect>> _activeUnitEffects;
	std::vector<std::unique_ptr<Ability>> abilities_;

public:

	Unit();
	Unit(const BWAPI::UnitType unitType, const IDType & playerID, const Position & pos);
	Unit(const BWAPI::UnitType unitType, const Position & pos, const IDType & unitID, const IDType & playerID, 
		 const HealthType & hp, const HealthType & energy, const TimeType & tm, const TimeType & ta);
	Unit(const Unit& u);
	Unit& operator=(const Unit& u);
	const bool operator < (const Unit & rhs) const;

    // action functions
	void                    setPreviousAction(const Action & m, const TimeType & previousMoveTime);
	void                    updateAttackActionTime(const TimeType & newTime);
	void                    updateMoveActionTime(const TimeType & newTime);
	void                    attack(const Action & move, const Unit & target, const TimeType & gameTime);
	void                    move(const Action & move, const TimeType & gameTime) ;
	void                    pass(const Action & move, const TimeType & gameTime);
	void                    takeAttack(const Unit & attacker, const double multiplier = 1.0);

	// conditional functions
	const bool			    isMobile()                  const;
	const bool			    isOrganic()                 const;
	const bool              isAlive()                   const;
	const bool              isInvisible()               const;
	const bool			    canAttackNow()              const;
	const bool			    canMoveNow()                const;
	const bool			    canKite()                   const;
	const bool			    canHeal()                   const;
	const bool              equalsID(const Unit & rhs)  const;
	bool					canSeeTarget(const Unit & unit, const TimeType & gameTime) const;
	const bool              canAttackTarget(const Unit & unit, const TimeType & gameTime) const;
	const bool              dealsSplashDamage()         const;

	//-----------
	//MY ADDED FUNCTIONS
	//-----------

	//abilities related
	const bool                                   hasAbilities() const;
	const bool                                   canCastAbilityNow() const;
	const TimeType                               nextAbilityCastTime() const;
	std::vector<std::unique_ptr<Ability>>& getAbilities();
	void clearVectors();

	void addAbility(std::unique_ptr<Ability>&& ability);

	void                    updateTimeAfterAbilityCast(const Action& move, const Ability* castedAbility, const TimeType & gameTime);
	void                    updateAbilitiesTime(const TimeType& newTime);

		
	//used only for line drawing
	const Position& getPrevActionTargetPosition() const;
	void setPreviousActionTargetPosition(Position position);

	void                    updateEnergy(const double amount);
	const double            getBasicEnergyRegenerationRate() const;	
	void changeIsInvisible(bool newIsInvisible);
	const bool hasPath() const;
	const TimeType getWaitFramesCount() const;
	std::deque<Position>& getPath();
	void				clearPath();
	void				pathPop();
	void				setPath(std::deque<Position> & path);
	void				changeDealsSplashDamage(bool dealsSplashDamage);
	const Position		getPreviousPosition() const;

	void				stuck(const Action & move, const TimeType & gameTime);
	void				moveToPosition(const Action & move, const TimeType & gameTime);
	void				waitOneFrame(const Action & move, const TimeType & gameTime);
	void				actionWait(const Action& move, const TimeType& gameTime);

	//mainly Siege tank related
	void				switchUnitType(const BWAPI::UnitType newType);
	const int			getMinRange() const;
	void				changeRange(const BWAPI::UnitType newType);

	void				changeMoveSpeedMultiplier(const double newMultiplier);
	void				changeAttackSpeedMultiplier(const double newMultiplier);
	void				setAttackActionTime(const TimeType& newTime); //used for example for lurkers
	void				setMoveActionTime(const TimeType& newTime);

	//helpers used mainly with collision detection
	const int			dimUp() const;
	const int			dimBot() const;
	const int			dimRight() const;
	const int			dimLeft() const;
	const int			width() const;
	const int			height() const;
	const Position		topLeft() const;
	const int			topLeftY() const;
	const int			topLeftX() const;
	const Position		topRight() const;
	const int			topRightY() const;
	const int			topRightX() const;
	const Position		botLeft() const;
	const int			botLeftY() const;
	const int			botLeftX() const;
	const Position		botRight() const;
	const int			botRightY() const;
	const int			botRightX() const;

	
	//Effects related
	const bool containsStateEffect(EffectTypes::EffectType effectType) const;
	void resetStateEffects();
	void addStateEffect(EffectTypes::EffectType effectType);
	void takeDamage(double damageTaken);

	const bool containsUnitEffect(EffectTypes::EffectType effectType) const;
	void removeUnitEffectByEffectId(EffectTypes::EffectType effectType);
	void addUnitEffect(std::unique_ptr<Effect>&& unitEffect);

	void removeEndedUnitEffects(TimeType& currentGameTime);
	std::vector<std::unique_ptr<Effect>>& getUnitEffects();

    // id related
	void                    setUnitID(const IDType & id);
	const IDType		    ID()                        const;
	const IDType		    player()                    const;

    // position related functions
	const Position &        position()                  const;
	const PositionType      x()                         const;
	const PositionType      y()                         const;
	const PositionType      range()                     const;
    void                    setPreviousPosition(const TimeType & gameTime);

    // health and damage related functions
	const HealthType        damage()                    const;
	const HealthType	    maxHP()                     const;
	const HealthType	    currentHP()                 const;
	const double		    currentEnergy()             const;
	const double		    maxEnergy()                 const;
    const HealthType        getArmor()                  const;
	const float			    dpf()                       const;
	void                    updateCurrentHP(const HealthType & newHP);
    const BWAPI::UnitSizeType getSize()                 const;
    const BWAPI::WeaponType getWeapon(BWAPI::UnitType target) const;
    const HealthType        getDamageTo(const Unit & unit) const;
    const PlayerWeapon      getWeapon(const Unit & target) const;

    // time and cooldown related functionsc
	const TimeType		    attackCooldown()            const;
	const TimeType		    nextAttackActionTime()      const;
	const TimeType		    nextMoveActionTime()        const;
	const TimeType		    previousActionTime()        const;
	const TimeType		    firstTimeFree()             const;
	const TimeType 		    attackInitFrameTime()       const;
	const TimeType 		    attackRepeatFrameTime()     const;
	void                    setCooldown(TimeType attack, TimeType move);

    // other functions
	const int			    typeID()                    const;
	const double		    speed()                     const;
	const BWAPI::UnitType   type()                      const;
	const Action &	        previousAction()            const;
	const std::string       name()                      const;
	void                    print()                     const;
    const std::string       debugString()               const;

	// hash functions
	const HashType          calculateHash(const size_t & hashNum, const TimeType & gameTime) const;
	void                    debugHash(const size_t & hashNum, const TimeType & gameTime) const;
};
}