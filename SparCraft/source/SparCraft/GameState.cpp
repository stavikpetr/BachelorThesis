#include "GameState.h"
#include "Player.h"
#include "Game.h"
#include "GUIGame.h"
#include <chrono>
#define logTrue 1
#define logFalse 0
#define LOG logFalse

using namespace std;
using namespace std::chrono;
using namespace SparCraft;

#define TABS(N) for (int i(0); i<N; ++i) { fprintf(stderr, "\t"); }

class UnitIndexCompare
{
    const GameState & state;
    int player;

public:

    UnitIndexCompare(const GameState & s, const int & p)
        : state(s)
        , player(p)
    {

    }

	const bool operator() (const int & u1, const int & u2) const
	{
        return state.getUnitDirect(player, u1) < state.getUnitDirect(player, u2);
    }
};

GameState::GameState()
	: _map()
	, _currentTime(0)
	, _maxUnits(Constants::Max_Units)
    , _sameHPFrames(0)
{
	_numUnits.fill(0);
	_prevNumUnits.fill(0);
	_numMovements.fill(0);
    _prevHPSum.fill(0);
	_units[0] = std::vector<Unit>(Constants::Max_Units, Unit());
	_units[1] = std::vector<Unit>(Constants::Max_Units, Unit());
	

    _unitIndex[0] = std::vector<int>(Constants::Max_Units, 0);
    _unitIndex[1] = std::vector<int>(Constants::Max_Units, 0);

	for (size_t u(0); u<_maxUnits; ++u)
	{
        _unitIndex[0][u] = u;
		_unitIndex[1][u] = u;
	}
}

GameState& GameState::operator=(const GameState& g)
{
	_map = g._map;
	_numUnits = g._numUnits;
	_prevNumUnits = g._prevNumUnits;
	_totalLTD = g._totalLTD;
	_totalSumSQRT = g._totalSumSQRT;
	_numMovements = g._numMovements;
	_prevHPSum = g._prevHPSum;
	_currentTime = g._currentTime;
	_maxUnits = g._maxUnits;
	_sameHPFrames = g._sameHPFrames;
	_pathFinder = g._pathFinder;
	_collisionResolver = g._collisionResolver;
	infrastructureHits_ = g.infrastructureHits_;
	_activeAbilityEffects.clear();
	_activeProjectiles.clear();

	doCopyCtorWork(g);
	return *this;
}

void GameState::doCopyCtorWork(const GameState& g)
{
	_units[0] = g._units[0];
	_units[1] = g._units[1];
	_unitIndex[0] = g._unitIndex[0];
	_unitIndex[1] = g._unitIndex[1];
	_collisionTilesHolder = CollisionTilesHolder();

	for (IDType p(0); p < Constants::Num_Players; p++)
	{
		for (IDType u(0); u < numUnits(p); u++)
		{
			Unit* unit = getUnitPtrRaw(p, u);
			_collisionTilesHolder.addUnitToTiles(unit);
		}
	}

	for (auto& p : g._activeProjectiles)
	{
		if (p->getProjectileType() == ProjectileTypes::ProjectileType::EMPPROJECTILE)
		{
			auto casted = dynamic_cast<EMPShockwaveProjectile*>(p.get());
			_activeProjectiles.push_back(ProjectileFactory::getInstance().getProjectile(
				casted->getProjectileType(), casted->getCurrentPosition(), casted->getTargetPosition(),
				(int)(Position::getDist(casted->getCurrentPosition(), casted->getTargetPosition())),
				casted->getNextActionTime(),
				getUnitByIDPtrRaw(casted->getCaster()->ID())));

			//let's now look at each effect that may be tied with this projectile
		}
		else if (p->getProjectileType() ==
			ProjectileTypes::ProjectileType::LURKERATTACKPROJECTILE)
		{
			auto casted = dynamic_cast<LurkerAttackProjectile*>(p.get());
			auto projectile = std::move(ProjectileFactory::getInstance().getProjectile(casted->getProjectileType(),
				casted->getCurrentPosition(), casted->getTargetPosition(),
				(int)(Position::getDist(casted->getCurrentPosition(), casted->getTargetPosition())),
				casted->getNextActionTime(),
				getUnitByIDPtrRaw(casted->getCaster()->ID())));

			std::shared_ptr<std::unordered_set<IDType>> ptr = std::make_shared<std::unordered_set<IDType>>
				(*(casted->getAffectedUnits().get()));

			auto castedPr = dynamic_cast<LurkerAttackProjectile*>(p.get());
			castedPr->setAffectedUnits(ptr);

			_activeProjectiles.push_back(std::move(projectile));

			for (auto& e : g._activeAbilityEffects)
			{
				if (e->getEffectType() == EffectTypes::EffectType::LURKERATTACK)
				{
					auto castedE = dynamic_cast<LurkerAttackEffect*>(e.get());
					if (castedE->getCaster()->ID() == casted->getCaster()->ID())
					{
						_activeAbilityEffects.push_back(EffectFactory::getInstance().getLurkerAttackEffect(
							castedE->getStartingTime(), castedPr->getAffectedUnits(),
							castedE->getPosition(), castedE->getDirectionUnitVector(),
							getUnitByIDPtrRaw(castedE->getCaster()->ID())));
					}
				}
			}
		}
	}

	for (auto& e : g._activeAbilityEffects)
	{
		if (e->getEffectType() == EffectTypes::EffectType::EMP)
		{
			auto casted = dynamic_cast<EMPShockwaveEffect*>(e.get());
			_activeAbilityEffects.push_back(EffectFactory::getInstance().getEMPShockwaveEffect(casted->getStartingTime(),
				casted->getCastedByUnitID(), casted->getPosition()));
		}
		else if (e->getEffectType() == EffectTypes::EffectType::PSIONICSTORM)
		{
			auto casted = dynamic_cast<PsionicStormEffect*>(e.get());
			_activeAbilityEffects.push_back(EffectFactory::getInstance().getPsionicStormEffect(casted->getStartingTime(),
				casted->getPosition()));
		}
		else if (e->getEffectType() == EffectTypes::EffectType::LURKERATTACK)
		{
			//this effect does not have to copied 
			//handled inside the projectiles
		}
	}
}

GameState::GameState(const GameState& g):
	_map(g._map), 
	_numUnits(g._numUnits),
	_prevNumUnits(g._prevNumUnits),
	_totalLTD(g._totalLTD),
	_totalSumSQRT(g._totalSumSQRT),
	_numMovements(g._numMovements),
	_prevHPSum(g._prevHPSum),
	_currentTime(g._currentTime),
	_maxUnits(g._maxUnits),
	_sameHPFrames(g._sameHPFrames),
	_pathFinder(g._pathFinder),
	_collisionResolver(g._collisionResolver),
	infrastructureHits_(g.infrastructureHits_),
	_collisionTilesHolder()
{

	doCopyCtorWork(g);	
}

// construct state from a save file (option StateRawDataFile in config file)
GameState::GameState(const std::string & filename)
{
	read(filename);
}


// call this whenever we are done with moves
void GameState::finishedMoving()
{
	// sort the unit vector based on time left to move
	sortUnits();
	// update the current time of the state
	updateGameTime();

    // calculate the hp sum of each player
    int hpSum[2];
    for (IDType p(0); p<Constants::Num_Players; ++p)
	{
		hpSum[p] = 0;

		for (IDType u(0); u<numUnits(p); ++u)
		{ 
            hpSum[p] += getUnit(p, u).currentHP();
        }
    }

    // if the hp sums match the last hp sum
    if (hpSum[0] == _prevHPSum[0] && hpSum[1] == _prevHPSum[1])
    {
        _sameHPFrames++;
    }
    else
    {
        _sameHPFrames = 0;
    }

    for (IDType p(0); p<Constants::Num_Players; ++p)
	{
        _prevHPSum[p] = hpSum[p];
    }
}

const HashType GameState::calculateHash(const size_t & hashNum) const
{
	HashType hash(0);

	for (IDType p(0); p < Constants::Num_Players; ++p)
	{
		for (IDType u(0); u < _numUnits[p]; ++u)
		{
			hash ^= Hash::magicHash(getUnit(p,u).calculateHash(hashNum, _currentTime), p, u);
		}
	}

	return hash;
}

//method for the old AIs not capable of taking advantage of the newly generated moves
void GameState::generateMoves(MoveArray & moves, const IDType & playerIndex) const
{
	moves.clear();

    // which is the enemy player
	IDType enemyPlayer  = getEnemy(playerIndex);

    // make sure this player can move right now
    const IDType canMove(whoCanMove());
	//std::cout << "generating moves for player: " << std::to_string(canMove) << "\n";
    if (canMove == enemyPlayer)
    {
        System::FatalError("GameState Error - Called generateMoves() for a player that cannot currently move");
    }

	// we are interested in all simultaneous moves
	// so return all units which can move at the same time as the first
	TimeType firstUnitMoveTime = getUnit(playerIndex, 0).firstTimeFree();
		
	bool shouldFollowFormation(false);

	for (IDType unitIndex(0); unitIndex < _numUnits[playerIndex]; ++unitIndex)
	{
		// unit reference
		const Unit & unit(getUnit(playerIndex,unitIndex));
		

		// if this unit can't move at the same time as the first
		if (unit.firstTimeFree() != firstUnitMoveTime)
		{
			// stop checking
			break;
		}

		if (unit.previousActionTime() == _currentTime && _currentTime != 0)
		{
            System::FatalError("Previous Move Took 0 Time: " + unit.previousAction().moveString());
		}

		moves.addUnit();

		// generate attack moves
		if (unit.canAttackNow())
		{
			
			for (IDType u(0); u<_numUnits[enemyPlayer]; ++u)
			{
				const Unit & enemyUnit(getUnit(enemyPlayer, u));
				bool invisible = false;
				if (enemyUnit.type().hasPermanentCloak())
				{
					invisible = true;
					for (IDType detectorIndex(0); detectorIndex < _numUnits[playerIndex]; ++detectorIndex)
					{
						// unit reference
						const Unit & detector(getUnit(playerIndex, detectorIndex));
						if (detector.type().isDetector() && detector.canSeeTarget(enemyUnit, _currentTime))
						{
							invisible = false;
							break;
						}
					}
				}	
				if (!invisible && unit.canAttackTarget(enemyUnit, _currentTime) && enemyUnit.isAlive())
				{
					if (unit.dealsSplashDamage())
					{
						moves.add(Action(unitIndex, playerIndex, ActionTypes::ATTACKSPLASH, u));
					}
					else
					{
						moves.add(Action(unitIndex, playerIndex, ActionTypes::ATTACK, u));
					}
				}
			}
		}
		else
		{
			if (!unit.canHeal())
			{
				moves.add(Action(unitIndex, playerIndex, ActionTypes::RELOAD, 0));
			}
		}
		
		// generate movement moves
		if (unit.isMobile())
		{
            // In order to not move when we could be shooting, we want to move for the minimum of:
            // 1) default move distance move tiem
            // 2) time until unit can attack, or if it can attack, the next cooldown
            double timeUntilAttack          = unit.nextAttackActionTime() - getTime();
            timeUntilAttack                 = timeUntilAttack == 0 ? unit.attackCooldown() : timeUntilAttack;

            // the default move duration
            //double defaultMoveDuration      = (double)Constants::Move_Distance / unit.speed();
			//based on the changes with collision, i had to set move duration to only one frame
			double defaultMoveDuration = 1.0;
	

            // if we can currently attack
			double chosenTime = timeUntilAttack != 0 ? std::min(timeUntilAttack, defaultMoveDuration) : defaultMoveDuration;

            // the chosen movement distance
            PositionType moveDistance       = (PositionType)(chosenTime * unit.speed());

            // DEBUG: If chosen move distance is ever 0, something is wrong
            if (moveDistance == 0)
            {
                System::FatalError("Move Action with distance 0 generated. timeUntilAttack:"+
					std::to_string(timeUntilAttack)+", speed:"+std::to_string(unit.speed()));
            }

            // we are only generating moves in the cardinal direction specified in common.h
			for (IDType d(0); d<Constants::Num_Directions; ++d)
			{			
                // the direction of this movement
              	Position dir(Constants::Move_Dir[d][0], Constants::Move_Dir[d][1]);
            
                if (moveDistance == 0)
                {
                    printf("%lf %lf %lf\n", timeUntilAttack, defaultMoveDuration, chosenTime);
                }

                // the final destination position of the unit
                Position dest = unit.position() + Position(moveDistance*dir.x(), moveDistance*dir.y());

                // if that poisition on the map is walkable 

                if (_map->isWalkableBaseTile(dest) || (unit.type().isFlyer() && _map->isFlyableBaseTile(dest)))
				{
                    // add the move to the MoveArray
					moves.add(Action(unitIndex, playerIndex, ActionTypes::MOVE, d, dest));
				}
			}
		}

		//ALWAYS ADD PASS ACTION TO THE BACK 
		//moves.add(Action(unitIndex, playerIndex, ActionTypes::PASS, moves.numMoves(unitIndex)));

		if (moves.numMoves(unitIndex) == 0)
		{
			moves.add(Action(unitIndex, playerIndex, ActionTypes::PASS, 0));
		}
	}
}

void GameState::generateMoves2(MoveArray & moves, const IDType & playerIndex)
{
	moves.clear();
	
	// which is the enemy player
	IDType enemyPlayer = getEnemy(playerIndex);

	// make sure this player can move right now
	const IDType canMove(whoCanMove());
	if (canMove == enemyPlayer)
	{
		System::FatalError("GameState Error - Called generateMoves() for a player that cannot currently move");
	}

	// we are interested in all simultaneous moves
	// so return all units which can move at the same time as the first
	TimeType firstUnitMoveTime = getUnit(playerIndex, 0).firstTimeFree();

	for (IDType unitIndex(0); unitIndex < _numUnits[playerIndex]; ++unitIndex)
	{
		// unit reference
		Unit & unit(getUnit(playerIndex, unitIndex));

		// if this unit can't move at the same time as the first
		if (unit.firstTimeFree() != firstUnitMoveTime)
		{
			// stop checking
			break;
		}

		if (unit.previousActionTime() == _currentTime && _currentTime != 0)
		{
			System::FatalError("Previous Move Took 0 Time: " + unit.previousAction().moveString());
		}

		moves.addUnit();

		// generate attack moves
		if (unit.canAttackNow())
		{
			for (IDType u(0); u<_numUnits[enemyPlayer]; ++u)
			{
				const Unit & enemyUnit(getUnit(enemyPlayer, u));
				
				//unit.canAttackTarget contains check that the enemy unit is not invisible..
				if (unit.canAttackTarget(enemyUnit, _currentTime) && enemyUnit.isAlive())
				{
					if (unit.dealsSplashDamage())
					{
						moves.add(Action(unitIndex, playerIndex, ActionTypes::ATTACKSPLASH, u));
					}
					else
					{
						moves.add(Action(unitIndex, playerIndex, ActionTypes::ATTACK, u));
					}
				}
			}
		}

		//canCastAbilityNow contains check for _abilities.count>0
		if (unit.canCastAbilityNow())
		{
			for (auto& ability : unit.getAbilities())
			{
				if (ability->isCastableNow(_currentTime))
				{
					if (ability->getAbilityTargetType() == AbilityTargetTypes::SINGLE)
					{
						auto castedToSingle = dynamic_cast<SingleTargetAbility*>(ability.get());

						//iterate through every unit
						for (IDType playerID = 0; playerID < Constants::Num_Players; playerID++)
						{
							for (IDType unitIndexNested(0); unitIndexNested < _numUnits[playerID]; ++unitIndexNested)
							{
								const Unit & candidateUnit(getUnit(playerID, unitIndexNested));
								if (castedToSingle->isCastableToUnit(candidateUnit))
								{
									moves.add(Action(unitIndex, playerIndex, ActionTypes::CASTABILITY,
										unitIndexNested, ability.get()));
								}
							}
						}
					}
					else
					{
						//else we have aoe or state ability of which we know that it is castable
						moves.add(Action(unitIndex, playerIndex, ActionTypes::CASTABILITY, -1, ability.get()));
					}
				}
			}			
		}
		//let's always add possibility for action wait, action wait is one frame wait
		moves.add(Action(unitIndex, playerIndex, ActionTypes::ACTIONWAIT, 0));

#pragma region moving

		moves.add(Action(unitIndex, playerIndex, ActionTypes::MOVETOPOSITION, 0));

		if(unit.isMobile() && unit.canMoveNow()&& unit.hasPath() && unit.previousAction().type() != ActionTypes::STUCK)
		{
			//we have path, lets take move from it
			std::deque<Position>& path = unit.getPath();
			Position& pathPosition = path.front();
			const Position& unitPosition = unit.position();
			int speed = (int)unit.speed();
			int resultPositionX;
			int resultPositionY;
			int direction;
			int distance;
			
			//we're moving left
			if (pathPosition.x() < unitPosition.x())
			{
				distance = unitPosition.x() - pathPosition.x();
				if (distance < speed)
				{
					if (path.size() > 1 && path[1].x() < pathPosition.x())
					{
						//two consecutiveMoves to the left						
						resultPositionX = unitPosition.x() - speed;
					}
					else
					{
						resultPositionX = pathPosition.x();
					}
				}
				else if (distance >= speed)
					resultPositionX = unitPosition.x() - speed;
				resultPositionY = pathPosition.y();
				direction = 0;
			}
			//we're moving right
			else if (pathPosition.x() > unitPosition.x())
			{
				distance = pathPosition.x() - unitPosition.x();
				if (distance < speed)
				{
					if (path.size() > 1 && path[1].x() > pathPosition.x())
					{
						//two consecutive moves to the right
						resultPositionX = unitPosition.x() + speed;
					}
					else
					{
						resultPositionX = pathPosition.x();
					}
				}
				else if (distance >= speed)
					resultPositionX = unitPosition.x() + speed;
				resultPositionY = pathPosition.y();
				direction = 1;
			}
			//we're moving down
			else if (pathPosition.y() > unitPosition.y())
			{
				distance = pathPosition.y() - unitPosition.y();
				if (distance < speed)
				{
					if (path.size() >1 && path[1].y() > pathPosition.y())
					{
						//two consecutive moves to the bot
						resultPositionY = unitPosition.y() + speed;
					}
					else
					{
						resultPositionY = pathPosition.y();
					}
				}
				else if (distance >= speed)
					resultPositionY = unitPosition.y() + speed;
				resultPositionX = pathPosition.x();
				direction = 2;
			}
			//we're moving up
			else if (pathPosition.y() < unitPosition.y())
			{
				distance = unitPosition.y() - pathPosition.y();
				if (distance < speed)
				{
					//two consecutive moves up
					if (path.size() > 1 && path[1].y() < pathPosition.y())
					{
						resultPositionY = unitPosition.y() - speed;
					}
					else
					{
						resultPositionY = pathPosition.y();
					}
				}
				else
					resultPositionY = unitPosition.y() - speed;
				resultPositionX = pathPosition.x();
				direction = 3;
			}

			moves.add(Action(unitIndex, playerIndex, ActionTypes::MOVE, direction, Position(resultPositionX, resultPositionY)));
		}
#pragma endregion

		//ALWAYS ADD PASS ACTION TO THE BACK 
		moves.add(Action(unitIndex, playerIndex, ActionTypes::PASS, moves.numMoves(unitIndex)));
		//this is my convention and is needed because of formation policies...
		//because i need to have PASS action in moves array...

	}
}

void GameState::checkInvalidSpellCasts(std::vector<Action> & moves)
{
	std::unordered_set<IDType> healTargetUnits;

	for (auto& action : moves)
	{
		if (action.hasAbility())
		{
			auto ability = action.getAbility();

			//problem is, when generating moves for casting aoe ability, i only generate one move - cast Ability
			//and it is up to AI to determine  where the ability is casted; but, i can't possibly controle if the AI
			//casts ability to position that is our of range, so i need to check this
			if (ability->getAbilityTargetType() == AbilityTargetTypes::AOE)
			{
				const Position p = action.pos();
				const Unit& u = getUnit(action.player(), action.unit());
				auto castedToAoE = dynamic_cast<const AoEAbility *>(ability);
				if (Position::getDist(u.position(), p) > castedToAoE->getRange())
					System::FatalError("AI generated casted aoe move that was out of range, which is invalid");
			}
			//heal ability is kind of special, because, if the AI chose badly that two medics should cast heal to the same unit,
			//one approach would be to just add heal effects but not allow them both to heal unit, but, that would unfortunately drain medics
			//mana, which would be kind of weird... basically, i want to exclued such possibilities with heal
			else if (ability->getAbilityType() == AbilityTypes::HEALABILITY)
			{
				const Unit& u = getUnit(action.player(), action.index()); //on "index" is the target unit index..
				auto search = healTargetUnits.find(u.ID());
				if (search != healTargetUnits.end())
				{
					//unit id was found, it was already targeted
					action.setNewType(ActionTypes::ACTIONWAIT);
					continue;
				}
				else
				{
					healTargetUnits.insert(u.ID());
				}
			}
			
		}
	}
}

void GameState::makeMoves(std::vector<Action> & moves)
{   

#if LOG == logTrue
	Logger::Instance().logMakeMoves(_currentTime);
#endif
	
    if (moves.size() > 0)
    {
        const IDType canMove(whoCanMove());
        const IDType playerToMove(moves[0].player());

        if (canMove == getEnemy(playerToMove))
        {
			std::cout << "fatal error\n";
			std::cout << "player that can move: " << std::to_string(canMove) << "\n";
			std::cout << "player that is marked as to move: " << std::to_string(playerToMove) << "\n";
			std::cout << "-----------------------\n";
			for (IDType p(0); p < Constants::Num_Players; p++)
			{
				std::cout << "current time: " << _currentTime << "\n";
				for (IDType u(0); u < numUnits(p); u++)
				{
					Unit& unit = getUnit(p, u);
					std::cout << "this unit belongs to player: " << std::to_string(unit.player()) << "\n";
					std::cout << "units first time free: " << unit.firstTimeFree() << "\n";
					std::cout << "next attack action time: " << unit.nextAttackActionTime() << "\n";
					std::cout << "next move action time: " << unit.nextMoveActionTime() << "\n";
				}
			}
            System::FatalError("GameState Error - Called makeMove() for a player that cannot currently move");
        }
    }

	//HANDLE INVALID SPELLS
	//heal is used on the same target
	//aoe ability is casted out of range
	checkInvalidSpellCasts(moves);

	/*now, we will have to handle possible collisions between units
	i will start by removing move actions from moves vector*/
	if (Constants::General::collisions)
	{
		std::vector<Action> moveActions;
		for (size_t i(0); i < moves.size(); i++)
		{
			if (moves[i].type() == ActionTypes::MOVE)
			{
				const Unit& u = getUnit(moves[i].player(), moves[i].unit());

				moveActions.push_back(moves[i]);
			}
		}
		moves.erase(std::remove_if(moves.begin(), moves.end(), [](Action& a){
			return a.type() == ActionTypes::MOVE; }), moves.end());

			if (moveActions.size() > 0)
			{
				_collisionResolver->resolveMoveCollisions(moveActions, *this, _collisionTilesHolder);
				//let's add modified move actions to the back...
				moves.insert(moves.end(), moveActions.begin(), moveActions.end());
			}
	}

    for (size_t m(0); m<moves.size(); ++m)
    {
        performAction(moves[m]);
    }
	//for debugging path finding and move making
	checkValidityOfMoves();
}

//reason for this function is evaluator...
void GameState::performTakeAttack(Unit& ourUnit, Unit& enemyUnit, double multiplier)
{
	if (enemyUnit.isAlive())
	{
		int curHp = enemyUnit.currentHP();
		enemyUnit.takeAttack(ourUnit, multiplier);
		int afterHp = enemyUnit.currentHP();

		if (enemyUnit.type() == BWAPI::UnitTypes::Zerg_Drone ||
			enemyUnit.type() == BWAPI::UnitTypes::Terran_SCV ||
			enemyUnit.type() == BWAPI::UnitTypes::Protoss_Probe)
			infrastructureHits_.push_back(std::pair<int, int>(_currentTime, curHp - afterHp));

		if (!enemyUnit.isAlive())
		{
			_collisionTilesHolder.removeUnitFromTiles(enemyUnit);
			_numUnits[enemyUnit.player()]--;
		}
	}

}

const std::vector<std::pair<int, int>>& GameState::getInfrastructureHits() const
{
	return infrastructureHits_;
}

void GameState::checkValidityOfMoves()
{
#ifndef _WIN32
	return;
#endif
	std::vector<std::pair<int, int>> walkTileIndices;
	for (IDType p(0); p < Constants::Num_Players; p++)
	{
		for (IDType u(0); u < numUnits(p); ++u)
		{
			Unit* unit = getUnitPtrRaw(p, u);
			if (!(unit->isAlive()))
				continue;

			if (unit->topLeftX() < 0 || unit->topLeftX() >= Constants::General::mapWidth ||
				unit->topLeftY() < 0 || unit->topLeftY() >= Constants::General::mapHeight ||
				unit->botRightX() > Constants::General::mapWidth ||
				unit->botRightY() > Constants::General::mapHeight)
			{
				std::cout << " topLeft: (" << std::to_string(unit->topLeftX()) << ", " << std::to_string(unit->topLeftY())
					<< "), " << "botRight: (" << std::to_string(unit->botRightX()) << ", " << std::to_string(unit->botRightY())
					<< ")" << std::endl;
				std::cout << "mapWidth: " << std::to_string(Constants::General::mapWidth) <<
					", mapHeight: " << std::to_string(Constants::General::mapHeight) << std::endl;
				System::FatalError("SparCraft: one of units is out of map");
			}
			

			_map->getBaseTileIndices(unit->topLeft(), unit->botRight(), walkTileIndices);
			auto a = unit->topLeft();
			auto b = unit->botRight();
			for (auto& p : walkTileIndices)
			{
				if (!_map->isWalkableBaseTile(p.first, p.second))
					System::FatalError("SparCraft: one of units is not on walkable tile");
			}

			for (IDType p_(0); p_ < Constants::Num_Players; p_++)
			{
				for (IDType u_(0); u_ < numUnits(p_); ++u_)
				{
					Unit* unitInner = getUnitPtrRaw(p_, u_);
					if (!(unitInner->isAlive()))
						continue;
					if (unit->ID() == unitInner->ID())
						continue;
					if (CollisionHelperFunctions::areAlignedCollisionBoxesColliding(
						unit->topLeftX(), unit->topLeftY(),
						unit->width(), unit->height(),
						unitInner->topLeftX(), unitInner->topLeftY(),
						unitInner->width(), unitInner->height()))
					{
						std::cout << "two units are colliding after make moves" << std::endl;
						if (unit->previousAction().type() == ActionTypes::MOVE &&
							unitInner->previousAction().type() == ActionTypes::MOVE)
						{
							System::FatalError("SparCraft: collision was between two moving units");
						}
						else
						{
							System::FatalError("SparCraft: collision was between standing and moving unit");
						}
					}
				}
			}
		}
	}
}

void GameState::performAction(Action & move)
{
	Unit & ourUnit		= getUnit(move.player(), move.unit());
	//std::shared_ptr<Unit> unitPtr = getUnitPtr(move.player(), move.unit());
	Unit* unitPtrRaw = getUnitPtrRaw(move.player(), move.unit());
	IDType player		= ourUnit.player();
	IDType enemyPlayer  = getEnemy(player);
#if LOG == logTrue
	Logger::Instance().logCurrentAction(move, player, ourUnit, *this);
#endif
	//always update energy by basic energy regeneration rate of our unit times the number of frames difference
	if (ourUnit.type().isSpellcaster())
	{
		double multiplier = _currentTime - ourUnit.previousActionTime();
		ourUnit.updateEnergy(ourUnit.getBasicEnergyRegenerationRate()*multiplier);
	}

	if (move.type() == ActionTypes::ATTACK)
	{
		Unit & enemyUnit(getUnit(enemyPlayer,move.index()));
        //Unit & enemyUnit(getUnitByID(enemyPlayer ,move.index()));
		ourUnit.setPreviousActionTargetPosition(enemyUnit.position());
			
		// attack the unit
		ourUnit.attack(move, enemyUnit, _currentTime);

		performTakeAttack(ourUnit, enemyUnit);
		ourUnit.clearPath();
	}
	else if (move.type() == ActionTypes::ATTACKSPLASH)
	{		
		Unit& enemyUnit(getUnit(enemyPlayer, move.index()));
		ourUnit.attack(move, enemyUnit, _currentTime);
		SplashDamageDealer::getInstance().dealSplashDamage(ourUnit, enemyUnit, *this);
	
		ourUnit.clearPath();
		ourUnit.setPreviousActionTargetPosition(enemyUnit.position());
	}
	else if (move.type() == ActionTypes::CASTABILITY)
	{
		Ability* ability = move.getAbility();
		if (ability->getAbilityTargetType() == AbilityTargetTypes::AOE)
		{
			auto castedToAoE = dynamic_cast<AoEAbility*>(ability);

			ourUnit.setPreviousActionTargetPosition(move.pos());
			castedToAoE->beCasted(*this, move.pos());
			ourUnit.updateTimeAfterAbilityCast(move, ability, _currentTime);

		}
		else if (ability->getAbilityTargetType() == AbilityTargetTypes::SELF)
		{
			auto castedToState = dynamic_cast<StateAbility*>(ability);
			ourUnit.setPreviousActionTargetPosition(ourUnit.position());
			castedToState->beCasted(_currentTime);
			ourUnit.updateTimeAfterAbilityCast(move, ability, _currentTime);
		}
		else if (ability->getAbilityTargetType() == AbilityTargetTypes::SINGLE)
		{
			auto castedToSingle = dynamic_cast<SingleTargetAbility*>(ability);

			Unit& targetUnit(getUnit(move.player(), move.index()));
			ourUnit.setPreviousActionTargetPosition(targetUnit.position());
		    castedToSingle->beCasted(targetUnit, _currentTime);
			ourUnit.updateTimeAfterAbilityCast(move, ability, _currentTime);
			//targetUnit.removeAffectingAbility(ability);

		}
		ourUnit.clearPath();
	}
	else if (move.type() == ActionTypes::MOVE)
	{
		_numMovements[player]++;
		ourUnit.move(move, _currentTime);
		_collisionTilesHolder.updateCollisionTilesAfterMovement(unitPtrRaw);
	}
	else if (move.type() == ActionTypes::ACTIONWAIT)
	{
		ourUnit.actionWait(move, _currentTime);
	}
	else if (move.type() == ActionTypes::PASS)
	{
		ourUnit.pass(move, _currentTime);
	}
	else if (move.type() == ActionTypes::STUCK)
	{
		ourUnit.stuck(move, _currentTime);
	}
	else if (move.type() == ActionTypes::WAITONEFRAME)
	{
		//the method is actually not correct TODO
		ourUnit.waitOneFrame(move, _currentTime);

	}
	else if (move.type() == ActionTypes::MOVETOPOSITION)
	{
		//bool collideWithFriendlyUnits = _currentTime == 0 ? false : true;
		std::deque<Position> path(_pathFinder->getPath(*this, ourUnit, move.pos()));
		if (path.size() == 0)
		{
#if LOG == logTrue
			Logger::Instance().logMessage("path was not found, unit is passing\n");
#endif
			ourUnit.pass(move, _currentTime);
		}
		else
		{
#if LOG == logTrue
			Logger::Instance().logMessage("path was found\n");
#endif
			GUIGame::drawPath(path);
			ourUnit.setPath(path);
			ourUnit.moveToPosition(move, _currentTime);
		}
	}
#if LOG == logTrue
	Logger::Instance().logNextActionTimes(ourUnit);
#endif

}


std::vector<std::unique_ptr<Projectile>>& GameState::getActiveProjectiles()
{
	return _activeProjectiles;
};

void GameState::handleStateEffects()
{
#if LOG == logTrue
	Logger::Instance().logMessage("---------------------\n");
	Logger::Instance().logMessage("handling state effects at time: " + std::to_string(_currentTime) + "\n");
#endif
	
	auto timeCopy = _currentTime;

	while (true)
	{
		bool erased = false;
		for (auto it = _activeAbilityEffects.begin(); it != _activeAbilityEffects.end(); ++it)
		{
			if ((*it)->getStartingTime() + (*it)->getLastingTime() < timeCopy)
			{
				_activeAbilityEffects.erase(it);
				erased = true;
				break;
			}
		}
		if (!erased)
			break;
	}

	for (auto& effect : _activeAbilityEffects)
	{
		if (effect->getNextActionTime() != _currentTime)
			continue;
#if LOG == logTrue
		Logger::Instance().logStateEffect(effect);
#endif
		auto castedToStateEffect = dynamic_cast<StateEffect*>(effect.get());
		for (auto& unitToAffect : castedToStateEffect->getUnitsToAffect(*this))
		{
			castedToStateEffect->AffectUnit(unitToAffect, _currentTime);
		}
		effect->updateNextActionTime();
	}
	//lets check if any unit died (during storm or during lurker attack)

	for (IDType p(0); p < Constants::Num_Players; p++)
	{
		for (IDType u(0); u < numUnits(p); u++)
		{
			Unit& unit(getUnit(p, u));
			unit.resetStateEffects();
			if (!unit.isAlive())
			{
				_collisionTilesHolder.removeUnitFromTiles(unit);
				_numUnits[p]--;
			}
		}
	}
	//lurker attack effect is not drawn in this part (see lurker attack effect)
	GUIGame::drawEffects(_activeAbilityEffects);
}

void GameState::handleUnitEffects()
{
#if LOG == logTrue
	Logger::Instance().logMessage("---------------------\n");
	Logger::Instance().logMessage("handling unit effects at time: " + std::to_string(_currentTime) + "\n");
#endif

	for (IDType player(0); player < Constants::Num_Players; player++)
	{
		for (IDType u(0); u < numUnits(player); u++)
		{
			Unit* unit = getUnitPtrRaw(player, u);
			unit->removeEndedUnitEffects(_currentTime);
			for (auto& effect : unit->getUnitEffects())
			{
				if (effect->getNextActionTime() != _currentTime)
					continue;
#if LOG == logTrue
				Logger::Instance().logUnitEffect(effect, *unit);
#endif

				effect->AffectUnit(unit, _currentTime);
				effect->updateNextActionTime();
			}
		}
	}

}

void GameState::handleProjectiles()
{
#if LOG == logTrue
	Logger::Instance().logMessage("---------------------\n");
	Logger::Instance().logMessage("handling projectils at time: " + std::to_string(_currentTime) + "\n");
#endif

	//let's remove projectiles that reached their destination
	while (true)
	{
		bool erased = false;
		for (auto it = _activeProjectiles.begin(); it != _activeProjectiles.end(); ++it)
		{
			if ((*it)->getCurrentPosition() == (*it)->getTargetPosition())
			{				
				_activeProjectiles.erase(it);
				erased = true;
				break;
			}
		}
		if (!erased)
			break;
	}

	for (auto& projectile : _activeProjectiles)
	{
		if (projectile->getNextActionTime() != _currentTime)
			continue;
		projectile->nextAction(*this);
#if LOG == logTrue
		Logger::Instance().logProjectile(projectile);
#endif
		projectile->updateNextActionTime();
	}
	GUIGame::drawProjectiles(_activeProjectiles);
}

void GameState::addEffect(std::unique_ptr<Effect>&& effect)
{
	_activeAbilityEffects.push_back(std::move(effect));
}

void GameState::addProjectile(std::unique_ptr<Projectile>&& projectile)
{
	_activeProjectiles.push_back(std::move(projectile));
}

const Unit & GameState::getUnitByID(const IDType & unitID) const
{
	for (IDType p(0); p<Constants::Num_Players; ++p)
	{
		for (IDType u(0); u<numUnits(p); ++u)
		{
			if (getUnit(p, u).ID() == unitID)
			{
				return getUnit(p, u);
			}
		}
	}

	System::FatalError("GameState Error: getUnitByID() Unit not found, id:" + std::to_string(unitID));
	return getUnit(0,0);
}

Unit* GameState::getUnitByIDPtrRaw(const IDType& unitID)
{
	for (IDType p(0); p<Constants::Num_Players; ++p)
	{
		for (IDType u(0); u<numUnits(p); ++u)
		{
			if (getUnitPtrRaw(p, u)->ID() == unitID)
			{
				return getUnitPtrRaw(p, u);
			}
		}
	}
	System::FatalError("GameState Error: getUnitByID() Unit not found, id:" + std::to_string(unitID));
	return getUnitPtrRaw(0, 0);
}

const Unit & GameState::getUnitByID(const IDType & player, const IDType & unitID) const
{
	for (IDType u(0); u<numUnits(player); ++u)
	{
		if (getUnit(player, u).ID() == unitID)
		{
			return getUnit(player, u);
		}
	}

	System::FatalError("GameState Error: getUnitByID() Unit not found, player:"+std::to_string(player)+" id:" + std::to_string(unitID));
	return getUnit(0,0);
}

Unit & GameState::getUnitByID(const IDType & player, const IDType & unitID) 
{
	for (IDType u(0); u<numUnits(player); ++u)
	{
		if (getUnit(player, u).ID() == unitID)
		{
			return getUnit(player, u);
		}
	}

	System::FatalError("GameState Error: getUnitByID() Unit not found, player:" + std::to_string(player) + " id:" + std::to_string(unitID));
	return getUnit(0,0);
}

const IDType GameState::getEnemy(const IDType & player) const
{
	return (player + 1) % 2;
}

const Unit & GameState::getClosestOurUnit(const IDType & player, const IDType & unitIndex)
{
	const Unit & myUnit(getUnit(player,unitIndex));

	size_t minDist(1000000);
	IDType minUnitInd(0);

	Position currentPos = myUnit.position();

	for (IDType u(0); u<_numUnits[player]; ++u)
	{
		if (u == unitIndex || getUnit(player, u).canHeal())
		{
			continue;
		}

		//size_t distSq(myUnit.distSq(getUnit(enemyPlayer,u)));
		size_t distSq(currentPos.getDistanceSq(getUnit(player, u).position()));

		if (distSq < minDist)
		{
			minDist = distSq;
			minUnitInd = u;
		}
	}

	return getUnit(player, minUnitInd);
}

const Unit & GameState::getClosestEnemyUnit(const IDType & player, const IDType & unitIndex, bool checkCloaked)
{
	const IDType enemyPlayer(getEnemy(player));
	const Unit & myUnit(getUnit(player,unitIndex));

	PositionType minDist(1000000);
	IDType minUnitInd(0);
    IDType minUnitID(255);

	Position currentPos = myUnit.position();

	for (IDType u(0); u<_numUnits[enemyPlayer]; ++u)
	{
        Unit & enemyUnit(getUnit(enemyPlayer, u));
		if (enemyUnit.isInvisible())
			continue;

		PositionType distSq = (int)Position::getDist(myUnit.position(), enemyUnit.position());

		if ((distSq < minDist))// || ((distSq == minDist) && (enemyUnit.ID() < minUnitID)))
		{
			minDist = distSq;
			minUnitInd = u;
            minUnitID = enemyUnit.ID();
		}
        else if ((distSq == minDist) && (enemyUnit.ID() < minUnitID))
        {
            minDist = distSq;
			minUnitInd = u;
            minUnitID = enemyUnit.ID();
        }
	}

	return getUnit(enemyPlayer, minUnitInd);
}

const Unit & GameState::getClosestEnemyWorker(const IDType & player, const IDType & unitIndex)
{
	const IDType enemyPlayer(getEnemy(player));
	const Unit & myUnit(getUnit(player, unitIndex));

	PositionType minDist(1000000);
	IDType minUnitInd(0);
	IDType minUnitID(255);

	Position currentPos = myUnit.position();

	for (IDType u(0); u<_numUnits[enemyPlayer]; ++u)
	{
		Unit & enemyUnit(getUnit(enemyPlayer, u));
		if (enemyUnit.type() != BWAPI::UnitTypes::Protoss_Probe &&
			enemyUnit.type() != BWAPI::UnitTypes::Zerg_Drone &&
			enemyUnit.type() != BWAPI::UnitTypes::Terran_SCV)
			continue;
		PositionType dist = (int)Position::getDist(myUnit.position(), enemyUnit.position());

		if ((dist < minDist))// || ((distSq == minDist) && (enemyUnit.ID() < minUnitID)))
		{
			minDist = dist;
			minUnitInd = u;
			minUnitID = enemyUnit.ID();
		}
		else if ((dist == minDist) && (enemyUnit.ID() < minUnitID))
		{
			minDist = dist;
			minUnitInd = u;
			minUnitID = enemyUnit.ID();
		}
	}

	return getUnit(enemyPlayer, minUnitInd);
}



bool GameState::playerHasWorkers(const IDType& player)
{
	for (IDType u(0); u < _numUnits[player]; ++u)
	{
		Unit & unit(getUnit(player, u));
		if (unit.type() == BWAPI::UnitTypes::Protoss_Probe ||
			unit.type() == BWAPI::UnitTypes::Zerg_Drone ||
			unit.type() == BWAPI::UnitTypes::Terran_SCV)
			return true;
	}
	return false;
}

//do i have more units than is the max allowed number?
const bool GameState::checkFull(const IDType & player) const
{
    if (numUnits(player) >= Constants::Max_Units)
    {
        std::stringstream ss;
        ss << "GameState has too many units. Constants::Max_Units = " << Constants::Max_Units;
        System::FatalError(ss.str());
        return false;
    }

    return false;
}


// Add a unit with given parameters to the state
// This function will give the unit a unique unitID
void GameState::addUnit(const BWAPI::UnitType type, const IDType playerID, const Position & pos)
{	
    checkFull(playerID);
    System::checkSupportedUnitType(type);

    // Calculate the unitID for this unit
    // This will just be the current total number of units in the state
    IDType unitID = _numUnits[Players::Player_One] + _numUnits[Players::Player_Two];	

	_units[playerID][_unitIndex[playerID][_numUnits[playerID]]] = Unit(type, playerID, pos);

	//_collisionTilesHolder.addUnitToTiles(unit);

    getUnit(playerID, _numUnits[playerID]).setUnitID(unitID);
	//getUnit(playerID, _numUnits[playerID]).addMock(MockAbility(&getUnit(playerID, _numUnits[playerID])));
	AbilityFactory::getInstance().addAbilitiesToUnit(
		_units[playerID][_unitIndex[playerID][_numUnits[playerID]]]);
	// Increment the number of units this player has
	_numUnits[playerID]++;
	_prevNumUnits[playerID]++;

    // And do the clean-up
	finishedMoving();
	calculateStartingHealth();
	
    if (!checkUniqueUnitIDs())
    {
        System::FatalError("GameState has non-unique Unit ID values");
    }
}

void GameState::sortUnits()
{
	// sort the units based on time free
	for (size_t p(0); p<Constants::Num_Players; ++p)
	{
		if (_prevNumUnits[p] <= 1)
		{
			_prevNumUnits[p] = _numUnits[p];
			continue;
		}
		else
		{
			/*for (int i=1; i<_prevNumUnits[p]; ++i)
			{
				// A[ i ] is added in the sorted sequence A[0, .. i-1]
				// save A[i] to make a hole at index iHole
				//Unit * item = _unitPtrs[p][i];
                int itemIndex = _unitIndex[p][i];
                Unit & itemUnit = getUnit(p, i);
                int iHole = i;
				// keep moving the hole to next smaller index until A[iHole - 1] is <= item
				//while ((iHole > 0) && (*item < *(_unitPtrs[p][iHole - 1])))
                while ((iHole > 0) && (itemUnit < getUnit(p, iHole-1)))
				{
					// move hole to next smaller index
					//_unitPtrs[p][iHole] = _unitPtrs[p][iHole - 1];
                    _unitIndex[p][iHole] = _unitIndex[p][iHole - 1];
					iHole = iHole - 1;
				}
				// put item in the hole
                _unitIndex[p][iHole] = itemIndex;
				//_unitPtrs[p][iHole] = item;
			}*/
	
			
			//_unitPtrs[p].sort(_prevNumUnits[p], UnitPtrCompare());
            std::sort(&_unitIndex[p][0], &_unitIndex[p][0] + _prevNumUnits[p], UnitIndexCompare(*this, p));
			_prevNumUnits[p] = _numUnits[p];
		}
	}	
}

void GameState::clearUnits(const IDType playerId)
{
	IDType enemyPlayer = getEnemy(playerId);

	for (IDType unitIndex(0); unitIndex < _numUnits[playerId]; ++unitIndex)
	{
		// unit reference
		Unit & unit(getUnit(playerId, unitIndex));
		_collisionTilesHolder.removeUnitFromTiles(unit);
	}

	//let's clear this players vector
	_units[playerId].clear();
	_units[playerId] = std::vector<Unit>(Constants::Max_Units, Unit());
	_numUnits[playerId] = 0;
	_prevNumUnits[playerId] = 0;
	
}


Unit* GameState::getUnitPtrRaw(const IDType& player, const UnitCountType& unitIndex)
{
	return &_units[player][_unitIndex[player][unitIndex]];
}

const Unit* GameState::getUnitPtrRaw(const IDType& player, const UnitCountType& unitIndex) const
{
	return &(_units[player][_unitIndex[player][unitIndex]]);
}

Unit & GameState::getUnit(const IDType & player, const UnitCountType & unitIndex)
{
    return _units[player][_unitIndex[player][unitIndex]];
}

const Unit & GameState::getUnit(const IDType & player, const UnitCountType & unitIndex) const
{
    return _units[player][_unitIndex[player][unitIndex]];
}

const size_t GameState::closestEnemyUnitDistance(const Unit & unit) const
{
	IDType enemyPlayer(getEnemy(unit.player()));

	size_t closestDist(0);

	for (IDType u(0); u<numUnits(enemyPlayer); ++u)
	{
		size_t dist((size_t)Position::getDist(unit.position(), getUnit(enemyPlayer, u).position()));
		if (dist > closestDist)
		{
			closestDist = dist;
		}
	}

	return closestDist;
}

const bool GameState::playerDead(const IDType & player) const
{
	if (numUnits(player) <= 0)
	{
		return true;
	}

	//added by me
	if (numUnits(player) > 0)
		return false;

	for (size_t u(0); u<numUnits(player); ++u)
	{
		/*if (getUnit(player, u).damage() > 0)
		{
			return false;
		}*/
		
	}

	return true;
}



const TimeType GameState::nextAbilitiesTime() const
{
	int leastTime = std::numeric_limits<int>::max();
	for (auto& stateEffect : _activeAbilityEffects)
	{
		if (stateEffect->getNextActionTime() < leastTime)
			leastTime = stateEffect->getNextActionTime();
	}
	return leastTime;
}

const TimeType GameState::nextProjectilesTime() const
{
	int leastTime = std::numeric_limits<int>::max();
	for (auto& projectile : _activeProjectiles)
	{
		if (projectile->getNextActionTime() < leastTime)
			leastTime = projectile->getNextActionTime();
	}
	return leastTime;
}


TimeType GameState::nextUnitEffectsTime()
{
	int leastTime = std::numeric_limits<int>::max();
	for (IDType p(0); p < Constants::Num_Players; p++)
	{
		for (IDType u(0); u < numUnits(p); u++)
		{			
			Unit& unit(getUnit(p, u));
			for (auto & unitEffect : unit.getUnitEffects())
			{
				if (unitEffect->getNextActionTime() < leastTime)
					leastTime = unitEffect->getNextActionTime();
			}
		}
	}

	return leastTime;
}


const TimeType GameState::nextPlayerTime() const
{
	TimeType p1Time(getUnit(0, 0).firstTimeFree());
	TimeType p2Time(getUnit(1, 0).firstTimeFree());

	return p1Time < p2Time ? p1Time : p2Time;
}


const IDType GameState::whoCanMove() const
{
	TimeType p1Time(getUnit(0,0).firstTimeFree());
	TimeType p2Time(getUnit(1,0).firstTimeFree());


	// if player one is to move first
	if (p1Time < p2Time)
	{
		return Players::Player_One;
	}
	// if player two is to move first
	else if (p1Time > p2Time)
	{
		return Players::Player_Two;
	}
	else
	{
		return Players::Player_Both;
	}
}

CollisionTilesHolder & GameState::getCollisionTilesHolder()
{
	return _collisionTilesHolder;
}


const bool GameState::checkUniqueUnitIDs() const
{
    std::set<IDType> unitIDs;

    for (size_t p(0); p<Constants::Num_Players; ++p)
    {
        for (size_t u(0); u<numUnits(p); ++u)
        {
            IDType unitID(getUnit(p, u).ID());
            if (unitIDs.find(unitID) != unitIDs.end())
            {
                return false;
            }
            else
            {
                unitIDs.insert(unitID);
            }
        }
    }

    return true;
}

void GameState::updateGameTime()
{
	int stateEffectsTime = nextAbilitiesTime();
	int unitEffectsTime = nextUnitEffectsTime();
	int projectilesTime = nextProjectilesTime();
	int p1Time = getUnit(Players::Player_One, 0).firstTimeFree();
	int p2Time = getUnit(Players::Player_Two, 0).firstTimeFree();
	_currentTime = std::min(std::min(std::min(std::min(p1Time, p2Time), projectilesTime), unitEffectsTime), stateEffectsTime);
	return;

	const IDType who(whoCanMove());

	// if the first player is to move, set the time to his time
	if (who == Players::Player_One)
	{
		_currentTime = getUnit(Players::Player_One, 0).firstTimeFree();
	}
	// otherwise it is player two or both, so it's equal to player two's time
	else
	{
		_currentTime = getUnit(Players::Player_Two, 0).firstTimeFree();
	}
}

const StateEvalScore GameState::eval(const IDType & player, const IDType & evalMethod, const IDType p1Script, const IDType p2Script) const
{
	StateEvalScore score;
	const IDType enemyPlayer(getEnemy(player));

	// if both players are dead, return 0
	if (playerDead(enemyPlayer) && playerDead(player))
	{
		return StateEvalScore(0, 0);
	}
	auto a = PlayerModels::Random;
	auto b = PlayerModels::NOKDPS;
	StateEvalScore simEval;

	if (evalMethod == SparCraft::EvaluationMethods::LTD)
	{
		score = StateEvalScore(evalLTD(player), 0);
	}
	else if (evalMethod == SparCraft::EvaluationMethods::LTD2)
	{
		score = StateEvalScore(evalLTD2(player), 0);
	}
	else if (evalMethod == SparCraft::EvaluationMethods::Playout)
	{
		score = evalSim(player, p1Script, p2Script);
	}

	if (score.val() == 0)
	{
		return score;
	}

	ScoreType winBonus(0);

	if (playerDead(enemyPlayer) && !playerDead(player))
	{
		winBonus = 100000;
	}
	else if (playerDead(player) && !playerDead(enemyPlayer))
	{
		winBonus = -100000;
	}

	return StateEvalScore(score.val() + winBonus, score.numMoves());
}

// evaluate the state for _playerToMove
const ScoreType GameState::evalLTD(const IDType & player) const
{
	const IDType enemyPlayer(getEnemy(player));
	
	return LTD(player) - LTD(enemyPlayer);
}

// evaluate the state for _playerToMove
const ScoreType GameState::evalLTD2(const IDType & player) const
{
	const IDType enemyPlayer(getEnemy(player));

	return LTD2(player) - LTD2(enemyPlayer);
}

const StateEvalScore GameState::evalSim(const IDType & player, const IDType & p1Script, const IDType & p2Script) const
{
	auto a = 5;
	const IDType p1Model = (p1Script == PlayerModels::Random) ? PlayerModels::NOKDPS : p1Script;
	auto b = 6;
	const IDType p2Model = (p2Script == PlayerModels::Random) ? PlayerModels::NOKDPS : p2Script;

	PlayerPtr p1(AllPlayers::getPlayerPtr(Players::Player_One, p1Model));
	PlayerPtr p2(AllPlayers::getPlayerPtr(Players::Player_Two, p2Model));
	
	Game* game = new Game(*this, p1, p2, 200);

	game->play();

	ScoreType evalReturn = game->getState().evalLTD2(player);

	StateEvalScore result(evalReturn, game->getState().getNumMovements(player));

	delete game;

	return result;
}


void GameState::calculateStartingHealth()
{
	for (IDType p(0); p<Constants::Num_Players; ++p)
	{
		float totalHP(0);
		float totalSQRT(0);

		for (IDType u(0); u<_numUnits[p]; ++u)
		{
			totalHP += getUnit(p, u).maxHP() * getUnit(p, u).dpf();
			totalSQRT += sqrtf(getUnit(p,u).maxHP()) * getUnit(p, u).dpf();;
		}

		_totalLTD[p] = totalHP;
		_totalSumSQRT[p] = totalSQRT;
	}
}

const ScoreType	GameState::LTD2(const IDType & player) const
{
	if (numUnits(player) == 0)
	{
		return 0;
	}

	float sum(0);

	for (IDType u(0); u<numUnits(player); ++u)
	{
		const Unit & unit(getUnit(player, u));

		sum += sqrtf(unit.currentHP()) * unit.dpf();
	}

	ScoreType ret = (ScoreType)(1000 * sum / _totalSumSQRT[player]);

	return ret;
}

const ScoreType GameState::LTD(const IDType & player) const
{
	if (numUnits(player) == 0)
	{
		return 0;
	}

	float sum(0);

	for (IDType u(0); u<numUnits(player); ++u)
	{
		const Unit & unit(getUnit(player, u));

		sum += unit.currentHP() * unit.dpf();
	}

	return (ScoreType)(1000 * sum / _totalLTD[player]);
}

void GameState::checkUnitsAreOnWalkableTiles() const
{
    // check to see if all units are on walkable tiles
	// called just before starting simulation for state
    for (size_t p(0); p<Constants::Num_Players; ++p)
    {
        for (size_t u(0); u<numUnits(p); ++u)
        {
            const Position & pos(getUnit(p, u).position());

            if (!_map->isWalkableBaseTile(pos))
            {
                std::stringstream ss;
                ss << "Unit initial position on non-walkable map tile: " << getUnit(p, u).name() << " (" << pos.x() << "," << pos.y() << ")";
                System::FatalError(ss.str());
            }
        }
    }
}

const size_t GameState::numUnits(const IDType & player) const
{
	return _numUnits[player];
}

const size_t GameState::prevNumUnits(const IDType & player) const
{
	return _prevNumUnits[player];
}

const Unit & GameState::getUnitDirect(const IDType & player, const IDType & unit) const
{
	return _units[player][unit];
}

const bool GameState::bothCanMove() const
{
	return getUnit(0, 0).firstTimeFree() == getUnit(1, 0).firstTimeFree();
}

void GameState::setTime(const TimeType & time)
{
	_currentTime = time;
}

const int & GameState::getNumMovements(const IDType & player) const
{
	return _numMovements[player];
}

const TimeType GameState::getTime() const
{
	return _currentTime;
}

const float & GameState::getTotalLTD(const IDType & player) const
{
	return _totalLTD[player];
}

const float & GameState::getTotalLTD2(const IDType & player)	const
{
	return _totalSumSQRT[player];
}

void GameState::setTotalLTD(const float & p1, const float & p2)
{
	_totalLTD[Players::Player_One] = p1;
	_totalLTD[Players::Player_Two] = p2;
}

// detect if there is a deadlock, such that no team can possibly win
const bool GameState::isTerminal() const
{
    // if someone is dead, then nobody can moveev
    if (playerDead(Players::Player_One) || playerDead(Players::Player_Two))
    {
        return true;
    }
	//original value was 200
    if (_sameHPFrames > 2000)
    {
        return true;
    }

	for (size_t p(0); p<Constants::Num_Players; ++p)
	{
		for (size_t u(0); u<numUnits(p); ++u)
		{
			// if any unit on any team is a mobile attacker
			if (getUnit(p, u).isMobile() && !getUnit(p, u).canHeal())
			{
				// there is no deadlock, so return false
				return false;
			}
		}
	}

	// at this point we know everyone must be immobile, so check for attack deadlock
	for (size_t u1(0); u1<numUnits(Players::Player_One); ++u1)
	{
		const Unit & unit1(getUnit(Players::Player_One, u1));

		for (size_t u2(0); u2<numUnits(Players::Player_Two); ++u2)
		{
			const Unit & unit2(getUnit(Players::Player_Two, u2));

			// if anyone can attack anyone else
			if (unit1.canAttackTarget(unit2, _currentTime) || unit2.canAttackTarget(unit1, _currentTime))
			{
				// then there is no deadlock
				return false;
			}
		}
	}
	
	// if everyone is immobile and nobody can attack, then there is a deadlock
	return true;
}

void GameState::setTotalLTD2(const float & p1, const float & p2)
{
	_totalSumSQRT[Players::Player_One] = p1;
	_totalSumSQRT[Players::Player_Two] = p2;
}

Map* GameState::getMap() const
{
	return _map;
}

void GameState::setMap(Map* map)
{
	_map = map;
}

void GameState::setPathFinder(PathFinder* pathFinder)
{
	_pathFinder = pathFinder;
}

void GameState::setCollisionResolver(CollisionResolver* collisionResolver)
{
	_collisionResolver = collisionResolver;
}

void GameState::insertUnitsIntoTiles()
{
	_collisionTilesHolder = CollisionTilesHolder();
	std::vector<std::pair<int, int>> colTileIndices;
	for (IDType p(0); p < Constants::Num_Players; ++p)
	{
		for (IDType u(0); u < numUnits(p); ++u)
		{
			Unit* unit = getUnitPtrRaw(p, u);
			_collisionTilesHolder.addUnitToTiles(unit);
		}
	}
}

// print the state in a neat way
void GameState::print(int indent) const
{
	TABS(indent);
	std::cout << calculateHash(0) << "\n";
	fprintf(stderr, "State - Time: %d\n", _currentTime);

	for (IDType p(0); p<Constants::Num_Players; ++p)
	{
		for (UnitCountType u(0); u<_numUnits[p]; ++u)
		{
			const Unit & unit(getUnit(p, u));

			TABS(indent);
			fprintf(stderr, "  P%d %5d %5d    (%3d, %3d)     %s\n", unit.player(), unit.currentHP(), unit.firstTimeFree(), unit.x(), unit.y(), unit.name().c_str());
		}
	}
	fprintf(stderr, "\n\n");
}

std::string GameState::toString() const
{

	std::stringstream ss;

	ss << calculateHash(0) << "\n";
	ss << "Time: " << _currentTime << std::endl;

	for (IDType p(0); p<Constants::Num_Players; ++p)
	{
		for (UnitCountType u(0); u<_numUnits[p]; ++u)
		{
			const Unit & unit(getUnit(p, u));

			ss << "  P" << (int)unit.player() << " " << unit.currentHP() << " (" << unit.x() << ", " << unit.y() << ") " << unit.name() << std::endl;
		}
	}
	ss << std::endl;

	return ss.str();
}

std::string GameState::toStringCompact() const
{
	std::stringstream ss;

	for (IDType p(0); p<Constants::Num_Players; ++p)
	{
        std::map<BWAPI::UnitType, size_t> typeCount;

		for (UnitCountType u(0); u<_numUnits[p]; ++u)
		{
			const Unit & unit(getUnit(p, u));

            if (typeCount.find(unit.type()) != std::end(typeCount))
            {
                typeCount[unit.type()]++;
            }
            else
            {
                typeCount[unit.type()] = 1;
            }
		}

        for (auto & kv : typeCount)
        {
            const BWAPI::UnitType & type = kv.first;
            const size_t count = kv.second;

            ss << "P" << (int)p << " " << count << " " << type.getName() << "\n";
        }
	}

	return ss.str();
}

void GameState::write(const std::string & filename) const
{
    std::ofstream fout (filename.c_str(), std::ios::out | std::ios::binary); 
    fout.write((char *)this, sizeof(*this)); 
    fout.close();
}
//this method servers for creating state out of pure filename (option StateRawDataFile in config file)
void GameState::read(const std::string & filename)
{
    std::ifstream fin (filename.c_str(), std::ios::in | std::ios::binary);
    fin.read((char *)this, sizeof(*this));
    fin.close();
}
