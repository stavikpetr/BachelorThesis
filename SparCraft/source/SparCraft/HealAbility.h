#pragma once

#include "Common.h"
#include "SingleTargetAbility.h"

namespace SparCraft
{
	class HealAbility :public SingleTargetAbility
	{
	public:

		const bool isCastableNow(const TimeType& currentTime) const;
		const bool isCastableToUnit(const Unit& unit) const;
		void beCasted(Unit& target, TimeType gameTime = -1);
		const bool matchesType(const Unit& target) const;

		HealAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
			 int range);
	};
}