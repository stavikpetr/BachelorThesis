#pragma once

#include "Common.h"
#include "StateAbility.h"

namespace SparCraft
{
	enum class SiegeState { SIEGED, UNSIEGED };

	class ChangeSiegeState : public StateAbility
	{
		SiegeState _currentSiegeState;
	public:
		const bool isCastableNow(const TimeType& currentTime) const;
		void beCasted(TimeType gameTime = -1);
		const SiegeState getCurrentState() const;

		ChangeSiegeState(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
			SiegeState currentSiegeState);
	};
}