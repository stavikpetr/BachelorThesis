#include "Map.h"

using namespace SparCraft;

Map::Map()
{
	_walkTilesNumberHorizontal = Constants::General::mapWidth / Constants::Map::walkTileSize;
	_walkTilesNumberVertical = Constants::General::mapHeight / Constants::Map::walkTileSize;
	_buildTilesNumberVertical = Constants::General::mapWidth / Constants::Map::buildTileSize;
	_buildTilesNumberHorizontal = Constants::General::mapHeight / Constants::Map::buildTileSize;
	

	//let's set every base tile to walkable
	_baseWalkTiles = std::vector<std::vector<bool>>(_walkTilesNumberHorizontal, std::vector<bool>(_walkTilesNumberVertical, true));

}

int Map::getWalkTilesNumberHorizontal()
{
	return _walkTilesNumberHorizontal;
}

int Map::getWalkTilesNumberVertical()
{
	return _walkTilesNumberVertical;
}


void Map::setWalkableBaseTile(int walkTileXIndex, int walkTileYIndex, bool walkable)
{
	if (isValidWalkTileXIndex(walkTileXIndex) && isValidWalkTileYIndex(walkTileYIndex))
	{
		_baseWalkTiles[walkTileXIndex][walkTileYIndex] = walkable;
	}
	else
	{
		System::FatalError("set walkable base tile... invalid walk tile indices");
	}
}


void Map::getBaseTileIndices(const Position& topLeft, const Position& botRight, std::vector<std::pair<int, int>>& toFill)
{
	toFill.clear();
	int origStartX = topLeft.x();
	int curX = topLeft.x();
	int curY = topLeft.y();
	int prevIndex, curIndex;
	while (true)
	{
		while (true)
		{
			auto& wt = getWalkTileNumber(Position(curX, curY));
			toFill.push_back(std::pair<int, int>(wt.x(), wt.y()));
			if (curX == botRight.x() - 1) //remember collision box convention
				break;
			//indices prevents adding same tile multiple times
			prevIndex = getWalkTileXIndex(curX);
			curX += (curX + Constants::Map::walkTileSize) >= botRight.x() ? botRight.x() - curX - 1 : Constants::Map::walkTileSize;
			curIndex = getWalkTileXIndex(curX);
			if (prevIndex == curIndex)
				break;
		}
		if (curY == botRight.y() - 1) //remember collision box convention
			break;
		curX = origStartX;
		prevIndex = getWalkTileYIndex(curY);
		curY += (curY + Constants::Map::walkTileSize) >= botRight.y() ? botRight.y() - curY - 1 : Constants::Map::walkTileSize;
		curIndex = getWalkTileYIndex(curY);
		if (prevIndex == curIndex)
			break;
	}
}


const bool Map::isValidWalkTileXIndex(int walkTileXIndex) const
{
	if (walkTileXIndex >= 0 && walkTileXIndex < (int)_walkTilesNumberHorizontal)
		return true;
	return false;
}

const bool Map::isValidWalkTileYIndex(int walkTileYIndex) const
{
	if (walkTileYIndex >= 0 && walkTileYIndex < (int)_walkTilesNumberVertical)
		return true;
	return false;
}


const Position Map::getWalkTileNumber(const Position& position) const
{
	return getWalkTileNumber(position.x(), position.y());
}

const Position Map::getWalkTileNumber(const int posX, const int posY) const
{
	return Position(posX / Constants::Map::walkTileSize, posY / Constants::Map::walkTileSize);
}

const bool Map::isWalkableBaseTile(const size_t & walkTileX, const size_t & walkTileY) const
{
	return walkTileX >= 0 && walkTileX < _walkTilesNumberHorizontal &&
		walkTileY >= 0 && walkTileY < _walkTilesNumberVertical &&
		_baseWalkTiles[walkTileX][walkTileY];
}

const bool Map::isWalkableBaseTile(const SparCraft::Position& position) const
{
	const Position& walkTileNumber(getWalkTileNumber(position));
	return isWalkableBaseTile(walkTileNumber.x(), walkTileNumber.y());
}

const bool Map::isFlyableBaseTile(const size_t & walkTileX, const size_t & walkTileY) const
{
	return walkTileX >= 0 && walkTileX < _walkTilesNumberHorizontal &&
		walkTileY >= 0 && walkTileY < _walkTilesNumberVertical;
}

const bool Map::isFlyableBaseTile(const SparCraft::Position & position) const
{
	const Position& walkTileNumber(getWalkTileNumber(position));
	return isFlyableBaseTile(walkTileNumber.x(), walkTileNumber.y());
}

const int Map::getWalkTileXIndex(int XPixelAmount) const
{
	return XPixelAmount / Constants::Map::walkTileSize;
}
const int Map::getWalkTileYIndex(int YPixelAmount) const
{
	return YPixelAmount / Constants::Map::walkTileSize;
}
