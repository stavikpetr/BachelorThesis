#pragma once

#include "Common.h"
#include "Vector.hpp"
#include "Position.hpp"

namespace SparCraft
{
	namespace Constants
	{
		namespace Units
		{
			extern int siegeTankSiegedMinRange;
			extern int siegeTankSplashZone1Radius;
			extern int siegeTankSplashZone2Radius;
			extern double siegeTankSplashZone1Multiplier;
			extern double siegeTankSplashZone2Multiplier;

			extern int firebatSplashConeWidth;
			extern int firebatSplashConeLength;
			extern int firebatSplashRadialWidth;
			extern int firebatSplashRadialLength;

			extern int MutaliskBounceRange;
			extern double MutaliskSecondHitMultiplier;
			extern double MutaliskThirdHitMultiplier;

			extern int LurkerAttackDistance;
		}
	}

	class Unit;
	class GameState;
	class SplashDamageDealer
	{

		void performMutaliskAttack(Unit& ourUnit, Unit& impactUnit,
			GameState& state);
		void performSiegeTankAttack(Unit& ourUnit, Unit& impactUnit,
			GameState& state);
		void performFirebatAttack(Unit& ourUnit, Unit& impactUnit,
			GameState& state);
		void performLurkerAttack(Unit& ourUnit, Unit& impactUnit,
			GameState& state);
		SplashDamageDealer(){};
	public:
		static SplashDamageDealer& getInstance()
		{
			static SplashDamageDealer s;
			return s;
		};
		double maximumOfProjectedValues(std::vector<Vector> projectedValues, Position axis);
		double minimumOfProjectedValues(std::vector<Vector> projectedValues, Position axis);

		std::vector<Vector> getProjectedPositions(std::array<Position, 4>& positionsToProject, Position axisToProjectOn);
		std::vector<double> getScaledProjectedPositions(std::vector<Vector> projectedValues, Position axis);
		void dealSplashDamage(Unit& ourUnit, Unit& impactUnit,
			GameState& state);
	};
}