#pragma once

#include "Common.h"
#include "Position.hpp"
#include <memory>

namespace SparCraft
{
	namespace Constants
	{
		namespace Projectiles
		{
			extern double EMPSpeed;
			extern double LurkerSpeed;
		}
	}

	namespace ProjectileTypes
	{
		enum class ProjectileType;
	}

	class Projectile;

	class ProjectileFactory
	{
		ProjectileFactory()
		{};
		ProjectileFactory& operator=(const ProjectileFactory&) = delete;
		ProjectileFactory(const ProjectileFactory &) = delete;
		
		std::unique_ptr<Projectile> getLurkerAttackProjectile(
			const Position& startingPosition, const Position& targetPosition,
			int destinationDistance, TimeType nextActionTime, Unit* caster);

		std::unique_ptr<Projectile> getEMPShockwaveProjectile(
			const Position& startingPosition, const Position& targetPosition,
			int destinationDistance, TimeType nextActionTime, Unit* caster);
	public:
		static ProjectileFactory& getInstance()
		{
			static ProjectileFactory projFactory;
			return projFactory;
		};

		std::unique_ptr<Projectile> getProjectile(ProjectileTypes::ProjectileType projectileType,
			const Position& startingPosition, const Position& targetPosition,
			int destinationDistance, TimeType nextActionTime,
			Unit* caster);
	};
}