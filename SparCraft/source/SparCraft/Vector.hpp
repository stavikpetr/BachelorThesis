#pragma once

#include "Common.h" 
#include <math.h>

namespace SparCraft
{
	struct Vector
	{
		double _x;
		double _y;

		double x() {
			return _x;
		}

		double y() {
			return _y;
		}

		Vector()
		{
			_x = 0;
			_y = 0;
		}

		Vector(const double x, const double y) :
			_x(x), _y(y)
		{
		}
	};
}