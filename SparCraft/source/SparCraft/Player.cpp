#include "Player.h"

using namespace SparCraft;


void Player::getMoves(GameState & state, const MoveArray & moves, std::vector<Action> & moveVec)
{
	// not implemented
}

const IDType Player::ID() 
{ 
	return _playerID; 
}

void Player::setID(const IDType & playerID)
{
	_playerID = playerID;
}

bool Player::followsFormationPolicy() const
{
	return _followsFormationPolicy;
}

Formation * Player::getFormation() const
{
	return _formation;
}

void Player::setFormationPolicy(bool followsFormationPolicy)
{
	_followsFormationPolicy = followsFormationPolicy;
}


