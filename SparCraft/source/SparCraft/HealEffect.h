#pragma once

#include "Common.h"
#include "Effect.h"

namespace SparCraft
{
	class HealEffect : public Effect
	{
		double _healPerTickAmount;
		
	public:
		HealEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval,
			double healAmount);

		const double getHealPerTickAmount() const;
		void AffectUnit(Unit* unitToAffect, TimeType& currentGameTime);
	};
}