#include "PathFindingTile.h"

using namespace SparCraft;

PathFindingTile::PathFindingTile(int gcost, int hcost, Position p) : gcost_(gcost), hcost_(hcost), p_(p),
state_(1), heapIndex_(-1), parentNode_(NULL), walkable_(true)
{
}

PathFindingTile::PathFindingTile(Position p, bool walkable):p_(p), walkable_(walkable),
gcost_(0), hcost_(0), state_(1), heapIndex_(-1), parentNode_(NULL)
{	
}

const int PathFindingTile::getgCost() const
{
	return gcost_;
}

const int PathFindingTile::gethCost() const
{
	return hcost_;
}

const int PathFindingTile::getfCost() const
{
	return gcost_ + hcost_;
}

const int PathFindingTile::getState() const
{
	return state_;
}

const size_t PathFindingTile::getHeapIndex() const
{
	return heapIndex_;
}

const bool PathFindingTile::isWalkable() const
{
	return walkable_;
}

const bool PathFindingTile::hasUnits() const
{
	return unitVec_.size() > 0;
}

//position is in the middle of tile, so (x,y) + (4,4)
const Position PathFindingTile::getPosition() const
{
	return p_;
}

const int PathFindingTile::xIndex() const
{
	return p_.x() / Constants::Map::walkTileSize;
}

const int PathFindingTile::yIndex() const
{
	return p_.y() / Constants::Map::walkTileSize;
}

const std::vector<const Unit*>& PathFindingTile::getUnits() const
{
	return unitVec_;
}

PathFindingTile* PathFindingTile::getParent() const
{
	return parentNode_;
}

void PathFindingTile::addUnit(const Unit* unit)
{
	unitVec_.push_back(unit);
}

void PathFindingTile::setParentNode(PathFindingTile* pathFindingTile)
{
	parentNode_ = pathFindingTile;
}

void PathFindingTile::setgCost(const int newCost)
{
	gcost_ = newCost;
}

void PathFindingTile::sethCost(const int newCost)
{
	hcost_ = newCost;
}

void PathFindingTile::updateHeapIndex(const int heapIndex)
{
	heapIndex_ = heapIndex;
}

void PathFindingTile::updateState(const int state)
{
	state_ = state;
}

void PathFindingTile::updateWalkable(bool walkable)
{
	walkable_ = walkable;
}

void PathFindingTile::reset()
{
	gcost_ = 0;
	hcost_ = 0;
	heapIndex_ = -1;
	state_ = 1;
	walkable_ = true;
	unitVec_.clear();
	parentNode_ = NULL;

}