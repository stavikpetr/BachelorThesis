#pragma once

#include "Common.h"
#include "Unit.h"
#include "CollisionTile.h"

namespace SparCraft
{
	class Map
	{
		size_t							_walkTilesNumberHorizontal;
		size_t							_walkTilesNumberVertical;
		size_t							_buildTilesNumberHorizontal;
		size_t							_buildTilesNumberVertical;

		std::vector<std::vector<bool>>  _baseWalkTiles; //indicates whether is [x][y] tile walkable

		const Position getWalkTileNumber(const Position & position) const;
		const Position getWalkTileNumber(const int posX, const int posY) const;

	public:
		Map();

		const bool isWalkableBaseTile(const size_t & walkTileX, const size_t & walkTileY) const;
		const bool isWalkableBaseTile(const SparCraft::Position& position) const;
		const bool isFlyableBaseTile(const size_t & walkTileX, const size_t & walkTileY) const;
		const bool isFlyableBaseTile(const SparCraft::Position & position) const;

		const int getWalkTileXIndex(int XPixelAmount) const;
		const int getWalkTileYIndex(int YPixelAmount) const;
		const bool isValidWalkTileXIndex(int walkTileXIndex) const;
		const bool isValidWalkTileYIndex(int walkTileYIndex) const;

		int getWalkTilesNumberHorizontal();
		int getWalkTilesNumberVertical();

		const bool areUnitsColiding(PositionType topLeftX1, PositionType topLeftY1, int width1, int height1,
			PositionType topLeftX2, PositionType topLeftY2, int width2, int height2) const;
		void getBaseTileIndices(const Position& topLeft, const Position& botRight, std::vector<std::pair<int, int>>& toFill);
		void setWalkableBaseTile(int walkTileXIndex, int walkTileYIndex, bool walkable);
		
	};

}