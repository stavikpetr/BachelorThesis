#include "Player_MyScriptedAI_D.h"

using namespace SparCraft;

Player_MyScriptedAI_D::Player_MyScriptedAI_D(const IDType & playerID)
{
	_playerID = playerID;
}

Player_MyScriptedAI_D::Player_MyScriptedAI_D(const IDType & playerID, bool followsFormationPolicy, Formation * formation)
{
	_playerID = playerID;
	_followsFormationPolicy = followsFormationPolicy;
	_formation = formation;
}

void Player_MyScriptedAI_D::getMoves(GameState& state, const MoveArray& moves, std::vector<Action>& moveVec)
{
	moveVec.clear();
	bool broken;
	if (_followsFormationPolicy)
	{
		broken = _formation->isFormationBroken(_playerID, state);
		if (broken)
			_followsFormationPolicy = false;
	}
	IDType enemyPlayer = state.getEnemy(_playerID);


	Array<int, Constants::Max_Units> hpRemaining;

	for (IDType u(0); u < state.numUnits(enemyPlayer); ++u)
	{
		hpRemaining[u] = state.getUnit(enemyPlayer, u).currentHP();
	}

	for (IDType u(0); u<moves.numUnits(); ++u)
	{
		int attackMoveIndex(-1);
		int abilityMoveIndex(-1);
		int waitMoveIndex(-1);
		int moveMoveIndex(-1);
		int pathMoveIndex(-1);

		double actionHighestDPS(0);

		unsigned long long actionDistance(std::numeric_limits<unsigned long long>::max());
		unsigned long long closestMoveDist(std::numeric_limits<unsigned long long>::max());
		if (_followsFormationPolicy)
		{
			moveVec.push_back(_formation->getBestAction(moves, u));
			continue;
		}

		Unit & ourUnit(state.getUnit(_playerID, u));

		if (ourUnit.type() == BWAPI::UnitTypes::Terran_Medic || 
			ourUnit.type() == BWAPI::UnitTypes::Terran_Siege_Tank_Tank_Mode ||
			ourUnit.type() == BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode)
		{

#pragma region Medic

			if (ourUnit.type() == BWAPI::UnitTypes::Terran_Medic)
			{
				auto& healAbilityTmp = ourUnit.getAbilities()[0]; //medics have only healAbilities for now
				auto castedToHeal = dynamic_cast<HealAbility*>(healAbilityTmp.get());
				//auto castedToHeal = std::dynamic_pointer_cast<HealAbility>(castedToSingle);
				int setMoveIndex(-1);
				std::vector<Position> injuredFriendlyUnitsPositions;

				for (IDType unitIndex(0); unitIndex < state.numUnits(_playerID); ++unitIndex)
				{
					const Unit& unit = state.getUnit(_playerID, unitIndex);
					if (castedToHeal->matchesType(unit))
					{
						if (unit.ID() != ourUnit.ID())
						{
							if (unit.currentHP() < unit.maxHP())
								injuredFriendlyUnitsPositions.push_back(unit.position());
						}
					}
				}

				bool inRangeOfFriendlyInjuredUnit(false);

				for (size_t i(0); i < injuredFriendlyUnitsPositions.size(); ++i)
				{
					//we have friendly injured unit that we can cast heal to...
					if (Position::getDist(ourUnit.position(), injuredFriendlyUnitsPositions[i]) <
						Constants::Abilities::healRange)
						inRangeOfFriendlyInjuredUnit = true;
				}


				//if there is not an injured unit, we will wait
				//there is always option for waiting
				if (injuredFriendlyUnitsPositions.size() == 0)
				{
					for (size_t m(0); m < moves.numMoves(u); ++m)
					{
						const Action& move(moves.getMove(u, m));
						if (move.type() == ActionTypes::ACTIONWAIT)
						{
							setMoveIndex = m;
							break;
						}
					}
				}
				else
				{
					if (inRangeOfFriendlyInjuredUnit)
					{
						//let's try to find abilityMoveIndex (heal); if there is none, we will wait (medic's heal may be on cooldown)
						for (size_t m(0); m < moves.numMoves(u); ++m)
						{
							const Action& move(moves.getMove(u, m));
							if (move.type() == ActionTypes::CASTABILITY)
							{
								const Unit& target = state.getUnitDirect(_playerID, move.unit());
								if (Position::getDist(ourUnit.position(), target.position()) < Constants::Abilities::healRange)
								{
									setMoveIndex = m; //we don't care which unit we'll heal;
									break;
								}
							}
						}
						if (setMoveIndex == -1)
						{
							for (size_t m(0); m < moves.numMoves(u); ++m)
							{
								const Action& move(moves.getMove(u, m));
								if (move.type() == ActionTypes::ACTIONWAIT)
								{
									setMoveIndex = m; //we don't care which unit we'll heal;
									break;
								}
							}
						}
					}
					else
					{
						//in this case, i want to move to unit
						//either this is the first frame that we find out there is some injured friendly unit that is not in range 
						// in that case, i want to execute move to position, in other case or this is not a first time, in that case
						//there must be a move action to execute

						for (size_t m(0); m < moves.numMoves(u); ++m)
						{
							const Action& move(moves.getMove(u, m));
							if (move.type() == ActionTypes::MOVE)
							{
								setMoveIndex = m;
								break;
							}
							else if(move.type() == ActionTypes::MOVETOPOSITION)
							{
								//let's find the closest injured unit
								int shortestDist = std::numeric_limits<int>::max();
								Position chosenPos;
								for (size_t i(0); i < injuredFriendlyUnitsPositions.size(); ++i)
								{
									int dist = (int)Position::getDist(ourUnit.position(), injuredFriendlyUnitsPositions[i]);
									if (dist < shortestDist)
									{
										shortestDist = dist;
										chosenPos = injuredFriendlyUnitsPositions[i];
									}
								}
								move.setNewPosition(chosenPos);
								setMoveIndex = m;
							}
						}
					}
				}

				//add move
				if (setMoveIndex == -1)
					System::FatalError("medic's set move index is -1, this can't happen");
				moveVec.push_back(moves.getMove(u, setMoveIndex));
				continue;

			}

#pragma endregion

#pragma region SiegeTank

			if (ourUnit.type() == BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode ||
				ourUnit.type() == BWAPI::UnitTypes::Terran_Siege_Tank_Tank_Mode)
			{
				int setMoveIndex(-1);
				auto& changeSiegeState = ourUnit.getAbilities()[0]; //tanks have only change Siege state ability
				auto castedToChangeSiegeState = dynamic_cast<ChangeSiegeState*>(changeSiegeState.get());
				//first, let's find if there are any units in the SIEGED tank mode tank range (to be precise... in the arc, so min range is included aswell)
				bool anyUnitInSiegedTankArcRange(false);
				BWAPI::UnitType tankType(BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode);
				int tankTypeMaxRange = PlayerWeapon(&PlayerProperties::Get(_playerID), tankType.groundWeapon()).GetMaxRange() + Constants::Range_Addition;
				int tankTypeMinRange = Constants::Units::siegeTankSiegedMinRange;

				for (IDType unitIndex(0); unitIndex < state.numUnits(enemyPlayer); ++unitIndex)
				{
					const Unit& unit = state.getUnit(enemyPlayer, unitIndex);
					//let's count these units only if they won't die this turn
					if (hpRemaining[unitIndex] > 0)
					{
						double dist(Position::getDist(ourUnit.position(), unit.position()));
						if (dist >= tankTypeMinRange && dist <= tankTypeMaxRange)
							anyUnitInSiegedTankArcRange = true;
					}


				}
				
				if (castedToChangeSiegeState->getCurrentState() == SiegeState::UNSIEGED)
				{
					if (anyUnitInSiegedTankArcRange)
					{
						//if yes, let's change the siege state if we can
						//there will always be option to change siege state, because the cast time is longer than cooldown
						//ok, the two comments above are not true :)
						for (size_t m(0); m < moves.numMoves(u); ++m)
						{
							const Action& move(moves.getMove(u, m));
							if (move.type() == ActionTypes::CASTABILITY)
							{
								setMoveIndex = m;
								break;
							}
						}
						if (setMoveIndex == -1)
						{
							for (size_t m(0); m < moves.numMoves(u); ++m)
							{
								const Action& move(moves.getMove(u, m));
								if (move.type() == ActionTypes::ACTIONWAIT)
								{
									setMoveIndex = m;
									break;
								}
							}
						}
					}
					else
					{
						//is there any normal attack move?
						int highestDPSInRangeIndex(-1);
						int closestDist = std::numeric_limits<int>::max();


						for (size_t m(0); m < moves.numMoves(u); ++m)
						{
							const Action& move(moves.getMove(u, m));
							const Unit& unit(state.getUnit(state.getEnemy(_playerID), move.unit()));
							if (move.type() == ActionTypes::ATTACK && hpRemaining[move.index()] > 0)
							{
								double dpsHPValue = (unit.dpf() / hpRemaining[move.index()]);

								if (dpsHPValue > actionHighestDPS)
								{
									actionHighestDPS = dpsHPValue;
									highestDPSInRangeIndex = m;
								}

								/*int dist = state.getDistanceToPosition(ourUnit.pos(), unit.pos());
								if (dist < closestDist)
								{
									closestDist = dist;
									closestIndex = m;
								}*/
							}
						}
						setMoveIndex = highestDPSInRangeIndex;
						if (setMoveIndex != -1)
						{
							const Action& move(moves.getMove(u, setMoveIndex));
							hpRemaining[move.index()] -= ourUnit.damage();
						}

						if (setMoveIndex == -1)
						{
							IDType closestUnitIndex = -1;
							int closestDist_ = std::numeric_limits<int>::max();
							//let's find the closest enemyUnit that we know won't die this turn
							for (IDType unitIndex(0); unitIndex < state.numUnits(enemyPlayer); ++unitIndex)
							{
								if (hpRemaining[unitIndex] <= 0)
									continue;

								const Unit& unit = state.getUnit(enemyPlayer, unitIndex);

								double dist(Position::getDist(ourUnit.position(), unit.position()));
								if (dist <= closestDist_)
								{
									dist = closestDist_;
									closestUnitIndex = unitIndex;
								}

							}
							const Unit& closestUnitAliveThisTurn(state.getUnit(enemyPlayer, closestUnitIndex));

							if (ourUnit.canAttackTarget(closestUnitAliveThisTurn, state.getTime()))
							{
								//if there is enemy in range and we can't attack, let's just wait
								for (size_t m(0); m < moves.numMoves(u); ++m)
								{
									const Action& move(moves.getMove(u, m));
									if (move.type() == ActionTypes::ACTIONWAIT)
									{
										setMoveIndex = m;
										break;
									}
								}
								
							}
							else
							{

								for (size_t m(0); m < moves.numMoves(u); ++m)
								{
									const Action& move(moves.getMove(u, m));
									if (move.type() == ActionTypes::MOVE)
									{
										setMoveIndex = m;
										break;
									}
									else if (move.type() == ActionTypes::MOVETOPOSITION)
									{
										move.setNewPosition(closestUnitAliveThisTurn.position());
										setMoveIndex = m;
										break;
									}
								}
							}
						}
					}
				}
				else if (castedToChangeSiegeState->getCurrentState() == SiegeState::SIEGED)
				{
					if (!anyUnitInSiegedTankArcRange)
					{
						//let's change siege state, again, this move has to be here
						for (size_t m(0); m < moves.numMoves(u); ++m)
						{
							const Action& move(moves.getMove(u, m));
							if (move.type() == ActionTypes::CASTABILITY)
							{
								setMoveIndex = m;
								break;
							}
						}
						if (setMoveIndex == -1)
						{
							for (size_t m(0); m < moves.numMoves(u); ++m)
							{
								const Action& move(moves.getMove(u, m));
								if (move.type() == ActionTypes::ACTIONWAIT)
								{
									setMoveIndex = m;
									break;
								}
							}
						}
					}
					else
					{
						//let's again attack the closest enemy
						//is there any attacksplash move?
						double highestDPSAliveUnitInArc(0);
						int highestDPSAliveUnitInArcIndex(-1);
						for (size_t m(0); m < moves.numMoves(u); ++m)
						{
							const Action& move(moves.getMove(u, m));
							const Unit& unit(state.getUnit(state.getEnemy(_playerID), move.unit()));
							if (move.type() == ActionTypes::ATTACKSPLASH && hpRemaining[move.index()] > 0)
							{								
								double dpsHPValue = (unit.dpf() / hpRemaining[move.index()]);

								if (dpsHPValue > highestDPSAliveUnitInArc)
								{
									highestDPSAliveUnitInArc = dpsHPValue;
									highestDPSAliveUnitInArcIndex = m;
								}
								//int dist = state.getDistanceToPosition(ourUnit.pos(), unit.pos());
								//if (dist < closestDist)
								//{
								//	closestDist = dist;
								//	closestIndex = m;
								//}
							}
						}
						setMoveIndex = highestDPSAliveUnitInArcIndex;
						//if we didn't set the setMoveIndex then let's pick waitOneFrame action (there is always one)
						if (setMoveIndex == -1)
						{
							for (size_t m(0); m < moves.numMoves(u); ++m)
							{
								const Action& move(moves.getMove(u, m));
								if (move.type() == ActionTypes::ACTIONWAIT)
								{
									setMoveIndex = m;
									break;
								}
							}
						}
						else
						{
							//we've picked highestDPSaliveUnitInArcIndex and we have to subtract the hps of targeted unit
							const Action& move(moves.getMove(u, setMoveIndex));
							hpRemaining[move.index()] -= ourUnit.damage();
						}
					}
				}
				//let's add the move
				if (setMoveIndex == -1)
					System::FatalError("set move index for siege tank is -1, this can'T happen");
				moveVec.push_back(moves.getMove(u, setMoveIndex));
				continue;
			}

#pragma endregion

		}
		else
		{
			const Unit & closestUnit(state.getClosestEnemyUnit(_playerID, u));

			// we will prioritize workers
			const IDType enemyPlayer(state.getEnemy(_playerID));
			Position moveToPosition;
			if (state.playerHasWorkers(enemyPlayer))
				moveToPosition = state.getClosestEnemyWorker(_playerID, u).position();
			else
				moveToPosition = closestUnit.position();

			for (size_t m(0); m < moves.numMoves(u); ++m)
			{
				const Action& move(moves.getMove(u, m));

				if ((move.type() == ActionTypes::ATTACK || move.type() == ActionTypes::ATTACKSPLASH) && 
					hpRemaining[move.index()] > 0)
				{
					const Unit & target(state.getUnit(state.getEnemy(move.player()), move.index()));

					double dpsHPValue = (target.dpf() / hpRemaining[move.index()]);

					if (dpsHPValue > actionHighestDPS)
					{
						actionHighestDPS = dpsHPValue;
						attackMoveIndex = m;
					}

					//size_t dist(ourUnit.getDistanceSqToUnit(target, state.getTime()));

					//if (dist < actionDistance)
					//{
					//	actionDistance = dist;
					//	attackMoveIndex = m;
					//}
				}

				if (move.type() == ActionTypes::ACTIONWAIT)
				{
					if (attackMoveIndex == -1 && abilityMoveIndex == -1)
					{
						if (ourUnit.canAttackTarget(closestUnit, state.getTime()) && hpRemaining[move.index()] > 0)
						{
							waitMoveIndex = m;
						}
					}
				}

				if (move.type() == ActionTypes::MOVETOPOSITION)
				{
					move.setNewPosition(moveToPosition);
					pathMoveIndex = m;
				}


				if (move.type() == ActionTypes::MOVE)
				{
					// we have only one move, where are we moving depends on the logic behind move to position
					moveMoveIndex = m;
				}
			}

			int bestMoveIndex = attackMoveIndex != -1 ? attackMoveIndex : (waitMoveIndex != -1 ? waitMoveIndex :
				(moveMoveIndex != -1 ? moveMoveIndex : (pathMoveIndex !=-1 ? pathMoveIndex : -1)));
			if (bestMoveIndex != -1)
				moveVec.push_back(moves.getMove(u, bestMoveIndex));
			else
			{
				//there is always pass turn at the end of this vector
				moveVec.push_back(moves.getMove(u, moves.numMoves(u) - 1));
			}

			if (attackMoveIndex != -1)
			{
				hpRemaining[moves.getMove(u, bestMoveIndex).index()] -= ourUnit.damage();
			}
		}

	}
}