#pragma once

#include "Common.h"
#include "StateAbility.h"

namespace SparCraft
{
	enum class BurrowState { BURROWED, UNBURROWED };

	class BurrowAbility : public StateAbility
	{
		BurrowState _currentBorrowState;
	public:
		const bool isCastableNow(const TimeType& currentTime) const;
		void beCasted(TimeType gameTime = -1);
		const BurrowState getCurrentBurrowState();

		BurrowAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
			 BurrowState currentBurrowState);
	};
}