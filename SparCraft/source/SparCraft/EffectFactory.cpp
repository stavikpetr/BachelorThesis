#include "EffectFactory.h"
#include "Unit.h"

using namespace SparCraft;

namespace SparCraft
{
	namespace Constants
	{
		namespace Effects
		{
			bool EMPIsStackable = false;
			int EMPShieldDrain = 100;
			int EMPEnergyDrain = 100;
			int EMPRadius = 50;
			int EMPtickInterval = 0;
			TimeType EMPLastingTime = 0;

			bool LurkerIsStackable = true;
			int LurkerDamage = 20;
			int LurkerEffectLength = 30;
			int LurkerEffectWidth = 5;
			int LurkerTickInterval = 0;
			TimeType LurkerLastingTime = 0;

			bool StormIsStackable = false;
			int StormRadius = 60;
			int StormDamage = 5;
			TimeType StormTickInterval = 1;
			TimeType StormDamageInteval = 10;
			TimeType StormLastingTime = 80;

			bool StimpackIsStackable = false;
			TimeType StimpackLastingTime = 200;
			TimeType StimpackTickInterval = StimpackLastingTime;
			double StimpackASMultiplier = 1.5;
			double StimpackMSMultiplier = 0.5;

			double HealPerTickAmount = 2;
			TimeType HealLastingTime = 2;
			TimeType HealTickInterval = 1;
		}
	}
}

std::unique_ptr<Effect> EffectFactory::getHealEffect(TimeType startingTime)
{
	return std::make_unique<HealEffect>(
		startingTime,
		Constants::Effects::HealLastingTime,
		Constants::Effects::HealTickInterval,
		Constants::Effects::HealPerTickAmount);

}

std::unique_ptr<Effect> EffectFactory::getChangeInvisibilityEffect(TimeType startingTime,
	bool newInvisibilityValue)
{
	return std::make_unique<ChangeInvisibilityEffect>(
		startingTime, 0, 0, newInvisibilityValue);
}

std::unique_ptr<Effect> EffectFactory::getStimpackEffect(TimeType startingTime)
{
	return std::make_unique<StimpackEffect>(
		startingTime,
		Constants::Effects::StimpackLastingTime,
		Constants::Effects::StimpackTickInterval,
		Constants::Effects::StimpackMSMultiplier,
		Constants::Effects::StimpackASMultiplier
		);
}

std::unique_ptr<Effect> EffectFactory::getEMPShockwaveEffect(TimeType startingTime,
	IDType castedByUnitId, Position position)
{
	return std::make_unique<EMPShockwaveEffect>(
		startingTime,
		Constants::Effects::EMPLastingTime,
		Constants::Effects::EMPtickInterval,
		Constants::Effects::EMPRadius,
		Constants::Effects::EMPEnergyDrain,
		Constants::Effects::EMPShieldDrain,
		position,
		castedByUnitId
		);
}

std::unique_ptr<Effect> EffectFactory::getPsionicStormEffect(TimeType startingTime,
	Position position)
{
	return std::make_unique<PsionicStormEffect>(
		startingTime,
		Constants::Effects::StormLastingTime,
		Constants::Effects::StormTickInterval,
		Constants::Effects::StormRadius,
		Constants::Effects::StormDamage,
		position,
		Constants::Effects::StormDamageInteval
		);
}

std::unique_ptr<Effect> EffectFactory::getLurkerAttackEffect(TimeType startingTime,
	std::shared_ptr<std::unordered_set<IDType>> affectedUnitIds, Position position,
	Vector dirUnitVector, Unit* caster)
{
	return std::make_unique<LurkerAttackEffect>(
		startingTime,
		Constants::Effects::LurkerLastingTime,
		Constants::Effects::LurkerTickInterval,
		affectedUnitIds,
		position,
		Constants::Effects::LurkerEffectWidth,
		Constants::Effects::LurkerEffectLength,
		dirUnitVector,
		caster
		);
}

std::unique_ptr<Effect> EffectFactory::getEffect(EffectTypes::EffectType effectType,
	TimeType startingTime)
{
	if (effectType == EffectTypes::EffectType::STIMPACK)
	{
		return getStimpackEffect(startingTime);
	}
	else if (effectType == EffectTypes::EffectType::HEAL)
	{
		return getHealEffect(startingTime);
	}
	else
	{
		System::FatalError("not supported effect in EffectFactory::getEffect, call more specific function");
		return nullptr;
	}
}
