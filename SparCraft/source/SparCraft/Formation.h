#pragma once

#include "Common.h"
#include "GameState.h"

namespace SparCraft
{
	namespace FormationTypes
	{
		enum FormationType{ HOLD };
	};

	class Formation
	{
		FormationTypes::FormationType _formationType;

		const bool isHoldFormationBroken(const IDType playerID, GameState & state) const;
		const Action& getBestActionHoldFormation(const MoveArray & moves, IDType unitIndex) const;

	public:
		Formation(FormationTypes::FormationType type);
		const bool isFormationBroken(const IDType playerID, GameState & state) const;
		const Action& getBestAction(const MoveArray & moves, IDType unitIndex) const;
	};
}