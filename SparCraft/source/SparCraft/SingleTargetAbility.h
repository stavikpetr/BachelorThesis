#pragma once 

#include "Common.h"
#include "Ability.h"

namespace SparCraft
{
	class SingleTargetAbility :public Ability
	{
	protected:
		int _range;
		SingleTargetAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
			int range);
	public:

		const virtual bool isCastableToUnit(const Unit& unit) const = 0;
		virtual void beCasted(Unit& target, TimeType gameTime = -1) = 0;
		const int getRange() const;
	};
}