#pragma once

#include "Common.h"
#include "Projectile.h"

namespace SparCraft
{
	class EMPShockwaveProjectile : public Projectile
	{
		Unit* _caster;
	public:
		EMPShockwaveProjectile(Position startingPosition, Position targetPosition, double speed, int destinationDistance,
			TimeType nextActionTime, Unit* caster);
		void nextAction(GameState& state);
		Unit* getCaster();
	};
}