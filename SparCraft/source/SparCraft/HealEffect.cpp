#include "HealEffect.h"
#include "Unit.h"

using namespace SparCraft;

HealEffect::HealEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval, double healPerTickAmount) :
Effect(startingTime, lastingTime, tickInterval)
{
	_healPerTickAmount = healPerTickAmount;
	_effectType = EffectTypes::EffectType::HEAL;
}


const double HealEffect::getHealPerTickAmount() const
{
	return _healPerTickAmount;
}

void HealEffect::AffectUnit(Unit* unitToAffect, TimeType& currentGameTime)
{
	//although heal is not stackable, it was handled in special way and there is no way that two heals are 
	//affecting this unit at the same time
	if (unitToAffect->isAlive())
	{
		if (unitToAffect->currentHP() + _healPerTickAmount > unitToAffect->maxHP())
			unitToAffect->updateCurrentHP(unitToAffect->maxHP());
		else
			unitToAffect->updateCurrentHP(unitToAffect->currentHP() + (int)_healPerTickAmount);
	}
}