#pragma once

#include "Common.h"
#include "StateEffect.h"
#include "Position.hpp"

namespace SparCraft
{
	class PsionicStormEffect : public StateEffect
	{
		int _radius;
		int _damage;
		Position _p;
		TimeType _damageTickInterval;
	public:
		PsionicStormEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval, 
			int radius, int damage, Position position, TimeType damageTickInterval);
		int getRangeLow();
		int getRangeHigh();
		int getRadius();
		Position getPosition();
		void AffectUnit(Unit* unitToAffect, TimeType& currentGameTime);
		std::vector<Unit*> getUnitsToAffect(GameState& state);
	};
}