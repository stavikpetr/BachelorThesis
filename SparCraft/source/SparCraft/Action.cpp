#include "Action.h"

using namespace SparCraft;

Action::Action()
	: _unit(255)
	, _player(255)
	, _moveType(ActionTypes::NONE)
	, _moveIndex(255)
{
}

Action::Action( const IDType & unitIndex, const IDType & player, const IDType & type, const IDType & moveIndex, const Position & dest)
    : _unit(unitIndex)
    , _player(player)
    , _moveType(type)
    , _moveIndex(moveIndex)
    , _p(dest)
{
	
	_ability = NULL;
}

Action::Action( const IDType & unitIndex, const IDType & player, const IDType & type, const IDType & moveIndex)
	: _unit(unitIndex)
	, _player(player)
	, _moveType(type)
	, _moveIndex(moveIndex)
{
	_ability = NULL;

}

Action::Action(const IDType & unitIndex, const IDType & player, const IDType & type, const IDType & moveIndex,
	Ability* ability)
: _unit(unitIndex)
, _player(player)
, _moveType(type)
, _moveIndex(moveIndex)
, _ability(ability)
{
}

const bool Action::operator == (const Action & rhs)
{
	return _unit == rhs._unit && _player == rhs._player && _moveType == rhs._moveType && _moveIndex == rhs._moveIndex && _p == rhs._p;
}

const IDType & Action::unit() const	
{ 
    return _unit; 
}

const IDType & Action::player() const	
{ 
    return _player; 
}

const IDType & Action::type() const	
{ 
    return _moveType; 
}

const IDType & Action::index() const	
{ 
    return _moveIndex; 
}

const Position & Action::pos() const   
{ 
    return _p; 
}

Ability* Action::getAbility()
{
	return _ability;
}

const bool Action::hasAbility() const
{
	return _ability != NULL ? true : false;
}

const std::string Action::moveString() const
{
	if (_moveType == ActionTypes::ATTACK) 
	{
		return "ATTACK";
	}
	else if (_moveType == ActionTypes::MOVE)
	{
		return "MOVE";
	}
	else if (_moveType == ActionTypes::RELOAD)
	{
		return "RELOAD";
	}
	else if (_moveType == ActionTypes::PASS)
	{
		return "PASS";
	}
	else if (_moveType == ActionTypes::HEAL)
	{
		return "HEAL";
	}
	else if (_moveType == ActionTypes::ATTACKSPLASH)
	{
		return "ATTACKSPLASH";
	}
	else if (_moveType == ActionTypes::ACTIONWAIT)
	{
		return "ACTIONWAIT";
	}
	else if (_moveType == ActionTypes::CASTABILITY)
	{
		return "CASTABILITY";
	}
	else if (_moveType == ActionTypes::MOVETOPOSITION)
	{
		return "MOVETOPOSITION";
	}
	else if (_moveType == ActionTypes::STUCK)
	{
		return "STUCK";
	}
	else if (_moveType == ActionTypes::WAITONEFRAME)
	{
		return "WAITONEFRAME";
	}

	return "NONE";
}

const int* Action::getDir() const
{
	return Constants::Move_Dir[_moveIndex];
}

const std::string Action::debugString() const
{
    std::stringstream ss;
    ss << moveString() << ": (" << (int)unit() << "," << (int)player() << "," << (int)type() << "," << (int)index() << ")  " << "(" << pos().x() << "," << pos().y()   << ")";
    return ss.str();
}

void Action::setNewPosition(const Position& dest) const
{
	_p = dest;
}

void Action::setNewType(const IDType& moveTypeId)
{
	_moveType = moveTypeId;
	//_debugString = ActionTypes::EnumString[moveTypeId];
}

