#include "Player_MyScriptedAI_A.h"

using namespace SparCraft;

Player_MyScriptedAI_A::Player_MyScriptedAI_A(const IDType & playerID)
{
	_playerID = playerID;
}

Player_MyScriptedAI_A::Player_MyScriptedAI_A(const IDType & playerID, bool followsFormationPolicy, Formation * formation)
{
	_playerID = playerID;
	_followsFormationPolicy = followsFormationPolicy;
	_formation = formation;
}

void Player_MyScriptedAI_A::getMoves(GameState& state, const MoveArray& moves, std::vector<Action>& moveVec)
{
	moveVec.clear();
	
	bool broken;
	if (_followsFormationPolicy)
	{
		broken = _formation->isFormationBroken(_playerID, state);
		if (broken)
			_followsFormationPolicy = false;
	}
	 
	//first getMoves, let's insert 0 for each id
	if (state.getTime() == 0)
	{
		unitStates_.clear();
		stuckCounter_.clear();
		for (IDType u(0); u < moves.numUnits(); ++u)
		{
			const Unit & unit(state.getUnit(_playerID, u));
			unitStates_.insert(std::pair<int, int>(unit.ID(), 0));
			stuckCounter_.insert(std::pair<int, int>(unit.ID(), 0));
		}
	}

	IDType enemyPlayer = state.getEnemy(_playerID);
	bool enemyHasWorkers = state.playerHasWorkers(enemyPlayer);


	for (IDType u(0); u < moves.numUnits(); ++u)
	{
		if (_followsFormationPolicy)
		{
			moveVec.push_back(_formation->getBestAction(moves, u));
			continue;
		}

		const Unit & ourUnit(state.getUnit(_playerID, u));
		const Unit & closestUnit(state.getClosestEnemyUnit(_playerID, u));
		auto it = unitStates_.find(ourUnit.ID());

		int unitState = (*it).second;
		//now we need to remove the value in order to insert new state with this id
		unitStates_.erase(it);

		if (unitState == 0)
		{
			if (enemyHasWorkers)
				unitState = 1;
			else
			{
				unitState = 20;
			}
		}

		int setMoveIndex = -1;
		if (unitState == 1)
		{
			if (enemyHasWorkers)
			{
				const Unit& closestEnemyWorker(state.getClosestEnemyWorker(_playerID, u));

				for (size_t m(0); m < moves.numMoves(u); ++m)
				{
					const Action& move(moves.getMove(u, m));
					if (move.type() == ActionTypes::MOVETOPOSITION)
					{
						setMoveIndex = m;
						move.setNewPosition(closestEnemyWorker.position());
						unitState = 2;
					}
				}
			}
			else
			{
				unitState = 20;
			}
		}

		//doing moveToPosition
		if (unitState == 2)
		{
			moveVec.push_back(moves.getMove(u, setMoveIndex));
			unitState = 3;
			
		}
		//last time, moveToPosition was performed, we for sure have
		else if (unitState == 3)
		{
			if (enemyHasWorkers)
			{
				const Unit& closestEnemyWorker(state.getClosestEnemyWorker(_playerID, u));
				if (ourUnit.canAttackTarget(closestEnemyWorker, state.getTime()))
				{
					int attackMoveIndex(-1);
					int waitMoveIndex(-1);
					for (size_t m(0); m < moves.numMoves(u); ++m)
					{
						const Action& move(moves.getMove(u, m));
						if (move.type() == ActionTypes::ATTACK)
						{
							const Unit & target(state.getUnit(state.getEnemy(move.player()), move.index()));
							if (target.ID() == closestEnemyWorker.ID())
							{
								unitState = 4;
								attackMoveIndex = m;
							}
						}
						if (move.type() == ActionTypes::ACTIONWAIT)
							waitMoveIndex = m;
					}
					if (attackMoveIndex == -1)
					{
						setMoveIndex = waitMoveIndex;
						moveVec.push_back(moves.getMove(u, setMoveIndex));
					}
					else
					{
						setMoveIndex = attackMoveIndex;
					}

				}
				else
				{
					//we can't attack the worker

					if (ourUnit.previousAction().type() == ActionTypes::STUCK)
					{
						unitState = 5;
					}
					else
					{
						if (ourUnit.hasPath())
						{
							//let's find the move action that has to be present
							for (size_t m(0); m < moves.numMoves(u); ++m)
							{
								const Action& move(moves.getMove(u, m));
								if (move.type() == ActionTypes::MOVE)
								{
									setMoveIndex = m;
									moveVec.push_back(moves.getMove(u, setMoveIndex));
									unitState = 3;
								}
							}
						}
						else
						{
							const Unit& closestEnemyWorker(state.getClosestEnemyWorker(_playerID, u));

							for (size_t m(0); m < moves.numMoves(u); ++m)
							{
								const Action& move(moves.getMove(u, m));
								if (move.type() == ActionTypes::MOVETOPOSITION)
								{
									setMoveIndex = m;
									move.setNewPosition(closestEnemyWorker.position());
									moveVec.push_back(moves.getMove(u, setMoveIndex));
									unitState = 3;
								}
							}
						}

						auto it = additionalVals_.find(ourUnit.ID());
						auto numOfEnemyUnits = state.numUnits(enemyPlayer);
						if (it == additionalVals_.end()) //not yet present
							additionalVals_.insert(std::pair<int, int>(ourUnit.ID(), numOfEnemyUnits));
						else
						{
							additionalVals_.erase(it);
							additionalVals_.insert(std::pair<int, int>(ourUnit.ID(), numOfEnemyUnits));
						}
					}
				}
				
			}
			else
			{
				unitState = 20;
			}
		}

		//attacking worker
		if (unitState == 4)
		{
			moveVec.push_back(moves.getMove(u, setMoveIndex));
			unitState = 3;
		}

		if (unitState == 5)
		{
			//let's try to find the path once more (this is really typical)
			//i know that previously, the unit was stuck
			
			stuckCounter_[ourUnit.ID()] = 0;
			auto numOfPrevEnemyUnits = (*(additionalVals_.find(ourUnit.ID()))).second;
			int curNumberOfUnits = state.numUnits(enemyPlayer);

			if (numOfPrevEnemyUnits == curNumberOfUnits)
			{
				if (ourUnit.canAttackTarget(closestUnit, state.getTime()))
				{
					int attackMoveIndex(-1);
					int actionWaitMoveIndex(-1);
					int weakestEnemyUnitToAttackHps(std::numeric_limits<int>::max());
					int weakestEnemyUnitId(-1);
					//if we can attack something, let's attack enemy unit with the lowest hps;

					for (size_t enemyUnitIndex(0); enemyUnitIndex < state.numUnits(enemyPlayer); ++enemyUnitIndex)
					{
						const Unit& enemyUnit(state.getUnit(enemyPlayer, enemyUnitIndex));
						if (ourUnit.canAttackTarget(enemyUnit, state.getTime()))
						{
							if (enemyUnit.currentHP() < weakestEnemyUnitToAttackHps)
							{
								weakestEnemyUnitToAttackHps = enemyUnit.currentHP();
								weakestEnemyUnitId = enemyUnit.ID();
							}
						}

					}

					for (size_t m(0); m < moves.numMoves(u); ++m)
					{
						const Action& move(moves.getMove(u, m));
						
						if (move.type() == ActionTypes::ATTACK)
						{
							const Unit& unitToAttack = state.getUnit(enemyPlayer, move.index());
							if (unitToAttack.ID() == weakestEnemyUnitId)
								attackMoveIndex = m;
						}
						if (move.type() == ActionTypes::ACTIONWAIT)
						{
							actionWaitMoveIndex = m;
						}
					}
					if (attackMoveIndex == -1)
					{
						setMoveIndex = actionWaitMoveIndex;
						moveVec.push_back(moves.getMove(u, setMoveIndex));
					}
					else
					{
						setMoveIndex = attackMoveIndex;
						moveVec.push_back(moves.getMove(u, setMoveIndex));
					}
					
				}
				unitState = 3;
			}

			if (setMoveIndex == -1)
			{
				const Unit& closestEnemyWorker(state.getClosestEnemyWorker(_playerID, u));
				for (size_t m(0); m < moves.numMoves(u); ++m)
				{
					const Action& move(moves.getMove(u, m));
					if (move.type() == ActionTypes::MOVETOPOSITION)
					{
						setMoveIndex = m;
						move.setNewPosition(closestEnemyWorker.position());
						moveVec.push_back(moves.getMove(u, setMoveIndex));
						unitState = 3;
					}
				}
			}

		}

		if (unitState != 20 && setMoveIndex == -1)
		{
			System::FatalError("myscriptedAI_A, setMoveIndex = -1");
		}
		

#pragma region StandardNOKAV

		if (unitState == 20)
		{
			Array<int, Constants::Max_Units> hpRemaining;

			for (IDType f(0); f < state.numUnits(enemyPlayer); ++f)
			{
				hpRemaining[f] = state.getUnit(enemyPlayer, f).currentHP();
			}



			int attackMoveIndex(-1);
			int abilityMoveIndex(-1);
			int waitMoveIndex(-1);
			int moveMoveIndex(-1);
			int pathMoveIndex(-1);
			double actionHighestDPS(0);

			unsigned long long actionDistance(std::numeric_limits<unsigned long long>::max());
			unsigned long long closestMoveDist(std::numeric_limits<unsigned long long>::max());

			const Unit & closestUnit(state.getClosestEnemyUnit(_playerID, u));

			for (size_t m(0); m < moves.numMoves(u); ++m)
			{
				const Action& move(moves.getMove(u, m));

				if ((move.type() == ActionTypes::ATTACK || move.type() == ActionTypes::ATTACKSPLASH) && hpRemaining[move.index()] > 0)
				{
					const Unit & target(state.getUnit(state.getEnemy(move.player()), move.index()));
					double dpsHPValue = (target.dpf() / hpRemaining[move.index()]);

					if (dpsHPValue > actionHighestDPS)
					{
						actionHighestDPS = dpsHPValue;
						attackMoveIndex = m;
					}

					//size_t dist(ourUnit.getDistanceSqToUnit(target, state.getTime()));
					//if (dist < actionDistance)
					//{
					//	actionDistance = dist;
					//	attackMoveIndex = m;
					//}
				}

				if (move.type() == ActionTypes::ACTIONWAIT)
				{
					if (attackMoveIndex == -1 && abilityMoveIndex == -1)
					{
						if (ourUnit.canAttackTarget(closestUnit, state.getTime()))
						{
							waitMoveIndex = m;
						}
					}
				}

				if (move.type() == ActionTypes::MOVETOPOSITION)
				{
					move.setNewPosition(closestUnit.position());
					pathMoveIndex = m;
				}


				if (move.type() == ActionTypes::MOVE)
				{
					// we have only one move, where are we moving depends on the logic behind move to position
					moveMoveIndex = m;
				}
			}

			int bestMoveIndex = attackMoveIndex != -1 ? attackMoveIndex : (waitMoveIndex != -1 ? waitMoveIndex :
				(moveMoveIndex != -1 ? moveMoveIndex : (pathMoveIndex != -1 ? pathMoveIndex :-1)));
			if (bestMoveIndex != -1)
				moveVec.push_back(moves.getMove(u, bestMoveIndex));
			else
			{
				//there is always pass turn at the end of this vector
				moveVec.push_back(moves.getMove(u, moves.numMoves(u) - 1));
			}

			if (attackMoveIndex != -1)
			{
				hpRemaining[moves.getMove(u, bestMoveIndex).index()] -= ourUnit.damage();
			}

		}

#pragma endregion

		unitStates_.insert(std::pair<int, int>(ourUnit.ID(), unitState));

	}
	
}