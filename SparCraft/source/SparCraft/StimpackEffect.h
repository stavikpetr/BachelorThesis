#pragma once

#include "Common.h"
#include "Effect.h"

namespace SparCraft
{
	class StimpackEffect : public Effect
	{
		double _attackSpeedMultiplier;
		double _moveSpeedMultiplier;
	public:
		void AffectUnit(Unit* unitToAffect, TimeType& currentGameTime);

		StimpackEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval,
			double attackSpeedMultiplier, double moveSpeedMultiplier);
	};
}