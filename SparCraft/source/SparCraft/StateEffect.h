#pragma once

#include "Common.h"
#include "Effect.h"

namespace SparCraft
{
	class StateEffect : public Effect
	{
	protected:
		StateEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval);
	public:
		virtual std::vector<Unit*> getUnitsToAffect(GameState& state) = 0;
	};
}