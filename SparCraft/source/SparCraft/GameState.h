#pragma once

 
#include "Common.h" //dealt with
#include <algorithm>
#include "MoveArray.h"
#include "Hash.h"
#include "Map.h"
#include "Unit.h"
#include "GraphViz.hpp"
#include "Array.hpp"
#include "Logger.h"
#include "PathFinder.h"
#include "CollisionResolver.h"
#include "Projectile.h"
#include "AllProjectiles.h"
#include "CollisionTilesHolder.h"

#include "CollisionHelperFunctions.h"
#include "SplashDamageDealer.h"

#include "AbilityFactory.h"
#include "ProjectileFactory.h"

namespace SparCraft
{
class GameState 
{
	Map* _map;
	std::vector<Unit> _units[Constants::Num_Players];
    std::vector<int>  _unitIndex[Constants::Num_Players];     

    Array<UnitCountType, Constants::Num_Players>                    _numUnits; // [0]=0..0, [1]=0..0
    Array<UnitCountType, Constants::Num_Players>                    _prevNumUnits;// [0]=0..0, [1]=0..0

    Array<float, Constants::Num_Players>                            _totalLTD; //
    Array<float, Constants::Num_Players>                            _totalSumSQRT;//

    Array<int, Constants::Num_Players>                              _numMovements;// [0]=0..0, [1]=0..0
    Array<int, Constants::Num_Players>                              _prevHPSum;// [0]=0..0, [1]=0..0
	
    TimeType                                                        _currentTime;//0
    size_t                                                          _maxUnits;//constants:max_units
    TimeType                                                        _sameHPFrames;//0

	PathFinder* _pathFinder;
	CollisionResolver* _collisionResolver;

	CollisionTilesHolder _collisionTilesHolder;
	
	std::vector<std::unique_ptr<Effect>> _activeAbilityEffects;
	std::vector<std::unique_ptr<Projectile>> _activeProjectiles;

	//this vector is only for the purpose of evaluation ....
	std::vector<std::pair<int, int>> infrastructureHits_;


    // checks to see if the unit array is full before adding a unit to the state
    const bool              checkFull(const IDType & player)                                        const;
    const bool              checkUniqueUnitIDs()                                                    const;

    void                    performAction(Action & theMove);

	void                    checkInvalidSpellCasts(std::vector<Action> & moves);
	void					checkValidityOfMoves();
	void doCopyCtorWork(const GameState& g);
public:
		
    GameState();
    GameState(const std::string & filename);
	GameState(const GameState & g);
	GameState& operator=(const GameState& g);

	// misc functions
    void                    finishedMoving();
    void                    updateGameTime();
    const bool              playerDead(const IDType & player)                                       const;
    const bool              isTerminal()                                                            const;

    // unit data functions
    const size_t            numUnits(const IDType & player)                                         const;
    const size_t            prevNumUnits(const IDType & player)                                     const;
    const size_t            closestEnemyUnitDistance(const Unit & unit)                             const;

    // Unit functions
    void                    sortUnits();
	void					clearUnits(const IDType playerId);
    void                    addUnit(const BWAPI::UnitType unitType, const IDType playerID, const Position & pos);
    const Unit &            getUnit(const IDType & player, const UnitCountType & unitIndex)         const;
    const Unit &            getUnitByID(const IDType & unitID)                                      const;
          Unit &            getUnit(const IDType & player, const UnitCountType & unitIndex);
    const Unit &            getUnitByID(const IDType & player, const IDType & unitID)               const;
          Unit &            getUnitByID(const IDType & player, const IDType & unitID);
		  Unit*				getUnitByIDPtrRaw(const IDType& unitID);
    const Unit &            getClosestEnemyUnit(const IDType & player, const IDType & unitIndex, bool checkCloaked=false);
    const Unit &            getClosestOurUnit(const IDType & player, const IDType & unitIndex);
    const Unit &            getUnitDirect(const IDType & player, const IDType & unit)               const;
  
	//------------
	// MY FUNCTIONS
	//------------
	const TimeType nextPlayerTime() const;

	const Unit* getUnitPtrRaw(const IDType& player, const UnitCountType& unitIndex) const;
	Unit* getUnitPtrRaw(const IDType& player, const UnitCountType& unitIndex);
	Unit& getUnitRef(const IDType& player, const UnitCountType& unitIndex);
	void performTakeAttack(Unit& ourUnit, Unit& enemyUnit, double multiplier = 1.0);
	const Unit & getClosestEnemyWorker(const IDType & player, const IDType & unitIndex);
	

	bool playerHasWorkers(const IDType& player);
	void                    generateMoves2(MoveArray & moves, const IDType & playerIndex)          ;

	const TimeType nextAbilitiesTime() const;
	TimeType nextUnitEffectsTime();
	const TimeType nextProjectilesTime() const;
	CollisionTilesHolder & getCollisionTilesHolder();

	void addEffect(std::unique_ptr<Effect>&& effect);
	void addProjectile(std::unique_ptr<Projectile>&& projectile);

	void handleStateEffects();
	void handleUnitEffects();
	void handleProjectiles();

	std::vector<std::unique_ptr<Projectile>>& getActiveProjectiles();

	// game time functions
    void                    setTime(const TimeType & time);
    const TimeType          getTime()                                                               const;

    // evaluation functions
    const StateEvalScore    eval(   const IDType & player, const IDType & evalMethod, 
                                    const IDType p1Script = PlayerModels::NOKDPS,
                                    const IDType p2Script = PlayerModels::NOKDPS)                   const;
    const ScoreType         evalLTD(const IDType & player)                                        const;
    const ScoreType         evalLTD2(const IDType & player)                                       const;
    const ScoreType         LTD(const IDType & player)                                            const;
    const ScoreType         LTD2(const IDType & player)                                           const;
    const StateEvalScore    evalSim(const IDType & player, const IDType & p1, const IDType & p2)    const;
    const IDType            getEnemy(const IDType & player)                                         const;

    // unit hitpoint calculations, needed for LTD2 evaluation
    void                    calculateStartingHealth();
    void                    setTotalLTD(const float & p1, const float & p2);
    void                    setTotalLTD2(const float & p1, const float & p2);
    const float &           getTotalLTD(const IDType & player)                                    const;
    const float &           getTotalLTD2(const IDType & player)                                   const;

    // move related functions
    void                    generateMoves(MoveArray & moves, const IDType & playerIndex)            const;
    void                    makeMoves(std::vector<Action> & moves);
    const int &             getNumMovements(const IDType & player)                                  const;
    const IDType            whoCanMove()                                                            const;
    const bool              bothCanMove()                                                           const;
		  
    // map-related functions
	void                    checkUnitsAreOnWalkableTiles()											const;
    Map*    getMap()                                                                const;

	void setMap(Map* map);
	void setPathFinder(PathFinder* pathFinder);
	void setCollisionResolver(CollisionResolver* collisionResolver);
	void insertUnitsIntoTiles();

	const std::vector<std::pair<int, int>>& getInfrastructureHits() const;

    // hashing functions
    const HashType          calculateHash(const size_t & hashNum)                                   const;

    // state i/o functions
    void                    print(int indent = 0) const;
	std::string             toString() const;
    std::string             toStringCompact() const;
    void                    write(const std::string & filename)                                     const;
    void                    read(const std::string & filename);
};

}

