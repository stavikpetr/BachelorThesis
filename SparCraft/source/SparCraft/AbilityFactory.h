#pragma once

#include "Common.h"
#include <memory>

namespace SparCraft
{
	class Unit;
	class Ability;
	enum class BurrowState;
	enum class SiegeState;

	namespace Constants
	{
		namespace Abilities
		{
			extern int burrowRange;
			extern int burrowCooldown;
			extern int burrowCastTime;
			extern int burrowResourceCost;
			extern bool burrowIsStackable;

			extern int healRange;
			extern int healCooldown;
			extern int healCastTime;
			extern int healResourceCost;
			extern bool healIsStackable;
			extern int healHealAmount;

			extern int EMPRange;
			extern int EMPCooldown;
			extern int EMPCastTime;
			extern int EMPResourceCost;
			extern bool EMPIsStackable;

			extern int ChangeSiegeStateRange;
			extern int ChangeSiegeStateCooldown;
			extern int ChangeSiegeStateCastTime;
			extern int ChangeSiegeStateResourceCost;
			extern bool ChangeSiegeStateIsStackable;

			extern int StormRange;
			extern int StormCooldown;
			extern int StormCastTime;
			extern int StormResourceCost;
			extern bool StormIsStackable;

			extern int StimpackRange;
			extern int StimpackCooldown;
			extern int StimpackCastTime;
			extern int StimpackFirebatResourceCost;
			extern int StimpackMarineResourceCost;
			extern bool StimpackIsStackable;
		}
	}

	class AbilityFactory
	{
	private:
		AbilityFactory()
		{};
		std::unique_ptr<Ability> getHealAbility(Unit* u);

		std::unique_ptr<Ability> getPsionicStormAbility(Unit* u);

		std::unique_ptr<Ability> getChangeSiegeStateAbility(Unit* u, SiegeState curState);
		std::unique_ptr<Ability> getStimpackAbility(Unit* u);
		std::unique_ptr<Ability> getEMPShockwaveAbility(Unit* u);
		std::unique_ptr<Ability> getBurrowingAbility(Unit* u, BurrowState curState);
	
	public:
		AbilityFactory& operator=(const AbilityFactory& other) = delete;
		AbilityFactory(const AbilityFactory& other) = delete;

		static AbilityFactory& getInstance()
		{
			static AbilityFactory fact;
			return fact;
		};

		//method used when adding new units into GameState
		std::unique_ptr<Ability> getAbility(Unit* u, BWAPI::TechType abilityTechType);
		//this method is used in copying the unit... in the copying, i am dependent on the
		//state of previous ability
		std::unique_ptr<Ability> getAbility(Unit* u, Ability* dependingAbility);

		void addAbilitiesToUnit(Unit& u);
	};


}