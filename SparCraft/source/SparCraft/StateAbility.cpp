#include "StateAbility.h"

using namespace SparCraft;

StateAbility::StateAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown) :
	Ability(castingTime, techTypeID, unit, cooldown)
{
	_abilityTargetType = AbilityTargetTypes::SELF;
}