#include "ProjectileFactory.h"
#include "AllProjectiles.h"
#include "Unit.h"

using namespace SparCraft;

namespace SparCraft
{

	namespace Constants
	{
		namespace Projectiles
		{
			double EMPSpeed = 20.0;

			double LurkerSpeed = 20.0;
		}
	}
}

std::unique_ptr<Projectile> ProjectileFactory::getLurkerAttackProjectile(
	const Position& startingPosition, const Position& targetPosition,
	int destinationDistance, TimeType nextActionTime, Unit* caster)
{
	return std::make_unique<LurkerAttackProjectile>(
		startingPosition,
		targetPosition,
		Constants::Projectiles::LurkerSpeed,
		destinationDistance,
		nextActionTime,
		caster
		);
}

std::unique_ptr<Projectile> ProjectileFactory::getEMPShockwaveProjectile(
	const Position& startingPosition, const Position& targetPosition,
	int destinationDistance, TimeType nextActionTime, Unit* caster)
{
	return std::make_unique<EMPShockwaveProjectile>(
		startingPosition,
		targetPosition,
		Constants::Projectiles::LurkerSpeed,
		destinationDistance,
		nextActionTime,
		caster
		);
}

std::unique_ptr<Projectile> ProjectileFactory::getProjectile(ProjectileTypes::ProjectileType projectileType,
	const Position& startingPosition, const Position& targetPosition,
	int destinationDistance, TimeType nextActionTime,
	Unit* caster)
{
	if (projectileType == ProjectileTypes::ProjectileType::EMPPROJECTILE)
	{
		return getEMPShockwaveProjectile(startingPosition,
			targetPosition,
			destinationDistance,
			nextActionTime,
			caster);
	}
	else if (projectileType == ProjectileTypes::ProjectileType::LURKERATTACKPROJECTILE)
	{
		return getLurkerAttackProjectile(startingPosition,
			targetPosition,
			destinationDistance,
			nextActionTime,
			caster);
	}
	else
	{
		System::FatalError("ProjectileFactory::getProjectile not supported projectile");
		return nullptr;
	}
}