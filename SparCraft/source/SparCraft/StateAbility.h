#pragma once

#include "Common.h"
#include "Ability.h"

namespace SparCraft
{
	class StateAbility: public Ability
	{
	protected:
		StateAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown);
	public:

		virtual void beCasted(TimeType gameTime=-1) = 0;
	};
}