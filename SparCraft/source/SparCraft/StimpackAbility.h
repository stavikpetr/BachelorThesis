#pragma once

#include "Common.h"
#include "StateAbility.h"

namespace SparCraft
{
	class StimpackAbility : public StateAbility
	{
	public: 
		const bool isCastableNow(const TimeType& currentTime) const;
		void beCasted(TimeType gameTime = -1);

		StimpackAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown);
	};
}