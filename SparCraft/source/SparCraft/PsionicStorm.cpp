#include "PsionicStorm.h"
#include "Unit.h"
#include "GameState.h"

using namespace SparCraft;

PsionicStorm::PsionicStorm(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
	int range):
	AoEAbility(castingTime, techTypeID, unit, cooldown, range)
{
	_resourceCost = Constants::Abilities::StormResourceCost;
	_abilityType = AbilityTypes::PSIONICSTORM;
}

const bool PsionicStorm::isCastableNow(const TimeType& currentTime) const
{
	if (currentTime != _timeCanBeCasted)
		return false;
	if (_unit->currentEnergy() < _resourceCost)
		return false;
	return true;
}

void PsionicStorm::beCasted(GameState& state, Position p)
{
	_unit->updateEnergy(-_resourceCost);
	state.addEffect(EffectFactory::getInstance().getPsionicStormEffect(
		state.getTime(), p));
}
