#pragma once

#include "Common.h"
#include "Ability.h"

#include "StateAbility.h"
#include "SingleTargetAbility.h"
#include "AoEAbility.h"

//state abilities
#include "ChangeSiegeState.h"
#include "StimpackAbility.h"
#include "BurrowAbility.h"

//single targe abilities
#include "HealAbility.h"

//aoe abilities
#include "PsionicStorm.h"
#include "EMPShockwaveAbility.h"