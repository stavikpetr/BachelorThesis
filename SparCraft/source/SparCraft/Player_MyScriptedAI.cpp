#include "Player_MyScriptedAI.h"

using namespace SparCraft;

Player_MyScriptedAI::Player_MyScriptedAI(const IDType & playerID)
{
	_playerID = playerID;
}

Player_MyScriptedAI::Player_MyScriptedAI(const IDType & playerID, bool followsFormationPolicy, Formation * formation)
{
	_playerID = playerID;
	_followsFormationPolicy = followsFormationPolicy;
	_formation = formation;
}

void Player_MyScriptedAI::getMoves(GameState& state, const MoveArray& moves, std::vector<Action>& moveVec)
{
	moveVec.clear();
	bool broken;
	if (_followsFormationPolicy)
	{
		broken = _formation->isFormationBroken(_playerID, state);
		if (broken)
			_followsFormationPolicy = false;
	}
	for (IDType u(0); u < moves.numUnits(); ++u)
	{
		int attackMoveIndex(-1);
		int abilityMoveIndex(-1);
		int waitMoveIndex(-1);
		int moveMoveIndex(-1);

		int actionDistance(std::numeric_limits<int>::max());
		int closestMoveDist(std::numeric_limits<int>::max());
		if (_followsFormationPolicy)
		{
			moveVec.push_back(_formation->getBestAction(moves, u));
			continue;
		}

		Unit & ourUnit(state.getUnit(_playerID, u));
		if (ourUnit.hasAbilities())
		{

#pragma region SiegeTank

			if (ourUnit.type() == BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode ||
				ourUnit.type() == BWAPI::UnitTypes::Terran_Siege_Tank_Tank_Mode)
			{
				auto& ab = ourUnit.getAbilities()[0];
				auto stateAbility = dynamic_cast<StateAbility*>(ab.get());
				auto castedToChangeSiegeState = dynamic_cast<ChangeSiegeState*>(stateAbility);

				const Unit & closestUnit(state.getClosestEnemyUnit(_playerID, u));

				bool anyInRange = false;
				IDType enemyPlayer = state.getEnemy(_playerID);

				if (castedToChangeSiegeState->getCurrentState() == SiegeState::UNSIEGED)
				{
					//we will cast this only if tehre are no enemy units in the arc of siege tank range
					for (IDType unitIndex(0); unitIndex < state.numUnits(enemyPlayer); unitIndex++)
					{
						const Unit & unit = state.getUnit(enemyPlayer, unitIndex);
						if (ourUnit.canAttackTarget(unit, state.getTime()))
						{
							anyInRange = true;
							break;
						}
					}
				}
				else if (castedToChangeSiegeState->getCurrentState() == SiegeState::SIEGED)
				{
					for (IDType unitIndex(0); unitIndex < state.numUnits(enemyPlayer); unitIndex++)
					{
						const Unit & unit = state.getUnit(enemyPlayer, unitIndex);
						if (ourUnit.canAttackTarget(unit, state.getTime()))
						{
							anyInRange = true;
							break;
						}
					}
				}

				for (size_t m(0); m < moves.numMoves(u); ++m)
				{
					const Action& move(moves.getMove(u, m));

					if (move.type() == ActionTypes::ATTACK || move.type() == ActionTypes::ATTACKSPLASH)
					{
						const Unit & target(state.getUnit(state.getEnemy(move.player()), move.index()));
						int dist = (int)Position::getDist(ourUnit.position(), target.position());

						if (dist < actionDistance)
						{
							actionDistance = dist;
							attackMoveIndex = m;
						}
					}

					if (move.type() == ActionTypes::CASTABILITY)
					{
						if (!anyInRange && castedToChangeSiegeState->getCurrentState() == SiegeState::SIEGED)
						{
							abilityMoveIndex = m;
						}
						if (anyInRange && castedToChangeSiegeState->getCurrentState() == SiegeState::UNSIEGED)
						{
							//there should be check that we would be able to attack the unit in siege mode....
							abilityMoveIndex = m;
						}
					}


					if (move.type() == ActionTypes::ACTIONWAIT)
					{
						if (attackMoveIndex == -1 && abilityMoveIndex == -1)
						{
							if (anyInRange)
								waitMoveIndex = m;
						}
					}

					if (move.type() == ActionTypes::MOVETOPOSITION)
					{
						move.setNewPosition(closestUnit.position());
						moveMoveIndex = m;
					}


					if (move.type() == ActionTypes::MOVE)
					{
						// we have only one move, where are we moving depends on the logic behind move to position
						moveMoveIndex = m;
					}
				}
				int bestMoveIndex = abilityMoveIndex != -1 ? abilityMoveIndex : (
					attackMoveIndex != -1 ? attackMoveIndex : (
					waitMoveIndex != -1 ? waitMoveIndex : (
					moveMoveIndex != -1 ? moveMoveIndex : -1)));

				if (bestMoveIndex != -1)
					moveVec.push_back(moves.getMove(u, bestMoveIndex));
				else
				{
					//there is always pass turn at the end of this vector
					moveVec.push_back(moves.getMove(u, moves.numMoves(u) - 1));
				}
			}

#pragma endregion

#pragma region highTemplar

			else if (ourUnit.type() == BWAPI::UnitTypes::Protoss_High_Templar)
			{
				auto& ab = ourUnit.getAbilities()[0];
				auto castedToAoE = dynamic_cast<AoEAbility*>(ab.get());
				const Unit & closestUnit(state.getClosestEnemyUnit(_playerID, u));

				bool anyInRange = false;
				IDType enemyPlayer = state.getEnemy(_playerID);


				for (size_t m(0); m < moves.numMoves(u); ++m)
				{
					const Action& move(moves.getMove(u, m));

					if (move.type() == ActionTypes::CASTABILITY)
					{
						if(Position::getDist(ourUnit.position(), closestUnit.position()) < castedToAoE->getRange())
						{
							abilityMoveIndex = m;
							move.setNewPosition(closestUnit.position());
						}
					}

					if (move.type() == ActionTypes::ACTIONWAIT)
					{
						if (attackMoveIndex == -1 && abilityMoveIndex == -1)
						{
							if(Position::getDist(ourUnit.position(), closestUnit.position()) < castedToAoE->getRange())
							{
								waitMoveIndex = m;
							}
						}
					}

					if (move.type() == ActionTypes::MOVETOPOSITION)
					{
						auto pos = Position::getPositionBetween(ourUnit.position(), closestUnit.position());
						move.setNewPosition(pos);
						moveMoveIndex = m;
					}


					if (move.type() == ActionTypes::MOVE)
					{
						// we have only one move, where are we moving depends on the logic behind move to position
						moveMoveIndex = m;
					}
				}
				int bestMoveIndex = abilityMoveIndex != -1 ? abilityMoveIndex : (
					waitMoveIndex != -1 ? waitMoveIndex : (
					moveMoveIndex != -1 ? moveMoveIndex : -1));

				if (bestMoveIndex != -1)
					moveVec.push_back(moves.getMove(u, bestMoveIndex));
				else
				{
					//there is always pass turn at the end of this vector
					moveVec.push_back(moves.getMove(u, moves.numMoves(u) - 1));
				}
			}

#pragma endregion

#pragma region ScienceVessel

			else if (ourUnit.type() == BWAPI::UnitTypes::Terran_Science_Vessel)
			{
				auto& ab = ourUnit.getAbilities()[0];
				auto casted = dynamic_cast<AoEAbility*>(ab.get());
				const Unit & closestUnit(state.getClosestEnemyUnit(_playerID, u));

				for (size_t m(0); m < moves.numMoves(u); ++m)
				{
					const Action& move(moves.getMove(u, m));

					if (move.type() == ActionTypes::CASTABILITY)
					{
						if(Position::getDist(ourUnit.position(), closestUnit.position()) < casted->getRange())
						{
							abilityMoveIndex = m;
							move.setNewPosition(closestUnit.position());
						}
					}

					if (move.type() == ActionTypes::ACTIONWAIT)
					{
						if (attackMoveIndex == -1 && abilityMoveIndex == -1)
						{
							if(Position::getDist(ourUnit.position(), closestUnit.position()) < casted->getRange())
							{
								waitMoveIndex = m;
							}
						}
					}

					if (move.type() == ActionTypes::MOVETOPOSITION)
					{
						auto position = Position::getPositionBetween(ourUnit.position(), closestUnit.position());
						move.setNewPosition(position);
						moveMoveIndex = m;
					}


					if (move.type() == ActionTypes::MOVE)
					{
						// we have only one move, where are we moving depends on the logic behind move to position
						moveMoveIndex = m;
					}
				}
				int bestMoveIndex = abilityMoveIndex != -1 ? abilityMoveIndex : (
					waitMoveIndex != -1 ? waitMoveIndex : (
					moveMoveIndex != -1 ? moveMoveIndex : -1));

				if (bestMoveIndex != -1)
					moveVec.push_back(moves.getMove(u, bestMoveIndex));
				else
				{
					//there is always pass turn at the end of this vector
					moveVec.push_back(moves.getMove(u, moves.numMoves(u) - 1));
				}
			}

#pragma endregion

#pragma region MarineFirebat

			else if (ourUnit.type() == BWAPI::UnitTypes::Terran_Marine || ourUnit.type() == BWAPI::UnitTypes::Terran_Firebat)
			{
				auto& stimAbility = ourUnit.getAbilities()[0];

				const Unit & closestUnit(state.getClosestEnemyUnit(_playerID, u));
				for (size_t m(0); m < moves.numMoves(u); ++m)
				{
					const Action& move(moves.getMove(u, m));

					if (move.type() == ActionTypes::ATTACK || move.type() == ActionTypes::ATTACKSPLASH)
					{
						const Unit & target(state.getUnit(state.getEnemy(move.player()), move.index()));
						int dist = (int)Position::getDist(ourUnit.position(), target.position());
						if (dist < actionDistance)
						{
							actionDistance = dist;
							attackMoveIndex = m;
						}
					}

					if (move.type() == ActionTypes::CASTABILITY)
					{
						if (ourUnit.canAttackTarget(closestUnit, state.getTime()) && !ourUnit.containsUnitEffect(EffectTypes::EffectType::STIMPACK))
							abilityMoveIndex = m;
					}

					if (move.type() == ActionTypes::ACTIONWAIT)
					{
						if (attackMoveIndex == -1 && abilityMoveIndex == -1)
						{
							if (ourUnit.canAttackTarget(closestUnit, state.getTime()))
							{
								waitMoveIndex = m;
							}
						}
					}

					if (move.type() == ActionTypes::MOVETOPOSITION)
					{
						move.setNewPosition(closestUnit.position());
						moveMoveIndex = m;
					}


					if (move.type() == ActionTypes::MOVE)
					{
						//we have only one move, where are we moving depends on the logic behind move to position
						moveMoveIndex = m;
					}
				}


				int bestMoveIndex = abilityMoveIndex != -1 ? abilityMoveIndex : (
					attackMoveIndex != -1 ? attackMoveIndex : (
					waitMoveIndex != -1 ? waitMoveIndex : (
					moveMoveIndex != -1 ? moveMoveIndex : -1)));
				if (bestMoveIndex != -1)
					moveVec.push_back(moves.getMove(u, bestMoveIndex));
				else
				{
					//there is always pass turn at the end of this vector
						moveVec.push_back(moves.getMove(u, moves.numMoves(u) - 1));
				}
			}

#pragma endregion

#pragma region Lurker

			else if (ourUnit.type() == BWAPI::UnitTypes::Zerg_Lurker)
			{
				const Unit & closestUnit(state.getClosestEnemyUnit(_playerID, u));

				auto& ab = ourUnit.getAbilities()[0];
				auto castedToState = dynamic_cast<StateAbility*>(ab.get());
				auto castedToBurrow = dynamic_cast<BurrowAbility*>(castedToState);

				bool anyInRange = false;
				IDType enemyPlayer = state.getEnemy(_playerID);


				for (IDType unitIndex(0); unitIndex < state.numUnits(enemyPlayer); unitIndex++)
				{
					const Unit & unit = state.getUnit(enemyPlayer, unitIndex);
					if (ourUnit.canAttackTarget(unit, state.getTime()))
					{
						anyInRange = true;
						break;
					}
				}

				for (size_t m(0); m < moves.numMoves(u); ++m)
				{
					const Action& move(moves.getMove(u, m));

					if (move.type() == ActionTypes::ATTACKSPLASH)
					{
						const Unit & target(state.getUnit(state.getEnemy(move.player()), move.index()));
						int dist = (int)Position::getDist(ourUnit.position(), target.position());

						if (dist < actionDistance)
						{
							actionDistance = dist;
							attackMoveIndex = m;
						}
					}

					if (move.type() == ActionTypes::CASTABILITY)
					{
						if (castedToBurrow->getCurrentBurrowState() == BurrowState::BURROWED)
						{
							if (!anyInRange)
								abilityMoveIndex = m;
						}
						else if (castedToBurrow->getCurrentBurrowState() == BurrowState::UNBURROWED)
						{
							if (anyInRange)
								abilityMoveIndex = m;
						}
					}


					if (move.type() == ActionTypes::ACTIONWAIT)
					{
						if (attackMoveIndex == -1 && abilityMoveIndex == -1)
						{
							if (anyInRange)
								waitMoveIndex = m;
						}
					}

					if (move.type() == ActionTypes::MOVETOPOSITION)
					{
						move.setNewPosition(closestUnit.position());
						moveMoveIndex = m;
					}


					if (move.type() == ActionTypes::MOVE)
					{
						// we have only one move, where are we moving depends on the logic behind move to position
						moveMoveIndex = m;
					}
				}

				int bestMoveIndex = abilityMoveIndex != -1 ? abilityMoveIndex : (
					attackMoveIndex != -1 ? attackMoveIndex : (
					waitMoveIndex != -1 ? waitMoveIndex : (
					moveMoveIndex != -1 ? moveMoveIndex : -1)));

				auto& mou = moves.getMove(u, bestMoveIndex);
				if (bestMoveIndex != -1)
					moveVec.push_back(moves.getMove(u, bestMoveIndex));
				else
				{
					//there is always pass turn at the end of this vector
					moveVec.push_back(moves.getMove(u, moves.numMoves(u) - 1));
				}
			}
			else
			{
				const Unit & closestUnit(state.getClosestEnemyUnit(_playerID, u));

				const IDType enemyPlayer(state.getEnemy(_playerID));
				Position moveToPosition;
				if (state.playerHasWorkers(enemyPlayer))
					moveToPosition = state.getClosestEnemyWorker(_playerID, u).position();
				else
					moveToPosition = closestUnit.position();

				for (size_t m(0); m < moves.numMoves(u); ++m)
				{
					const Action& move(moves.getMove(u, m));

					if (move.type() == ActionTypes::ATTACK || move.type() == ActionTypes::ATTACKSPLASH)
					{
						const Unit & target(state.getUnit(state.getEnemy(move.player()), move.index()));
						int dist = (int)Position::getDist(ourUnit.position(), target.position());

						if (dist < actionDistance)
						{
							actionDistance = dist;
							attackMoveIndex = m;
						}
					}

					if (move.type() == ActionTypes::ACTIONWAIT)
					{
						if (attackMoveIndex == -1 && abilityMoveIndex == -1)
						{
							if (ourUnit.canAttackTarget(closestUnit, state.getTime()))
							{
								waitMoveIndex = m;
							}
						}
					}

					if (move.type() == ActionTypes::MOVETOPOSITION)
					{
						move.setNewPosition(moveToPosition);
						moveMoveIndex = m;
					}


					if (move.type() == ActionTypes::MOVE)
					{
						// we have only one move, where are we moving depends on the logic behind move to position
						moveMoveIndex = m;
					}
				}

				int bestMoveIndex = attackMoveIndex != -1 ? attackMoveIndex : (waitMoveIndex != -1 ? waitMoveIndex :
					(moveMoveIndex != -1 ? moveMoveIndex : -1));
				if (bestMoveIndex != -1)
					moveVec.push_back(moves.getMove(u, bestMoveIndex));
				else
				{
					//there is always pass turn at the end of this vector
					moveVec.push_back(moves.getMove(u, moves.numMoves(u) - 1));
				}
			}

#pragma endregion
		}
		//		//otherwise, this unit is a simple attacker with no abilities
		else
		{
			const Unit & closestUnit(state.getClosestEnemyUnit(_playerID, u));

			// we will prioritize workers
			const IDType enemyPlayer(state.getEnemy(_playerID));
			Position moveToPosition;
			if (state.playerHasWorkers(enemyPlayer))
				moveToPosition = state.getClosestEnemyWorker(_playerID, u).position();
			else
				moveToPosition = closestUnit.position();

			for (size_t m(0); m < moves.numMoves(u); ++m)
			{
				const Action& move(moves.getMove(u, m));

				if (move.type() == ActionTypes::ATTACK || move.type() == ActionTypes::ATTACKSPLASH)
				{
					const Unit & target(state.getUnit(state.getEnemy(move.player()), move.index()));
					int dist = (int)Position::getDist(ourUnit.position(), target.position());

					if (dist < actionDistance)
					{
						actionDistance = dist;
						attackMoveIndex = m;
					}
				}

				if (move.type() == ActionTypes::ACTIONWAIT)
				{
					if (attackMoveIndex == -1 && abilityMoveIndex == -1)
					{
						if (ourUnit.canAttackTarget(closestUnit, state.getTime()))
						{
							waitMoveIndex = m;
						}
					}
				}

				if (move.type() == ActionTypes::MOVETOPOSITION)
				{
					move.setNewPosition(moveToPosition);
					moveMoveIndex = m;
				}


				if (move.type() == ActionTypes::MOVE)
				{
					// we have only one move, where are we moving depends on the logic behind move to position
					moveMoveIndex = m;
				}
			}

			int bestMoveIndex = attackMoveIndex != -1 ? attackMoveIndex : (waitMoveIndex != -1 ? waitMoveIndex :
				(moveMoveIndex != -1 ? moveMoveIndex : -1));
			if (bestMoveIndex != -1)
				moveVec.push_back(moves.getMove(u, bestMoveIndex));
			else
			{
				//there is always pass turn at the end of this vector
				moveVec.push_back(moves.getMove(u, moves.numMoves(u) - 1));
			}
		}
	}

}