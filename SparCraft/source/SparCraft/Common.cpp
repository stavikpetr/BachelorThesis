#include "Common.h"
// SEARCH PARAMETERS
char SPARCRAFT_LOGFILE[100] { "sparcraft_error_log.txt" };

namespace SparCraft
{
	namespace Constants
	{
		namespace General
		{
			// add between a move and attack as penalty
			TimeType Move_Penalty = 4;

			// pass move duration
			size_t Pass_Move_Duration = 10;

			int waitFrameMaxCount = 2;

			bool collisions = true;
			int guiWidth = 1280;
			int guiHeight = 720;
			int mapWidth = 1500;
			int mapHeight = 1000;
			bool invisibility = true;
			void setSimulationParameters(int mW, int mH,
				int gW, int gH, bool col, bool invis)
			{
				invisibility = invis;
				collisions = col;
				mapWidth = mW;
				mapHeight = mH;
				guiWidth = gW;
				guiHeight = gH;
			}

			void setSimulationParametersMap(int mW, int mH)
			{
				mapWidth = mW;
				mapHeight = mH;
			}
			void setSimulationParametersGui(int gW, int gH)
			{
				if(gW > guiWidth)
					guiWidth = gW;
				if(gH > guiHeight)
					guiHeight = gH;
			}
			void setSimulationParametersCollisionsInvisibility(bool col, bool invis)
			{
				collisions = col;
				invisibility = invis;
			}
		}

		namespace Map
		{
			int walkTileSize = 8;
			int buildTileSize = 32;
			int collisionTileSize = 50;
		}

		namespace Units
		{
			HealthType Starting_Energy = 100;
			double basicEnergyRegenerationRate = 0.7875 / 24.0;
		}
	}
    namespace System
    {
        void FatalError(const std::string & errorMessage)
        {
            std::cerr << "\n\n\nSparCraft Fatal Error: \n\n\n      " << errorMessage << "\n\n";

			/*std::ofstream logStream;
			logStream.open(SPARCRAFT_LOGFILE, std::ofstream::app);
			logStream << "\n\n\nSparCraft Fatal Error: \n\n\n      " << errorMessage << "\n\n";
			logStream.flush();
			logStream.close();*/

            throw(SPARCRAFT_FATAL_ERROR);
        }
        
        void checkSupportedUnitType(const BWAPI::UnitType & type)
        {
            if (type == BWAPI::UnitTypes::None || type == BWAPI::UnitTypes::Unknown)
            {
                System::FatalError("Unknown unit type in experiment file, not supported");
            }
	

            if (type == BWAPI::UnitTypes::Protoss_Corsair || 
                type == BWAPI::UnitTypes::Zerg_Devourer || 
                type == BWAPI::UnitTypes::Zerg_Scourge ||
                type == BWAPI::UnitTypes::Terran_Valkyrie)
            {
                System::FatalError("Units with just air weapons currently not supported correctly: " + type.getName());
            }

            if (type.isBuilding() && !(type == BWAPI::UnitTypes::Protoss_Photon_Cannon || type == BWAPI::UnitTypes::Zerg_Sunken_Colony || type == BWAPI::UnitTypes::Terran_Missile_Turret))
            {
                System::FatalError("Non-attacking buildings not currently supported: " + type.getName());
            }

            if (type.isSpellcaster() && (type != BWAPI::UnitTypes::Terran_Medic && type!= BWAPI::UnitTypes::Protoss_High_Templar
				&& type != BWAPI::UnitTypes::Terran_Science_Vessel) )
            {
                System::FatalError("Spell casting units not currently supported: " + type.getName());
            }

             // Don't support units loading other units yet
            if (type == BWAPI::UnitTypes::Terran_Vulture_Spider_Mine ||
				
                type == BWAPI::UnitTypes::Protoss_Carrier || 
                type == BWAPI::UnitTypes::Protoss_Interceptor || 
                type == BWAPI::UnitTypes::Protoss_Reaver ||
                type == BWAPI::UnitTypes::Protoss_Scarab ||
                type == BWAPI::UnitTypes::Zerg_Broodling)
            {

                System::FatalError("Units which have unit projectiles not supported: " + type.getName());
            }
        }

        bool isSupportedUnitType(const BWAPI::UnitType & type)
        {
            if (type == BWAPI::UnitTypes::None || type == BWAPI::UnitTypes::Unknown)
            {
                return false;
            }

            if (type == BWAPI::UnitTypes::Protoss_Corsair || 
                type == BWAPI::UnitTypes::Zerg_Devourer || 
                type == BWAPI::UnitTypes::Zerg_Scourge ||
                type == BWAPI::UnitTypes::Terran_Valkyrie)
            {
                return false;
            }

            if (type.isBuilding() && !(type == BWAPI::UnitTypes::Protoss_Photon_Cannon || type == BWAPI::UnitTypes::Zerg_Sunken_Colony || type == BWAPI::UnitTypes::Terran_Missile_Turret))
            {
                return false;
            }

            if (type.isSpellcaster() && (type != BWAPI::UnitTypes::Terran_Medic && type!= BWAPI::UnitTypes::Protoss_High_Templar 
				&& type!= BWAPI::UnitTypes::Terran_Science_Vessel))
            {
                return false;
            }

            // Don't support units loading other units yet
            if (type == BWAPI::UnitTypes::Terran_Vulture_Spider_Mine || 
                type == BWAPI::UnitTypes::Protoss_Carrier || 
                type == BWAPI::UnitTypes::Protoss_Interceptor || 
                type == BWAPI::UnitTypes::Protoss_Reaver ||
                type == BWAPI::UnitTypes::Protoss_Scarab ||
                type == BWAPI::UnitTypes::Zerg_Broodling)
            {
                return false;
            }

            return true;
        }

		bool isSupportedAbility(const BWAPI::TechType& type)
		{
			if (type == BWAPI::TechTypes::Healing)
				return true;
			else if (type == BWAPI::TechTypes::Tank_Siege_Mode)
				return true;
			else if (type == BWAPI::TechTypes::Psionic_Storm)
				return true;
			else if (type == BWAPI::TechTypes::Stim_Packs)
				return true;
			else if (type == BWAPI::TechTypes::EMP_Shockwave)
				return true;
			else if (type == BWAPI::TechTypes::Burrowing)
				return true;
			return false;
		}
    }
};
