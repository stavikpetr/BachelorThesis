#pragma once

#include "Common.h"
#include "Position.hpp"
#include <memory>

namespace SparCraft
{

namespace ActionTypes
{
	enum ActionType{NONE, ATTACK, RELOAD, MOVE, PASS, HEAL, 
	ATTACKSPLASH, CASTABILITY,
	ACTIONWAIT,
	STUCK, WAITONEFRAME,
	MOVETOPOSITION};
};

class Ability;
class Action 
{
	IDType  _unit;
	IDType	_player;
	IDType	_moveType;
	IDType	_moveIndex;
	Ability* _ability;
    mutable Position _p;

public:


	Action();

	

    Action( const IDType & unitIndex, const IDType & player, const IDType & type, const IDType & moveIndex, const Position & dest);

	Action( const IDType & unitIndex, const IDType & player, const IDType & type, const IDType & moveIndex);

	Action(const IDType & unitIndex, const IDType & player, const IDType & type,
		const IDType & moveIndex, Ability* ability);

	const bool operator == (const Action & rhs);

	const IDType & unit()	const;
	const IDType & player() const;
	const IDType & type()	const;
	const IDType & index()	const;
    const Position & pos()  const;
	Ability* getAbility();
	const bool hasAbility() const;

	const std::string moveString() const;

	const int* getDir() const;

    const std::string debugString() const;

	//used in handling collisions
	void setNewPosition(const Position& dest) const;
	void setNewType(const IDType & moveTypeId);

};

}