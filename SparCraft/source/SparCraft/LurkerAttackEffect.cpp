#include "LurkerAttackEffect.h"
#include "GameState.h"
#include "LurkerAttackProjectile.h"
#include "GUIGame.h"

using namespace SparCraft;

LurkerAttackEffect::LurkerAttackEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval,
	std::shared_ptr<std::unordered_set<IDType>> affectedUnitIds, Position position, int rectHeight, int rectWidth, Vector dirUnitVector,
	Unit* caster) : _affectedUnitIDs(affectedUnitIds), _caster(caster),
	StateEffect(startingTime, lastingTime, tickInterval)
{
	_dirUnitVector = dirUnitVector;
	_p = position;
	_rectWidth = rectWidth;
	_rectHeight = rectHeight;
	_effectType = EffectTypes::EffectType::LURKERATTACK;
}

void LurkerAttackEffect::AffectUnit(Unit* unitToAffect, TimeType& currentGameTime)
{
	//check that unit wasn't affected will be done in gameState
	unitToAffect->takeAttack(*_caster);
	(*_affectedUnitIDs).insert(unitToAffect->ID());	

}

int LurkerAttackEffect::getRectWidth()
{
	return _rectWidth;
}
int LurkerAttackEffect::getRectHeight()
{
	return _rectHeight;
}

Position LurkerAttackEffect::getPosition()
{
	return _p;
}

Vector LurkerAttackEffect::getDirectionUnitVector()
{
	return _dirUnitVector;
}


std::unordered_set<IDType>* LurkerAttackEffect::getAffectedUnitIDs()
{
	return _affectedUnitIDs.get();
}

std::vector<Unit*> LurkerAttackEffect::getUnitsToAffect(GameState& state)
{
	std::vector<Unit*> unitsToAffect;

	Position p2 = Position(_p.x() + (int)(_dirUnitVector.x() * _rectWidth), _p.y() + (int)(_dirUnitVector.y() * _rectWidth));
	Position p1 = Position(_p.x() - (int)(_dirUnitVector.x() * _rectWidth), _p.y() - (int)(_dirUnitVector.y() * _rectWidth));
	Vector perpUnitVector = Vector((-1 * _dirUnitVector.y())*_rectHeight, (_dirUnitVector.x())*_rectHeight);
	Position p1_ = Position(p1.x() + (int)perpUnitVector.x(), p1.y() + (int)perpUnitVector.y());
	Position p2_ = Position(p1.x() - (int)perpUnitVector.x(), p1.y() - (int)perpUnitVector.y());
	Position p3_ = Position(p2.x() + (int)perpUnitVector.x(), p2.y() + (int)perpUnitVector.y());
	Position p4_ = Position(p2.x() - (int)perpUnitVector.x(), p2.y() - (int)perpUnitVector.y());

	Position ax1 = Position(1, 0);
	//vertical axis
	Position ax2 = Position(0, 1);
	Position ax3 = Position(p3_.x() - p1_.x(), p3_.y() - p1_.y());
	Position ax4 = Position(p1_.x() - p2_.x(), p1_.y() - p2_.y());

	std::array<Position, 4> axes({ { ax1, ax2, ax3, ax4 } });
	std::array<Position, 4> pos({ { p1_, p2_, p3_, p4_ } });
	auto possibleAffectedUnits = state.getCollisionTilesHolder().getUniqueUnitsFromCollisionTiles(p1_, p3_, p4_, p2_);
	for (auto& unit : possibleAffectedUnits)
	{
		//lurker attack affects only enemies
		if (unit->player() == _caster->player())
			continue;
		//this unit was already hit
		if ((*_affectedUnitIDs).find(unit->ID()) != (*_affectedUnitIDs).end())
			continue;
		bool didUnitCollide = true;
		std::array<Position, 4> unitPos({ { unit->topLeft(), unit->topRight(), unit->botLeft(), unit->botRight() } });
		for (auto& axis : axes)
		{
			auto projectedPositionsA = SplashDamageDealer::getInstance().getProjectedPositions(pos, axis);
			auto projectedPositionsB = SplashDamageDealer::getInstance().getProjectedPositions(unitPos, axis);

			int minA = (int)SplashDamageDealer::getInstance().minimumOfProjectedValues(projectedPositionsA, axis);
			int maxA = (int)SplashDamageDealer::getInstance().maximumOfProjectedValues(projectedPositionsA, axis);
			int minB = (int)SplashDamageDealer::getInstance().minimumOfProjectedValues(projectedPositionsB, axis);
			int maxB = (int)SplashDamageDealer::getInstance().maximumOfProjectedValues(projectedPositionsB, axis);

			if ((minB <= maxA && minB >= minA) || (maxB <= maxA && maxB >= maxA)
				|| (minA <= maxB && minA >= minB) || (maxA <= maxB && maxA >= minB))
			{
				//we have collision and we need to continue with the other axis
			}
			else
			{
				//they are colliding, we dont need to check anymore
				didUnitCollide = false;
				break;

			}
		}
		if (didUnitCollide)
			unitsToAffect.push_back(unit);
	}
	GUIGame::drawLurkerEffectRectangles(p1_, p2_, p3_, p4_);
	return unitsToAffect;
}

Unit* LurkerAttackEffect::getCaster()
{
	return _caster;
}