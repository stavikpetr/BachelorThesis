#pragma once

#include "Common.h"
#include "Position.hpp"

///just a set of global helper functions that are used in several
///modules throughout the whole sparcraft

namespace SparCraft
{
	namespace CollisionHelperFunctions
	{
		const bool areAlignedCollisionBoxesColliding(
			int topLeftX1, int topLeftY1, int width1, int height1,
			int topLeftX2, int topLeftY2, int width2, int height2);

		const bool alignedRectCircleCollision(
			Position p1, Position p2, Position circleCenter,
			int radius);
	}
}