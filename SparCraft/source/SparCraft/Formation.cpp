#include "Formation.h"

using namespace SparCraft;

Formation::Formation(FormationTypes::FormationType type)
{
	_formationType = type;
}

const bool Formation::isFormationBroken(const IDType playerID, GameState & state) const
{
	if (_formationType == FormationTypes::HOLD)
	{
		return isHoldFormationBroken(playerID, state);
	}
	else
	{
		System::FatalError("invalid formation type");
		return false;
	}
}

const bool Formation::isHoldFormationBroken(const IDType playerID, GameState & state) const
{

	IDType enemyPlayer = state.getEnemy(playerID);

	for (size_t u(0); u < state.numUnits(playerID); u++)
	{
		const Unit& ourUnit(state.getUnit(playerID, u));
		if (ourUnit.currentHP() != ourUnit.maxHP())
			return true;

		for (size_t u(0); u < state.numUnits(enemyPlayer); ++u)
		{
			const Unit & enemyUnit(state.getUnit(enemyPlayer, u));
			if (ourUnit.canAttackTarget(enemyUnit, state.getTime()))
				return true;
		}
	}
	

	return false;
}

const Action& Formation::getBestActionHoldFormation(const MoveArray & moves, IDType unitIndex) const
{
	return moves.getMove(unitIndex, moves.numMoves(unitIndex) - 1);
}

const Action& Formation::getBestAction(const MoveArray & moves, IDType unitIndex) const
{
	if (_formationType == FormationTypes::HOLD)
	{
		return getBestActionHoldFormation(moves, unitIndex);
	}
	else
	{
		System::FatalError("invalid formation type");
		//return the pass action...
		return moves.getMove(unitIndex, moves.numMoves(unitIndex) - 1);
	}

}