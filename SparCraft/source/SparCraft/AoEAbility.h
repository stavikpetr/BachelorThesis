#pragma once

#include "Common.h"
#include "Ability.h"
#include "Position.hpp"

namespace SparCraft
{
	class GameState;
	class AoEAbility : public Ability
	{
	protected:
		int _range;
		AoEAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
			int range);
	public:
		virtual void beCasted(GameState& state, Position p) = 0;
		const int getRange() const;

	};
}