#include "Projectile.h"

using namespace SparCraft;

Projectile::Projectile(Position startingPosition, Position targetPosition, double speed, int destinationDistance, TimeType nextActionTime)
:_startingPosition(startingPosition), _targetPosition(targetPosition), _speed(speed), _currentPosition(_startingPosition),
_destinationDistance(destinationDistance)
{
	double x = targetPosition.x() - startingPosition.x();
	double y = targetPosition.y() - startingPosition.y();
	double divisor = sqrt((pow(x, 2) + pow(y, 2)));
	_dirUnitVector = Vector(x / divisor, y / divisor);
	_nextActionTime = nextActionTime;
}

const Position Projectile::getStartingPosition() const
{
	return _startingPosition;
}

const Position Projectile::getTargetPosition() const
{
	return _targetPosition;
}

const Position Projectile::getCurrentPosition() const
{
	return _currentPosition;
}

const double Projectile::getSpeed() const
{
	return _speed;
}

const ProjectileTypes::ProjectileType Projectile::getProjectileType() const
{
	return _projectileType;
}

const TimeType Projectile::getNextActionTime() const
{
	return _nextActionTime;
}

void Projectile::updateNextActionTime()
{
	_nextActionTime += 1;
}