#include "PositionHelperFunctions.h"

using namespace SparCraft;

namespace PositionHelperFunctions
{
	Position getPositionBetween(const Position& pos1, const Position& pos2)
	{
		int x = (pos1.x() - pos2.x()) / 2;
		int y = (pos1.y() - pos2.y()) / 2;

		return Position(pos1.x() - x, pos1.y() - y);
	}

	double getDistanceBetweenPositions(const Position& posFrom, const Position& posTo)
	{
		int first = posFrom.x() - posTo.x();
		first = first * first;
		int second = posFrom.y() - posTo.y();
		second = second * second;
		return sqrt(first + second);
	}
}