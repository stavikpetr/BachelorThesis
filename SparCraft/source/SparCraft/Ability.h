#pragma once

#include "Common.h"
#include <memory>
#include "AbilityFactory.h"
#include "EffectFactory.h"
#include "ProjectileFactory.h"

namespace SparCraft
{
	class Unit;
	namespace AbilityTargetTypes
	{
		//why do i even need these when i already have three inheriting classes that represent those...?
		//well, because of dynamic cast, there were two options, one would be something like:
		/*
		A* x = new C;
		if(B* b_ptr = dynamic_cast<B*>(x)) {
		// it's a B
		} else if(C* c_ptr = dynamic_cast<C*>(x)) {
		// it's a C
		}
		*/
		//but...readbility is zero, and try to cast directly would throw exception, so 
		// the solution is to decide the type based on enum value and then cast

		enum AbilityTargetType { SELF, SINGLE, AOE };
	}
	
	//again, for same purpose as AbilityTargetTypes
	namespace AbilityTypes
	{
		enum AbilityType { HEALABILITY, CHANGESIEGESTATE, PSIONICSTORM, STIMPACK, EMP, BURROW };
	}


	class Ability
	{
	protected:
		BWAPI::TechType         _techTypeID;
		int						_castingTime;		//how many turns does this ability require to be casted
		int                     _cooldown;			//when is this ability castable again
		int                     _resourceCost;
		bool                    _isStackable;

		TimeType                _timeCanBeCasted;

		AbilityTypes::AbilityType _abilityType;
		Unit*					_unit;
		AbilityTargetTypes::AbilityTargetType			_abilityTargetType;


		Ability(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown);
		
	public:
		const int						getCastingTime() const;
		const int                       getId() const;
		const Unit*                    getUnit() const;
		const BWAPI::TechType &         getOriginalType();
		const AbilityTargetTypes::AbilityTargetType getAbilityTargetType() const;
		
		const int getCooldown() const;
		const int getResourceCost() const;
		const AbilityTypes::AbilityType getAbilityType() const;

		const TimeType nextAbilityCastTime() const;

		void updateAbilityCastTime(const TimeType& newTime);
		const virtual bool isCastableNow(const TimeType& currentTime) const = 0;
		
	};
}