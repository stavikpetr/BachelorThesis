#pragma once

#include "Common.h"
#include "StateEffect.h"
#include "Position.hpp"

namespace SparCraft
{
	class EMPShockwaveEffect : public StateEffect
	{
		int _radius;
		int _energyDrain;
		int _shieldDrain;
		Position _p;
		IDType _castedByUnitID;
		
	public:
		EMPShockwaveEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval,
			int radius, int energyDrain, int shieldDrain, Position p, IDType castedByUnitID);

		int getRangeLow();
		int getRangeHigh();
		int getRadius();
		Position getPosition();
		void AffectUnit(Unit* unitToAffect, TimeType& currentGameTime);
		std::vector<Unit*> getUnitsToAffect(GameState& state);
		IDType getCastedByUnitID();

	};
}