#include "Unit.h"

using namespace SparCraft;

Unit::Unit()
    : _unitType             (BWAPI::UnitTypes::None)
    , _range                (0)
    , _unitID               (255)
    , _playerID             (255)
    , _currentHP            (0)
    , _currentEnergy        (0)
    , _timeCanMove          (0)
    , _timeCanAttack        (0)
    , _previousActionTime   (0)
    , _prevCurrentPosTime   (0)
{
    
}

// test constructor for setting all variables of a unit
Unit::Unit(const BWAPI::UnitType unitType, const Position & pos, const IDType & unitID, const IDType & playerID, 
           const HealthType & hp, const HealthType & energy, const TimeType & tm, const TimeType & ta) 
    : _unitType             (unitType)
    , _range                (PlayerWeapon(&PlayerProperties::Get(playerID), unitType.groundWeapon()).GetMaxRange() + Constants::Range_Addition)
    , _position             (pos)
    , _unitID               (unitID)
    , _playerID             (playerID)
    , _currentHP            (hp)
    , _currentEnergy        (energy)
    , _timeCanMove          (tm)
    , _timeCanAttack        (ta)
    , _previousActionTime   (0)
    , _prevCurrentPosTime   (0)
    , _previousPosition     (pos)
    , _prevCurrentPos       (pos)
{
    System::checkSupportedUnitType(unitType);
}

// constructor for units to construct basic units, sets some things automatically
Unit::Unit(const BWAPI::UnitType unitType, const IDType & playerID, const Position & pos) 
    : _unitType             (unitType)
    , _range                (PlayerWeapon(&PlayerProperties::Get(playerID), unitType.groundWeapon()).GetMaxRange() + Constants::Range_Addition)
    , _position             (pos)
    , _unitID               (0)
    , _playerID             (playerID)
    , _currentHP            (maxHP())
    , _currentEnergy        (unitType == BWAPI::UnitTypes::Terran_Medic || unitType==BWAPI::UnitTypes::Protoss_High_Templar
								|| unitType == BWAPI::UnitTypes::Terran_Science_Vessel ? Constants::Units::Starting_Energy : 0)
    , _timeCanMove          (0)
    , _timeCanAttack        (0)
    , _previousActionTime   (0)
    , _prevCurrentPosTime   (0)
    , _previousPosition     (pos)
    , _prevCurrentPos       (pos)
	, _dealsSplashDamage    (false)
	, _energyRegenerationRate (0)
	, _actionTypeBeforeWait (ActionTypes::NONE)
	, _attackSpeedMultiplier(1.0)
	, _moveSpeedMultiplier(1.0)
	, _isInvisible(unitType.hasPermanentCloak() ? true : false)
{
	if (unitType == BWAPI::UnitTypes::Zerg_Mutalisk || unitType == BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode
		|| unitType == BWAPI::UnitTypes::Terran_Firebat || unitType == BWAPI::UnitTypes::Zerg_Lurker)
		_dealsSplashDamage = true;
	if (unitType.isSpellcaster())
		_energyRegenerationRate = SparCraft::Constants::Units::basicEnergyRegenerationRate;
	if (unitType == BWAPI::UnitTypes::Terran_Medic)
	{
		_timeCanAttack = std::numeric_limits<int>::max();
	}
	if (unitType == BWAPI::UnitTypes::Protoss_High_Templar || unitType==BWAPI::UnitTypes::Terran_Science_Vessel
		|| unitType == BWAPI::UnitTypes::Zerg_Lurker)
	{
		_timeCanAttack = std::numeric_limits<int>::max();
	}

	if (unitType == BWAPI::UnitTypes::Terran_SCV ||
		unitType == BWAPI::UnitTypes::Zerg_Drone ||
		unitType == BWAPI::UnitTypes::Protoss_Probe)
	{
		_timeCanAttack = std::numeric_limits<int>::max();
		_timeCanMove = std::numeric_limits<int>::max();
	}
	
    System::checkSupportedUnitType(unitType);

}

Unit::Unit(const Unit& u)
	:_unitType(u._unitType),
	_range(u._range),
	_position(u._position),
	_unitID(u._unitID),
	_playerID(u._playerID),
	_currentHP(u._currentHP),
	_currentEnergy(u._currentEnergy),
	_timeCanMove(u._timeCanMove),
	_timeCanAttack(u._timeCanAttack),
	_previousActionTime(u._previousActionTime),
	_previousPosition(u._previousPosition),
	_prevCurrentPosTime(u._prevCurrentPosTime),
	_prevCurrentPos(u._prevCurrentPos),
	_dealsSplashDamage(u._dealsSplashDamage),
	_energyRegenerationRate(u._energyRegenerationRate),
	_attackSpeedMultiplier(u._attackSpeedMultiplier),
	_moveSpeedMultiplier(u._moveSpeedMultiplier),
	_isInvisible(u._isInvisible),
	_previousActionTargetPosition(u._previousActionTargetPosition),
	_waitFrames(u._waitFrames),
	_actionTypeBeforeWait(u._actionTypeBeforeWait),
	_path(u._path),
	_activeStateEffects(u._activeStateEffects),
	_previousAction(u._previousAction)
{

	abilities_.clear();
	_activeUnitEffects.clear();
	size_t i(0);
	for (auto& a : u.abilities_)
	{
		if (a->getId() == BWAPI::TechTypes::Healing)
		{
			abilities_.push_back(std::make_unique<HealAbility>(
				Constants::Abilities::healCastTime, a->getId(), this,
				Constants::Abilities::healCooldown, Constants::Abilities::healRange));
		}
		else if (a->getId() == BWAPI::TechTypes::Tank_Siege_Mode)
		{
			auto castedToChangeSiegeState = dynamic_cast<ChangeSiegeState*>(a.get());
			SiegeState curState = castedToChangeSiegeState->getCurrentState();
			abilities_.push_back(std::make_unique<ChangeSiegeState>(Constants::Abilities::ChangeSiegeStateCastTime,
				a->getId(), this, Constants::Abilities::ChangeSiegeStateCooldown, curState));
				
		}
		else if (a->getId() == BWAPI::TechTypes::Psionic_Storm)
		{
			abilities_.push_back(std::make_unique<PsionicStorm>(
				Constants::Abilities::StormCastTime, a->getId(), this,
				Constants::Abilities::StormCooldown, Constants::Abilities::StormRange));
		}
		else if (a->getId() == BWAPI::TechTypes::Stim_Packs)
		{
			abilities_.push_back(std::make_unique<StimpackAbility>(
				Constants::Abilities::StimpackCastTime, a->getId(), this,
				Constants::Abilities::StimpackCooldown));
		}
		else if (a->getId() == BWAPI::TechTypes::EMP_Shockwave)
		{
			abilities_.push_back(std::make_unique<EMPShockwaveAbility>(
				Constants::Abilities::EMPCastTime, a->getId(), this,
				Constants::Abilities::EMPCooldown, Constants::Abilities::EMPRange));
		}
		else if (a->getId() == BWAPI::TechTypes::Burrowing)
		{
			auto castedToBurrow = dynamic_cast<BurrowAbility*>(a.get());
			BurrowState curState = castedToBurrow->getCurrentBurrowState();
			abilities_.push_back(std::make_unique<BurrowAbility>(
				Constants::Abilities::burrowCastTime, a->getId(), this,
				Constants::Abilities::burrowCooldown, curState));
		}
		abilities_[i]->updateAbilityCastTime(a->nextAbilityCastTime());
		++i;
	}

	for (auto& a : u._activeUnitEffects)
	{
		if (a->getEffectType() == EffectTypes::EffectType::STIMPACK || 
			a->getEffectType() == EffectTypes::EffectType::HEAL)
		{
			_activeUnitEffects.push_back(EffectFactory::getInstance().getEffect(
				a->getEffectType(), a->getStartingTime()));
		}
		else if (a->getEffectType() == EffectTypes::EffectType::CHANGEINVISIBILITY)
		{
			auto casted = dynamic_cast<ChangeInvisibilityEffect*>(a.get());
			_activeUnitEffects.push_back(EffectFactory::getInstance().getChangeInvisibilityEffect(
				casted->getStartingTime(), casted->getNewInvisValue()));
		}
	}
}

Unit& Unit::operator=(const Unit& u)
{
	_unitType = u._unitType;
	_range = u._range;
	_position = u._position;
	_unitID = u._unitID;
	_playerID = u._playerID;
	_currentHP = u._currentHP;
	_currentEnergy = u._currentEnergy;
	_timeCanMove = u._timeCanMove;
	_timeCanAttack = u._timeCanAttack;
	_previousActionTime = u._previousActionTime;
	_previousPosition = u._previousPosition;
	_prevCurrentPosTime = u._prevCurrentPosTime;
	_prevCurrentPos = u._prevCurrentPos;
	_dealsSplashDamage = u._dealsSplashDamage;
	_energyRegenerationRate = u._energyRegenerationRate;
	_attackSpeedMultiplier = u._attackSpeedMultiplier;
	_moveSpeedMultiplier = u._moveSpeedMultiplier;
	_isInvisible = u._isInvisible;
	_previousActionTargetPosition = u._previousActionTargetPosition;
	_waitFrames = u._waitFrames;
	_actionTypeBeforeWait = u._actionTypeBeforeWait;
	_path = u._path;
	_activeStateEffects = u._activeStateEffects;
	_previousAction = u._previousAction;
	
	abilities_.clear();
	_activeUnitEffects.clear();
	size_t i(0);
	for (auto& a : u.abilities_)
	{
		if (a->getId() == BWAPI::TechTypes::Healing)
		{
			abilities_.push_back(std::make_unique<HealAbility>(
				Constants::Abilities::healCastTime, a->getId(), this,
				Constants::Abilities::healCooldown, Constants::Abilities::healRange));
		}
		else if (a->getId() == BWAPI::TechTypes::Tank_Siege_Mode)
		{
			auto castedToChangeSiegeState = dynamic_cast<ChangeSiegeState*>(a.get());
			SiegeState curState = castedToChangeSiegeState->getCurrentState();
			abilities_.push_back(std::make_unique<ChangeSiegeState>(Constants::Abilities::ChangeSiegeStateCastTime,
				a->getId(), this, Constants::Abilities::ChangeSiegeStateCooldown, curState));

		}
		else if (a->getId() == BWAPI::TechTypes::Psionic_Storm)
		{
			abilities_.push_back(std::make_unique<PsionicStorm>(
				Constants::Abilities::StormCastTime, a->getId(), this,
				Constants::Abilities::StormCooldown, Constants::Abilities::StormRange));
		}
		else if (a->getId() == BWAPI::TechTypes::Stim_Packs)
		{
			abilities_.push_back(std::make_unique<StimpackAbility>(
				Constants::Abilities::StimpackCastTime, a->getId(), this,
				Constants::Abilities::StimpackCooldown));
		}
		else if (a->getId() == BWAPI::TechTypes::EMP_Shockwave)
		{
			abilities_.push_back(std::make_unique<EMPShockwaveAbility>(
				Constants::Abilities::EMPCastTime, a->getId(), this,
				Constants::Abilities::EMPCooldown, Constants::Abilities::EMPRange));
		}
		else if (a->getId() == BWAPI::TechTypes::Burrowing)
		{
			auto castedToBurrow = dynamic_cast<BurrowAbility*>(a.get());
			BurrowState curState = castedToBurrow->getCurrentBurrowState();
			abilities_.push_back(std::make_unique<BurrowAbility>(
				Constants::Abilities::burrowCastTime, a->getId(), this,
				Constants::Abilities::burrowCooldown, curState));
		}
		abilities_[i]->updateAbilityCastTime(a->nextAbilityCastTime());
		++i;
	}

	for (auto& a : u._activeUnitEffects)
	{
		if (a->getEffectType() == EffectTypes::EffectType::STIMPACK ||
			a->getEffectType() == EffectTypes::EffectType::HEAL)
		{
			_activeUnitEffects.push_back(EffectFactory::getInstance().getEffect(
				a->getEffectType(), a->getStartingTime()));
		}
		else if (a->getEffectType() == EffectTypes::EffectType::CHANGEINVISIBILITY)
		{
			auto casted = dynamic_cast<ChangeInvisibilityEffect*>(a.get());
			_activeUnitEffects.push_back(EffectFactory::getInstance().getChangeInvisibilityEffect(
				casted->getStartingTime(), casted->getNewInvisValue()));
		}
	}

	return *this;
}

// Less than operator, used for sorting the GameState unit array.
// Units are sorted in this order:
//		1) alive < dead
//		2) firstTimeFree()
//		3) currentHP()
//		4) pos()
const bool Unit::operator < (const Unit & rhs) const
{
    if (!isAlive())
    {
        return false;
    }
    else if (!rhs.isAlive())
    {
        return true;
    }

    if (firstTimeFree() == rhs.firstTimeFree())
    {
        return ID() < rhs.ID();
    }
    else
    {
        return firstTimeFree() < rhs.firstTimeFree();
    }

    /*if (firstTimeFree() == rhs.firstTimeFree())
    {
        if (currentHP() == rhs.currentHP())
        {
            return pos() < rhs.pos();
        }
        else
        {
            return currentHP() < rhs.currentHP();
        }
    }
    else
    {
        return firstTimeFree() < rhs.firstTimeFree();
    }*/
}

// compares a unit based on unit id
const bool Unit::equalsID(const Unit & rhs) const
{ 
    return _unitID == rhs._unitID; 
}

//-------------------------------
//----------
//---------- ACTIONS RELATED BEGINS
//----------
//-------------------------------

// returns whether or not this unit can see a given unit at a given time
bool Unit::canSeeTarget(const Unit & unit, const TimeType & gameTime) const
{

	// range of this unit attacking
	PositionType r = type().sightRange();

	// return whether the target unit is in range
	return r >= Position::getDist(position(), unit.position());
}

// returns whether or not this unit can attack a given unit at a given time
const bool Unit::canAttackTarget(const Unit & unit, const TimeType & gameTime) const
{
	if (unit.isInvisible())
		return false;
	BWAPI::WeaponType weapon = unit.type().isFlyer() ? type().airWeapon() : type().groundWeapon();

	if (weapon.damageAmount() == 0)
	{
		return false;
	}

	// range of this unit attacking
	PositionType r = range();
	int minRange = getMinRange();

	// return whether the target unit is in range
	return r >= Position::getDist(position(), unit.position()) &&
		Position::getDist(position(), unit.position()) >= minRange;
}

const Position & Unit::position() const
{
    return _position;
}

// take an attack, subtract the hp
void Unit::takeAttack(const Unit & attacker, const double multiplier)
{
    PlayerWeapon    weapon(attacker.getWeapon(*this));
    HealthType      damage(weapon.GetDamageBase());

    // calculate the damage based on armor and damage types
    damage = std::max((int)((damage-getArmor()) * weapon.GetDamageMultiplier(getSize())), 2);
    
    if (attacker.type() == BWAPI::UnitTypes::Protoss_Zealot)
    {
        damage *= 2;
    }
			
    updateCurrentHP((int)(_currentHP - damage*multiplier));
}

// returns whether or not this unit is alive
const bool Unit::isAlive() const
{
    return _currentHP > 0;
}

// attack a unit, set the times accordingly
void Unit::attack(const Action & move, const Unit & target, const TimeType & gameTime)
{
	_waitFrames = 0;
    // if this is a repeat attack
    if (_actionTypeBeforeWait ==ActionTypes::ATTACK || _actionTypeBeforeWait == ActionTypes::ATTACKSPLASH || _previousAction.type() == ActionTypes::RELOAD)
    {
        // add the repeat attack animation duration
        // can't attack again until attack cooldown is up
        updateMoveActionTime      (gameTime + attackRepeatFrameTime());
		updateAbilitiesTime       (gameTime + attackRepeatFrameTime());
        updateAttackActionTime    (gameTime + attackCooldown());
    }
    // if there previous action was a MOVE action, add the move penalty
    else if (_previousAction.type() == ActionTypes::MOVE)
    {
        updateMoveActionTime      (gameTime + attackInitFrameTime() + 2);
		updateAbilitiesTime       (gameTime + attackInitFrameTime() + 2);
        updateAttackActionTime    (gameTime + attackCooldown() + Constants::General::Move_Penalty);
    }
    else
    {
        // add the initial attack animation duration
        updateMoveActionTime      (gameTime + attackInitFrameTime() + 2);
		updateAbilitiesTime       (gameTime + attackInitFrameTime() + 2);
        updateAttackActionTime    (gameTime + attackCooldown());
    }

    // if the unit is not mobile, set its next move time to its next attack time
   
    setPreviousAction(move, gameTime);
}

// unit update for moving based on a given Move
void Unit::move(const Action & move, const TimeType & gameTime) 
{
	_waitFrames = 0;
    _previousPosition = position();

    // get the distance to the move action destination
    PositionType dist = move.pos().getDistance(position());
	const int* dir = move.getDir();
	//right
	if (dir[0] == 1)
	{
		if (move.pos().x() >=_path.front().x())
			pathPop();
	}
	if (dir[0] == -1)
	{
		if (move.pos().x() <= _path.front().x())
			pathPop();
	}
	if (dir[1] == 1)
	{
		if (move.pos().y() >= _path.front().y())
			pathPop();
	}
	if (dir[1] == -1)
	{
		if (move.pos().y() <= _path.front().y())
			pathPop();
	}
    
    // how long will this move take?
    TimeType moveDuration = (TimeType)((double)dist / speed());
	
	if (moveDuration > 1)
		System::FatalError("move duration is bigger than 1, this can't happen");

    // update the next time we can move, make sure a move always takes 1 time step
	//sometimes can be zero, for example, for zergling, the speed is 5.49... but distance is int, so
	//the distance is 5, but, well, 
    updateMoveActionTime(gameTime + std::max(moveDuration, 1));
    // assume we need 4 frames to wait after moving
	//updateAttackActionTime(gameTime + std::max(moveDuration, 4));
	//updateAttackActionTime(gameTime + std::max(moveDuration, 1));
	updateAttackActionTime(std::max(nextAttackActionTime(), nextMoveActionTime()));
	//updateAttackActionTime(std::max(nextAttackActionTime(), nextMoveActionTime()));
	// won't wait for casting abilities
	updateAbilitiesTime(gameTime + std::max(moveDuration, 1));
	
    _position.moveTo(move.pos());
	
    setPreviousAction(move, gameTime);
}

void Unit::updateTimeAfterAbilityCast(const Action& move, const Ability* castedAbility, const TimeType & gameTime)
{
	//first, let's update time for each ability
	for (auto& ability : abilities_)
	{
		//for casted ability, the next possible time for cast is cooldown + casting time
		if (ability->getUnit()->ID() == castedAbility->getUnit()->ID())
		{
			ability->updateAbilityCastTime(gameTime + ability->getCooldown() + ability->getCastingTime());
		}
		//for the rest of abilities it's gameTime + castedAbility.CastingTime (if it wasn't already bigger)
		else
		{
			int currentTime = ability->nextAbilityCastTime();
			if (!(currentTime > gameTime + castedAbility->getCastingTime()))
			{
				ability->updateAbilityCastTime(gameTime + castedAbility->getCastingTime());
			}
		}
	}
	//now let's update timeCanAttack and timeCanMove
	updateAttackActionTime(gameTime + castedAbility->getCastingTime());
	updateMoveActionTime(gameTime + castedAbility->getCastingTime());
	setPreviousAction(move, gameTime);
}

void Unit::actionWait(const Action& move, const TimeType& gameTime)
{
	//careful, action "waitOneFrame" is a bit different from this one;
	//"waitOneFrame" is performed during colliding
	_waitFrames = 0;
	updateMoveActionTime(gameTime + 1);
	updateAttackActionTime(gameTime + 1);
	updateAbilitiesTime(gameTime + 1);
	setPreviousAction(move, gameTime);
}

void Unit::pass(const Action & move, const TimeType & gameTime)
{
	_waitFrames = 0;
    updateMoveActionTime(gameTime + Constants::General::Pass_Move_Duration);
	updateAttackActionTime(gameTime + Constants::General::Pass_Move_Duration);
	updateAbilitiesTime(gameTime + Constants::General::Pass_Move_Duration);
    setPreviousAction(move, gameTime);
}


void Unit::stuck(const Action & move, const TimeType & gameTime)
{
	_waitFrames = 0;
	updateMoveActionTime(gameTime + 1);
	updateAttackActionTime(gameTime + 1);
	updateAbilitiesTime(gameTime + 1);
	setPreviousAction(move, gameTime);
}


void Unit::waitOneFrame(const Action & move, const TimeType & gameTime)
{
	_waitFrames++;
	updateMoveActionTime(gameTime + 1);
	updateAttackActionTime(gameTime + 1);	
	updateAbilitiesTime(gameTime + 1);
	setPreviousAction(move, gameTime);
}


void Unit::moveToPosition(const Action & move, const TimeType & gameTime)
{
	_waitFrames = 0;
	updateMoveActionTime(gameTime + 1);
	updateAttackActionTime(gameTime + 1);
	updateAbilitiesTime(gameTime + 1);
	setPreviousAction(move, gameTime);
}

void Unit::updateAttackActionTime(const TimeType & newTime)
{ 
	if(_timeCanAttack < newTime)
		_timeCanAttack = newTime; 
}

void Unit::updateMoveActionTime(const TimeType & newTime)
{ 
	if(_timeCanMove < newTime)
		_timeCanMove = newTime; 
} 

void Unit::updateAbilitiesTime(const TimeType& newTime)
{
	for (auto& ability : abilities_)
	{
		if (ability->nextAbilityCastTime() < newTime)
			ability->updateAbilityCastTime(newTime);
	}
}

/* the canDoSomethingNow works correctly, because, these methods will be called only if unit.firstTimeFree == state.currentTime
and because in firstTimeFree we took min, this compare is all i need*/
const bool Unit::canAttackNow() const
{
	return (_timeCanAttack <= _timeCanMove && _timeCanAttack <= nextAbilityCastTime());
}

const bool Unit::canMoveNow() const
{
	return isMobile() && (_timeCanMove <= _timeCanAttack && _timeCanMove <= nextAbilityCastTime());
}

const bool Unit::canCastAbilityNow() const
{
	return hasAbilities() && (nextAbilityCastTime() <= _timeCanAttack && nextAbilityCastTime() <= _timeCanMove);
}

const TimeType Unit::nextAbilityCastTime() const
{
	TimeType min = std::numeric_limits<int>::max();
	for (auto& ability : abilities_)
	{
		if (ability->nextAbilityCastTime() < min)
		{
			min = ability->nextAbilityCastTime();
		}
	}
	return min;
}

const TimeType Unit::nextAttackActionTime() const
{
	return _timeCanAttack;
}

const TimeType Unit::nextMoveActionTime() const
{
	return _timeCanMove;
}

const TimeType Unit::firstTimeFree() const
{
	return std::min(_timeCanAttack, std::min(_timeCanMove, nextAbilityCastTime()));
}
//--------------------------
//----- HELPERS
//--------------------------

// returns the damage a unit does
const HealthType Unit::damage() const
{
	return _unitType == BWAPI::UnitTypes::Protoss_Zealot ?
		2 * (HealthType)_unitType.groundWeapon().damageAmount() :
		(HealthType)_unitType.groundWeapon().damageAmount();
}


void Unit::setPreviousAction(const Action & m, const TimeType & previousMoveTime)
{
	// if it was an attack move, store the unitID of the opponent unit
	if (m.type() != ActionTypes::MOVE)
		_actionTypeBeforeWait = ActionTypes::ActionType(m.type());
	_previousAction = m;
	_previousActionTime = previousMoveTime;
}


const bool Unit::hasAbilities() const
{
	return abilities_.size() > 0 ? true : false;
}

void Unit::clearVectors()
{
	abilities_.clear();
	_activeUnitEffects.clear();
}

std::vector<std::unique_ptr<Ability>>& Unit::getAbilities()
{
	return abilities_;
}

void Unit::addAbility(std::unique_ptr<Ability> && ability)
{
	abilities_.push_back(std::move(ability));
}

const float Unit::dpf() const 
{ 
    return (float)std::max(Constants::Min_Unit_DPF, (float)damage() / ((float)attackCooldown() + 1)); 
}


const TimeType Unit::attackCooldown() const 
{ 
    return (TimeType)(_unitType.groundWeapon().damageCooldown() * _attackSpeedMultiplier); 
}


const TimeType Unit::previousActionTime() const	
{ 
    return _previousActionTime; 
}

const TimeType Unit::attackInitFrameTime() const	
{ 
    return (int)(AnimationFrameData::getAttackFrames(_unitType).first * _attackSpeedMultiplier); 
}

const TimeType Unit::attackRepeatFrameTime() const	
{
    return (int)(AnimationFrameData::getAttackFrames(_unitType).second * _attackSpeedMultiplier); 
}

const bool Unit::hasPath() const
{
	return _path.size() > 0 ? true : false;
}
void Unit::setPath(std::deque<Position> & path)
{
	_path = path;
}

void Unit::clearPath()
{
	_path.clear();
}


//-------------------------------
//----------
//---------- ACTIONS RELATED ENDS
//----------
//-------------------------------



void Unit::changeIsInvisible(bool newIsInvisible)
{
	_isInvisible = newIsInvisible;
}


const Position Unit::getPreviousPosition() const
{
	return _previousPosition;
}

void Unit::setMoveActionTime(const TimeType& newTime)
{
	_timeCanMove = newTime;
}

void Unit::setAttackActionTime(const TimeType& newTime) //used for example for lurkers
{
	_timeCanAttack = newTime;
}

void Unit::removeUnitEffectByEffectId(EffectTypes::EffectType effectType)
{
	for (auto iterator = _activeUnitEffects.begin(); iterator != _activeUnitEffects.end(); iterator++)
	{
		//iterator is just a pointer to to collection that allows me to iterate it...
		//erase while iterating returns new iterator
		if ((*iterator)->getEffectType() == effectType)
		{
			iterator = _activeUnitEffects.erase(iterator);
			break;
		}
	}
}

std::vector<std::unique_ptr<Effect>>& Unit::getUnitEffects()
{
	return _activeUnitEffects;
}

const bool Unit::containsUnitEffect(EffectTypes::EffectType effectType) const
{
	for (auto& effect : _activeUnitEffects)
	{
		if (effect->getEffectType() == effectType)
			return true;
	}
	return false;
}

void Unit::addUnitEffect(std::unique_ptr<Effect>&& unitEffect)
{
	_activeUnitEffects.push_back(std::move(unitEffect));
}

void Unit::removeEndedUnitEffects(TimeType& currentGameTime)
{

	while (true)
	{
		bool erased = false;
		for (auto it = _activeUnitEffects.begin(); it != _activeUnitEffects.end(); ++it)
		{
			if ((*it)->getStartingTime() + (*it)->getLastingTime() <= currentGameTime)
			{
				_activeUnitEffects.erase(it);
				erased = true;
				break;
			}
		}
		if (!erased)
			break;
	}
}

void Unit::takeDamage(double damageTaken)
{
	_currentHP -= (int)damageTaken;
}

const bool Unit::containsStateEffect(EffectTypes::EffectType effectType) const
{
	for (auto& effectId : _activeStateEffects)
	{
		if (effectId == effectType)
			return true;
	}
	return false;
}
void Unit::resetStateEffects()
{
	_activeStateEffects.clear();
}
void Unit::addStateEffect(EffectTypes::EffectType effectType)
{
	_activeStateEffects.push_back(effectType);
}

void Unit::changeDealsSplashDamage(bool dealsSplashDamage)
{
	_dealsSplashDamage = dealsSplashDamage;
}

const int Unit::getMinRange() const
{
	return _unitType == BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode ? Constants::Units::siegeTankSiegedMinRange : 0;
}

void Unit::changeRange(const BWAPI::UnitType newType)
{
	_range = PlayerWeapon(&PlayerProperties::Get(_playerID), newType.groundWeapon()).GetMaxRange() + Constants::Range_Addition;
}

//this is mainly for siege tank and his sieging...
void Unit::switchUnitType(const BWAPI::UnitType newType)
{
	_unitType = newType;
}

void Unit::setPreviousPosition(const TimeType & gameTime)
{
	TimeType moveDuration = _timeCanMove - _previousActionTime;
	float moveTimeRatio = (float)(gameTime - _previousActionTime) / moveDuration;
	_prevCurrentPosTime = gameTime;
	_prevCurrentPos = _previousPosition + (_position - _previousPosition).scale(moveTimeRatio);
}


void Unit::print() const
{
	printf("%s %5d [%5d %5d] (%5d, %5d)\n", _unitType.getName().c_str(), currentHP(), nextAttackActionTime(), nextMoveActionTime(), x(), y());
}

void Unit::updateCurrentHP(const HealthType & newHP)
{
	_currentHP = std::min(maxHP(), newHP);
}

void Unit::setCooldown(TimeType attack, TimeType move)
{
	_timeCanAttack = attack; _timeCanMove = move;
}

void Unit::setUnitID(const IDType & id)
{
	_unitID = id;
}

void Unit::changeMoveSpeedMultiplier(const double newMultiplier)
{
	_moveSpeedMultiplier = newMultiplier;
}
void Unit::changeAttackSpeedMultiplier(const double newMultiplier)
{
	_attackSpeedMultiplier = newMultiplier;
}

const bool Unit::canKite() const
{
	return _timeCanMove < _timeCanAttack;
}

const bool Unit::isMobile() const
{
	return _unitType.canMove();
}

const bool Unit::canHeal() const
{
	return _unitType == BWAPI::UnitTypes::Terran_Medic;
}

const bool Unit::isInvisible() const
{
	if (!Constants::General::invisibility)
		return false;
	return _isInvisible;
}

const bool Unit::isOrganic() const
{
	return _unitType.isOrganic();
}

const IDType Unit::ID() const
{
	return _unitID;
}

const bool Unit::dealsSplashDamage() const
{
	return _dealsSplashDamage;
}

const IDType Unit::player() const
{
	return _playerID;
}

const PositionType Unit::x() const
{
	return _position.x();
}

const PositionType Unit::y() const
{
	return _position.y();
}

const PositionType Unit::range() const
{
	return _range;
}

const HealthType Unit::maxHP() const
{
	//hardcoded because of evaluation
	if (_unitType == BWAPI::UnitTypes::Protoss_Probe
		|| _unitType == BWAPI::UnitTypes::Zerg_Drone)
		return 1000;
	return (HealthType)_unitType.maxHitPoints() + (HealthType)_unitType.maxShields();
}

const HealthType Unit::currentHP() const
{
	return (HealthType)_currentHP;
}

const double Unit::currentEnergy() const
{
	return (double)_currentEnergy;
}

const double Unit::maxEnergy() const
{
	return (double)_unitType.maxEnergy();
}

const Position& Unit::getPrevActionTargetPosition() const
{
	return _previousActionTargetPosition;
}

void Unit::setPreviousActionTargetPosition(Position position)
{
	_previousActionTargetPosition = position;
}

void Unit::updateEnergy(const double amount)
{
	_currentEnergy += amount;
}

const TimeType Unit::getWaitFramesCount() const
{
	return _waitFrames;
}

const double Unit::getBasicEnergyRegenerationRate() const
{
	return _energyRegenerationRate;
}
const int Unit::typeID() const	
{ 
    return _unitType.getID(); 
}

const double Unit::speed() const 
{ 
    return _unitType.topSpeed() * _moveSpeedMultiplier; 
}

const BWAPI::UnitType Unit::type() const 
{ 
    return _unitType; 
}

std::deque<Position>& Unit::getPath()
{
	return _path;
}

void Unit::pathPop()
{
	_path.pop_front();
}

const int Unit::dimUp() const
{
	return _unitType.dimensionUp();
}

const int Unit::dimBot() const
{
	return _unitType.dimensionDown();
}

const int Unit::dimRight() const
{
	return _unitType.dimensionRight();
}

const int Unit::dimLeft() const
{
	return _unitType.dimensionLeft();
}

const int Unit::width() const
{
	return _unitType.width();
}

const int Unit::height() const
{
	return _unitType.height();
}

const Action & Unit::previousAction() const 
{ 
    return _previousAction; 
}

const BWAPI::UnitSizeType Unit::getSize() const
{
    return _unitType.size();
}

const Position Unit::topLeft() const
{
	return Position(topLeftX(), topLeftY());
}

const int Unit::topLeftY() const
{
	return _position.y() - _unitType.dimensionUp();
}

const int Unit::topLeftX() const
{
	return _position.x() - _unitType.dimensionLeft();
}

const Position Unit::topRight() const
{
	return Position(topRightX(), topRightY());
}
const int Unit::topRightY() const
{
	return _position.y() - _unitType.dimensionUp();
}

const int Unit::topRightX() const
{
	return _position.x() + _unitType.dimensionRight() +1;
}

const Position Unit::botLeft() const
{
	return Position(botLeftX(), botLeftY());
}
const int Unit::botLeftY() const
{
	return _position.y() + _unitType.dimensionDown()+1;
}

const int Unit::botLeftX() const
{
	return _position.x() - _unitType.dimensionLeft();
}

const Position Unit::botRight() const
{
	return Position(botRightX(), botRightY());
}

const int Unit::botRightY() const
{
	return _position.y() + _unitType.dimensionDown()+1;
}

const int Unit::botRightX() const
{
	return _position.x() + _unitType.dimensionRight()+1;
}

const PlayerWeapon Unit::getWeapon(const Unit & target) const
{
    return PlayerWeapon(&PlayerProperties::Get(player()), target.type().isFlyer() ? _unitType.airWeapon() : _unitType.groundWeapon());
}

const HealthType Unit::getArmor() const
{
    return UnitProperties::Get(type()).GetArmor(PlayerProperties::Get(player())); 
}

const BWAPI::WeaponType Unit::getWeapon(BWAPI::UnitType target) const
{
    return target.isFlyer() ? _unitType.airWeapon() : _unitType.groundWeapon();
}

const std::string Unit::name() const 
{ 
    std::string n(_unitType.getName());
    std::replace(n.begin(), n.end(), ' ', '_');
    return n;
}

// calculates the hash of this unit based on a given game time
const HashType Unit::calculateHash(const size_t & hashNum, const TimeType & gameTime) const
{
	Position currentPos = position();

    return	  Hash::values[hashNum].positionHash(_playerID, currentPos.x(), currentPos.y()) 
            ^ Hash::values[hashNum].getAttackHash(_playerID, nextAttackActionTime() - gameTime) 
            ^ Hash::values[hashNum].getMoveHash(_playerID, nextMoveActionTime() - gameTime)
            ^ Hash::values[hashNum].getCurrentHPHash(_playerID, currentHP())
            ^ Hash::values[hashNum].getUnitTypeHash(_playerID, typeID());
}

// calculates the hash of this unit based on a given game time, and prints debug info
void Unit::debugHash(const size_t & hashNum, const TimeType & gameTime) const
{
    std::cout << " Pos   " << Hash::values[hashNum].positionHash(_playerID, position().x(), position().y());
    std::cout << " Att   " << Hash::values[hashNum].getAttackHash(_playerID, nextAttackActionTime() - gameTime);
    std::cout << " Mov   " << Hash::values[hashNum].getMoveHash(_playerID, nextMoveActionTime() - gameTime);
    std::cout << " HP    " << Hash::values[hashNum].getCurrentHPHash(_playerID, currentHP());
    std::cout << " Typ   " << Hash::values[hashNum].getUnitTypeHash(_playerID, typeID()) << "\n";;

    HashType hash = Hash::values[hashNum].positionHash(_playerID, position().x(), position().y()); std::cout << hash << "\n";
    hash ^= Hash::values[hashNum].getAttackHash(_playerID, nextAttackActionTime() - gameTime) ; std::cout << hash << "\n";
    hash ^= Hash::values[hashNum].getMoveHash(_playerID, nextMoveActionTime() - gameTime); std::cout << hash << "\n";
    hash ^= Hash::values[hashNum].getCurrentHPHash(_playerID, currentHP()); std::cout << hash << "\n";
    hash ^= Hash::values[hashNum].getUnitTypeHash(_playerID, typeID()); std::cout << hash << "\n";
}

const std::string Unit::debugString() const
{
    std::stringstream ss;

    ss << "Unit Type:           " << type().getName()                               << "\n";
    ss << "Unit ID:             " << (int)ID()                                      << "\n";
    ss << "Player:              " << (int)player()                                  << "\n";
    ss << "Range:               " << range()                                        << "\n";
    ss << "Position:            " << "(" << _position.x() << "," << _position.y()   << ")\n";
    ss << "Current HP:          " << currentHP()                                    << "\n";
    ss << "Next Move Time:      " << nextMoveActionTime()                           << "\n";
    ss << "Next Attack Time:    " << nextAttackActionTime()                         << "\n";
    ss << "Previous Action:     " << previousAction().debugString()                 << "\n";
    ss << "Previous Pos Time:   " << _prevCurrentPosTime                            << "\n";
    ss << "Previous Pos:        " << "(" << _prevCurrentPos.x() << "," << _prevCurrentPos.y()   << ")\n";

    return ss.str();
}