#include "BurrowAbility.h"
#include "Unit.h"

using namespace SparCraft;

BurrowAbility::BurrowAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
	BurrowState currentBurrowState) :
	StateAbility(castingTime, techTypeID, unit, cooldown)
{
	_abilityType = AbilityTypes::BURROW;
	_currentBorrowState = currentBurrowState;
}

const bool BurrowAbility::isCastableNow(const TimeType& currentTime) const
{
	return _timeCanBeCasted == currentTime;
}

void BurrowAbility::beCasted(TimeType gameTime)
{
	//have to implement invisibility

	if (_currentBorrowState == BurrowState::BURROWED)
	{
		_unit->setAttackActionTime(std::numeric_limits<int>::max());
		_unit->setMoveActionTime(gameTime);
		_currentBorrowState = BurrowState::UNBURROWED;
		_unit->addUnitEffect(EffectFactory::getInstance().getChangeInvisibilityEffect(gameTime, false));
	}
	else if (_currentBorrowState == BurrowState::UNBURROWED)
	{
		/*setting attackActionTime to gameTime is basically a trick.
		Because, after this method call, the times of all actions will be updated based on the ability that was
		casted (burrow in this example) and key thing is to set attackActionTime to something that will
		cause attackActionTimeToupdate properly, and basicall anything with time < gameTime + castingTime
		would work, std::numeric_limits<int>::min() is reasonable aswell*/
		/*same logic is behind setting the move Action Time*/
		_unit->setAttackActionTime(gameTime);
		_unit->setMoveActionTime(std::numeric_limits<int>::max());
		_currentBorrowState = BurrowState::BURROWED;
		_unit->addUnitEffect(EffectFactory::getInstance().getChangeInvisibilityEffect(
			gameTime + _castingTime, true));
	}
}

const BurrowState BurrowAbility::getCurrentBurrowState()
{
	return _currentBorrowState;
}