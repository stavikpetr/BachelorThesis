#pragma once

#include "Common.h"
#include "CollisionTile.h"
#include "Position.hpp"

/*
Our implementation of collision resolving is based on division of the map into smaller blocks --- instances of class 
CollisionTile. These collision tiles are a blocks of size 50x50 and they are used for grouping of units that
are close together. The collisionTiles are being held in a special class --- collisionTilesHolder. This class
provides some nice api over the individual collisionTiles and prevents the pollution of the simple Map class.
GameState holds one such instance of CollisionTilesHolder and it uses them for various tasks --- because 
the local grouping of units may be with advantage used in other cases not only in the process of collision resolving
(for example, they can be used to determine the targets of High Templar's Psionic storm ability).

For the CollisionTiles to be usable, the GameState must updates them after each frame --- it must transfer the units
between collision tiles after a move action and it must take care of dead units --- he must remove them from
collision tiles that hold them.

Whenever the GameState sees that the players chose to perform some move actions, it calls an appropriate method
on the class CollisionResolver --- resolveMoveCollisions(), which will take the move actions
and repair the ones that will collide. This process of repairing is done in two phases; 
first, move actions that cause collisions between moving and standing units are repaired and then, the collisions
between moving units are repaired

API of collision resolver
void resolveMoveCollisions(std::vector<Action>& moveActions, GameState& currentState, CollisionTilesHolder& collisionTilesHolder)
*/

namespace SparCraft
{
	class CollisionTilesHolder
	{
		size_t collisionTilesNumberHorizontal_;
		size_t collisionTilesNumberVertical_;
		std::vector<std::vector<CollisionTile>> collisionTiles_;
		std::vector<std::pair<int, int>> indices_;

	public:
		CollisionTilesHolder();

		int getColTileXNumber(int posX);
		int getColTileYNumber(int posY);
		bool isValidColTileXIndex(int colTileXIndex);
		bool isValidColTileYIndex(int colTileYIndex);
		bool isValidCollisionTilePosition(const Position& pos);
		std::pair<int, int> getColTileNumber(Position pos);		
		CollisionTile& getCollisionTile(int vectorXIndex, int vectorYIndex);

		void getCollisionTileIndices(const Position& topLeft, const Position& botRight, std::vector<std::pair<int, int>>& toFill);
		void addUnitToTiles(Unit* unit);
		void updateCollisionTilesAfterMovement(Unit* unit);
		void removeUnitFromTiles(Unit& deadUnit);
		void resetCollisionTiles();

		void getCollisionTilesIndicesForOrientedRectangle(Position p1,
			Position p2, Position p3, Position p4, std::vector<std::pair<int, int>>& toFill);

		std::vector<Unit*> getUniqueUnitsFromCollisionTiles(Position topLeft, Position botRight);
		std::vector<Unit*> getUniqueUnitsFromCollisionTiles(Position p1, Position p2, Position p3_oppositeP1, Position p4_oppositeP2);
		std::vector<Unit*> getUniqueUnitsFromCollisionTiles(Position center, int radius);
		std::vector<Unit*> getUniqueUnitsFromCollisionTiles(std::vector<std::pair<int,int>>& collisionTileIndices);
	
	};
}