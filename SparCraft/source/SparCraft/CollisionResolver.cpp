#include "CollisionResolver.h"

#include "GameState.h"

using namespace SparCraft;

CollisionResolver::CollisionResolver()
{
	//nothing, the default constructor for collisionTilesHolder is enough
}

void CollisionResolver::resolveMoveCollisions(std::vector<Action>& moveActions,
	GameState& currentState, CollisionTilesHolder& collisionTilesHolder)
{
	/*
	Optimization idea: do i need to keep collision tiles in gameState in general? in moving units collision 
	(which are the hardest) i am already using separate new collision tiles, so maybe i don't need to update
	collision tiles after each movement in game state and whenever i need to deal with collisions i just simply
	insert units into appropriate collision tiles holder (separate for moving to moving and for moving to standing collisions)
	*/
	std::vector<Action> groundMoveActions;
	std::vector<Action> flyerMoveActions;
	std::unordered_set<int> movingUnitsIds;

	for (auto& moveAction : moveActions)
	{
		const Unit* u = currentState.getUnitPtrRaw(moveAction.player(), moveAction.unit());
		if (u->type().isFlyer())
			flyerMoveActions.push_back(moveAction);
		else
		{
			movingUnitsIds.insert(u->ID());
			groundMoveActions.push_back(moveAction);
		}
	}

	//-----------
	//	MOVING UNIT COLLISION WITH STANDING UNITS
	//-----------
	resolveMovingToStandingUnitsCollisions(groundMoveActions,
		currentState, collisionTilesHolder, movingUnitsIds);

	//-----------
	//	COLLISIONS BETWEEN MOVING UNITS
	//-----------
	insertMovingUnitsIntoCollisionTiles(currentState, groundMoveActions);
	resolveMovingToMovingUnitsCollisions(groundMoveActions, currentState);

	//now reset things like collisionTilesHolder
	reset();

	//now let's merge flyer and ground move actions, they were both taken care of
	moveActions.clear();

	for (auto& a : flyerMoveActions)
		moveActions.push_back(a);
	for (auto& a : groundMoveActions)
		moveActions.push_back(a);
		
}

void CollisionResolver::possiblyChangeMoveTypePosition(const Position& beforeMovePos,
	const Position& currentPosition, Unit* u, Action& moveAction)
{
	if (beforeMovePos.x() == currentPosition.x() &&
		beforeMovePos.y() == currentPosition.y())
	{
		if (u->getWaitFramesCount() < Constants::General::waitFrameMaxCount)
			moveAction.setNewType(ActionTypes::WAITONEFRAME);
		else
			moveAction.setNewType(ActionTypes::STUCK);
	}
}

void CollisionResolver::resolveMovingToStandingUnitsCollisions(std::vector<Action>& groundMoveActions, 
	GameState& currentState, CollisionTilesHolder& collisionTilesHolder,
	std::unordered_set<int>& movingUnitsIds)
{
	//it's important to realize that only one iteration is enough to handle this (if done a bit cleverly like below)
	int oppositeDir[2];
	for (auto& moveAction : groundMoveActions)
	{
		Unit* unit = currentState.getUnitPtrRaw(moveAction.player(), moveAction.unit());
		Position aimedPosition(moveAction.pos());
		Position aimedTopLeft(aimedPosition.x() - unit->dimLeft(), aimedPosition.y() - unit->dimUp());
		Position aimedBotRight(aimedPosition.x() + unit->dimRight() + 1, aimedPosition.y() + unit->dimBot() + 1);

		collisionTilesHolder.getCollisionTileIndices(aimedTopLeft, aimedBotRight, colTileIndices_);

		int overlap = 0;
		for (auto& p : colTileIndices_)
		{
			auto & unitsInTile = collisionTilesHolder.getCollisionTile(p.first, p.second).getUnits();

			for (auto& colTileUnit : unitsInTile)
			{
				if (colTileUnit->ID() == unit->ID())
					continue;
				if (movingUnitsIds.find(colTileUnit->ID()) != movingUnitsIds.end())
					continue;
				if (CollisionHelperFunctions::areAlignedCollisionBoxesColliding(
					aimedTopLeft.x(), aimedTopLeft.y(), unit->width(), unit->height(),
					colTileUnit->topLeftX(), colTileUnit->topLeftY(),
					colTileUnit->width(), colTileUnit->height()))
				{
					int newOverlap = getOverlapBasedOnDirection(
						aimedTopLeft, aimedBotRight,
						colTileUnit->topLeft(), colTileUnit->botRight(), moveAction.getDir());
					if (newOverlap > overlap)
						overlap = newOverlap;
				}
			}
		}
		//we've determined the maximum overlap with standing units, i need to move this
		//unit in opposite direction by this overlap
		getOppositeDirection(moveAction.getDir(), oppositeDir);
		Position actualPos(aimedPosition.x() + overlap*oppositeDir[0],
			aimedPosition.y() + overlap * oppositeDir[1]);
		possiblyChangeMoveTypePosition(unit->position(), actualPos, unit, moveAction);
		//this will work in every case
		moveAction.setNewPosition(actualPos);
	}
}


void CollisionResolver::resolveMovingToMovingUnitsCollisions(std::vector<Action>& groundMoveActions,
	GameState& currentState)
{
	//this structure will help me a great deal in upcoming loop
	std::unordered_map<int, Action> idActionMap;
	for (auto& moveAction : groundMoveActions)
	{
		Unit* u = currentState.getUnitPtrRaw(moveAction.player(), moveAction.unit());
		idActionMap.insert(std::pair<int, Action>(u->ID(), moveAction));
	}

	//there is unofortunately no way around this ugly loop
	//nice thing is that whenever we deal with collision between two moving units (collision must first occur)
	//then i don't need to handle these two units again (with one exception when the direction is the same...)
	std::unordered_set<IntTuple> handledIdTuples;
	int oppositeDir[2];
	while (true)
	{
		bool collision = false;
		for (auto& moveActionPair : idActionMap)
		{
			if (moveActionPair.second.type() != ActionTypes::MOVE)
				continue;

			Unit* unit = currentState.getUnitPtrRaw(moveActionPair.second.player(), moveActionPair.second.unit());
			//for debug
			auto dimL = unit->dimLeft();
			auto dimR = unit->dimRight();
			auto dimU = unit->dimUp();
			auto dimB = unit->dimBot();
			Position aimedPosition(moveActionPair.second.pos());
			Position aimedTopLeft(aimedPosition.x() - unit->dimLeft(), aimedPosition.y() - unit->dimUp());
			Position aimedBotRight(aimedPosition.x() + unit->dimRight() + 1, aimedPosition.y() + unit->dimBot() + 1);
			
			
			movingUnitsCollisionTiles_.getCollisionTileIndices(aimedTopLeft, aimedBotRight, colTileIndices_);


			for (auto& p : colTileIndices_)
			{
				auto & units = movingUnitsCollisionTiles_.getCollisionTile(p.first, p.second).getUnits();

				for (size_t i(0); i < units.size(); ++i)
				{
					Unit* colTileUnit = units[i];
					if (colTileUnit->ID() == unit->ID())
						continue;
					//if we've already processed these two moving units, we don't need to perform the whole check again
					if (handledIdTuples.find(IntTuple(unit->ID(), colTileUnit->ID())) != handledIdTuples.end()
						|| handledIdTuples.find(IntTuple(colTileUnit->ID(), unit->ID())) != handledIdTuples.end())
						continue;

					auto& colTileUnitMoveAction = idActionMap[colTileUnit->ID()];
					Position aimedColTileUnitPosition(colTileUnitMoveAction.pos());


					Position aimedColTileUnitTopLeft(aimedColTileUnitPosition.x() - colTileUnit->dimLeft(), aimedColTileUnitPosition.y() - colTileUnit->dimUp());
					Position aimedColTileUnitBotRight(aimedColTileUnitPosition.x() + colTileUnit->dimRight() + 1,
						aimedColTileUnitPosition.y() + colTileUnit->dimBot() + 1);

					if (CollisionHelperFunctions::areAlignedCollisionBoxesColliding(
						aimedTopLeft.x(), aimedTopLeft.y(), unit->width(), unit->height(),
						aimedColTileUnitTopLeft.x(), aimedColTileUnitTopLeft.y(), colTileUnit->width(), colTileUnit->height()))
					{
						//for debug
						int id1 = unit->ID();
						int id2 = colTileUnit->ID();
						//if collision occured i need to now determine which unit caused the collision
						//(or more precisely, which unit bumped to the other)
						//options are of course a) first, b) second, c) both
						int firstScenarioCollision(0);
						int secondScenarioCollision(0);
						collision = true;

						//first scenario: "unit" is standing and colTileUnit is Moving
						if (CollisionHelperFunctions::areAlignedCollisionBoxesColliding(
							unit->topLeftX(), unit->topLeftY(), unit->width(), unit->height(),
							aimedColTileUnitTopLeft.x(), aimedColTileUnitTopLeft.y(), colTileUnit->width(), colTileUnit->height()))
							firstScenarioCollision = 1;

						//second scenario: unit is moving and colTileUnit is standing
						if (CollisionHelperFunctions::areAlignedCollisionBoxesColliding(
							aimedTopLeft.x(), aimedTopLeft.y(), unit->width(), unit->height(),
							colTileUnit->topLeftX(), colTileUnit->topLeftY(), colTileUnit->width(), colTileUnit->height()))
							secondScenarioCollision = 1;

						//based on these values i can now determine the way to handle these collisions

						/*if 0 0 then solution is easy, let's say that colTileUnit will move and unit will move as much
						as possible (that is, just determine the overlap)
						after this, i FOR SURE know these units won't collide ever again, so i can insert the tuple into
						handled moving units
						*/

						if (firstScenarioCollision == 0 && secondScenarioCollision == 0)
						{
							int overlap = getOverlapBasedOnDirection(aimedTopLeft, aimedBotRight,
								aimedColTileUnitTopLeft, aimedColTileUnitBotRight, moveActionPair.second.getDir());
							getOppositeDirection(moveActionPair.second.getDir(), oppositeDir);
							Position newPosition(aimedPosition.x() + overlap*oppositeDir[0],
								aimedPosition.y() + overlap *oppositeDir[1]);
							moveActionPair.second.setNewPosition(newPosition);

							//check if the newPosition is same as the current unit position, if yes then
							//set new actionType to wait or stuck
							//but these are handled
							possiblyChangeMoveTypePosition(unit->position(), newPosition, unit, moveActionPair.second);
							handledIdTuples.insert(IntTuple(unit->ID(), colTileUnit->ID()));

							//either way, be sure to fix tiles!!						
							removeSingleUnitFromTiles(unit, aimedPosition);
							insertSignleUnitIntoTiles(unit, newPosition);
						}

						/*if 1 1 then i need to be careful, because let's say that unit A will move by 10 and unit B by
						1, i can't push unit B by overlap let's say 5, so, i will start by determining overlap, then, i
						will move one unit as much as i can and if that is not enough, i will move the second one
						now again, i can insert the tuple into handled moving units*/

						/*if 1 1 then one unit must stand where it is and the other will get
						pushed back by overlap, anyway, it holds that these units are handled*/
						else if (firstScenarioCollision == 1 && secondScenarioCollision == 1)
						{
							//let's say that the colTileUnit will stand
							colTileUnitMoveAction.setNewPosition(colTileUnit->position());
							//set wait or stuck
							possiblyChangeMoveTypePosition(colTileUnit->position(), colTileUnit->position(), colTileUnit, colTileUnitMoveAction);

							//fix tiles for colTileUnit

							int overlap = getOverlapBasedOnDirection(aimedTopLeft, aimedBotRight,
								colTileUnit->topLeft(), colTileUnit->botRight(), moveActionPair.second.getDir());
							getOppositeDirection(moveActionPair.second.getDir(), oppositeDir);
							Position newPosition(aimedPosition.x() + overlap * oppositeDir[0],
								aimedPosition.y() + overlap * oppositeDir[1]);
							moveActionPair.second.setNewPosition(newPosition);

							//check if the newPosition is same as the current unit position, if yes then
							//set new actionType to wait or stuck
							//fix colTiles for this unit
							//these are handled
							possiblyChangeMoveTypePosition(unit->position(), newPosition, unit, moveActionPair.second);
							handledIdTuples.insert(IntTuple(unit->ID(), colTileUnit->ID()));
							//the fix must be done AFTER THE PREVIOUS COMMANDS

							removeSingleUnitFromTiles(colTileUnit, aimedColTileUnitPosition);
							removeSingleUnitFromTiles(unit, aimedPosition);
							insertSignleUnitIntoTiles(colTileUnit, colTileUnit->position());
							insertSignleUnitIntoTiles(unit, newPosition);
						}
						/*1 0 and 0 1 cases are similiar, if 1 0 then i will determine overlap by second unit and i will push
						the second unti by overlap

						stuone would think that i can always insert these tuples into handled moving units, that is not true however
						because in the case where the directions of movement are SAME, i can't do that, because i don't know if
						the unit that i bumped into can't be pushed even more to the back by some other units

						however, if the directions are perpendicular, these units won't collide again
						*/

						else if (firstScenarioCollision == 0 && secondScenarioCollision == 1)
						{
							//when the unit was standing, no collision occured, but when it was moving, collision did occur
							//that means that i want overlap with unit moving in that direction and then i will push it back
							int overlap = getOverlapBasedOnDirection(aimedTopLeft, aimedBotRight,
								aimedColTileUnitTopLeft, aimedColTileUnitBotRight, moveActionPair.second.getDir());
							getOppositeDirection(moveActionPair.second.getDir(), oppositeDir);
							Position newPosition(aimedPosition.x() + overlap * oppositeDir[0],
								aimedPosition.y() + overlap * oppositeDir[1]);
							moveActionPair.second.setNewPosition(newPosition);

							possiblyChangeMoveTypePosition(unit->position(), newPosition, unit, moveActionPair.second);
							//if the directions are same then don't add this tuple to handled ones...
							if (moveActionPair.second.getDir() != colTileUnitMoveAction.getDir())
								handledIdTuples.insert(IntTuple(unit->ID(), colTileUnit->ID()));

							//Don't forget, the fix must be always the last one...
							
							removeSingleUnitFromTiles(unit, aimedPosition);
							insertSignleUnitIntoTiles(unit, newPosition);

							//check if the newPosition is same as the current unit position, if yes then
							//set new actionType to wait or stuck
							//fix colTiles for this unit

						}
						else if (firstScenarioCollision == 1 && secondScenarioCollision == 0)
						{
							//when the unit was moving, no collisions occured, but when it was standing, collision did occur
							int overlap = getOverlapBasedOnDirection(aimedColTileUnitTopLeft, aimedColTileUnitBotRight,
								aimedTopLeft, aimedBotRight, colTileUnitMoveAction.getDir());
							getOppositeDirection(colTileUnitMoveAction.getDir(), oppositeDir);
							Position newPosition(aimedColTileUnitPosition.x() + overlap * oppositeDir[0],
								aimedColTileUnitPosition.y() + overlap * oppositeDir[1]);
							colTileUnitMoveAction.setNewPosition(newPosition);

							possiblyChangeMoveTypePosition(colTileUnit->position(), newPosition, colTileUnit, colTileUnitMoveAction);
							//if the directions are same then don't add this tuple to handled ones...
							if (moveActionPair.second.getDir() != colTileUnitMoveAction.getDir())
								handledIdTuples.insert(IntTuple(unit->ID(), colTileUnit->ID()));

							
							removeSingleUnitFromTiles(colTileUnit, aimedColTileUnitPosition);
							insertSignleUnitIntoTiles(colTileUnit, newPosition);

							//check if the newPosition is same as the current unit position, if yes then
							//set new actionType to wait or stuck
							//fix colTiles for this unit

						}

					}
					if (collision)
						break;

				}
				if (collision)
					break;
			}
			if (collision)
				break;

		}
		if (!collision)
			break;
	}

	groundMoveActions.clear();
	for (auto& p : idActionMap)
	{
		groundMoveActions.push_back(p.second);
	}
}

void CollisionResolver::insertMovingUnitsIntoCollisionTiles(GameState& currentState,
	std::vector<Action>& unresolvedMoveActions)
{
	for (auto& moveAction : unresolvedMoveActions)
	{
		Unit* unit = currentState.getUnitPtrRaw(moveAction.player(), moveAction.unit());
		Position aimedPos(moveAction.pos());
		insertSignleUnitIntoTiles(unit, aimedPos);
	}
}

void CollisionResolver::insertSignleUnitIntoTiles(Unit* unit, const Position& location)
{
	Position aimedTopLeft(location.x() - unit->dimLeft(), location.y() - unit->dimUp());
	Position aimedBotRight(location.x() + unit->dimRight() + 1, location.y() + unit->dimBot() + 1);
	movingUnitsCollisionTiles_.getCollisionTileIndices(aimedTopLeft, aimedBotRight, colTileIndices_);

	for (auto& p : colTileIndices_)
	{
		auto& colTile = movingUnitsCollisionTiles_.getCollisionTile(p.first, p.second);
		colTile.addUnit(unit);
	}

}

void CollisionResolver::removeSingleUnitFromTiles(Unit* unit, const Position& location)
{
	Position aimedTopLeft(location.x() - unit->dimLeft(), location.y() - unit->dimUp());
	Position aimedBotRight(location.x() + unit->dimRight() + 1, location.y() + unit->dimBot() + 1);
	movingUnitsCollisionTiles_.getCollisionTileIndices(aimedTopLeft, aimedBotRight, colTileIndices_);
	for (auto& p : colTileIndices_)
	{
		auto& colTile = movingUnitsCollisionTiles_.getCollisionTile(p.first, p.second);
		colTile.removeUnit(unit->ID());
	}
}

void CollisionResolver::reset()
{
	movingUnitsCollisionTiles_.resetCollisionTiles();
}


void CollisionResolver::getOppositeDirection(const int currentDirection[2], int(&oppositeDir)[2])
{
	oppositeDir[0] = -1 * currentDirection[0];
	oppositeDir[1] = -1 * currentDirection[1];
}

int CollisionResolver::getOverlapBasedOnDirection(const Position& topLeftMoving, const Position& botRightMoving,
	const Position& topLeftStanding, const Position& botRightStanding, const int direction[2])
{
	//moving to the right
	if (direction[0] == 1)
	{
		return botRightMoving.x() - topLeftStanding.x();
	}
	//moving left
	if (direction[0] == -1)
	{
		return botRightStanding.x() - topLeftMoving.x();
	}
	//remember, this is moving DOWN
	if (direction[1] == 1)
	{
		return botRightMoving.y() - topLeftStanding.y();
	}

	if (direction[1] == -1)
	{
		return botRightStanding.y() - topLeftMoving.y();
	}
	return 0;
}