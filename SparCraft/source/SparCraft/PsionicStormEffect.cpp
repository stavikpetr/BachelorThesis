#include "PsionicStormEffect.h"
#include "GameState.h"

using namespace SparCraft;

PsionicStormEffect::PsionicStormEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval, int radius, int damage,
	Position position, TimeType damageTickInterval) :
StateEffect(startingTime, lastingTime, tickInterval)
{
	_effectType = EffectTypes::EffectType::PSIONICSTORM;
	_radius = radius;
	_damage = damage;
	_p = position;
	_damageTickInterval = damageTickInterval;
}

int PsionicStormEffect::getRangeLow()
{
	return 0;
}

int PsionicStormEffect::getRangeHigh()
{
	return _radius;
}

Position PsionicStormEffect::getPosition()
{
	return _p;
}

int PsionicStormEffect::getRadius()
{
	return _radius;
}


void PsionicStormEffect::AffectUnit(Unit* unitToAffect, TimeType& currentGameTime)
{
	if (!unitToAffect->containsStateEffect(_effectType))
	{
		unitToAffect->addStateEffect(_effectType);
		if ((currentGameTime - _startingTime) % _damageTickInterval == 0)
		{
			unitToAffect->takeDamage(_damage);
		}		
	}

}

std::vector<Unit*> PsionicStormEffect::getUnitsToAffect(GameState& state)
{
	std::vector<Unit*> unitsToAffect;

	//i couldn't find info about how precisely is storm modeled, i will assume that it works the same way as
	//emp shockwave effect, so that it affects entities in 96x96 pixel matrix, radius is value from the middle to edge, so 48
	Position topLeft(_p.x() - _radius, _p.y() - _radius);
	Position botRight(_p.x() + _radius + 1, _p.y() + _radius + 1);
	int width = _radius * 2 + 1;
	int height = _radius * 2 + 1;
	//if id of unit != castedByUnitID... 
	std::vector<Unit*> possibleAffectedUnits = state.getCollisionTilesHolder().getUniqueUnitsFromCollisionTiles(topLeft, botRight);
	for (auto& unit : possibleAffectedUnits)
	{
		if (CollisionHelperFunctions::areAlignedCollisionBoxesColliding(
			unit->topLeftX(), unit->topLeftY(), unit->width(), unit->height(),
			topLeft.x(), topLeft.y(), width, height))
			unitsToAffect.push_back(unit);
	}

	return unitsToAffect;
}