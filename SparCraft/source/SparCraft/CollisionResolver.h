#pragma once

#include "Common.h"
#include "CollisionTilesHolder.h"
#include "CollisionHelperFunctions.h"

namespace SparCraft
{
	class GameState;
	class Action;
	class Position;

	class CollisionResolver
	{
		CollisionTilesHolder movingUnitsCollisionTiles_;
		std::vector<std::pair<int, int>> colTileIndices_;

		void resolveMovingToStandingUnitsCollisions(std::vector<Action>& groundMoveActions,
			GameState& currentState, CollisionTilesHolder& collisionTilesHolder,
			std::unordered_set<int>& movingUnitsIds);
		void resolveMovingToMovingUnitsCollisions(std::vector<Action>& groundMoveActions,
			GameState& currentState);

		void insertMovingUnitsIntoCollisionTiles(GameState& currentState,
			std::vector<Action>& unresolvedMoveActions);

		void insertSignleUnitIntoTiles(Unit* unit, const Position& location);
		void removeSingleUnitFromTiles(Unit* unit, const Position& location);
		void possiblyChangeMoveTypePosition(const Position& beforeMovePos,
			const Position& currentPosition, Unit* u, Action& moveAction);
		int getOverlapBasedOnDirection(const Position& topLeftMoving, const Position& botRightMoving,
			const Position& topLeftStanding, const Position& botRightStanding, const int direction[2]);
		void getOppositeDirection(const int currentDirection[2], int (&oppositeDir)[2] );
		void reset();

	public:
		CollisionResolver();
		//the core method of collision resolving, the first argument is a list 
		//of possibly colliding move actions that should be repaired
		void resolveMoveCollisions(std::vector<Action>& moveActions, GameState& currentState, CollisionTilesHolder& collisionTilesHolder);
				
	};

	class IntTuple
	{
	public:
		int first_;
		int second_;
		IntTuple(int first, int second) :first_(first),
			second_(second){};
		size_t hash(const IntTuple& t) const
		{
			return t.first_ * 10000 + t.second_;
		};

		const bool operator == (const IntTuple& rhs) const
		{
			return first_ == rhs.first_ && second_ == rhs.second_;
		};
	};
}

namespace std{
	template<>
	struct hash<SparCraft::IntTuple>{
		size_t operator () (const SparCraft::IntTuple& t) const { return t.hash(t); }
	};
}