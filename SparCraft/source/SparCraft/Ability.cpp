#include "Ability.h"

using namespace SparCraft;

Ability::Ability(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown) :
_unit(unit), _castingTime(castingTime), _cooldown(cooldown), _techTypeID(techTypeID),
_timeCanBeCasted(0)
{;
	_resourceCost = -1;
}

const int Ability::getId() const
{
	return _techTypeID;
}

const int Ability::getCastingTime() const
{
	return _castingTime;
}

const Unit* Ability::getUnit() const
{
	return _unit;
}

const BWAPI::TechType& Ability::getOriginalType()
{
	return _techTypeID;
}

const AbilityTargetTypes::AbilityTargetType Ability::getAbilityTargetType() const
{
	return _abilityTargetType;
}

const int Ability::getCooldown() const
{
	return _cooldown;
}

const int Ability::getResourceCost() const
{
	return _resourceCost;
}

const AbilityTypes::AbilityType Ability::getAbilityType() const
{
	return _abilityType;
}

const TimeType Ability::nextAbilityCastTime() const
{
	return _timeCanBeCasted;
}

void Ability::updateAbilityCastTime(const TimeType& newTime)
{
	_timeCanBeCasted = newTime;
}
