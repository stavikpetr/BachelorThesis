#include "AbilityFactory.h"
#include "Unit.h"

using namespace SparCraft;

namespace SparCraft
{
	namespace Constants
	{
		namespace Abilities
		{
			int burrowCooldown = 15;
			int burrowCastTime = 25;
			bool burrowIsStackable = false;

			int healRange = 80;
			int healCooldown = 10;
			int healCastTime = 2;
			int healResourceCost = 2;
			bool healIsStackable = false;
			int healHealAmount = 3;

			int EMPRange = 250;
			int EMPCooldown = 10;
			int EMPCastTime = 15;
			int EMPResourceCost = 100;
			bool EMPIsStackable = true;

			int ChangeSiegeStateCooldown = 10;
			int ChangeSiegeStateCastTime = 25;
			bool ChangeSiegeStateIsStackable = false;

			int StormRange = 350;
			int StormCooldown = 10;
			int StormCastTime = 15;
			int StormResourceCost = 75;
			bool StormIsStackable = false;

			int StimpackCooldown = 10;
			int StimpackCastTime = 1;
			int StimpackFirebatResourceCost = 20;
			int StimpackMarineResourceCost = 10;
			bool StimpackIsStackable = false;
		}
	}
}

std::unique_ptr<Ability> AbilityFactory::getHealAbility(Unit* u)
{
	return std::make_unique<HealAbility>(
		Constants::Abilities::healCastTime,
		BWAPI::TechTypes::Healing,
		u,
		Constants::Abilities::healCooldown,
		Constants::Abilities::healRange);
}

std::unique_ptr<Ability> AbilityFactory::getPsionicStormAbility(Unit* u)
{
	return std::make_unique<PsionicStorm>(
		Constants::Abilities::StormCastTime,
		BWAPI::TechTypes::Psionic_Storm,
		u,
		Constants::Abilities::StormCooldown,
		Constants::Abilities::StormRange);
}

std::unique_ptr<Ability> AbilityFactory::getChangeSiegeStateAbility(Unit* u, SiegeState curState)
{
	return std::make_unique<ChangeSiegeState>(
		Constants::Abilities::ChangeSiegeStateCastTime,
		BWAPI::TechTypes::Tank_Siege_Mode,
		u,
		Constants::Abilities::ChangeSiegeStateCooldown,
		curState);
}

std::unique_ptr<Ability> AbilityFactory::getStimpackAbility(Unit* u)
{
	return std::make_unique<StimpackAbility>(
		Constants::Abilities::StimpackCastTime,
		BWAPI::TechTypes::Stim_Packs,
		u,
		Constants::Abilities::StimpackCooldown);
}

std::unique_ptr<Ability> AbilityFactory::getEMPShockwaveAbility(Unit* u)
{
	return std::make_unique<EMPShockwaveAbility>(
		Constants::Abilities::EMPCastTime,
		BWAPI::TechTypes::EMP_Shockwave,
		u,
		Constants::Abilities::EMPCooldown,
		Constants::Abilities::EMPRange);
}

std::unique_ptr<Ability> AbilityFactory::getBurrowingAbility(Unit* u, BurrowState curState)
{
	return std::make_unique<BurrowAbility>(
		Constants::Abilities::burrowCastTime,
		BWAPI::TechTypes::Burrowing,
		u,
		Constants::Abilities::burrowCooldown,
		curState);
}


std::unique_ptr<Ability> AbilityFactory::getAbility(
	Unit* u, BWAPI::TechType abilityTechType)
{
	if (abilityTechType == BWAPI::TechTypes::Healing)
	{
		return getHealAbility(u);
	}
	else if (abilityTechType == BWAPI::TechTypes::Psionic_Storm)
	{
		return getPsionicStormAbility(u);
	}
	else if (abilityTechType == BWAPI::TechTypes::Burrowing)
	{
		return getBurrowingAbility(u, BurrowState::UNBURROWED);
	}
	else if (abilityTechType == BWAPI::TechTypes::EMP_Shockwave)
	{
		return getEMPShockwaveAbility(u);
	}
	else if (abilityTechType == BWAPI::TechTypes::Tank_Siege_Mode)
	{
		SiegeState curState = u->type() == BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode ?
			SiegeState::SIEGED : SiegeState::UNSIEGED;
		return getChangeSiegeStateAbility(u, curState);
	}
	else if (abilityTechType == BWAPI::TechTypes::Stim_Packs)
	{
		return getStimpackAbility(u);
	}
	else
	{
		System::FatalError("Ability factory: invalid ability option in getAbility() method");
		return nullptr;
	}
}

std::unique_ptr<Ability> AbilityFactory::getAbility(
	Unit* u, Ability* dependingAbility)
{
	std::unique_ptr<Ability> ability;
	if (dependingAbility->getId() == BWAPI::TechTypes::Tank_Siege_Mode)
	{
		auto casted = dynamic_cast<ChangeSiegeState*>(dependingAbility);
		SiegeState curState = casted->getCurrentState();
		ability = getChangeSiegeStateAbility(u, curState);
	}
	else if (dependingAbility->getId() == BWAPI::TechTypes::Burrowing)
	{
		auto casted = dynamic_cast<BurrowAbility*>(dependingAbility);
		BurrowState curState = casted->getCurrentBurrowState();
		ability = getBurrowingAbility(u, curState);
	}
	else
	{
		ability = getAbility(u, dependingAbility->getId());
	}
	ability->updateAbilityCastTime(dependingAbility->nextAbilityCastTime());
	return std::move(ability);
}

void AbilityFactory::addAbilitiesToUnit(Unit& u)
{
	for (auto& a : u.type().abilities())
	{
		auto& techType = u.type().abilities();
		if (System::isSupportedAbility(a))
		{
			u.addAbility(getAbility(&u, a));
		}
	}
}
