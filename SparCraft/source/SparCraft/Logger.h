#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <memory>

namespace SparCraft
{
	class Action;
	class Unit;
	class GameState;
	class Effect;
	class Projectile;
class Logger 
{
    size_t totalCharsLogged;

	Logger();

public:

	static Logger &	Instance();
	void log(const std::string & logFile, std::string & msg);
	void clearLogFile(const std::string & logFile);
	void logMakeMoves(const int currentTime);
	void logCurrentAction(const Action move, const int player, const Unit& ourUnit, const GameState& state);
	void logNextActionTimes(const Unit& ourUnit);
	void logMessage(const std::string message);
	void logStateEffect(std::shared_ptr<Effect> effect);
	void logUnitEffect(std::shared_ptr<Effect> effect, Unit& affectedUnit);
	void logProjectile(std::shared_ptr<Projectile> projectile);
};
}