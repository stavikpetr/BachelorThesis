#pragma once

#include "Common.h"
#include "Player.h"

namespace SparCraft
{
	class Player_MyScriptedAI_A;
}

/*----------------------------------------------------------------------
| My scripted AI
|----------------------------------------------------------------------
| Smarter AI that uses moving to positions
| this ai was created for the purpose of defending chokepoints evaluation
`----------------------------------------------------------------------*/
class SparCraft::Player_MyScriptedAI_A : public SparCraft::Player
{
	std::unordered_map<int, int> unitStates_;
	std::unordered_map<int, int> additionalVals_;
	std::unordered_map<int, int> stuckCounter_;
public:
	Player_MyScriptedAI_A(const IDType & playerID);
	Player_MyScriptedAI_A(const IDType & playerID, bool followsFormationPolicy, Formation * formation);
	void getMoves(GameState & state, const MoveArray & moves, std::vector<Action> & moveVec);
	IDType getType() { return PlayerModels::MyScriptedAI; }
};