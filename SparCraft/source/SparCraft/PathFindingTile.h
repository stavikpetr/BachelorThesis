#pragma once

#include "Common.h"
#include "Position.hpp"

///The PathFindingTiles are used in the process of path finding with the A* algorithm
///Upon initialization, these tiles mirror the BaseTiles of the Map (the BaseTiles provide only very limited
///functionality like methods providing information about their walkability) --- each 
///PathFindingTile mirrors one BaseTile. However, this functionality is not sufficient for the A* algorithm
///and it would not make sense to flood the interface of BaseTiles with a variety of different methods only for
///the purpose of path finding. For these reasos, I have decided to use a separate class for representation
///of the tiles during PathFinding
///
///These tiles are repeatedly modified and reset with each call to pathfinder. Apart from the information about
///walkability, they provide some information that may be conveniently used in the A* algorithm ---
/// --- the nodes current gcost_, hcost_ and so on...
namespace SparCraft
{
	class PathFindingTile
	{
		bool walkable_;
		//gcost = how far away is that node from starting node
		//hcost = how far away is that node from end node
		//fcost = gcost + hcost
		int gcost_;
		int hcost_;
		//state == 1 ? unexplored
		//state == 0 ? opened
		//state == -1 ? closed
		//heapindex  < 0 ? item is not in the heap
		size_t heapIndex_;	//index in heap
		int state_; //state of node
		PathFindingTile* parentNode_;
		Position p_;
		std::vector<const Unit*> unitVec_;
	public:

		PathFindingTile(int gcost, int hcost, Position p);
		PathFindingTile(Position p, bool walkable);

		const int getgCost() const;
		const int gethCost() const;
		const int getfCost() const;
		const int getState() const;
		const size_t getHeapIndex() const;
		const bool isWalkable() const;
		const bool hasUnits() const;
		//position is in the middle of tile, so (x,y) + (4,4)
		const Position getPosition() const;
		const int xIndex() const;
		const int yIndex() const;

		const std::vector<const Unit*>& getUnits() const;
		PathFindingTile* getParent() const;
		void addUnit(const Unit* unit);
		void setParentNode(PathFindingTile* pathFindingTile);

		void setgCost(const int newCost);
		void sethCost(const int newCost);
		void updateHeapIndex(const int heapIndex);
		void updateState(const int state);
		void updateWalkable(bool walkable);

		void reset();

		friend bool operator==(const PathFindingTile& lh, const PathFindingTile& rh)
		{
			return lh.getPosition().x() == rh.getPosition().x() && lh.getPosition().y() == rh.getPosition().y();
		};
		friend bool operator!=(const PathFindingTile& lh, const PathFindingTile& rh)
		{
			return !(rh == lh);
		};
		friend bool operator>(const PathFindingTile& lh, const PathFindingTile& rh){
			if (lh.getfCost() > rh.getfCost())
				return true;
			if (lh.getfCost() == rh.getfCost())
			{
				if (lh.gethCost() > rh.gethCost())
					return true;
				return false;
			}
			return false;

		};
		friend bool operator<(const PathFindingTile& lh, const PathFindingTile& rh)
		{
			if (lh.getfCost() < rh.getfCost())
				return true;
			if (lh.getfCost() == rh.getfCost())
			{
				if (lh.gethCost() < rh.gethCost())
					return true;
				return false;
			}
			return false;
		};
		friend bool operator<=(const PathFindingTile& lh, const PathFindingTile& rh)
		{
			return !(rh < lh);
		};
		friend bool operator>=(const PathFindingTile& lh, const PathFindingTile& rh)
		{
			return !(lh < rh);
		};
	};
}