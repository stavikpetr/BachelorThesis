#include "CollisionTile.h"
#include "Unit.h"

using namespace SparCraft;

void CollisionTile::addUnit(Unit* unit)
{
	_unitsInTile.push_back(unit);
}

void CollisionTile::removeUnit(IDType unitId)
{
	for (auto iterator = _unitsInTile.begin(); iterator != _unitsInTile.end(); iterator++)
	{
		if ((*iterator)->ID() == unitId)
		{
			iterator = _unitsInTile.erase(iterator);
			break;
		}
	}
}

void CollisionTile::resetTile()
{
	_unitsInTile.clear();
}

const int CollisionTile::getUnitsCount() const
{
	return _unitsInTile.size();
}

const std::vector<Unit*>& CollisionTile::getUnits() const
{
	return _unitsInTile;
}
