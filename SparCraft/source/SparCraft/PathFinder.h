#pragma once

#include "Common.h"
#include "Heap.h"
#include "PathFindingTile.h"
#include "Map.h"

///this class is used in the process of finding path from one location to another
///this algorithm uses a traditional A* algorithm to find the path
///as explained in the PathFindingTile.h, the A* algorithm uses another view 
///over the map in the form of PathFindingTiles; the heuristic cost of tile X that guides
///the A* is just a difference between the x index of tile X and the goal node Z plus
///the difference between y index of tile X and the goal node Z

///the life cycle of this class follows the life cycle of the main SparCraft map,
///whenever a new map is set, a new instance of PathFinder must be initialized,
///as the PathFinder must create and array of PathFindingTiles that will mirror 
///the map's BaseTiles

///the interface of this class is really simple, as it provides only a single method ---
///getPath(const GameState& state, const Unit& unit, Position targetPosition) 
///which serves exactly the purpose of this class - finding a path, this method
///then returns a deque of Positions which guides the unit to its destination 
///(a simple queue does not in our case suffice due to its very limited capabilities)

namespace SparCraft
{
	class Unit;
	class GameState;
	class Position;

	class PathFinder
	{
		std::vector<std::vector<std::unique_ptr<PathFindingTile>>> pathFindingTiles_;
		std::vector<PathFindingTile*> modifiedPathFindingTiles_;

		int numberOfPathFindingTilesVertical_; //mirrors map->getNumberOfWalkTilesVertical
		int numberOfPathFindingTilesHorizontal_; //mirrors map->getNumberOfWalkTilesHorizontal

		void insertUnitsIntoTiles(const Unit& startingUnit, const GameState& state);

		const bool isValidPFTileXIndex(const int xIndex) const;
		const bool isValidPFTileYIndex(const int yIndex) const;
		const bool isValidPFTilePos(const Position& pos) const;
		std::pair<int, int> getPFTileNumber(const Position & pos) const;
		int getPFTileXNumber(const int posX) const;
		int getPFTileYNumber(const int posY) const;

		void findPath(const Unit& startingUnit, Position& targetPosition, std::deque<Position>& pathToFill);
		
		void insertNeighbours(Heap<PathFindingTile>& heap, PathFindingTile* curNode,
			int baseDiffX, int baseDiffY, Position& targetPosition, const Unit& startingUnit);
	
		bool isValidPosition(const Position& position);

		std::pair<Position, Position> getDirectionTopLeftBotRight(Position& unitAimedTopLeft, Position& unitAimedBotRight, 
			const int moveDir[2]);
		void resetModifiedPathFindingTiles();

		PathFindingTile* getPathFindingTile(int posX, int posY);
		void getPathFindingTilesIndices(const Position& topLeft, const Position& botRight, std::vector<std::pair<int, int>>& toFill);
		const Position getActualTargetPosition(const Unit& startingUnit, Position& targetPosition);
		
	public:
		PathFinder(Map* map);
		std::deque<Position> getPath(const GameState& currentState, const Unit& startingUnit, Position targetPosition);
	};
}