#include "CollisionHelperFunctions.h"

namespace SparCraft
{

	namespace CollisionHelperFunctions
	{
		const bool areAlignedCollisionBoxesColliding(
			int topLeftX1, int topLeftY1, int width1, int height1,
			int topLeftX2, int topLeftY2, int width2, int height2)
		{
			if (topLeftX1 + width1 > topLeftX2 &&
				topLeftX1 < topLeftX2 + width2 &&
				topLeftY1 + height1 > topLeftY2 &&
				topLeftY1 < topLeftY2 + height2)
				return true;
			return false;
		}

		const bool alignedRectCircleCollision(
			Position p1, Position p2, Position circleCenter,
			int radius)
		{
			//if the line segment is horizontally aligned...
			if (p1.y() == p2.y())
			{
				if (((p1.x() <= circleCenter.x()) && (circleCenter.x() <= p2.x())) && (abs((p1.y() - circleCenter.y())) <= radius))
					return true;
			}
			if (p1.x() == p2.x())
			{
				if (((p1.y() <= circleCenter.y()) && (circleCenter.y() <= p2.y())) && (abs((p1.x() - circleCenter.x())) <= radius))
					return true;
			}

			if ((pow(p1.x() - circleCenter.x(), 2) + pow(p1.y() - circleCenter.y(), 2)) <= pow(radius, 2))
				return true;
			if ((pow(p2.x() - circleCenter.x(), 2) + pow(p2.y() - circleCenter.y(), 2)) <= pow(radius, 2))
				return true;
			return false;
		}
	}
}