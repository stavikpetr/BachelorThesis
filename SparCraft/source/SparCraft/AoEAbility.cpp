#include "AoEAbility.h"

using namespace SparCraft;

AoEAbility::AoEAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
	int range):
	Ability(castingTime, techTypeID, unit, cooldown)
{
	_range = range;
	_abilityTargetType = AbilityTargetTypes::AbilityTargetType::AOE;
}

const int AoEAbility::getRange() const
{
	return _range;
}

