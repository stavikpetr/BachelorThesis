#include "SplashDamageDealer.h"
#include "GameState.h"
#include "Unit.h"
#include "../SparCraft_experiment/GUIGame.h"

using namespace SparCraft;

namespace SparCraft
{
	namespace Constants
	{
		namespace Units
		{
			int siegeTankSiegedMinRange = 120;
			int siegeTankSplashZone1Radius = 30;
			int siegeTankSplashZone2Radius = 60;
			double siegeTankSplashZone1Multiplier = 0.5;
			double siegeTankSplashZone2Multiplier = 0.25;

			int firebatSplashConeWidth = 30;
			int firebatSplashConeLength = 80;
			int firebatSplashRadialWidth = 60;
			int firebatSplashRadialLength = 20;

			int MutaliskBounceRange = 200;
			double MutaliskSecondHitMultiplier = 1.0 / 3.0;
			double MutaliskThirdHitMultiplier = 1.0 / 9.0;

			int LurkerAttackDistance = 260;
		}
	}
}

double SplashDamageDealer::maximumOfProjectedValues(std::vector<Vector> projectedValues, Position axis)
{
	double maxVal = std::numeric_limits<int>::min();
	auto scaled = getScaledProjectedPositions(projectedValues, axis);
	for (auto& s : scaled)
	{
		if (s > maxVal)
			maxVal = s;
	}
	return maxVal;
}

double SplashDamageDealer::minimumOfProjectedValues(std::vector<Vector> projectedValues, Position axis)
{
	double minVal = std::numeric_limits<int>::max();
	auto scaled = getScaledProjectedPositions(projectedValues, axis);
	for (auto& s : scaled)
	{
		if (s < minVal)
			minVal = s;
	}
	return minVal;
}

std::vector<double> SplashDamageDealer::getScaledProjectedPositions(std::vector<Vector> projectedValues, Position axis)
{
	std::vector<double> scaledProjectedPositions;
	for (auto& v : projectedValues)
	{
		scaledProjectedPositions.push_back(v.x()*axis.x() + v.y() + axis.y());
	}
	return scaledProjectedPositions;
}

std::vector<Vector> SplashDamageDealer::getProjectedPositions(std::array<Position, 4>& positionsToProject, Position axisToProjectOn)
{
	double divisor = pow(axisToProjectOn.x(), 2) + pow(axisToProjectOn.y(), 2);
	std::vector<Vector> projectedPositions;
	for (auto& p : positionsToProject)
	{
		double dividend = p.x()*axisToProjectOn.x() + p.y() * axisToProjectOn.y();
		double val = dividend / divisor;
		projectedPositions.push_back(Vector(val*axisToProjectOn.x(), val*axisToProjectOn.y()));
	}
	return projectedPositions;
}



void SplashDamageDealer::performMutaliskAttack(Unit& ourUnit, Unit& impactUnit,
	GameState& state)
{

	Position hitUnitsPositions[3];
	if (impactUnit.isAlive())
	{
		state.performTakeAttack(ourUnit, impactUnit);
		hitUnitsPositions[0] = impactUnit.position();
	}

	std::vector<IDType> hitUnits;
	hitUnits.push_back(impactUnit.ID());
	std::vector<double> mutaliskMultipliers = std::vector<double>({ Constants::Units::MutaliskSecondHitMultiplier, Constants::Units::MutaliskThirdHitMultiplier });


	int bounceRange = Constants::Units::MutaliskBounceRange;
	double multiplier = 1.0;
	RandomInt randInt(0, std::numeric_limits<int>::max());
	auto playerCopy = ourUnit.player();

	Position bounceUnitPosition = impactUnit.position();


	for (int i = 0; i < 2; i++)
	{
		multiplier *= mutaliskMultipliers[i];
		auto possibleUnits = state.getCollisionTilesHolder().getUniqueUnitsFromCollisionTiles(
			Position(bounceUnitPosition.x() - bounceRange, bounceUnitPosition.y() - bounceRange),
			Position(bounceUnitPosition.x() + bounceRange + 1, bounceUnitPosition.y() + bounceRange + 1));

		possibleUnits.erase(std::remove_if(possibleUnits.begin(), possibleUnits.end(), [&](Unit* u)
		{
			//remove friendly units and units that were already hit...
			return u->player() == playerCopy || (std::find(hitUnits.begin(), hitUnits.end(), u->ID()) != hitUnits.end()) || u->isInvisible();
		}), possibleUnits.end());
		if (possibleUnits.size() == 0)
			break;
		int unitIndex = randInt.nextInt() % possibleUnits.size();
		auto& unitToHit = possibleUnits[unitIndex];

		if (unitToHit->isAlive())
		{
			state.performTakeAttack(ourUnit, *(unitToHit), multiplier);
		}
		else
			break;

		bounceUnitPosition = unitToHit->position();
		hitUnits.push_back(unitToHit->ID());
	}

	//only for drawing...

	GUIGame::drawMutaliskAttack(hitUnitsPositions, ourUnit.player());

}

void SplashDamageDealer::performSiegeTankAttack(Unit& ourUnit, Unit& impactUnit,
	GameState& state)
{
	state.performTakeAttack(ourUnit, impactUnit);
	auto possibleUnits = state.getCollisionTilesHolder().getUniqueUnitsFromCollisionTiles(
		impactUnit.position(), Constants::Units::siegeTankSplashZone2Radius);

	std::vector<double> zoneMultipliers = std::vector<double>({ Constants::Units::siegeTankSplashZone1Multiplier,
		Constants::Units::siegeTankSplashZone2Multiplier });
	std::vector<int> zoneRadii = std::vector<int>({ Constants::Units::siegeTankSplashZone1Radius,
		Constants::Units::siegeTankSplashZone2Radius });

	for (auto& unit : possibleUnits)
	{
		if (unit->ID() == impactUnit.ID())
			continue;
		std::vector<std::tuple<Position, Position>> lineSegments = std::vector<std::tuple<Position, Position>>({
			std::tuple<Position, Position>(unit->topLeft(), unit->topRight()),
			std::tuple<Position, Position>(unit->botLeft(), unit->botRight()),
			std::tuple<Position, Position>(unit->botLeft(), unit->topLeft()),
			std::tuple<Position, Position>(unit->botRight(), unit->botRight())
		});
		for (int i = 0; i < 2; i++)
		{
			int radius = zoneRadii[i];
			double dmgMultiplier = zoneMultipliers[i];
			bool anyCollide = false;
			for (auto& lineSegment : lineSegments)
			{
				if (CollisionHelperFunctions::alignedRectCircleCollision(
					std::get<0>(lineSegment), std::get<1>(lineSegment), impactUnit.position(), radius))
				{
					anyCollide = true;
					break;
				}
			}
			if (anyCollide)
			{
				state.performTakeAttack(ourUnit, *(unit), dmgMultiplier);
				break;
			}
		}
	}

	GUIGame::drawSiegeTankSplasCircle(impactUnit.position(), Constants::Units::siegeTankSplashZone1Radius, Constants::Units::siegeTankSplashZone2Radius);

}

void SplashDamageDealer::performFirebatAttack(Unit& ourUnit, Unit& impactUnit,
	GameState& state)
{
	Position targetPosition;
	Position tmp(impactUnit.position().x() - ourUnit.position().x(), impactUnit.position().y() - ourUnit.position().y());
	int x = impactUnit.position().x() - ourUnit.position().x();
	int y = impactUnit.position().y() - ourUnit.position().y();
	double divisor = sqrt((pow(x, 2) + pow(y, 2)));

	targetPosition = Position(ourUnit.position().x() + (int)(Constants::Units::firebatSplashConeLength * (x / divisor)),
		ourUnit.position().y() +(int) (Constants::Units::firebatSplashConeLength * (y / divisor)));

	Position diff(impactUnit.position().x() - ourUnit.position().x(), impactUnit.position().y() - ourUnit.position().y());
	double val = (Constants::Units::firebatSplashConeWidth / (sqrt((pow(diff.x(), 2) + pow(diff.y(), 2)))));
	Position perp(-1 * (int)(diff.y()* val),
		(int)(diff.x() * val) );

	Position p1 = ourUnit.position();
	Position p2 = targetPosition;
	Position p3 = Position(p1.x() + perp.x(), p1.y() + perp.y());
	Position p4 = Position(targetPosition.x() + perp.x(), targetPosition.y() + perp.y());

	Position p1_ = ourUnit.position();
	Position p2_ = targetPosition;
	Position p3_ = Position(p1.x() - perp.x(), p1.y() - perp.y());
	Position p4_ = Position(targetPosition.x() - perp.x(), targetPosition.y() - perp.y());

	Position tmp2 = Position(p4.x() - p2.x(), p4.y() - p2.y());
	Position p1__ = Position(targetPosition.x() + (int)(Constants::Units::firebatSplashRadialWidth / 2 * (tmp2.x() / (sqrt((pow(tmp2.x(), 2) + pow(tmp2.y(), 2)))))),
		targetPosition.y() + (int)(Constants::Units::firebatSplashRadialWidth / 2 * (tmp2.y() / (sqrt((pow(tmp2.x(), 2) + pow(tmp2.y(), 2)))))));
	tmp2 = Position(tmp2.x() * -1, tmp2.y()*-1);
	Position p2__ = Position(targetPosition.x() + (int)(Constants::Units::firebatSplashRadialWidth / 2 * (tmp2.x() / (sqrt((pow(tmp2.x(), 2) + pow(tmp2.y(), 2)))))),
		targetPosition.y() + (int)(Constants::Units::firebatSplashRadialWidth / 2 * (tmp2.y() / (sqrt((pow(tmp2.x(), 2) + pow(tmp2.y(), 2)))))));

	Position diff2 = Position(p1__.x() - p2__.x(), p1__.y() - p2__.y());
	Position perp2(-1 * (int)(diff2.y()*(Constants::Units::firebatSplashRadialLength / (sqrt((pow(diff2.x(), 2) + pow(diff2.y(), 2)))))),
		(int)(diff2.x() *Constants::Units::firebatSplashRadialLength / (sqrt((pow(diff2.x(), 2) + pow(diff2.y(), 2))))));
	Position p3__ = Position(p1__.x() - perp2.x(), p1__.y() - perp2.y());
	Position p4__ = Position(p2__.x() - perp2.x(), p2__.y() - perp2.y());

	//handling box p1, p2, p3, p4
	Position ax1 = Position(p2.x() - p1.x(), p2.y() - p1.y());
	Position ax2 = Position(p3.x() - p1.x(), p3.y() - p1.y());
	//ax3 is simple horizontal axis
	Position ax3 = Position(impactUnit.topRightX() - impactUnit.topLeftX(), impactUnit.topRightY() - impactUnit.topLeftY());
	//ax4 is simple vertical axis
	Position ax4 = Position(impactUnit.topLeftX() - impactUnit.botLeftX(), impactUnit.topLeftY() - impactUnit.botLeftY());

	//now two axes of the back box
	Position ax5 = Position(p3__.x() - p1__.x(), p3__.y() - p1__.y());
	Position ax6 = Position(p1__.x() - p2__.x(), p1__.y() - p2__.y());

	//FIRST ARE AXES, THEN POSITION OF BOXES;
	std::pair<std::array<Position, 4>, std::array<Position, 4>> boxesAndAxes[3];
	boxesAndAxes[0] = std::pair<std::array<Position, 4>, std::array<Position, 4>>(
	{ { ax1, ax2, ax3, ax4 } },
	{ { p1, p2, p3, p4 } });
	boxesAndAxes[1] = std::pair<std::array<Position, 4>, std::array<Position, 4>>(
	{ { ax1, ax2, ax3, ax4 } },
	{ { p1_, p2_, p3_, p4_ } });
	boxesAndAxes[2] = std::pair<std::array<Position, 4>, std::array<Position, 4>>(
	{ { ax1, ax2, ax5, ax6 } },
	{ { p1__, p2__, p3__, p4__ } });

	std::vector<Unit*> unitsThatWillTakeDamage;

	for (auto& boxAndAxis : boxesAndAxes)
	{
		for (auto& possibleUnit : state.getCollisionTilesHolder().getUniqueUnitsFromCollisionTiles(
			boxAndAxis.second[0], boxAndAxis.second[1], boxAndAxis.second[3], boxAndAxis.second[2]))
		{
			if (possibleUnit->player() == ourUnit.player())
				continue;
			bool didUnitCollide = true;
			for (auto& axis : boxAndAxis.first)
			{
				std::array<Position, 4> u({ { possibleUnit->topLeft(), possibleUnit->topRight(), possibleUnit->botLeft(), possibleUnit->botRight() } });
				auto projectedPositionsA = getProjectedPositions(boxAndAxis.second, axis);
				auto projectedPositionsB = getProjectedPositions(u, axis);

				int minA = (int)minimumOfProjectedValues(projectedPositionsA, axis);
				int maxA = (int)maximumOfProjectedValues(projectedPositionsA, axis);
				int minB = (int)minimumOfProjectedValues(projectedPositionsB, axis);
				int maxB = (int)maximumOfProjectedValues(projectedPositionsB, axis);

				if ((minB <= maxA && minB >= minA) || (maxB <= maxA && maxB >= maxA)
					|| (minA <= maxB && minA >= minB) || (maxA <= maxB && maxA >= minB))
				{
					//we have collision and we need to continue with the other axis
				}
				else
				{
					//they are colliding, we dont need to check anymore
					didUnitCollide = false;
					break;

				}
			}
			if (didUnitCollide)
				unitsThatWillTakeDamage.push_back(possibleUnit);
		}
		for (auto& unit : unitsThatWillTakeDamage)
		{
			state.performTakeAttack(ourUnit, *(unit));
		}
		unitsThatWillTakeDamage.clear();
	}

	GUIGame::drawFirebatRectangles(p1, p2, p3, p4, p1_, p2_, p3_, p4_, p1__, p2__, p3__, p4__);
}

void SplashDamageDealer::performLurkerAttack(Unit& ourUnit, Unit& impactUnit,
	GameState& state)
{
	double x = impactUnit.position().x() - ourUnit.position().x();
	double y = impactUnit.position().y() - ourUnit.position().y();
	double divisor = sqrt((pow(x, 2) + pow(y, 2)));
	Vector b = Vector(x / divisor, y / divisor);

	Position actualStartingPosition = Position(ourUnit.position().x() + (int)(20.0 * b.x()),
		ourUnit.position().y() + (int)(20.0 * b.y()));
	Position actualEndingPosition = Position(ourUnit.position().x() + (int)(Constants::Units::LurkerAttackDistance * b.x()),
		ourUnit.position().y() + (int)(Constants::Units::LurkerAttackDistance * b.y()));
	int dist = (int)Position::getDist(actualStartingPosition,
		actualEndingPosition);

	state.getActiveProjectiles().push_back(std::make_unique<LurkerAttackProjectile>(
		actualStartingPosition, actualEndingPosition,
		Constants::Projectiles::LurkerSpeed, dist, state.getTime(),
		&ourUnit));
}

void SplashDamageDealer::dealSplashDamage(Unit& ourUnit, Unit& impactUnit,
	GameState& state)
{
	if (ourUnit.type() == BWAPI::UnitTypes::Zerg_Mutalisk)
	{
		performMutaliskAttack(ourUnit, impactUnit, state);
	}
	else if (ourUnit.type() == BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode)
	{
		performSiegeTankAttack(ourUnit, impactUnit, state);
	}
	else if (ourUnit.type() == BWAPI::UnitTypes::Terran_Firebat)
	{
		performFirebatAttack(ourUnit, impactUnit, state);
	}
	else if (ourUnit.type() == BWAPI::UnitTypes::Zerg_Lurker)
	{
		performLurkerAttack(ourUnit, impactUnit, state);
	}
}