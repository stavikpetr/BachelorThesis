#include "StimpackEffect.h"
#include "Unit.h"

using namespace SparCraft;

StimpackEffect::StimpackEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval,
	double attackSpeedMultiplier, double moveSpeedMultiplier) :
	Effect(startingTime, lastingTime, tickInterval)
{
	_attackSpeedMultiplier = attackSpeedMultiplier;
	_moveSpeedMultiplier = moveSpeedMultiplier;
	_effectType = EffectTypes::EffectType::STIMPACK;
}

void StimpackEffect::AffectUnit(Unit* unitToAffect, TimeType& currentGameTime)
{
	//in the last tick, marine will have its multipliers reseted
	if (currentGameTime == _lastingTime + _startingTime)
	{
		unitToAffect->changeAttackSpeedMultiplier(1.0);
		unitToAffect->changeMoveSpeedMultiplier(1.0);
	}
	
	else if (currentGameTime == _startingTime)
	{
		unitToAffect->changeAttackSpeedMultiplier(_attackSpeedMultiplier);
		unitToAffect->changeMoveSpeedMultiplier(_moveSpeedMultiplier);
	}
}