#include "EMPShockwaveProjectile.h"
#include "GameState.h"

using namespace SparCraft;

EMPShockwaveProjectile::EMPShockwaveProjectile(Position startingPosition, Position targetPosition,
	double speed, int destinationDistance,
	TimeType nextActionTime, Unit* caster) :
	Projectile(startingPosition, targetPosition, speed, destinationDistance, nextActionTime),
	_caster(caster)
{
	_projectileType = ProjectileTypes::ProjectileType::EMPPROJECTILE;
}

void EMPShockwaveProjectile::nextAction(GameState& state)
{
	Position newPosition(_currentPosition.x() + (int)(_speed * _dirUnitVector.x()), _currentPosition.y() + (int)(_speed* _dirUnitVector.y()));
	
	int distanceTraveled = (int)(Position::getDist(newPosition, _currentPosition));
	_destinationDistance -= distanceTraveled;
	if (_destinationDistance <= 0)
	{
		_currentPosition = _targetPosition;
	}
	else
	{
		_currentPosition = newPosition;
	}

	//let'S add the shockwave explosion effect
	if (_targetPosition.x() == _currentPosition.x() && _targetPosition.y() == _currentPosition.y())
	{
		state.addEffect(EffectFactory::getInstance().getEMPShockwaveEffect(
			state.getTime(),
			_caster->ID(),
			_targetPosition));
	}
}

Unit* EMPShockwaveProjectile::getCaster()
{
	return _caster;
}