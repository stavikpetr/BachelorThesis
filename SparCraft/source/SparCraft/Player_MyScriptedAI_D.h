#pragma once

#include "Common.h"
#include "Player.h"

namespace SparCraft
{
	class Player_MyScriptedAI_D;
}

/*----------------------------------------------------------------------
| My scripted AI
|----------------------------------------------------------------------
| Smarter AI that uses moving to positions
| this ai was created for the purpose of defending chokepoints evaluation
| i needed speacilized AI for defender
`----------------------------------------------------------------------*/
class SparCraft::Player_MyScriptedAI_D : public SparCraft::Player
{
public:
	Player_MyScriptedAI_D(const IDType & playerID);
	Player_MyScriptedAI_D(const IDType & playerID, bool followsFormationPolicy, Formation * formation);
	void getMoves(GameState & state, const MoveArray & moves, std::vector<Action> & moveVec);
	IDType getType() { return PlayerModels::MyScriptedAI; }
};