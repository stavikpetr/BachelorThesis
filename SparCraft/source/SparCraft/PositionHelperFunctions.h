#pragma once

#include "Common.h"
#include "Position.hpp"

namespace SparCraft
{
	namespace PositionHelperFunctions
	{
		Position getPositionBetween(const Position& pos1, const Position& pos2);
		double getDistanceToPosition(const Position& posFrom, const Position& posTo);
	}
}