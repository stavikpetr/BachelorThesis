#include "ChangeInvisibilityEffect.h"
#include "Unit.h"

using namespace SparCraft;

ChangeInvisibilityEffect::ChangeInvisibilityEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval,
	bool newInvisibilityValue):
	Effect(startingTime, lastingTime, tickInterval)
{
	_newInvisibilityValue = newInvisibilityValue;
	_effectType = EffectTypes::EffectType::CHANGEINVISIBILITY;
}

void ChangeInvisibilityEffect::AffectUnit(Unit* unitToAffect, TimeType& currentGameTime)
{
	unitToAffect->changeIsInvisible(_newInvisibilityValue);
}

bool ChangeInvisibilityEffect::getNewInvisValue()
{
	return _newInvisibilityValue;
}