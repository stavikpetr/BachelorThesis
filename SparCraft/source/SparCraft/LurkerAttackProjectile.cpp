#include "LurkerAttackProjectile.h"
#include "GameState.h"

using namespace SparCraft;

LurkerAttackProjectile::LurkerAttackProjectile(Position startingPosition, Position targetPosition,
	double speed, int destinationDistance, TimeType nextActionTime, Unit* caster) : _caster(caster),
	Projectile(startingPosition, targetPosition, speed, destinationDistance, nextActionTime)
{
	_projectileType = ProjectileTypes::ProjectileType::LURKERATTACKPROJECTILE;
	_affectedUnits = std::make_shared<std::unordered_set<IDType>>();
}

void LurkerAttackProjectile::nextAction(GameState& state)
{
	//let's add the attackEffect before the position is updated

	state.addEffect(EffectFactory::getInstance().getLurkerAttackEffect(
		state.getTime(), _affectedUnits, _currentPosition, _dirUnitVector, _caster));

	Position newPosition(_currentPosition.x() + (int)(_speed * _dirUnitVector.x()),
		_currentPosition.y() + (int)(_speed* _dirUnitVector.y()));
	
	int distanceTraveled = (int)Position::getDist(newPosition, _currentPosition);
	_destinationDistance -= distanceTraveled;

	if (_destinationDistance <= 0)
	{
		_currentPosition = _targetPosition;
		state.addEffect(EffectFactory::getInstance().getLurkerAttackEffect(
			state.getTime(), _affectedUnits, _currentPosition,
			_dirUnitVector, _caster));
	}
	else
	{
		_currentPosition = newPosition;

	}
}


Unit* LurkerAttackProjectile::getCaster()
{
	return _caster;
}

//due to the copy ctor of gamestate
void LurkerAttackProjectile::setAffectedUnits(std::shared_ptr<std::unordered_set<IDType>> affectedUnits)
{
	_affectedUnits = affectedUnits;
}

std::shared_ptr<std::unordered_set<IDType>> LurkerAttackProjectile::getAffectedUnits()
{
	return _affectedUnits;
}
