#include "EMPShockwaveEffect.h"
#include "GameState.h"

using namespace SparCraft;

EMPShockwaveEffect::EMPShockwaveEffect(TimeType startingTime, TimeType lastingTime, TimeType tickInterval,
	int radius, int energyDrain, int shieldDrain, Position p, IDType castedByUnitID) :
	StateEffect(startingTime, lastingTime, tickInterval)
{
	_effectType = EffectTypes::EffectType::EMP;
	_radius = radius;
	_energyDrain = energyDrain;
	_shieldDrain = shieldDrain;
	_castedByUnitID = castedByUnitID;
	_p = p;
}

int EMPShockwaveEffect::getRangeLow()
{
	return 0;
}

int EMPShockwaveEffect::getRangeHigh()
{
	return _radius;
}

int EMPShockwaveEffect::getRadius()
{
	return _radius;
}
Position EMPShockwaveEffect::getPosition()
{
	return _p;
}

IDType EMPShockwaveEffect::getCastedByUnitID()
{
	return _castedByUnitID;
};

void EMPShockwaveEffect::AffectUnit(Unit* unitToAffect, TimeType& currentGameTime)
{

	if (unitToAffect->currentEnergy() >= _energyDrain)
	{
		unitToAffect->updateEnergy(-_energyDrain);
	}
	else
	{
		unitToAffect->updateEnergy(-unitToAffect->currentEnergy());
	}
	//shields are not yet implemented...

}

std::vector<Unit*> EMPShockwaveEffect::getUnitsToAffect(GameState& state)
{
	std::vector<Unit*> unitsToAffect;

	//emp effect affects entities in 96x96 matrix, radius is value from the middle to edge, so 48
	Position topLeft(_p.x() - _radius, _p.y() - _radius);
	Position botRight(_p.x() + _radius + 1, _p.y() + _radius + 1);
	int width = _radius * 2 + 1;
	int height = _radius * 2 + 1;
	//if id of unit != castedByUnitID... 
	std::vector<Unit*> possibleAffectedUnits = state.getCollisionTilesHolder().getUniqueUnitsFromCollisionTiles(topLeft, botRight);
	for (auto& unit : possibleAffectedUnits)
	{
		if (unit->ID() == _castedByUnitID)
			continue;
		if (CollisionHelperFunctions::areAlignedCollisionBoxesColliding(
			unit->topLeftX(), unit->topLeftY(), unit->width(), unit->height(),
			topLeft.x(), topLeft.y(), width, height))
			unitsToAffect.push_back(unit);
	}
	return unitsToAffect;
}
