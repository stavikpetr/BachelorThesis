#include "EMPShockwaveAbility.h"
#include "Unit.h"
#include "GameState.h"

using namespace SparCraft;

EMPShockwaveAbility::EMPShockwaveAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
	int range) :
	AoEAbility(castingTime, techTypeID, unit, cooldown,range)
{
	_resourceCost = Constants::Abilities::EMPResourceCost;
	_abilityType = AbilityTypes::EMP;
}

const bool EMPShockwaveAbility::isCastableNow(const TimeType& currentTime) const
{
	if (currentTime != _timeCanBeCasted)
		return false;
	if (_unit->currentEnergy() < _resourceCost)
		return false;
	return true;
}

void EMPShockwaveAbility::beCasted(GameState& state, Position p)
{
	_unit->updateEnergy(-_resourceCost);
	state.addProjectile(ProjectileFactory::getInstance().
		getProjectile(ProjectileTypes::ProjectileType::EMPPROJECTILE,
		_unit->position(), p, (int)(Position::getDist(_unit->position(), p)),
		state.getTime(), _unit));

}