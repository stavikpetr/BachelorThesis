#pragma once

#include "Common.h"

namespace SparCraft
{
	class Unit;
	class Position;

	class CollisionTile
	{
		std::vector<Unit*> _unitsInTile;

	public:
		void addUnit(Unit* unit);
		void removeUnit(IDType unitId);
		void resetTile();

		//part with deciding collisions between moving units
		const std::vector<Unit*>& getUnits() const;
		const int getUnitsCount() const;
	};
}