#pragma once

#include "Common.h"
#include "Position.hpp"
#include "Vector.hpp"
#include "EffectFactory.h"

namespace SparCraft
{
	class Unit;
	class GameState;

	namespace ProjectileTypes{
		enum class ProjectileType { EMPPROJECTILE, LURKERATTACKPROJECTILE };
	}

	class Projectile
	{

	protected:
		Position  _startingPosition;
		Position  _targetPosition;
		Position  _currentPosition;
		TimeType _nextActionTime;
		Vector _dirUnitVector;
		ProjectileTypes::ProjectileType _projectileType;
		int _destinationDistance;
		double _speed;

		Projectile(Position startingPosition, Position targetPosition, double speed, int destinationDistance,
			TimeType nextActionTime);

	public:
		virtual void nextAction(GameState& state) = 0;
		const Position getStartingPosition() const;
		const Position getTargetPosition() const;
		const Position getCurrentPosition() const;
		const double getSpeed() const;
		const ProjectileTypes::ProjectileType getProjectileType() const;
		const TimeType getNextActionTime() const;
		void updateNextActionTime();
	};
}