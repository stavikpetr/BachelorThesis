#include "Logger.h"
#include "Action.h"
#include "Unit.h"
#include "GameState.h"


using namespace SparCraft;

Logger::Logger() 
    : totalCharsLogged(0)
{
	
}

Logger & Logger::Instance() 
{
	static Logger instance;
	return instance;
}

void Logger::clearLogFile(const std::string & logFile)
{

}

void Logger::log(const std::string & logFile, std::string & msg)
{
    std::ofstream logStream;
	logStream.open(logFile.c_str(), std::ofstream::app);
	logStream << msg;
    logStream.flush();
    logStream.close();
}

void Logger::logMakeMoves(const int currentTime)
{
	std::cout << "-----\n";
	std::cout << "----- NEW MAKE MOVES -----" << "\n";
	std::cout << "-----\n";
	std::cout << "current time: " << currentTime << "\n";
}

void Logger::logMessage(const std::string message)
{
	std::cout << message;
}

void Logger::logCurrentAction(const Action move, const int player, const Unit& ourUnit, const GameState& state)
{
	std::cout << "performing action; player: "  << player + 0 << " unitType: " << ourUnit.type().getName() << "\n";
	std::cout << "previuos action time: " << ourUnit.previousActionTime() << "\n";

	if (move.type() == ActionTypes::ATTACK)
	{
		IDType enemyPlayer = state.getEnemy(player);
		const Unit& targetUnit = state.getUnit(enemyPlayer, move.index());
		std::cout << "performing action: Attack\n";
		std::cout << "target position is: " << "x: " << targetUnit.position().x() << " y: " << targetUnit.position().y() << "\n";		
	}
	else if (move.type() == ActionTypes::ATTACKSPLASH)
	{
		IDType enemyPlayer = state.getEnemy(player);
		const Unit& targetUnit = state.getUnit(enemyPlayer, move.index());
		std::cout << "performing action: AttackSplash\n";
		std::cout << "target position is: " << "x: " << targetUnit.position().x() << " y: " << targetUnit.position().y() << "\n";
	}
	else if (move.type() == ActionTypes::CASTABILITY)
	{
		std::cout << "performing action: CastAbility\n";
	}
	else if (move.type() == ActionTypes::MOVE)
	{
		std::cout << "performing action: Move\n";
		std::cout << "my position before move action: x: " << ourUnit.position().x() << " y: " << ourUnit.position().y() << "\n";
		std::cout << "my Position after move action: x: " << move.pos().x() << " y: " << move.pos().y() << "\n";
	}
	else if (move.type() == ActionTypes::ACTIONWAIT || move.type() == ActionTypes::RELOAD)
	{
		std::cout << "peforming action: action wait\n";
	}
	else if (move.type() == ActionTypes::PASS)
	{
		std::cout << "performing action: pass\n";
	}
	else if (move.type() == ActionTypes::STUCK)
	{
		std::cout << "performing action: stuck\n";
	}
	else if (move.type() == ActionTypes::WAITONEFRAME)
	{
		std::cout << "performing action: waiting one frame\n";
	}
	else if (move.type() == ActionTypes::MOVETOPOSITION)
	{
		std::cout << "performing action: move to position\n";
		std::cout << "moving to position: x: " << move.pos().x() << " y: " << move.pos().y() << "\n";
	}

}

void Logger::logStateEffect(std::shared_ptr<Effect> effect)
{
	if (effect->getEffectType() == EffectTypes::EffectType::PSIONICSTORM)
	{
		auto castedToStorm = std::dynamic_pointer_cast<PsionicStormEffect>(effect);
		std::cout << "handling psionic storm effect\n";
		std::cout << "at position x: " << castedToStorm->getPosition().x() << " y: " << castedToStorm->getPosition().y() << "\n";
		std::cout << "starting time was: " << castedToStorm->getStartingTime() << " ending time is: " << castedToStorm->getStartingTime() + castedToStorm->getLastingTime() << "\n";
	}
	else if (effect->getEffectType() == EffectTypes::EffectType::LURKERATTACK)
	{
		auto castedToLurkerAttack = std::dynamic_pointer_cast<LurkerAttackEffect>(effect);
		std::cout << "handling lurker attack effect\n";
		std::cout << "at position x: " << castedToLurkerAttack->getPosition().x() << " y: " << castedToLurkerAttack->getPosition().y() << "\n";
	}
	else if (effect->getEffectType() == EffectTypes::EffectType::EMP)
	{
		auto castedToEmp = std::dynamic_pointer_cast<EMPShockwaveEffect>(effect);
		std::cout << "handling emp effect\n";
		std::cout << "at position x: " << castedToEmp->getPosition().x() << " y: " << castedToEmp->getPosition().y() << "\n";

	}
	std::cout << " ----- \n";
}


void Logger::logUnitEffect(std::shared_ptr<Effect> effect, Unit& affectedUnit)
{
	if (effect->getEffectType() == EffectTypes::EffectType::STIMPACK)
	{
		std::cout << "handling unit effect: stimpack\n";
	}
	else if (effect->getEffectType() == EffectTypes::EffectType::HEAL)
	{
		std::cout << "handling unit effect: heal\n";
	}
	std::cout << "affected unit is: " << affectedUnit.type().getName() << "\n";
	std::cout << "at position: x: " << affectedUnit.position().x() << " y: " << affectedUnit.position().y() << "\n";
	std::cout << "------\n";
}
void Logger::logProjectile(std::shared_ptr<Projectile> projectile)
{
	if (projectile->getProjectileType() == ProjectileTypes::ProjectileType::EMPPROJECTILE)
	{
		std::cout << "handling projectile: emp projectile\n";
	}
	else if (projectile->getProjectileType() == ProjectileTypes::ProjectileType::LURKERATTACKPROJECTILE)
	{
		std::cout << "handling projectile: lurker attack projectile\n";
	}
	std::cout << "current position after moving is: x: " << projectile->getCurrentPosition().x() <<
		" y: " << projectile->getCurrentPosition().y() << "\n";
	std::cout << "------\n";
}

void Logger::logNextActionTimes(const Unit& ourUnit)
{
	std::cout << "next ability cast time: " << ourUnit.nextAbilityCastTime() << "\n";
	std::cout << "next attack action cast time: " << ourUnit.nextAttackActionTime() << "\n";
	std::cout << "next move time: " << ourUnit.nextMoveActionTime() << "\n";
	std::cout << "------\n";
}

