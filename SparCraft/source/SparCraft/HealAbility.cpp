#include "HealAbility.h"
#include "Unit.h"

using namespace SparCraft;

HealAbility::HealAbility(int castingTime, BWAPI::TechType techTypeID, Unit* unit, int cooldown,
	 int range) :
	SingleTargetAbility(castingTime, techTypeID, unit, cooldown, range)
{
	_resourceCost = Constants::Abilities::healResourceCost;
	_abilityType = AbilityTypes::HEALABILITY;
}

const bool HealAbility::isCastableToUnit(const Unit& unit) const
{
	//heal is not stackable, is castable only to friendly bio units and is not castable to self
	
	//first, lets check if this unit doesn't contain effect, if it does, i dont want
	//to generate heal cast move to this unit
	if (unit.containsUnitEffect(EffectTypes::EffectType::HEAL))
		return false;
	
	//castable only to friendly
	if (unit.player() == _unit->player())
	{
		//not castable to self
		if (unit.ID() != _unit->ID())
		{
			//is target unit injured?
			if (unit.currentHP() != unit.maxHP())
			{
				//castable only to bio
				if (unit.isOrganic())
				{
					if (unit.isAlive())
					{
						//now, let's check if the unit is in range..
						//_timeCanBeCasted is 
						auto& p = _unit->position();
						int dist = p.getDistance(unit.position());
						if (dist < _range)
						{
							return true;
						}
					}
				}
			}
		}
	}

	return false;
}

const bool HealAbility::matchesType(const Unit & unit) const
{
	if (unit.player() == _unit->player())
	{
		//not castable to self
		if (unit.ID() != _unit->ID())
		{
			if (unit.isOrganic())
			{
				return true;
			}
			//is target unit injured?
		
		}
	}
	return false;

}

const bool HealAbility::isCastableNow(const TimeType& currentTime) const
{
	//at first, i thought that i should check here things like: isnt this unit already casting something??
	//and: isn't this ability on cooldown? but that should be handled with performing the cast itself(the _timeCanBeCasted of abilities will be
	//modified accordingly)

	//first, let's check if this ability is castable at this time...
	if (currentTime != _timeCanBeCasted)
		return false;
	if (_unit->currentEnergy() < _resourceCost)
		return false;

	return true;
}

void HealAbility::beCasted(Unit& target, TimeType gameTime)
{
	_unit->updateEnergy(-_resourceCost);
	target.addUnitEffect(EffectFactory::getInstance().getEffect(
		EffectTypes::EffectType::HEAL, gameTime));
}